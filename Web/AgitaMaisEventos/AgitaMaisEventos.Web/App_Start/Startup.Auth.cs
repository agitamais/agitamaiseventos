﻿//using system;
//using microsoft.aspnet.identity;
//using microsoft.aspnet.identity.owin;
//using microsoft.owin;
//using microsoft.owin.security.cookies;
//using microsoft.owin.security.google;
//using owin;
//using socialmedialoginsmvc.models;
using Owin;

namespace agitamaiseventos
{
    public partial class startup
    {
        // for more information on configuring authentication, please visit https://go.microsoft.com/fwlink/?linkid=301864
        public void configureauth(IAppBuilder app)
        {
            // configure the db context, user manager and signin manager to use a single instance per request
            //app.createperowincontext(applicationdbcontext.create);
            //app.createperowincontext<applicationusermanager>(applicationusermanager.create);
            //app.createperowincontext<applicationsigninmanager>(applicationsigninmanager.create);

            //// enable the application to use a cookie to store information for the signed in user
            //// and to use a cookie to temporarily store information about a user logging in with a third party login provider
            //// configure the sign in cookie
            //app.usecookieauthentication(new cookieauthenticationoptions
            //{
            //    authenticationtype = defaultauthenticationtypes.applicationcookie,
            //    loginpath = new pathstring("/account/login"),
            //    provider = new cookieauthenticationprovider
            //    {
            //        // enables the application to validate the security stamp when the user logs in.
            //        // this is a security feature which is used when you change a password or add an external login to your account.  
            //        onvalidateidentity = securitystampvalidator.onvalidateidentity<applicationusermanager, applicationuser>(
            //            validateinterval: timespan.fromminutes(30),
            //            regenerateidentity: (manager, user) => user.generateuseridentityasync(manager))
            //    }
            //});
            //app.useexternalsignincookie(defaultauthenticationtypes.externalcookie);

            //// enables the application to temporarily store user information when they are verifying the second factor in the two-factor authentication process.
            //app.usetwofactorsignincookie(defaultauthenticationtypes.twofactorcookie, timespan.fromminutes(5));

            //// enables the application to remember the second login verification factor such as phone or email.
            //// once you check this option, your second step of verification during the login process will be remembered on the device where you logged in from.
            //// this is similar to the rememberme option when you log in.
            //app.usetwofactorrememberbrowsercookie(defaultauthenticationtypes.twofactorrememberbrowsercookie);

            //// uncomment the following lines to enable logging in with third party login providers
            ////app.usemicrosoftaccountauthentication(
            ////    clientid: "",
            ////    clientsecret: "");

            ////app.usetwitterauthentication(
            ////   consumerkey: "",
            ////   consumersecret: "");

            ////app.usefacebookauthentication(
            ////   appid: "",
            ////   appsecret: "");

            //app.usegoogleauthentication(new googleoauth2authenticationoptions()
            //{
            //    clientid = "989916545018-pk1e3g6ud6h091u2vvdstqsif1qkmcas.apps.googleusercontent.com",
            //    clientsecret = "gggxqbgiauxm_gtr3w515qog"
            //});
        }
    }
}