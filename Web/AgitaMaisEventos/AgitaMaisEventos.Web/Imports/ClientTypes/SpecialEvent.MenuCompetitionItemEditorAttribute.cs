﻿using Serenity;
using Serenity.ComponentModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

namespace AgitaMaisEventos.SpecialEvent
{
    public partial class MenuCompetitionItemEditorAttribute : CustomEditorAttribute
    {
        public const string Key = "AgitaMaisEventos.SpecialEvent.MenuCompetitionItemEditor";

        public MenuCompetitionItemEditorAttribute()
            : base(Key)
        {
        }
    }
}

