﻿using Serenity;
using Serenity.ComponentModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

namespace AgitaMaisEventos.Common
{
    public partial class PhoneEditorAttribute : CustomEditorAttribute
    {
        public const string Key = "AgitaMaisEventos.Common.PhoneEditor";

        public PhoneEditorAttribute()
            : base(Key)
        {
        }

        public Boolean IsCellPhoneNumber
        {
            get { return GetOption<Boolean>("isCellPhoneNumber"); }
            set { SetOption("isCellPhoneNumber", value); }
        }
    }
}

