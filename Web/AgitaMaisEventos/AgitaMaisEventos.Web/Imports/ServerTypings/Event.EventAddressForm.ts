﻿namespace AgitaMaisEventos.Event {
    export interface EventAddressForm {
        ProducerId: Serenity.LookupEditor;
        UserId: Serenity.IntegerEditor;
        LocationSearch: Serenity.StringEditor;
        Name: Serenity.StringEditor;
        PhoneNumber: Common.PhoneEditor;
        WhatsappNumber: Common.PhoneEditor;
        AddressNum: Serenity.StringEditor;
        AddressAlt: Serenity.StringEditor;
        EventAddressCategoryPrimId: Serenity.LookupEditor;
        EventAddressCategorySecId: Serenity.LookupEditor;
        IsActive: Serenity.BooleanEditor;
        Country: Serenity.StringEditor;
        Latitude: Serenity.DecimalEditor;
        Longitude: Serenity.DecimalEditor;
        ZipCode: Serenity.StringEditor;
        Address: Serenity.StringEditor;
        Neighborhood: Serenity.StringEditor;
        City: Serenity.StringEditor;
        State: Serenity.StringEditor;
        Banner: Serenity.ImageUploadEditor;
        Logo: Serenity.ImageUploadEditor;
        Gallery: Serenity.MultipleImageUploadEditor;
        Description: Serenity.TextAreaEditor;
        HtmlDescription: Serenity.HtmlContentEditor;
    }

    export class EventAddressForm extends Serenity.PrefixedContext {
        static formKey = 'Event.EventAddress';
        private static init: boolean;

        constructor(prefix: string) {
            super(prefix);

            if (!EventAddressForm.init)  {
                EventAddressForm.init = true;

                var s = Serenity;
                var w0 = s.LookupEditor;
                var w1 = s.IntegerEditor;
                var w2 = s.StringEditor;
                var w3 = Common.PhoneEditor;
                var w4 = s.BooleanEditor;
                var w5 = s.DecimalEditor;
                var w6 = s.ImageUploadEditor;
                var w7 = s.MultipleImageUploadEditor;
                var w8 = s.TextAreaEditor;
                var w9 = s.HtmlContentEditor;

                Q.initFormType(EventAddressForm, [
                    'ProducerId', w0,
                    'UserId', w1,
                    'LocationSearch', w2,
                    'Name', w2,
                    'PhoneNumber', w3,
                    'WhatsappNumber', w3,
                    'AddressNum', w2,
                    'AddressAlt', w2,
                    'EventAddressCategoryPrimId', w0,
                    'EventAddressCategorySecId', w0,
                    'IsActive', w4,
                    'Country', w2,
                    'Latitude', w5,
                    'Longitude', w5,
                    'ZipCode', w2,
                    'Address', w2,
                    'Neighborhood', w2,
                    'City', w2,
                    'State', w2,
                    'Banner', w6,
                    'Logo', w6,
                    'Gallery', w7,
                    'Description', w8,
                    'HtmlDescription', w9
                ]);
            }
        }
    }
}

