﻿namespace AgitaMaisEventos.Modules.Event {
    export interface EventListRequest extends Serenity.ListRequest {
        UserLatitude?: number;
        UserLongitude?: number;
    }
}

