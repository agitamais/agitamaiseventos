﻿namespace AgitaMaisEventos.SpecialEvent {
    export namespace MenuCompetitionService {
        export const baseUrl = 'SpecialEvent/MenuCompetition';

        export declare function Create(request: Serenity.SaveRequest<MenuCompetitionRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        export declare function Update(request: Serenity.SaveRequest<MenuCompetitionRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        export declare function Delete(request: Serenity.DeleteRequest, onSuccess?: (response: Serenity.DeleteResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        export declare function Retrieve(request: Serenity.RetrieveRequest, onSuccess?: (response: Serenity.RetrieveResponse<MenuCompetitionRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        export declare function List(request: Serenity.ListRequest, onSuccess?: (response: Serenity.ListResponse<MenuCompetitionRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;

        export declare const enum Methods {
            Create = "SpecialEvent/MenuCompetition/Create",
            Update = "SpecialEvent/MenuCompetition/Update",
            Delete = "SpecialEvent/MenuCompetition/Delete",
            Retrieve = "SpecialEvent/MenuCompetition/Retrieve",
            List = "SpecialEvent/MenuCompetition/List"
        }

        [
            'Create', 
            'Update', 
            'Delete', 
            'Retrieve', 
            'List'
        ].forEach(x => {
            (<any>MenuCompetitionService)[x] = function (r, s, o) {
                return Q.serviceRequest(baseUrl + '/' + x, r, s, o);
            };
        });
    }
}

