﻿namespace AgitaMaisEventos.Texts {

    declare namespace Db {

        namespace Administration {

            namespace Language {
                export const Id: string;
                export const LanguageId: string;
                export const LanguageName: string;
            }

            namespace Producer {
                export const Address: string;
                export const AddressAlt: string;
                export const AddressNum: string;
                export const City: string;
                export const CompanyName: string;
                export const CpfCnpj: string;
                export const Name: string;
                export const Neighborhood: string;
                export const ProducerId: string;
                export const State: string;
                export const ZipCode: string;
            }

            namespace Role {
                export const RoleId: string;
                export const RoleName: string;
            }

            namespace RolePermission {
                export const PermissionKey: string;
                export const RoleId: string;
                export const RolePermissionId: string;
                export const RoleRoleName: string;
            }

            namespace Translation {
                export const CustomText: string;
                export const EntityPlural: string;
                export const Key: string;
                export const OverrideConfirmation: string;
                export const SaveChangesButton: string;
                export const SourceLanguage: string;
                export const SourceText: string;
                export const TargetLanguage: string;
                export const TargetText: string;
            }

            namespace User {
                export const DisplayName: string;
                export const Email: string;
                export const InsertDate: string;
                export const InsertUserId: string;
                export const IsActive: string;
                export const IsProducer: string;
                export const LastDirectoryUpdate: string;
                export const Password: string;
                export const PasswordConfirm: string;
                export const PasswordHash: string;
                export const PasswordSalt: string;
                export const ProducerAddress: string;
                export const ProducerAddressAlt: string;
                export const ProducerAddressNum: string;
                export const ProducerCity: string;
                export const ProducerCompanyName: string;
                export const ProducerCpfCnpj: string;
                export const ProducerId: string;
                export const ProducerName: string;
                export const ProducerNeighborhood: string;
                export const ProducerState: string;
                export const ProducerZipCode: string;
                export const Source: string;
                export const UpdateDate: string;
                export const UpdateUserId: string;
                export const UserId: string;
                export const UserImage: string;
                export const Username: string;
            }

            namespace UserPermission {
                export const Granted: string;
                export const PermissionKey: string;
                export const User: string;
                export const UserId: string;
                export const UserPermissionId: string;
                export const Username: string;
            }

            namespace UserRole {
                export const RoleId: string;
                export const User: string;
                export const UserId: string;
                export const UserRoleId: string;
                export const Username: string;
            }
        }

        namespace Common {

            namespace UserPreference {
                export const Name: string;
                export const PreferenceType: string;
                export const UserId: string;
                export const UserPreferenceId: string;
                export const Value: string;
            }
        }

        namespace Event {

            namespace Event {
                export const Banner: string;
                export const CategoryPrimDescription: string;
                export const CategoryPrimId: string;
                export const CategorySecDescription: string;
                export const CategorySecId: string;
                export const Description: string;
                export const EndDate: string;
                export const EventAddressAddress: string;
                export const EventAddressAddressAlt: string;
                export const EventAddressAddressNum: string;
                export const EventAddressCity: string;
                export const EventAddressCountry: string;
                export const EventAddressDistanceKM: string;
                export const EventAddressId: string;
                export const EventAddressLatitude: string;
                export const EventAddressLongitude: string;
                export const EventAddressName: string;
                export const EventAddressNeighborhood: string;
                export const EventAddressPhoneNumber: string;
                export const EventAddressProducerId: string;
                export const EventAddressState: string;
                export const EventAddressUserId: string;
                export const EventAddressWhatsappNumber: string;
                export const EventAddressZipCode: string;
                export const EventId: string;
                export const HtmlDescription: string;
                export const IsActive: string;
                export const IsPrivateEvent: string;
                export const Logo: string;
                export const Name: string;
                export const Price: string;
                export const ProducerAddress: string;
                export const ProducerAddressAlt: string;
                export const ProducerAddressNum: string;
                export const ProducerCity: string;
                export const ProducerCompanyName: string;
                export const ProducerCpfCnpj: string;
                export const ProducerId: string;
                export const ProducerName: string;
                export const ProducerNeighborhood: string;
                export const ProducerState: string;
                export const ProducerZipCode: string;
                export const SpecialEventTypeDescription: string;
                export const SpecialEventTypeId: string;
                export const StartDate: string;
                export const UserDisplayName: string;
                export const UserEmail: string;
                export const UserId: string;
                export const UserIsActive: string;
                export const UserIsProducer: string;
                export const UserProducerId: string;
                export const UserSource: string;
                export const UserUsername: string;
            }

            namespace EventAddress {
                export const Address: string;
                export const AddressAlt: string;
                export const AddressNum: string;
                export const Banner: string;
                export const City: string;
                export const Country: string;
                export const Description: string;
                export const DistanceKM: string;
                export const EventAddressCategoryPrimDescription: string;
                export const EventAddressCategoryPrimId: string;
                export const EventAddressCategorySecDescription: string;
                export const EventAddressCategorySecId: string;
                export const EventAddressId: string;
                export const Gallery: string;
                export const HtmlDescription: string;
                export const IsActive: string;
                export const Latitude: string;
                export const LocationSearch: string;
                export const Logo: string;
                export const Longitude: string;
                export const Name: string;
                export const Neighborhood: string;
                export const PhoneNumber: string;
                export const ProducerAddress: string;
                export const ProducerAddressAlt: string;
                export const ProducerAddressNum: string;
                export const ProducerCity: string;
                export const ProducerCompanyName: string;
                export const ProducerCpfCnpj: string;
                export const ProducerId: string;
                export const ProducerName: string;
                export const ProducerNeighborhood: string;
                export const ProducerState: string;
                export const ProducerZipCode: string;
                export const State: string;
                export const UserId: string;
                export const WhatsappNumber: string;
                export const ZipCode: string;
            }

            namespace EventAddressCategory {
                export const Description: string;
                export const EventAddressCategoryId: string;
                export const Icon: string;
                export const IconFamily: string;
                export const Image: string;
            }

            namespace EventCategory {
                export const Description: string;
                export const EventCategoryId: string;
                export const Icon: string;
                export const IconFamily: string;
                export const Image: string;
            }
        }

        namespace Menu {

            namespace MenuItem {
                export const Description: string;
                export const Gallery: string;
                export const MenuItemId: string;
                export const Name: string;
                export const ProducerId: string;
            }
        }

        namespace SpecialEvent {

            namespace MenuCompetition {
                export const EventId: string;
                export const MenuCompetitionBanner: string;
                export const MenuCompetitionCategoryPrimId: string;
                export const MenuCompetitionCategorySecId: string;
                export const MenuCompetitionDescription: string;
                export const MenuCompetitionEndDate: string;
                export const MenuCompetitionEventAddressId: string;
                export const MenuCompetitionGalery: string;
                export const MenuCompetitionHtmlDescription: string;
                export const MenuCompetitionId: string;
                export const MenuCompetitionIsActive: string;
                export const MenuCompetitionIsPrivateEvent: string;
                export const MenuCompetitionItemList: string;
                export const MenuCompetitionLogo: string;
                export const MenuCompetitionName: string;
                export const MenuCompetitionPrice: string;
                export const MenuCompetitionProducerId: string;
                export const MenuCompetitionStartDate: string;
                export const MenuCompetitionUserId: string;
                export const QtdMenuItemAllowedPerPartner: string;
            }

            namespace MenuCompetitionItem {
                export const EventAddressAddress: string;
                export const EventAddressAddressAlt: string;
                export const EventAddressAddressNum: string;
                export const EventAddressBanner: string;
                export const EventAddressCity: string;
                export const EventAddressCountry: string;
                export const EventAddressDescription: string;
                export const EventAddressEventAddressCategoryPrimId: string;
                export const EventAddressEventAddressCategorySecId: string;
                export const EventAddressGallery: string;
                export const EventAddressHtmlDescription: string;
                export const EventAddressId: string;
                export const EventAddressIsActive: string;
                export const EventAddressLatitude: string;
                export const EventAddressLogo: string;
                export const EventAddressLongitude: string;
                export const EventAddressName: string;
                export const EventAddressNeighborhood: string;
                export const EventAddressProducerId: string;
                export const EventAddressState: string;
                export const EventAddressUserId: string;
                export const EventAddressZipCode: string;
                export const MenuCompetitionEventId: string;
                export const MenuCompetitionId: string;
                export const MenuCompetitionItemId: string;
                export const MenuCompetitionQtdMenuItemAllowedPerPartner: string;
                export const MenuItemDescription: string;
                export const MenuItemEventAddressId: string;
                export const MenuItemGallery: string;
                export const MenuItemId: string;
                export const MenuItemName: string;
            }
        }

        namespace _Ext {

            namespace AuditLog {
                export const ActionDate: string;
                export const ActionType: string;
                export const EntityId: string;
                export const EntityTableName: string;
                export const Id: string;
                export const IpAddress: string;
                export const NewEntity: string;
                export const OldEntity: string;
                export const SessionId: string;
                export const UserId: string;
                export const VersionNo: string;
            }
        }
    }

    declare namespace Forms {

        namespace Membership {

            namespace ChangePassword {
                export const FormTitle: string;
                export const SubmitButton: string;
                export const Success: string;
            }

            namespace ForgotPassword {
                export const BackToLogin: string;
                export const FormInfo: string;
                export const FormTitle: string;
                export const SubmitButton: string;
                export const Success: string;
            }

            namespace Login {
                export const FacebookButton: string;
                export const ForgotPassword: string;
                export const FormTitle: string;
                export const GoogleButton: string;
                export const OR: string;
                export const RememberMe: string;
                export const SignInButton: string;
                export const SignUpButton: string;
            }

            namespace ResetPassword {
                export const BackToLogin: string;
                export const EmailSubject: string;
                export const FormTitle: string;
                export const SubmitButton: string;
                export const Success: string;
            }

            namespace SignUp {
                export const AcceptTerms: string;
                export const ActivateEmailSubject: string;
                export const ActivationCompleteMessage: string;
                export const BackToLogin: string;
                export const ConfirmEmail: string;
                export const ConfirmPassword: string;
                export const DisplayName: string;
                export const Email: string;
                export const FormInfo: string;
                export const FormTitle: string;
                export const Password: string;
                export const SubmitButton: string;
                export const Success: string;
            }
        }
    }

    declare namespace Site {

        namespace AccessDenied {
            export const ClickToChangeUser: string;
            export const ClickToLogin: string;
            export const LackPermissions: string;
            export const NotLoggedIn: string;
            export const PageTitle: string;
        }

        namespace BasicProgressDialog {
            export const CancelTitle: string;
            export const PleaseWait: string;
        }

        namespace BulkServiceAction {
            export const AllHadErrorsFormat: string;
            export const AllSuccessFormat: string;
            export const ConfirmationFormat: string;
            export const ErrorCount: string;
            export const NothingToProcess: string;
            export const SomeHadErrorsFormat: string;
            export const SuccessCount: string;
        }

        namespace Dashboard {
            export const ContentDescription: string;
        }

        namespace Layout {
            export const FooterCopyright: string;
            export const FooterInfo: string;
            export const FooterRights: string;
            export const GeneralSettings: string;
            export const Language: string;
            export const Theme: string;
            export const ThemeBlack: string;
            export const ThemeBlackLight: string;
            export const ThemeBlue: string;
            export const ThemeBlueLight: string;
            export const ThemeGreen: string;
            export const ThemeGreenLight: string;
            export const ThemePurple: string;
            export const ThemePurpleLight: string;
            export const ThemeRed: string;
            export const ThemeRedLight: string;
            export const ThemeYellow: string;
            export const ThemeYellowLight: string;
        }

        namespace RolePermissionDialog {
            export const DialogTitle: string;
            export const EditButton: string;
            export const SaveSuccess: string;
        }

        namespace UserDialog {
            export const EditPermissionsButton: string;
            export const EditRolesButton: string;
        }

        namespace UserPermissionDialog {
            export const DialogTitle: string;
            export const Grant: string;
            export const Permission: string;
            export const Revoke: string;
            export const SaveSuccess: string;
        }

        namespace UserRoleDialog {
            export const DialogTitle: string;
            export const SaveSuccess: string;
        }

        namespace ValidationError {
            export const Title: string;
        }
    }

    declare namespace Validation {
        export const AuthenticationError: string;
        export const CantFindUserWithEmail: string;
        export const CurrentPasswordMismatch: string;
        export const DeleteForeignKeyError: string;
        export const EmailConfirm: string;
        export const EmailInUse: string;
        export const InvalidActivateToken: string;
        export const InvalidResetToken: string;
        export const MinRequiredPasswordLength: string;
        export const SavePrimaryKeyError: string;
    }

    AgitaMaisEventos['Texts'] = Q.proxyTexts(Texts, '', {Db:{Administration:{Language:{Id:1,LanguageId:1,LanguageName:1},Producer:{Address:1,AddressAlt:1,AddressNum:1,City:1,CompanyName:1,CpfCnpj:1,Name:1,Neighborhood:1,ProducerId:1,State:1,ZipCode:1},Role:{RoleId:1,RoleName:1},RolePermission:{PermissionKey:1,RoleId:1,RolePermissionId:1,RoleRoleName:1},Translation:{CustomText:1,EntityPlural:1,Key:1,OverrideConfirmation:1,SaveChangesButton:1,SourceLanguage:1,SourceText:1,TargetLanguage:1,TargetText:1},User:{DisplayName:1,Email:1,InsertDate:1,InsertUserId:1,IsActive:1,IsProducer:1,LastDirectoryUpdate:1,Password:1,PasswordConfirm:1,PasswordHash:1,PasswordSalt:1,ProducerAddress:1,ProducerAddressAlt:1,ProducerAddressNum:1,ProducerCity:1,ProducerCompanyName:1,ProducerCpfCnpj:1,ProducerId:1,ProducerName:1,ProducerNeighborhood:1,ProducerState:1,ProducerZipCode:1,Source:1,UpdateDate:1,UpdateUserId:1,UserId:1,UserImage:1,Username:1},UserPermission:{Granted:1,PermissionKey:1,User:1,UserId:1,UserPermissionId:1,Username:1},UserRole:{RoleId:1,User:1,UserId:1,UserRoleId:1,Username:1}},Common:{UserPreference:{Name:1,PreferenceType:1,UserId:1,UserPreferenceId:1,Value:1}},Event:{Event:{Banner:1,CategoryPrimDescription:1,CategoryPrimId:1,CategorySecDescription:1,CategorySecId:1,Description:1,EndDate:1,EventAddressAddress:1,EventAddressAddressAlt:1,EventAddressAddressNum:1,EventAddressCity:1,EventAddressCountry:1,EventAddressDistanceKM:1,EventAddressId:1,EventAddressLatitude:1,EventAddressLongitude:1,EventAddressName:1,EventAddressNeighborhood:1,EventAddressPhoneNumber:1,EventAddressProducerId:1,EventAddressState:1,EventAddressUserId:1,EventAddressWhatsappNumber:1,EventAddressZipCode:1,EventId:1,HtmlDescription:1,IsActive:1,IsPrivateEvent:1,Logo:1,Name:1,Price:1,ProducerAddress:1,ProducerAddressAlt:1,ProducerAddressNum:1,ProducerCity:1,ProducerCompanyName:1,ProducerCpfCnpj:1,ProducerId:1,ProducerName:1,ProducerNeighborhood:1,ProducerState:1,ProducerZipCode:1,SpecialEventTypeDescription:1,SpecialEventTypeId:1,StartDate:1,UserDisplayName:1,UserEmail:1,UserId:1,UserIsActive:1,UserIsProducer:1,UserProducerId:1,UserSource:1,UserUsername:1},EventAddress:{Address:1,AddressAlt:1,AddressNum:1,Banner:1,City:1,Country:1,Description:1,DistanceKM:1,EventAddressCategoryPrimDescription:1,EventAddressCategoryPrimId:1,EventAddressCategorySecDescription:1,EventAddressCategorySecId:1,EventAddressId:1,Gallery:1,HtmlDescription:1,IsActive:1,Latitude:1,LocationSearch:1,Logo:1,Longitude:1,Name:1,Neighborhood:1,PhoneNumber:1,ProducerAddress:1,ProducerAddressAlt:1,ProducerAddressNum:1,ProducerCity:1,ProducerCompanyName:1,ProducerCpfCnpj:1,ProducerId:1,ProducerName:1,ProducerNeighborhood:1,ProducerState:1,ProducerZipCode:1,State:1,UserId:1,WhatsappNumber:1,ZipCode:1},EventAddressCategory:{Description:1,EventAddressCategoryId:1,Icon:1,IconFamily:1,Image:1},EventCategory:{Description:1,EventCategoryId:1,Icon:1,IconFamily:1,Image:1}},Menu:{MenuItem:{Description:1,Gallery:1,MenuItemId:1,Name:1,ProducerId:1}},SpecialEvent:{MenuCompetition:{EventId:1,MenuCompetitionBanner:1,MenuCompetitionCategoryPrimId:1,MenuCompetitionCategorySecId:1,MenuCompetitionDescription:1,MenuCompetitionEndDate:1,MenuCompetitionEventAddressId:1,MenuCompetitionGalery:1,MenuCompetitionHtmlDescription:1,MenuCompetitionId:1,MenuCompetitionIsActive:1,MenuCompetitionIsPrivateEvent:1,MenuCompetitionItemList:1,MenuCompetitionLogo:1,MenuCompetitionName:1,MenuCompetitionPrice:1,MenuCompetitionProducerId:1,MenuCompetitionStartDate:1,MenuCompetitionUserId:1,QtdMenuItemAllowedPerPartner:1},MenuCompetitionItem:{EventAddressAddress:1,EventAddressAddressAlt:1,EventAddressAddressNum:1,EventAddressBanner:1,EventAddressCity:1,EventAddressCountry:1,EventAddressDescription:1,EventAddressEventAddressCategoryPrimId:1,EventAddressEventAddressCategorySecId:1,EventAddressGallery:1,EventAddressHtmlDescription:1,EventAddressId:1,EventAddressIsActive:1,EventAddressLatitude:1,EventAddressLogo:1,EventAddressLongitude:1,EventAddressName:1,EventAddressNeighborhood:1,EventAddressProducerId:1,EventAddressState:1,EventAddressUserId:1,EventAddressZipCode:1,MenuCompetitionEventId:1,MenuCompetitionId:1,MenuCompetitionItemId:1,MenuCompetitionQtdMenuItemAllowedPerPartner:1,MenuItemDescription:1,MenuItemEventAddressId:1,MenuItemGallery:1,MenuItemId:1,MenuItemName:1}},_Ext:{AuditLog:{ActionDate:1,ActionType:1,EntityId:1,EntityTableName:1,Id:1,IpAddress:1,NewEntity:1,OldEntity:1,SessionId:1,UserId:1,VersionNo:1}}},Forms:{Membership:{ChangePassword:{FormTitle:1,SubmitButton:1,Success:1},ForgotPassword:{BackToLogin:1,FormInfo:1,FormTitle:1,SubmitButton:1,Success:1},Login:{FacebookButton:1,ForgotPassword:1,FormTitle:1,GoogleButton:1,OR:1,RememberMe:1,SignInButton:1,SignUpButton:1},ResetPassword:{BackToLogin:1,EmailSubject:1,FormTitle:1,SubmitButton:1,Success:1},SignUp:{AcceptTerms:1,ActivateEmailSubject:1,ActivationCompleteMessage:1,BackToLogin:1,ConfirmEmail:1,ConfirmPassword:1,DisplayName:1,Email:1,FormInfo:1,FormTitle:1,Password:1,SubmitButton:1,Success:1}}},Site:{AccessDenied:{ClickToChangeUser:1,ClickToLogin:1,LackPermissions:1,NotLoggedIn:1,PageTitle:1},BasicProgressDialog:{CancelTitle:1,PleaseWait:1},BulkServiceAction:{AllHadErrorsFormat:1,AllSuccessFormat:1,ConfirmationFormat:1,ErrorCount:1,NothingToProcess:1,SomeHadErrorsFormat:1,SuccessCount:1},Dashboard:{ContentDescription:1},Layout:{FooterCopyright:1,FooterInfo:1,FooterRights:1,GeneralSettings:1,Language:1,Theme:1,ThemeBlack:1,ThemeBlackLight:1,ThemeBlue:1,ThemeBlueLight:1,ThemeGreen:1,ThemeGreenLight:1,ThemePurple:1,ThemePurpleLight:1,ThemeRed:1,ThemeRedLight:1,ThemeYellow:1,ThemeYellowLight:1},RolePermissionDialog:{DialogTitle:1,EditButton:1,SaveSuccess:1},UserDialog:{EditPermissionsButton:1,EditRolesButton:1},UserPermissionDialog:{DialogTitle:1,Grant:1,Permission:1,Revoke:1,SaveSuccess:1},UserRoleDialog:{DialogTitle:1,SaveSuccess:1},ValidationError:{Title:1}},Validation:{AuthenticationError:1,CantFindUserWithEmail:1,CurrentPasswordMismatch:1,DeleteForeignKeyError:1,EmailConfirm:1,EmailInUse:1,InvalidActivateToken:1,InvalidResetToken:1,MinRequiredPasswordLength:1,SavePrimaryKeyError:1}});
}

