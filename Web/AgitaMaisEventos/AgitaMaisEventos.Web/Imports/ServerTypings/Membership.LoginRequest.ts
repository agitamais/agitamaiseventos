﻿namespace AgitaMaisEventos.Membership {
    export interface LoginRequest extends Serenity.ServiceRequest {
        Username?: string;
        Password?: string;
        KeepLoggedIn?: boolean;
    }
}

