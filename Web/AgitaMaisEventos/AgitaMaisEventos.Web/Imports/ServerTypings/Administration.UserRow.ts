﻿namespace AgitaMaisEventos.Administration {
    export interface UserRow {
        UserId?: number;
        Username?: string;
        Source?: string;
        PasswordHash?: string;
        PasswordSalt?: string;
        DisplayName?: string;
        Email?: string;
        UserImage?: string;
        LastDirectoryUpdate?: string;
        IsActive?: number;
        Password?: string;
        PasswordConfirm?: string;
        IsProducer?: boolean;
        ProducerId?: number;
        ProducerName?: string;
        ProducerCompanyName?: string;
        ProducerCpfCnpj?: string;
        ProducerZipCode?: string;
        ProducerAddress?: string;
        ProducerAddressNum?: string;
        ProducerAddressAlt?: string;
        ProducerNeighborhood?: string;
        ProducerCity?: string;
        ProducerState?: string;
        InsertUserId?: number;
        InsertDate?: string;
        UpdateUserId?: number;
        UpdateDate?: string;
    }

    export namespace UserRow {
        export const idProperty = 'UserId';
        export const isActiveProperty = 'IsActive';
        export const nameProperty = 'Username';
        export const localTextPrefix = 'Administration.User';
        export const lookupKey = 'Administration.User';

        export function getLookup(): Q.Lookup<UserRow> {
            return Q.getLookup<UserRow>('Administration.User');
        }

        export declare const enum Fields {
            UserId = "UserId",
            Username = "Username",
            Source = "Source",
            PasswordHash = "PasswordHash",
            PasswordSalt = "PasswordSalt",
            DisplayName = "DisplayName",
            Email = "Email",
            UserImage = "UserImage",
            LastDirectoryUpdate = "LastDirectoryUpdate",
            IsActive = "IsActive",
            Password = "Password",
            PasswordConfirm = "PasswordConfirm",
            IsProducer = "IsProducer",
            ProducerId = "ProducerId",
            ProducerName = "ProducerName",
            ProducerCompanyName = "ProducerCompanyName",
            ProducerCpfCnpj = "ProducerCpfCnpj",
            ProducerZipCode = "ProducerZipCode",
            ProducerAddress = "ProducerAddress",
            ProducerAddressNum = "ProducerAddressNum",
            ProducerAddressAlt = "ProducerAddressAlt",
            ProducerNeighborhood = "ProducerNeighborhood",
            ProducerCity = "ProducerCity",
            ProducerState = "ProducerState",
            InsertUserId = "InsertUserId",
            InsertDate = "InsertDate",
            UpdateUserId = "UpdateUserId",
            UpdateDate = "UpdateDate"
        }
    }
}

