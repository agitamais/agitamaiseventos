﻿namespace AgitaMaisEventos.SpecialEvent {
    export interface MenuCompetitionRow {
        MenuCompetitionId?: number;
        EventId?: number;
        QtdMenuItemAllowedPerPartner?: number;
        MenuCompetitionProducerId?: number;
        MenuCompetitionUserId?: number;
        MenuCompetitionName?: string;
        MenuCompetitionLogo?: string;
        MenuCompetitionBanner?: string;
        MenuCompetitionStartDate?: string;
        MenuCompetitionEndDate?: string;
        MenuCompetitionPrice?: number;
        MenuCompetitionCategoryPrimId?: string;
        MenuCompetitionCategorySecId?: string;
        MenuCompetitionIsPrivateEvent?: boolean;
        MenuCompetitionEventAddressId?: number;
        MenuCompetitionGalery?: string;
        MenuCompetitionDescription?: string;
        MenuCompetitionHtmlDescription?: string;
        MenuCompetitionIsActive?: boolean;
        MenuCompetitionItemList?: MenuCompetitionItemRow[];
    }

    export namespace MenuCompetitionRow {
        export const idProperty = 'MenuCompetitionId';
        export const localTextPrefix = 'SpecialEvent.MenuCompetition';

        export declare const enum Fields {
            MenuCompetitionId = "MenuCompetitionId",
            EventId = "EventId",
            QtdMenuItemAllowedPerPartner = "QtdMenuItemAllowedPerPartner",
            MenuCompetitionProducerId = "MenuCompetitionProducerId",
            MenuCompetitionUserId = "MenuCompetitionUserId",
            MenuCompetitionName = "MenuCompetitionName",
            MenuCompetitionLogo = "MenuCompetitionLogo",
            MenuCompetitionBanner = "MenuCompetitionBanner",
            MenuCompetitionStartDate = "MenuCompetitionStartDate",
            MenuCompetitionEndDate = "MenuCompetitionEndDate",
            MenuCompetitionPrice = "MenuCompetitionPrice",
            MenuCompetitionCategoryPrimId = "MenuCompetitionCategoryPrimId",
            MenuCompetitionCategorySecId = "MenuCompetitionCategorySecId",
            MenuCompetitionIsPrivateEvent = "MenuCompetitionIsPrivateEvent",
            MenuCompetitionEventAddressId = "MenuCompetitionEventAddressId",
            MenuCompetitionGalery = "MenuCompetitionGalery",
            MenuCompetitionDescription = "MenuCompetitionDescription",
            MenuCompetitionHtmlDescription = "MenuCompetitionHtmlDescription",
            MenuCompetitionIsActive = "MenuCompetitionIsActive",
            MenuCompetitionItemList = "MenuCompetitionItemList"
        }
    }
}

