﻿namespace AgitaMaisEventos {
    export interface ExcelImportRequest extends Serenity.ServiceRequest {
        FileName?: string;
    }
}

