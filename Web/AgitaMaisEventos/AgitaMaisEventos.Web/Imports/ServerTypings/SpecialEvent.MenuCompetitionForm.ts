﻿namespace AgitaMaisEventos.SpecialEvent {
    export interface MenuCompetitionForm {
        EventId: Serenity.LookupEditor;
        QtdMenuItemAllowedPerPartner: Serenity.IntegerEditor;
        MenuCompetitionItemList: MenuCompetitionItemEditor;
    }

    export class MenuCompetitionForm extends Serenity.PrefixedContext {
        static formKey = 'SpecialEvent.MenuCompetition';
        private static init: boolean;

        constructor(prefix: string) {
            super(prefix);

            if (!MenuCompetitionForm.init)  {
                MenuCompetitionForm.init = true;

                var s = Serenity;
                var w0 = s.LookupEditor;
                var w1 = s.IntegerEditor;
                var w2 = MenuCompetitionItemEditor;

                Q.initFormType(MenuCompetitionForm, [
                    'EventId', w0,
                    'QtdMenuItemAllowedPerPartner', w1,
                    'MenuCompetitionItemList', w2
                ]);
            }
        }
    }
}

