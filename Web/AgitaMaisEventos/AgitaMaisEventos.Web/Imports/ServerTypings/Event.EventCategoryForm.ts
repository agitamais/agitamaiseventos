﻿namespace AgitaMaisEventos.Event {
    export interface EventCategoryForm {
        EventCategoryId: Serenity.StringEditor;
        Description: Serenity.StringEditor;
        Image: Serenity.ImageUploadEditor;
        Icon: Serenity.StringEditor;
        IconFamily: Serenity.StringEditor;
    }

    export class EventCategoryForm extends Serenity.PrefixedContext {
        static formKey = 'Event.EventCategory';
        private static init: boolean;

        constructor(prefix: string) {
            super(prefix);

            if (!EventCategoryForm.init)  {
                EventCategoryForm.init = true;

                var s = Serenity;
                var w0 = s.StringEditor;
                var w1 = s.ImageUploadEditor;

                Q.initFormType(EventCategoryForm, [
                    'EventCategoryId', w0,
                    'Description', w0,
                    'Image', w1,
                    'Icon', w0,
                    'IconFamily', w0
                ]);
            }
        }
    }
}

