﻿namespace AgitaMaisEventos.Event {
    export interface EventAddressCategoryRow {
        EventAddressCategoryId?: string;
        Description?: string;
        Image?: string;
        Icon?: string;
        IconFamily?: string;
    }

    export namespace EventAddressCategoryRow {
        export const idProperty = 'EventAddressCategoryId';
        export const nameProperty = 'EventAddressCategoryId';
        export const localTextPrefix = 'Event.EventAddressCategory';
        export const lookupKey = 'Event.EventAddressCategory';

        export function getLookup(): Q.Lookup<EventAddressCategoryRow> {
            return Q.getLookup<EventAddressCategoryRow>('Event.EventAddressCategory');
        }

        export declare const enum Fields {
            EventAddressCategoryId = "EventAddressCategoryId",
            Description = "Description",
            Image = "Image",
            Icon = "Icon",
            IconFamily = "IconFamily"
        }
    }
}

