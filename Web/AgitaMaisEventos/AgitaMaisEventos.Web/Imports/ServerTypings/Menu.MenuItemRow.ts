﻿namespace AgitaMaisEventos.Menu {
    export interface MenuItemRow {
        MenuItemId?: number;
        Name?: string;
        Description?: string;
        Gallery?: string;
        ProducerId?: number;
    }

    export namespace MenuItemRow {
        export const idProperty = 'MenuItemId';
        export const nameProperty = 'Name';
        export const localTextPrefix = 'Menu.MenuItem';
        export const lookupKey = 'Menu.MenuItem';

        export function getLookup(): Q.Lookup<MenuItemRow> {
            return Q.getLookup<MenuItemRow>('Menu.MenuItem');
        }

        export declare const enum Fields {
            MenuItemId = "MenuItemId",
            Name = "Name",
            Description = "Description",
            Gallery = "Gallery",
            ProducerId = "ProducerId"
        }
    }
}

