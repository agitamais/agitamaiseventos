﻿namespace AgitaMaisEventos.SpecialEvent {
    export interface MenuCompetitionItemForm {
        EventAddressId: Serenity.LookupEditor;
        MenuItemId: Serenity.LookupEditor;
    }

    export class MenuCompetitionItemForm extends Serenity.PrefixedContext {
        static formKey = 'SpecialEvent.MenuCompetitionItem';
        private static init: boolean;

        constructor(prefix: string) {
            super(prefix);

            if (!MenuCompetitionItemForm.init)  {
                MenuCompetitionItemForm.init = true;

                var s = Serenity;
                var w0 = s.LookupEditor;

                Q.initFormType(MenuCompetitionItemForm, [
                    'EventAddressId', w0,
                    'MenuItemId', w0
                ]);
            }
        }
    }
}

