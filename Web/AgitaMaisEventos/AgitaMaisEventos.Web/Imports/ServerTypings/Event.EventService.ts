﻿namespace AgitaMaisEventos.Event {
    export namespace EventService {
        export const baseUrl = 'Event/Event';

        export declare function Create(request: Serenity.SaveRequest<EventRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        export declare function Update(request: Serenity.SaveRequest<EventRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        export declare function Delete(request: Serenity.DeleteRequest, onSuccess?: (response: Serenity.DeleteResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        export declare function Retrieve(request: Serenity.RetrieveRequest, onSuccess?: (response: Serenity.RetrieveResponse<EventRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        export declare function List(request: Serenity.ListRequest, onSuccess?: (response: Serenity.ListResponse<EventRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        export declare function ListWT(request: Modules.Event.EventListRequest, onSuccess?: (response: Serenity.ListResponse<EventRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        export declare function RetrieveWT(request: Modules.Event.EventRetrieveRequest, onSuccess?: (response: Serenity.RetrieveResponse<EventRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        export declare function CountFutureEvents(request: Serenity.ServiceRequest, onSuccess?: (response: Serenity.ListResponse<EventRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;

        export declare const enum Methods {
            Create = "Event/Event/Create",
            Update = "Event/Event/Update",
            Delete = "Event/Event/Delete",
            Retrieve = "Event/Event/Retrieve",
            List = "Event/Event/List",
            ListWT = "Event/Event/ListWT",
            RetrieveWT = "Event/Event/RetrieveWT",
            CountFutureEvents = "Event/Event/CountFutureEvents"
        }

        [
            'Create', 
            'Update', 
            'Delete', 
            'Retrieve', 
            'List', 
            'ListWT', 
            'RetrieveWT', 
            'CountFutureEvents'
        ].forEach(x => {
            (<any>EventService)[x] = function (r, s, o) {
                return Q.serviceRequest(baseUrl + '/' + x, r, s, o);
            };
        });
    }
}

