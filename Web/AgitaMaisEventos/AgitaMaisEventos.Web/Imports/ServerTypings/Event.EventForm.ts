﻿namespace AgitaMaisEventos.Event {
    export interface EventForm {
        ProducerId: Serenity.LookupEditor;
        Name: Serenity.StringEditor;
        Logo: Serenity.ImageUploadEditor;
        Banner: Serenity.ImageUploadEditor;
        StartDate: Serenity.DateTimeEditor;
        EndDate: Serenity.DateTimeEditor;
        Price: Serenity.DecimalEditor;
        CategoryPrimId: Serenity.LookupEditor;
        CategorySecId: Serenity.LookupEditor;
        IsActive: Serenity.BooleanEditor;
        IsPrivateEvent: Serenity.BooleanEditor;
        EventAddressId: Serenity.LookupEditor;
        Description: Serenity.TextAreaEditor;
        HtmlDescription: Serenity.HtmlContentEditor;
    }

    export class EventForm extends Serenity.PrefixedContext {
        static formKey = 'Event.Event';
        private static init: boolean;

        constructor(prefix: string) {
            super(prefix);

            if (!EventForm.init)  {
                EventForm.init = true;

                var s = Serenity;
                var w0 = s.LookupEditor;
                var w1 = s.StringEditor;
                var w2 = s.ImageUploadEditor;
                var w3 = s.DateTimeEditor;
                var w4 = s.DecimalEditor;
                var w5 = s.BooleanEditor;
                var w6 = s.TextAreaEditor;
                var w7 = s.HtmlContentEditor;

                Q.initFormType(EventForm, [
                    'ProducerId', w0,
                    'Name', w1,
                    'Logo', w2,
                    'Banner', w2,
                    'StartDate', w3,
                    'EndDate', w3,
                    'Price', w4,
                    'CategoryPrimId', w0,
                    'CategorySecId', w0,
                    'IsActive', w5,
                    'IsPrivateEvent', w5,
                    'EventAddressId', w0,
                    'Description', w6,
                    'HtmlDescription', w7
                ]);
            }
        }
    }
}

