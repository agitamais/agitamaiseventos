﻿namespace AgitaMaisEventos.Event {
    export interface EventCategoryRow {
        EventCategoryId?: string;
        Description?: string;
        Image?: string;
        IconFamily?: string;
        Icon?: string;
    }

    export namespace EventCategoryRow {
        export const idProperty = 'EventCategoryId';
        export const nameProperty = 'EventCategoryId';
        export const localTextPrefix = 'Event.EventCategory';
        export const lookupKey = 'Event.EventCategory';

        export function getLookup(): Q.Lookup<EventCategoryRow> {
            return Q.getLookup<EventCategoryRow>('Event.EventCategory');
        }

        export declare const enum Fields {
            EventCategoryId = "EventCategoryId",
            Description = "Description",
            Image = "Image",
            IconFamily = "IconFamily",
            Icon = "Icon"
        }
    }
}

