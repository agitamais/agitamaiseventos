﻿namespace AgitaMaisEventos.SpecialEvent {
    export interface MenuCompetitionItemRow {
        MenuCompetitionItemId?: number;
        MenuCompetitionId?: number;
        EventAddressId?: number;
        MenuItemId?: number;
        MenuCompetitionEventId?: number;
        MenuCompetitionQtdMenuItemAllowedPerPartner?: number;
        EventAddressProducerId?: number;
        EventAddressUserId?: number;
        EventAddressName?: string;
        EventAddressDescription?: string;
        EventAddressHtmlDescription?: string;
        EventAddressZipCode?: string;
        EventAddressAddress?: string;
        EventAddressAddressNum?: string;
        EventAddressAddressAlt?: string;
        EventAddressNeighborhood?: string;
        EventAddressCity?: string;
        EventAddressState?: string;
        EventAddressCountry?: string;
        EventAddressLatitude?: number;
        EventAddressLongitude?: number;
        EventAddressGallery?: string;
        EventAddressBanner?: string;
        EventAddressLogo?: string;
        EventAddressEventAddressCategoryPrimId?: string;
        EventAddressEventAddressCategorySecId?: string;
        EventAddressIsActive?: boolean;
        MenuItemEventAddressId?: number;
        MenuItemName?: string;
        MenuItemDescription?: string;
        MenuItemGallery?: string;
    }

    export namespace MenuCompetitionItemRow {
        export const idProperty = 'MenuCompetitionItemId';
        export const localTextPrefix = 'SpecialEvent.MenuCompetitionItem';
        export const lookupKey = 'SpecialEvent.MenuCompetitionItem';

        export function getLookup(): Q.Lookup<MenuCompetitionItemRow> {
            return Q.getLookup<MenuCompetitionItemRow>('SpecialEvent.MenuCompetitionItem');
        }

        export declare const enum Fields {
            MenuCompetitionItemId = "MenuCompetitionItemId",
            MenuCompetitionId = "MenuCompetitionId",
            EventAddressId = "EventAddressId",
            MenuItemId = "MenuItemId",
            MenuCompetitionEventId = "MenuCompetitionEventId",
            MenuCompetitionQtdMenuItemAllowedPerPartner = "MenuCompetitionQtdMenuItemAllowedPerPartner",
            EventAddressProducerId = "EventAddressProducerId",
            EventAddressUserId = "EventAddressUserId",
            EventAddressName = "EventAddressName",
            EventAddressDescription = "EventAddressDescription",
            EventAddressHtmlDescription = "EventAddressHtmlDescription",
            EventAddressZipCode = "EventAddressZipCode",
            EventAddressAddress = "EventAddressAddress",
            EventAddressAddressNum = "EventAddressAddressNum",
            EventAddressAddressAlt = "EventAddressAddressAlt",
            EventAddressNeighborhood = "EventAddressNeighborhood",
            EventAddressCity = "EventAddressCity",
            EventAddressState = "EventAddressState",
            EventAddressCountry = "EventAddressCountry",
            EventAddressLatitude = "EventAddressLatitude",
            EventAddressLongitude = "EventAddressLongitude",
            EventAddressGallery = "EventAddressGallery",
            EventAddressBanner = "EventAddressBanner",
            EventAddressLogo = "EventAddressLogo",
            EventAddressEventAddressCategoryPrimId = "EventAddressEventAddressCategoryPrimId",
            EventAddressEventAddressCategorySecId = "EventAddressEventAddressCategorySecId",
            EventAddressIsActive = "EventAddressIsActive",
            MenuItemEventAddressId = "MenuItemEventAddressId",
            MenuItemName = "MenuItemName",
            MenuItemDescription = "MenuItemDescription",
            MenuItemGallery = "MenuItemGallery"
        }
    }
}

