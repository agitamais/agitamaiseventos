﻿namespace AgitaMaisEventos.Menu {
    export interface MenuItemForm {
        ProducerId: Serenity.LookupEditor;
        Name: Serenity.StringEditor;
        Description: Serenity.StringEditor;
        Gallery: Serenity.ImageUploadEditor;
    }

    export class MenuItemForm extends Serenity.PrefixedContext {
        static formKey = 'Menu.MenuItem';
        private static init: boolean;

        constructor(prefix: string) {
            super(prefix);

            if (!MenuItemForm.init)  {
                MenuItemForm.init = true;

                var s = Serenity;
                var w0 = s.LookupEditor;
                var w1 = s.StringEditor;
                var w2 = s.ImageUploadEditor;

                Q.initFormType(MenuItemForm, [
                    'ProducerId', w0,
                    'Name', w1,
                    'Description', w1,
                    'Gallery', w2
                ]);
            }
        }
    }
}

