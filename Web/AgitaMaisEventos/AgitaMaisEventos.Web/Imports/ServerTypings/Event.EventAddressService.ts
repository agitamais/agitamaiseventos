﻿namespace AgitaMaisEventos.Event {
    export namespace EventAddressService {
        export const baseUrl = 'Event/EventAddress';

        export declare function Create(request: Serenity.SaveRequest<EventAddressRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        export declare function Update(request: Serenity.SaveRequest<EventAddressRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        export declare function Delete(request: Serenity.DeleteRequest, onSuccess?: (response: Serenity.DeleteResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        export declare function Retrieve(request: Serenity.RetrieveRequest, onSuccess?: (response: Serenity.RetrieveResponse<EventAddressRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        export declare function List(request: Serenity.ListRequest, onSuccess?: (response: Serenity.ListResponse<EventAddressRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        export declare function ListWT(request: Modules.Event.EventListRequest, onSuccess?: (response: Serenity.ListResponse<EventAddressRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        export declare function RetrieveWT(request: Modules.Event.EventRetrieveRequest, onSuccess?: (response: Serenity.RetrieveResponse<EventAddressRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        export declare function CountAllEventAddress(request: Serenity.ServiceRequest, onSuccess?: (response: Serenity.ListResponse<EventAddressRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;

        export declare const enum Methods {
            Create = "Event/EventAddress/Create",
            Update = "Event/EventAddress/Update",
            Delete = "Event/EventAddress/Delete",
            Retrieve = "Event/EventAddress/Retrieve",
            List = "Event/EventAddress/List",
            ListWT = "Event/EventAddress/ListWT",
            RetrieveWT = "Event/EventAddress/RetrieveWT",
            CountAllEventAddress = "Event/EventAddress/CountAllEventAddress"
        }

        [
            'Create', 
            'Update', 
            'Delete', 
            'Retrieve', 
            'List', 
            'ListWT', 
            'RetrieveWT', 
            'CountAllEventAddress'
        ].forEach(x => {
            (<any>EventAddressService)[x] = function (r, s, o) {
                return Q.serviceRequest(baseUrl + '/' + x, r, s, o);
            };
        });
    }
}

