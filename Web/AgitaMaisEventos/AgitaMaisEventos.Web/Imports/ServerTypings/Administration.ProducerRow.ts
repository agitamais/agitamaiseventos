﻿namespace AgitaMaisEventos.Administration {
    export interface ProducerRow {
        ProducerId?: number;
        Name?: string;
        CompanyName?: string;
        CpfCnpj?: string;
        ZipCode?: string;
        Address?: string;
        AddressNum?: string;
        AddressAlt?: string;
        Neighborhood?: string;
        City?: string;
        State?: string;
    }

    export namespace ProducerRow {
        export const idProperty = 'ProducerId';
        export const nameProperty = 'Name';
        export const localTextPrefix = 'Administration.Producer';
        export const lookupKey = 'Administration.Producer';

        export function getLookup(): Q.Lookup<ProducerRow> {
            return Q.getLookup<ProducerRow>('Administration.Producer');
        }

        export declare const enum Fields {
            ProducerId = "ProducerId",
            Name = "Name",
            CompanyName = "CompanyName",
            CpfCnpj = "CpfCnpj",
            ZipCode = "ZipCode",
            Address = "Address",
            AddressNum = "AddressNum",
            AddressAlt = "AddressAlt",
            Neighborhood = "Neighborhood",
            City = "City",
            State = "State"
        }
    }
}

