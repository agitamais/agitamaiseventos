﻿namespace AgitaMaisEventos.Event {
    export interface EventRow {
        EventId?: number;
        ProducerId?: number;
        UserId?: number;
        Name?: string;
        Logo?: string;
        Banner?: string;
        StartDate?: string;
        EndDate?: string;
        Price?: number;
        CategoryPrimId?: string;
        CategorySecId?: string;
        IsPrivateEvent?: boolean;
        IsActive?: boolean;
        EventAddressId?: number;
        Description?: string;
        HtmlDescription?: string;
        SpecialEventTypeId?: string;
        SpecialEventTypeDescription?: string;
        ProducerName?: string;
        ProducerCompanyName?: string;
        ProducerCpfCnpj?: string;
        ProducerZipCode?: string;
        ProducerAddress?: string;
        ProducerAddressNum?: string;
        ProducerAddressAlt?: string;
        ProducerNeighborhood?: string;
        ProducerCity?: string;
        ProducerState?: string;
        UserUsername?: string;
        UserDisplayName?: string;
        UserEmail?: string;
        UserSource?: string;
        UserIsActive?: number;
        UserIsProducer?: boolean;
        UserProducerId?: number;
        CategoryPrimDescription?: string;
        CategorySecDescription?: string;
        EventAddressProducerId?: number;
        EventAddressUserId?: number;
        EventAddressName?: string;
        EventAddressZipCode?: string;
        EventAddressAddress?: string;
        EventAddressAddressNum?: string;
        EventAddressAddressAlt?: string;
        EventAddressNeighborhood?: string;
        EventAddressCity?: string;
        EventAddressState?: string;
        EventAddressDistanceKM?: number;
        EventAddressLongitude?: number;
        EventAddressLatitude?: number;
        EventAddressCountry?: string;
        EventAddressPhoneNumber?: string;
        EventAddressWhatsappNumber?: string;
    }

    export namespace EventRow {
        export const idProperty = 'EventId';
        export const nameProperty = 'Name';
        export const localTextPrefix = 'Event.Event';
        export const lookupKey = 'Event.Event';

        export function getLookup(): Q.Lookup<EventRow> {
            return Q.getLookup<EventRow>('Event.Event');
        }

        export declare const enum Fields {
            EventId = "EventId",
            ProducerId = "ProducerId",
            UserId = "UserId",
            Name = "Name",
            Logo = "Logo",
            Banner = "Banner",
            StartDate = "StartDate",
            EndDate = "EndDate",
            Price = "Price",
            CategoryPrimId = "CategoryPrimId",
            CategorySecId = "CategorySecId",
            IsPrivateEvent = "IsPrivateEvent",
            IsActive = "IsActive",
            EventAddressId = "EventAddressId",
            Description = "Description",
            HtmlDescription = "HtmlDescription",
            SpecialEventTypeId = "SpecialEventTypeId",
            SpecialEventTypeDescription = "SpecialEventTypeDescription",
            ProducerName = "ProducerName",
            ProducerCompanyName = "ProducerCompanyName",
            ProducerCpfCnpj = "ProducerCpfCnpj",
            ProducerZipCode = "ProducerZipCode",
            ProducerAddress = "ProducerAddress",
            ProducerAddressNum = "ProducerAddressNum",
            ProducerAddressAlt = "ProducerAddressAlt",
            ProducerNeighborhood = "ProducerNeighborhood",
            ProducerCity = "ProducerCity",
            ProducerState = "ProducerState",
            UserUsername = "UserUsername",
            UserDisplayName = "UserDisplayName",
            UserEmail = "UserEmail",
            UserSource = "UserSource",
            UserIsActive = "UserIsActive",
            UserIsProducer = "UserIsProducer",
            UserProducerId = "UserProducerId",
            CategoryPrimDescription = "CategoryPrimDescription",
            CategorySecDescription = "CategorySecDescription",
            EventAddressProducerId = "EventAddressProducerId",
            EventAddressUserId = "EventAddressUserId",
            EventAddressName = "EventAddressName",
            EventAddressZipCode = "EventAddressZipCode",
            EventAddressAddress = "EventAddressAddress",
            EventAddressAddressNum = "EventAddressAddressNum",
            EventAddressAddressAlt = "EventAddressAddressAlt",
            EventAddressNeighborhood = "EventAddressNeighborhood",
            EventAddressCity = "EventAddressCity",
            EventAddressState = "EventAddressState",
            EventAddressDistanceKM = "EventAddressDistanceKM",
            EventAddressLongitude = "EventAddressLongitude",
            EventAddressLatitude = "EventAddressLatitude",
            EventAddressCountry = "EventAddressCountry",
            EventAddressPhoneNumber = "EventAddressPhoneNumber",
            EventAddressWhatsappNumber = "EventAddressWhatsappNumber"
        }
    }
}

