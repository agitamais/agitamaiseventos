﻿namespace AgitaMaisEventos.Event {
    export interface EventAddressCategoryForm {
        EventAddressCategoryId: Serenity.StringEditor;
        Description: Serenity.StringEditor;
        Image: Serenity.ImageUploadEditor;
        Icon: Serenity.StringEditor;
        IconFamily: Serenity.StringEditor;
    }

    export class EventAddressCategoryForm extends Serenity.PrefixedContext {
        static formKey = 'Event.EventAddressCategory';
        private static init: boolean;

        constructor(prefix: string) {
            super(prefix);

            if (!EventAddressCategoryForm.init)  {
                EventAddressCategoryForm.init = true;

                var s = Serenity;
                var w0 = s.StringEditor;
                var w1 = s.ImageUploadEditor;

                Q.initFormType(EventAddressCategoryForm, [
                    'EventAddressCategoryId', w0,
                    'Description', w0,
                    'Image', w1,
                    'Icon', w0,
                    'IconFamily', w0
                ]);
            }
        }
    }
}

