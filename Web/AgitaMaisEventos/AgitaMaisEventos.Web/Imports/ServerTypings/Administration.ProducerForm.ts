﻿namespace AgitaMaisEventos.Administration {
    export interface ProducerForm {
        Name: Serenity.StringEditor;
        CompanyName: Serenity.StringEditor;
        CpfCnpj: Serenity.StringEditor;
        ZipCode: Serenity.StringEditor;
        Address: Serenity.StringEditor;
        AddressNum: Serenity.StringEditor;
        AddressAlt: Serenity.StringEditor;
        Neighborhood: Serenity.StringEditor;
        City: Serenity.StringEditor;
        State: Serenity.StringEditor;
    }

    export class ProducerForm extends Serenity.PrefixedContext {
        static formKey = 'Administration.Producer';
        private static init: boolean;

        constructor(prefix: string) {
            super(prefix);

            if (!ProducerForm.init)  {
                ProducerForm.init = true;

                var s = Serenity;
                var w0 = s.StringEditor;

                Q.initFormType(ProducerForm, [
                    'Name', w0,
                    'CompanyName', w0,
                    'CpfCnpj', w0,
                    'ZipCode', w0,
                    'Address', w0,
                    'AddressNum', w0,
                    'AddressAlt', w0,
                    'Neighborhood', w0,
                    'City', w0,
                    'State', w0
                ]);
            }
        }
    }
}

