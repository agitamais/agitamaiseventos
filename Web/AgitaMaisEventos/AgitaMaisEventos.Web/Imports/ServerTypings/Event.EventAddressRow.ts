﻿namespace AgitaMaisEventos.Event {
    export interface EventAddressRow {
        EventAddressId?: number;
        ProducerId?: number;
        UserId?: number;
        Name?: string;
        Description?: string;
        HtmlDescription?: string;
        ZipCode?: string;
        Address?: string;
        AddressNum?: string;
        AddressAlt?: string;
        Neighborhood?: string;
        City?: string;
        State?: string;
        WhatsappNumber?: string;
        PhoneNumber?: string;
        Country?: string;
        LocationSearch?: string;
        Latitude?: number;
        Longitude?: number;
        Logo?: string;
        Banner?: string;
        Gallery?: string;
        EventAddressCategoryPrimId?: string;
        EventAddressCategorySecId?: string;
        IsActive?: boolean;
        DistanceKM?: number;
        ProducerName?: string;
        ProducerCompanyName?: string;
        ProducerCpfCnpj?: string;
        ProducerZipCode?: string;
        ProducerAddress?: string;
        ProducerAddressNum?: string;
        ProducerAddressAlt?: string;
        ProducerNeighborhood?: string;
        ProducerCity?: string;
        ProducerState?: string;
        EventAddressCategoryPrimDescription?: string;
        EventAddressCategorySecDescription?: string;
    }

    export namespace EventAddressRow {
        export const idProperty = 'EventAddressId';
        export const nameProperty = 'Name';
        export const localTextPrefix = 'Event.EventAddress';
        export const lookupKey = 'Event.EventAddress';

        export function getLookup(): Q.Lookup<EventAddressRow> {
            return Q.getLookup<EventAddressRow>('Event.EventAddress');
        }

        export declare const enum Fields {
            EventAddressId = "EventAddressId",
            ProducerId = "ProducerId",
            UserId = "UserId",
            Name = "Name",
            Description = "Description",
            HtmlDescription = "HtmlDescription",
            ZipCode = "ZipCode",
            Address = "Address",
            AddressNum = "AddressNum",
            AddressAlt = "AddressAlt",
            Neighborhood = "Neighborhood",
            City = "City",
            State = "State",
            WhatsappNumber = "WhatsappNumber",
            PhoneNumber = "PhoneNumber",
            Country = "Country",
            LocationSearch = "LocationSearch",
            Latitude = "Latitude",
            Longitude = "Longitude",
            Logo = "Logo",
            Banner = "Banner",
            Gallery = "Gallery",
            EventAddressCategoryPrimId = "EventAddressCategoryPrimId",
            EventAddressCategorySecId = "EventAddressCategorySecId",
            IsActive = "IsActive",
            DistanceKM = "DistanceKM",
            ProducerName = "ProducerName",
            ProducerCompanyName = "ProducerCompanyName",
            ProducerCpfCnpj = "ProducerCpfCnpj",
            ProducerZipCode = "ProducerZipCode",
            ProducerAddress = "ProducerAddress",
            ProducerAddressNum = "ProducerAddressNum",
            ProducerAddressAlt = "ProducerAddressAlt",
            ProducerNeighborhood = "ProducerNeighborhood",
            ProducerCity = "ProducerCity",
            ProducerState = "ProducerState",
            EventAddressCategoryPrimDescription = "EventAddressCategoryPrimDescription",
            EventAddressCategorySecDescription = "EventAddressCategorySecDescription"
        }
    }
}

