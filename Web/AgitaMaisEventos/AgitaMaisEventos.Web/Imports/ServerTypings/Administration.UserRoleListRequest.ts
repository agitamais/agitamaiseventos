﻿namespace AgitaMaisEventos.Administration {
    export interface UserRoleListRequest extends Serenity.ServiceRequest {
        UserID?: number;
    }
}

