﻿namespace AgitaMaisEventos.SpecialEvent {
    export namespace MenuCompetitionItemService {
        export const baseUrl = 'SpecialEvent/MenuCompetitionItem';

        export declare function Create(request: Serenity.SaveRequest<MenuCompetitionItemRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        export declare function Update(request: Serenity.SaveRequest<MenuCompetitionItemRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        export declare function Delete(request: Serenity.DeleteRequest, onSuccess?: (response: Serenity.DeleteResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        export declare function Retrieve(request: Serenity.RetrieveRequest, onSuccess?: (response: Serenity.RetrieveResponse<MenuCompetitionItemRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        export declare function List(request: Serenity.ListRequest, onSuccess?: (response: Serenity.ListResponse<MenuCompetitionItemRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;

        export declare const enum Methods {
            Create = "SpecialEvent/MenuCompetitionItem/Create",
            Update = "SpecialEvent/MenuCompetitionItem/Update",
            Delete = "SpecialEvent/MenuCompetitionItem/Delete",
            Retrieve = "SpecialEvent/MenuCompetitionItem/Retrieve",
            List = "SpecialEvent/MenuCompetitionItem/List"
        }

        [
            'Create', 
            'Update', 
            'Delete', 
            'Retrieve', 
            'List'
        ].forEach(x => {
            (<any>MenuCompetitionItemService)[x] = function (r, s, o) {
                return Q.serviceRequest(baseUrl + '/' + x, r, s, o);
            };
        });
    }
}

