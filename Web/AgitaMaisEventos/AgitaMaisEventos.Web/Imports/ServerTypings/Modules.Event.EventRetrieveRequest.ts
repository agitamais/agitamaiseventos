﻿namespace AgitaMaisEventos.Modules.Event {
    export interface EventRetrieveRequest extends Serenity.RetrieveRequest {
        UserLatitude?: number;
        UserLongitude?: number;
    }
}

