﻿
using Serenity.Extensibility;
using System.ComponentModel;

namespace AgitaMaisEventos.Administration
{
    [NestedPermissionKeys]
    [DisplayName("Administration")]
    public class PermissionKeys
    {
        [Description("User, Role Management and Permissions")]
        public const string Security = "Administration:Security";

        public const string Producer = "Administration:Producer";

        public const string SystemAdmin = "Administration:SystemAdmin";

    }
}
