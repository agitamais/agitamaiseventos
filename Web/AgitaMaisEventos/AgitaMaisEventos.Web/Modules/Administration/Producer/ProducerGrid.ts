﻿
namespace AgitaMaisEventos.Administration {

    @Serenity.Decorators.registerClass()
    export class ProducerGrid extends Serenity.EntityGrid<ProducerRow, any> {
        protected getColumnsKey() { return 'Administration.Producer'; }
        protected getDialogType() { return ProducerDialog; }
        protected getIdProperty() { return ProducerRow.idProperty; }
        protected getLocalTextPrefix() { return ProducerRow.localTextPrefix; }
        protected getService() { return ProducerService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}