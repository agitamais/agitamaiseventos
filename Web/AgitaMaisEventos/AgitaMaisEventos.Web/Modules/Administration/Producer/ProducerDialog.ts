﻿
namespace AgitaMaisEventos.Administration {

    @Serenity.Decorators.registerClass()
    export class ProducerDialog extends Serenity.EntityDialog<ProducerRow, any> {
        protected getFormKey() { return ProducerForm.formKey; }
        protected getIdProperty() { return ProducerRow.idProperty; }
        protected getLocalTextPrefix() { return ProducerRow.localTextPrefix; }
        protected getNameProperty() { return ProducerRow.nameProperty; }
        protected getService() { return ProducerService.baseUrl; }

        protected form = new ProducerForm(this.idPrefix);

    }
}