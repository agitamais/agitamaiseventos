﻿
namespace AgitaMaisEventos.Administration.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("Administration/Producer"), Route("{action=index}")]
    [PageAuthorize(typeof(Entities.ProducerRow))]
    public class ProducerController : Controller
    {
        public ActionResult Index()
        {
            return View("~/Modules/Administration/Producer/ProducerIndex.cshtml");
        }
    }
}