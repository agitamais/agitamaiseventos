﻿
namespace AgitaMaisEventos.Administration.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("Administration.Producer")]
    [BasedOnRow(typeof(Entities.ProducerRow), CheckNames = true)]
    public class ProducerForm
    {
        public String Name { get; set; }
        public String CompanyName { get; set; }
        public String CpfCnpj { get; set; }
        public String ZipCode { get; set; }
        public String Address { get; set; }
        public String AddressNum { get; set; }
        public String AddressAlt { get; set; }
        public String Neighborhood { get; set; }
        public String City { get; set; }
        public String State { get; set; }
    }
}