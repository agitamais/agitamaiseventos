﻿
namespace AgitaMaisEventos.Administration.Entities
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Default"), Module("Administration"), TableName("[dbo].[Producer]")]
    [DisplayName("Producer"), InstanceName("Producer")]
    [ReadPermission(PermissionKeys.SystemAdmin)]
    [ModifyPermission(PermissionKeys.SystemAdmin)]
    [LookupScript("Administration.Producer")]
    public sealed class ProducerRow : Row, IIdRow, INameRow
    {
        [DisplayName("Producer Id"), Identity]
        public Int32? ProducerId
        {
            get { return Fields.ProducerId[this]; }
            set { Fields.ProducerId[this] = value; }
        }

        [DisplayName("Name"), Size(100), QuickSearch]
        public String Name
        {
            get { return Fields.Name[this]; }
            set { Fields.Name[this] = value; }
        }

        [DisplayName("Company Name"), Size(100)]
        public String CompanyName
        {
            get { return Fields.CompanyName[this]; }
            set { Fields.CompanyName[this] = value; }
        }

        [DisplayName("Cpf Cnpj"), Size(14), NotNull]
        public String CpfCnpj
        {
            get { return Fields.CpfCnpj[this]; }
            set { Fields.CpfCnpj[this] = value; }
        }

        [DisplayName("Zip Code"), Size(8)]
        public String ZipCode
        {
            get { return Fields.ZipCode[this]; }
            set { Fields.ZipCode[this] = value; }
        }

        [DisplayName("Address"), Size(100)]
        public String Address
        {
            get { return Fields.Address[this]; }
            set { Fields.Address[this] = value; }
        }

        [DisplayName("Address Num"), Size(10)]
        public String AddressNum
        {
            get { return Fields.AddressNum[this]; }
            set { Fields.AddressNum[this] = value; }
        }

        [DisplayName("Address Alt"), Size(100)]
        public String AddressAlt
        {
            get { return Fields.AddressAlt[this]; }
            set { Fields.AddressAlt[this] = value; }
        }

        [DisplayName("Neighborhood"), Size(50)]
        public String Neighborhood
        {
            get { return Fields.Neighborhood[this]; }
            set { Fields.Neighborhood[this] = value; }
        }

        [DisplayName("City"), Size(50)]
        public String City
        {
            get { return Fields.City[this]; }
            set { Fields.City[this] = value; }
        }

        [DisplayName("State"), Size(2)]
        public String State
        {
            get { return Fields.State[this]; }
            set { Fields.State[this] = value; }
        }

        IIdField IIdRow.IdField
        {
            get { return Fields.ProducerId; }
        }

        StringField INameRow.NameField
        {
            get { return Fields.Name; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public ProducerRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field ProducerId;
            public StringField Name;
            public StringField CompanyName;
            public StringField CpfCnpj;
            public StringField ZipCode;
            public StringField Address;
            public StringField AddressNum;
            public StringField AddressAlt;
            public StringField Neighborhood;
            public StringField City;
            public StringField State;
        }
    }
}
