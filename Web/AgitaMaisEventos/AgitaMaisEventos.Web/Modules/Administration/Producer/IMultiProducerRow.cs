﻿using Serenity.Data;

namespace AgitaMaisEventos.Modules.Administration
{
    public interface IMultiProducerRow
    {
        Int32Field ProducerIdField { get; }
    }
}