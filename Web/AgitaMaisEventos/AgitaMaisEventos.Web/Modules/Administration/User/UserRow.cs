﻿
namespace AgitaMaisEventos.Administration.Entities
{
    using AgitaMaisEventos.Modules.Administration;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;

    [ConnectionKey("Default"), Module("Administration"), TableName("Users")]
    [DisplayName("Users"), InstanceName("User")]
    [ReadPermission(PermissionKeys.Security)]
    [ModifyPermission(PermissionKeys.Security)]
    [LookupScript("Administration.User")]
    public sealed class UserRow : LoggingRow, IIdRow, INameRow, IIsActiveRow, IMultiProducerRow
    {
        [DisplayName("User Id"), Identity]
        public Int32? UserId
        {
            get { return Fields.UserId[this]; }
            set { Fields.UserId[this] = value; }
        }

        [DisplayName("Username"), Size(100), NotNull, QuickSearch, LookupInclude]
        public String Username
        {
            get { return Fields.Username[this]; }
            set { Fields.Username[this] = value; }
        }

        [DisplayName("Source"), Size(4), NotNull, Insertable(false), Updatable(false), DefaultValue("site")]
        public String Source
        {
            get { return Fields.Source[this]; }
            set { Fields.Source[this] = value; }
        }

        [DisplayName("Password Hash"), Size(86), NotNull, Insertable(false), Updatable(false), MinSelectLevel(SelectLevel.Never)]
        public String PasswordHash
        {
            get { return Fields.PasswordHash[this]; }
            set { Fields.PasswordHash[this] = value; }
        }

        [DisplayName("Password Salt"), Size(10), NotNull, Insertable(false), Updatable(false), MinSelectLevel(SelectLevel.Never)]
        public String PasswordSalt
        {
            get { return Fields.PasswordSalt[this]; }
            set { Fields.PasswordSalt[this] = value; }
        }

        [DisplayName("Display Name"), Size(100), NotNull, LookupInclude]
        public String DisplayName
        {
            get { return Fields.DisplayName[this]; }
            set { Fields.DisplayName[this] = value; }
        }

        [DisplayName("Email"), Size(100)]
        public String Email
        {
            get { return Fields.Email[this]; }
            set { Fields.Email[this] = value; }
        }

        [DisplayName("User Image"), Size(100)]
        [ImageUploadEditor(FilenameFormat = "UserImage/~", CopyToHistory = true)]
        public String UserImage
        {
            get { return Fields.UserImage[this]; }
            set { Fields.UserImage[this] = value; }
        }

        [DisplayName("Password"), Size(50), NotMapped]
        public String Password
        {
            get { return Fields.Password[this]; }
            set { Fields.Password[this] = value; }
        }

        [NotNull, Insertable(false), Updatable(true)]
        public Int16? IsActive
        {
            get { return Fields.IsActive[this]; }
            set { Fields.IsActive[this] = value; }
        }

        [DisplayName("Confirm Password"), Size(50), NotMapped]
        public String PasswordConfirm
        {
            get { return Fields.PasswordConfirm[this]; }
            set { Fields.PasswordConfirm[this] = value; }
        }

        [DisplayName("Last Directory Update"), Insertable(false), Updatable(false)]
        public DateTime? LastDirectoryUpdate
        {
            get { return Fields.LastDirectoryUpdate[this]; }
            set { Fields.LastDirectoryUpdate[this] = value; }
        }

        [DisplayName("Is Producer"), NotNull]
        public Boolean? IsProducer
        {
            get { return Fields.IsProducer[this]; }
            set { Fields.IsProducer[this] = value; }
        }
        [LookupEditor(typeof(ProducerRow)), ReadPermission(PermissionKeys.SystemAdmin)]
        [DisplayName("Producer"), ForeignKey("[dbo].[Producer]", "ProducerId"), LeftJoin("jProducer"), TextualField("ProducerName")]
        public Int32? ProducerId
        {
            get { return Fields.ProducerId[this]; }
            set { Fields.ProducerId[this] = value; }
        }

        [DisplayName("Producer Name"), Expression("jProducer.[Name]")]
        public String ProducerName
        {
            get { return Fields.ProducerName[this]; }
            set { Fields.ProducerName[this] = value; }
        }

        [DisplayName("Producer Company Name"), Expression("jProducer.[CompanyName]")]
        public String ProducerCompanyName
        {
            get { return Fields.ProducerCompanyName[this]; }
            set { Fields.ProducerCompanyName[this] = value; }
        }

        [DisplayName("Producer Cpf Cnpj"), Expression("jProducer.[CpfCnpj]")]
        public String ProducerCpfCnpj
        {
            get { return Fields.ProducerCpfCnpj[this]; }
            set { Fields.ProducerCpfCnpj[this] = value; }
        }

        [DisplayName("Producer Zip Code"), Expression("jProducer.[ZipCode]")]
        public String ProducerZipCode
        {
            get { return Fields.ProducerZipCode[this]; }
            set { Fields.ProducerZipCode[this] = value; }
        }

        [DisplayName("Producer Address"), Expression("jProducer.[Address]")]
        public String ProducerAddress
        {
            get { return Fields.ProducerAddress[this]; }
            set { Fields.ProducerAddress[this] = value; }
        }

        [DisplayName("Producer Address Num"), Expression("jProducer.[AddressNum]")]
        public String ProducerAddressNum
        {
            get { return Fields.ProducerAddressNum[this]; }
            set { Fields.ProducerAddressNum[this] = value; }
        }

        [DisplayName("Producer Address Alt"), Expression("jProducer.[AddressAlt]")]
        public String ProducerAddressAlt
        {
            get { return Fields.ProducerAddressAlt[this]; }
            set { Fields.ProducerAddressAlt[this] = value; }
        }

        [DisplayName("Producer Neighborhood"), Expression("jProducer.[Neighborhood]")]
        public String ProducerNeighborhood
        {
            get { return Fields.ProducerNeighborhood[this]; }
            set { Fields.ProducerNeighborhood[this] = value; }
        }

        [DisplayName("Producer City"), Expression("jProducer.[City]")]
        public String ProducerCity
        {
            get { return Fields.ProducerCity[this]; }
            set { Fields.ProducerCity[this] = value; }
        }

        [DisplayName("Producer State"), Expression("jProducer.[State]")]
        public String ProducerState
        {
            get { return Fields.ProducerState[this]; }
            set { Fields.ProducerState[this] = value; }
        }


        IIdField IIdRow.IdField
        {
            get { return Fields.UserId; }
        }

        StringField INameRow.NameField
        {
            get { return Fields.Username; }
        }

        Int16Field IIsActiveRow.IsActiveField
        {
            get { return Fields.IsActive; }
        }

        Int32Field IMultiProducerRow.ProducerIdField => Fields.ProducerId;

        public static readonly RowFields Fields = new RowFields().Init();

        public UserRow()
            : base(Fields)
        {
        }

        public class RowFields : LoggingRowFields
        {
            public Int32Field UserId;
            public StringField Username;
            public StringField Source;
            public StringField PasswordHash;
            public StringField PasswordSalt;
            public StringField DisplayName;
            public StringField Email;
            public StringField UserImage;
            public DateTimeField LastDirectoryUpdate;
            public Int16Field IsActive;

            public StringField Password;
            public StringField PasswordConfirm;

            public BooleanField IsProducer;
            public Int32Field ProducerId;
            public StringField ProducerName;
            public StringField ProducerCompanyName;
            public StringField ProducerCpfCnpj;
            public StringField ProducerZipCode;
            public StringField ProducerAddress;
            public StringField ProducerAddressNum;
            public StringField ProducerAddressAlt;
            public StringField ProducerNeighborhood;
            public StringField ProducerCity;
            public StringField ProducerState;
        }
    }
}