﻿using Serenity.Data;

namespace AgitaMaisEventos.Modules.Administration
{
    public interface IMultiUserRow
    {
        Int32Field UserIdField { get; }
    }
}