﻿using Serenity.Navigation;
using MyPages = AgitaMaisEventos.Administration.Pages;
using Administration = AgitaMaisEventos.Administration.Pages;

[assembly: NavigationMenu(9000, "Administration", icon: "fa-desktop")]
[assembly: NavigationLink(9000, "Administration/Exceptions Log", url: "~/errorlog.axd", permission: AgitaMaisEventos.Administration.PermissionKeys.SystemAdmin, icon: "fa-ban", Target = "_blank")]
[assembly: NavigationLink(9000, "Administration/Languages",typeof(Administration.LanguageController), icon: "fa-comments")]
[assembly: NavigationLink(9000, "Administration/Translations", typeof(Administration.TranslationController), icon: "fa-comment-o")]
[assembly: NavigationLink(9000, "Administration/Roles", typeof(Administration.RoleController), icon: "fa-lock")]
[assembly: NavigationLink(9000, "Administration/User Management", typeof(Administration.UserController), icon: "fa-users")]
[assembly: NavigationLink(int.MaxValue, "Administration/Producer", typeof(MyPages.ProducerController), icon: null, Permission = AgitaMaisEventos.Administration.PermissionKeys.SystemAdmin)]