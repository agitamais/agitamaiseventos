﻿
namespace AgitaMaisEventos.Event.Entities
{
    using AgitaMaisEventos.Administration.Entities;
    using AgitaMaisEventos.Event.Entities;
    using AgitaMaisEventos.Modules.Administration;
    using AgitaMaisEventos.Modules.Event;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;

    [ConnectionKey("Default"), Module("Event"), TableName("[dbo].[Event]")]
    [DisplayName("Event"), InstanceName("Event")]
    [ReadPermission(PermissionKeys.EventUser)]
    [ModifyPermission(PermissionKeys.EventAdmin)]
    [LookupScript]
    public sealed class EventRow : Row, IIdRow, INameRow, IMultiProducerRow,IMultiUserRow, ICalculateDistanceRow
    {
        [DisplayName("Event Id"), Identity]
        public Int32? EventId
        {
            get { return Fields.EventId[this]; }
            set { Fields.EventId[this] = value; }
        }
        [LookupEditor(typeof(ProducerRow))]
        [DisplayName("Producer"), ForeignKey("[dbo].[Producer]", "ProducerId"), LeftJoin("jProducer"), TextualField("ProducerName")]
        public Int32? ProducerId
        {
            get { return Fields.ProducerId[this]; }
            set { Fields.ProducerId[this] = value; }
        }
     

        [DisplayName("User"), NotNull, ForeignKey("[dbo].[Users]", "UserId"), LeftJoin("jUser"), TextualField("UserUsername")]
        public Int32? UserId
        {
            get { return Fields.UserId[this]; }
            set { Fields.UserId[this] = value; }
        }

        [DisplayName("Name"), Size(50), NotNull, QuickSearch]
        public String Name
        {
            get { return Fields.Name[this]; }
            set { Fields.Name[this] = value; }
        }

        [DisplayName("Logo"), Size(255)]
        [ImageUploadEditor(FilenameFormat = "EventLogo/~", CopyToHistory = true)]
        public String Logo
        {
            get { return Fields.Logo[this]; }
            set { Fields.Logo[this] = value; }
        }
        [ImageUploadEditor(FilenameFormat = "EventBanner/~", CopyToHistory = true)]
        [DisplayName("Banner"), Size(255)]
        public String Banner
        {
            get { return Fields.Banner[this]; }
            set { Fields.Banner[this] = value; }
        }
        [DateTimeEditor]
        [DisplayName("Start Date"), NotNull, DateFormatter(DisplayFormat = "dd-MM-yyyy HH:mm")]
        public DateTime? StartDate
        {
            get { return Fields.StartDate[this]; }
            set { Fields.StartDate[this] = value; }
        }
        [DateTimeEditor]
        [DisplayName("End Date"), NotNull, DateFormatter(DisplayFormat = "dd-MM-yyyy HH:mm")]
        public DateTime? EndDate
        {
            get { return Fields.EndDate[this]; }
            set { Fields.EndDate[this] = value; }
        }

        [DisplayName("Price"), Size(18), Scale(2), NotNull]
        public Decimal? Price
        {
            get { return Fields.Price[this]; }
            set { Fields.Price[this] = value; }
        }
        [LookupEditor(typeof(EventCategoryRow),InplaceAdd = true, InplaceAddPermission = Administration.PermissionKeys.SystemAdmin)]
        [DisplayName("Category Prim"), Size(15),NotNull, ForeignKey("[dbo].[EventCategory]", "EventCategoryId"), LeftJoin("jCategoryPrim"), TextualField("CategoryPrimDescription")]
        public String CategoryPrimId
        {
            get { return Fields.CategoryPrimId[this]; }
            set { Fields.CategoryPrimId[this] = value; }
        }
        [LookupEditor(typeof(EventCategoryRow), InplaceAdd = true, InplaceAddPermission = Administration.PermissionKeys.SystemAdmin)]
        [DisplayName("Category Sec"), Size(15),ForeignKey("[dbo].[EventCategory]", "EventCategoryId"), LeftJoin("jCategorySec"), TextualField("CategorySecDescription")]
        public String CategorySecId
        {
            get { return Fields.CategorySecId[this]; }
            set { Fields.CategorySecId[this] = value; }
        }

        [DisplayName("Is Private Event"), NotNull]
        public Boolean? IsPrivateEvent
        {
            get { return Fields.IsPrivateEvent[this]; }
            set { Fields.IsPrivateEvent[this] = value; }
        }

        [DisplayName("Is Active"), NotNull]
        public Boolean? IsActive
        {
            get { return Fields.IsActive[this]; }
            set { Fields.IsActive[this] = value; }
        }
        [LookupEditor(typeof(EventAddressRow), InplaceAdd = true)]
        [DisplayName("Event Address"), NotNull, ForeignKey("[dbo].[EventAddress]", "EventAddressId"), LeftJoin("jEventAddress"), TextualField("EventAddressName")]
        public Int32? EventAddressId
        {
            get { return Fields.EventAddressId[this]; }
            set { Fields.EventAddressId[this] = value; }
        }

        [DisplayName("Description"),TextAreaEditor]
        public String Description
        {
            get { return Fields.Description[this]; }
            set { Fields.Description[this] = value; }
        }
        [DisplayName("HtmlDescription"), HtmlContentEditor(Rows = 30)]
        public String HtmlDescription
        {
            get { return Fields.HtmlDescription[this]; }
            set { Fields.HtmlDescription[this] = value; }
        }
        
        [DisplayName("Special Event Type"), Size(20), ForeignKey("[ESP].[SpecialEventType]", "SpecialEventTypeId"), LeftJoin("jSpecialEventType"), TextualField("SpecialEventTypeDescription")]
        public String SpecialEventTypeId
        {
            get { return Fields.SpecialEventTypeId[this]; }
            set { Fields.SpecialEventTypeId[this] = value; }
        }

        [DisplayName("Special Event Type Description"), Expression("jSpecialEventType.[Description]")]
        public String SpecialEventTypeDescription
        {
            get { return Fields.SpecialEventTypeDescription[this]; }
            set { Fields.SpecialEventTypeDescription[this] = value; }
        }


        [DisplayName("Producer Name"), Expression("jProducer.[Name]")]
        public String ProducerName
        {
            get { return Fields.ProducerName[this]; }
            set { Fields.ProducerName[this] = value; }
        }

        [DisplayName("Producer Company Name"), Expression("jProducer.[CompanyName]")]
        public String ProducerCompanyName
        {
            get { return Fields.ProducerCompanyName[this]; }
            set { Fields.ProducerCompanyName[this] = value; }
        }

        [DisplayName("Producer Cpf Cnpj"), Expression("jProducer.[CpfCnpj]")]
        public String ProducerCpfCnpj
        {
            get { return Fields.ProducerCpfCnpj[this]; }
            set { Fields.ProducerCpfCnpj[this] = value; }
        }

        [DisplayName("Producer Zip Code"), Expression("jProducer.[ZipCode]")]
        public String ProducerZipCode
        {
            get { return Fields.ProducerZipCode[this]; }
            set { Fields.ProducerZipCode[this] = value; }
        }

        [DisplayName("Producer Address"), Expression("jProducer.[Address]")]
        public String ProducerAddress
        {
            get { return Fields.ProducerAddress[this]; }
            set { Fields.ProducerAddress[this] = value; }
        }

        [DisplayName("Producer Address Num"), Expression("jProducer.[AddressNum]")]
        public String ProducerAddressNum
        {
            get { return Fields.ProducerAddressNum[this]; }
            set { Fields.ProducerAddressNum[this] = value; }
        }

        [DisplayName("Producer Address Alt"), Expression("jProducer.[AddressAlt]")]
        public String ProducerAddressAlt
        {
            get { return Fields.ProducerAddressAlt[this]; }
            set { Fields.ProducerAddressAlt[this] = value; }
        }

        [DisplayName("Producer Neighborhood"), Expression("jProducer.[Neighborhood]")]
        public String ProducerNeighborhood
        {
            get { return Fields.ProducerNeighborhood[this]; }
            set { Fields.ProducerNeighborhood[this] = value; }
        }

        [DisplayName("Producer City"), Expression("jProducer.[City]")]
        public String ProducerCity
        {
            get { return Fields.ProducerCity[this]; }
            set { Fields.ProducerCity[this] = value; }
        }

        [DisplayName("Producer State"), Expression("jProducer.[State]")]
        public String ProducerState
        {
            get { return Fields.ProducerState[this]; }
            set { Fields.ProducerState[this] = value; }
        }

        [DisplayName("User Username"), Expression("jUser.[Username]"), MinSelectLevel(SelectLevel.Never)]
        public String UserUsername
        {
            get { return Fields.UserUsername[this]; }
            set { Fields.UserUsername[this] = value; }
        }

        [DisplayName("User Display Name"), Expression("jUser.[DisplayName]"), MinSelectLevel(SelectLevel.Never)]
        public String UserDisplayName
        {
            get { return Fields.UserDisplayName[this]; }
            set { Fields.UserDisplayName[this] = value; }
        }

        [DisplayName("User Email"), Expression("jUser.[Email]"), MinSelectLevel(SelectLevel.Never)]
        public String UserEmail
        {
            get { return Fields.UserEmail[this]; }
            set { Fields.UserEmail[this] = value; }
        }

        [DisplayName("User Source"), Expression("jUser.[Source]"), MinSelectLevel(SelectLevel.Never)]
        public String UserSource
        {
            get { return Fields.UserSource[this]; }
            set { Fields.UserSource[this] = value; }
        }

       
        [DisplayName("User Is Active"), Expression("jUser.[IsActive]")]
        public Int16? UserIsActive
        {
            get { return Fields.UserIsActive[this]; }
            set { Fields.UserIsActive[this] = value; }
        }

        [DisplayName("User Is Producer"), Expression("jUser.[IsProducer]")]
        public Boolean? UserIsProducer
        {
            get { return Fields.UserIsProducer[this]; }
            set { Fields.UserIsProducer[this] = value; }
        }

        [DisplayName("User Producer Id"), Expression("jUser.[ProducerId]")]
        public Int32? UserProducerId
        {
            get { return Fields.UserProducerId[this]; }
            set { Fields.UserProducerId[this] = value; }
        }

        [DisplayName("Category Prim Description"), Expression("jCategoryPrim.[Description]")]
        public String CategoryPrimDescription
        {
            get { return Fields.CategoryPrimDescription[this]; }
            set { Fields.CategoryPrimDescription[this] = value; }
        }

        [DisplayName("Category Sec Description"), Expression("jCategorySec.[Description]")]
        public String CategorySecDescription
        {
            get { return Fields.CategorySecDescription[this]; }
            set { Fields.CategorySecDescription[this] = value; }
        }

        
        [DisplayName("Event Address Producer Id"), Expression("jEventAddress.[ProducerId]")]
        public Int32? EventAddressProducerId
        {
            get { return Fields.EventAddressProducerId[this]; }
            set { Fields.EventAddressProducerId[this] = value; }
        }

        [DisplayName("Event Address User Id"), Expression("jEventAddress.[UserId]")]
        public Int32? EventAddressUserId
        {
            get { return Fields.EventAddressUserId[this]; }
            set { Fields.EventAddressUserId[this] = value; }
        }

        [DisplayName("Event Address Name"), Expression("jEventAddress.[Name]")]
        public String EventAddressName
        {
            get { return Fields.EventAddressName[this]; }
            set { Fields.EventAddressName[this] = value; }
        }
        [DisplayName("EventAddressPhoneNumber"), Expression("jEventAddress.[PhoneNumber]")]
        public String EventAddressPhoneNumber
        {
            get { return Fields.EventAddressPhoneNumber[this]; }
            set { Fields.EventAddressPhoneNumber[this] = value; }
        }
        [DisplayName("EventAddressWhatsappNumber"), Expression("jEventAddress.[WhatsappNumber]")]
        public String EventAddressWhatsappNumber
        {
            get { return Fields.EventAddressWhatsappNumber[this]; }
            set { Fields.EventAddressWhatsappNumber[this] = value; }
        }



        [DisplayName("Event Address Zip Code"), Expression("jEventAddress.[ZipCode]")]
        public String EventAddressZipCode
        {
            get { return Fields.EventAddressZipCode[this]; }
            set { Fields.EventAddressZipCode[this] = value; }
        }

        [DisplayName("Event Address Address"), Expression("jEventAddress.[Address]")]
        public String EventAddressAddress
        {
            get { return Fields.EventAddressAddress[this]; }
            set { Fields.EventAddressAddress[this] = value; }
        }

        [DisplayName("Event Address Address Num"), Expression("jEventAddress.[AddressNum]")]
        public String EventAddressAddressNum
        {
            get { return Fields.EventAddressAddressNum[this]; }
            set { Fields.EventAddressAddressNum[this] = value; }
        }

        [DisplayName("Event Address Address Alt"), Expression("jEventAddress.[AddressAlt]")]
        public String EventAddressAddressAlt
        {
            get { return Fields.EventAddressAddressAlt[this]; }
            set { Fields.EventAddressAddressAlt[this] = value; }
        }

        [DisplayName("Event Address Neighborhood"), Expression("jEventAddress.[Neighborhood]")]
        public String EventAddressNeighborhood
        {
            get { return Fields.EventAddressNeighborhood[this]; }
            set { Fields.EventAddressNeighborhood[this] = value; }
        }

        [DisplayName("Event Address City"), Expression("jEventAddress.[City]")]
        public String EventAddressCity
        {
            get { return Fields.EventAddressCity[this]; }
            set { Fields.EventAddressCity[this] = value; }
        }

        [DisplayName("Event Address State"), Expression("jEventAddress.[State]")]
        public String EventAddressState
        {
            get { return Fields.EventAddressState[this]; }
            set { Fields.EventAddressState[this] = value; }
        }
        [DisplayName("Event Address Latitude"), Expression("jEventAddress.[Latitude]")]
        public Decimal? EventAddressLatitude
        {
            get { return Fields.EventAddressLatitude[this]; }
            set { Fields.EventAddressLatitude[this] = value; }
        }

        [DisplayName("Event Address Longitude"), Expression("jEventAddress.[Longitude]")]
        public Decimal? EventAddressLongitude
        {
            get { return Fields.EventAddressLongitude[this]; }
            set { Fields.EventAddressLongitude[this] = value; }
        }

        [DisplayName("Event Address Country"), Expression("jEventAddress.[Country]")]
        public String EventAddressCountry
        {
            get { return Fields.EventAddressCountry[this]; }
            set { Fields.EventAddressCountry[this] = value; }
        }

        [DisplayName("Distance"),NotMapped]
        public Decimal? EventAddressDistanceKM
        {
            get { return Fields.EventAddressDistanceKM[this]; }
            set { Fields.EventAddressDistanceKM[this] = value; }
        }

        IIdField IIdRow.IdField
        {
            get { return Fields.EventId; }
        }

        StringField INameRow.NameField
        {
            get { return Fields.Name; }
        }

        Int32Field IMultiProducerRow.ProducerIdField => Fields.ProducerId;

        Int32Field IMultiUserRow.UserIdField => Fields.UserId;

        DecimalField ICalculateDistanceRow.DistanceKM => Fields.EventAddressDistanceKM;

        DecimalField ICalculateDistanceRow.Latitude => Fields.EventAddressLatitude;

        DecimalField ICalculateDistanceRow.Longitude => Fields.EventAddressLongitude;

        public static readonly RowFields Fields = new RowFields().Init();

        public EventRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field EventId;
            public Int32Field ProducerId;
            public Int32Field UserId;
            public StringField Name;
            public StringField Logo;
            public StringField Banner;
            public DateTimeField StartDate;
            public DateTimeField EndDate;
            public DecimalField Price;
            public StringField CategoryPrimId;
            public StringField CategorySecId;
            public BooleanField IsPrivateEvent;
            public BooleanField IsActive;

            
            public Int32Field EventAddressId;
            public StringField Description;
            public StringField HtmlDescription;
            public StringField SpecialEventTypeId;
            public StringField SpecialEventTypeDescription;

            public StringField ProducerName;
            public StringField ProducerCompanyName;
            public StringField ProducerCpfCnpj;
            public StringField ProducerZipCode;
            public StringField ProducerAddress;
            public StringField ProducerAddressNum;
            public StringField ProducerAddressAlt;
            public StringField ProducerNeighborhood;
            public StringField ProducerCity;
            public StringField ProducerState;

            public StringField UserUsername;
            public StringField UserDisplayName;
            public StringField UserEmail;
            public StringField UserSource;
            public Int16Field UserIsActive;
            public BooleanField UserIsProducer;
            public Int32Field UserProducerId;

            public StringField CategoryPrimDescription;

            public StringField CategorySecDescription;
            
            public Int32Field EventAddressProducerId;
            public Int32Field EventAddressUserId;
            public StringField EventAddressName;
            public StringField EventAddressZipCode;
            public StringField EventAddressAddress;
            public StringField EventAddressAddressNum;
            public StringField EventAddressAddressAlt;
            public StringField EventAddressNeighborhood;
            public StringField EventAddressCity;
            public StringField EventAddressState;
            public DecimalField EventAddressDistanceKM;
            public DecimalField EventAddressLongitude;
            public DecimalField EventAddressLatitude;
            public StringField EventAddressCountry;
			public StringField EventAddressPhoneNumber;
            public StringField EventAddressWhatsappNumber;
        }
    }
}
