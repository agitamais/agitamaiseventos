﻿
namespace AgitaMaisEventos.Event.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("Event.Event")]
    [BasedOnRow(typeof(Entities.EventRow), CheckNames = true)]
    public class EventForm
    {      
        public Int32 ProducerId { get; set; }
        //public Int32 UserId { get; set; }
        public String Name { get; set; }
        public String Logo { get; set; }
        public String Banner { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public Decimal Price { get; set; }
        public String CategoryPrimId { get; set; }
        public String CategorySecId { get; set; }        
        public Boolean IsActive { get; set; }
        public Boolean IsPrivateEvent { get; set; }
        public Int32 EventAddressId { get; set; }

        public String Description { get; set; }
        public String HtmlDescription { get; set; }

        

    }
}