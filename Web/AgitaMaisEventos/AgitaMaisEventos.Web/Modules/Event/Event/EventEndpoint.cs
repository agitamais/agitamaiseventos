﻿
namespace AgitaMaisEventos.Event.Endpoints
{
    using AgitaMaisEventos.Modules.Event;
    using Serenity;
    using Serenity.Data;
    using Serenity.Services;
    using System.Data;
    using System.Web.Mvc;
    using MyRepository = Repositories.EventRepository;
    using MyRow = Entities.EventRow;

    [RoutePrefix("Services/Event/Event"), Route("{action}")]
    [ConnectionKey(typeof(MyRow)), ServiceAuthorize(typeof(MyRow))]
    public class EventController : ServiceEndpoint
    {
        [HttpPost, AuthorizeCreate(typeof(MyRow))]
        public SaveResponse Create(IUnitOfWork uow, SaveRequest<MyRow> request)
        {
            return new MyRepository().Create(uow, request);
        }

        [HttpPost, AuthorizeUpdate(typeof(MyRow))]
        public SaveResponse Update(IUnitOfWork uow, SaveRequest<MyRow> request)
        {
            return new MyRepository().Update(uow, request);
        }

        [HttpPost, AuthorizeDelete(typeof(MyRow))]
        public DeleteResponse Delete(IUnitOfWork uow, DeleteRequest request)
        {
            return new MyRepository().Delete(uow, request);
        }

        [HttpPost]
        public RetrieveResponse<MyRow> Retrieve(IDbConnection connection, RetrieveRequest request)
        {
            return new MyRepository().Retrieve(connection, request);
        }

        [HttpPost]
        public ListResponse<MyRow> List(IDbConnection connection, ListRequest request)
        {
            return new MyRepository().List(connection, request);
        }
        [HttpPost]
        public ListResponse<MyRow> ListWT(IDbConnection connection, EventListRequest request)
        {
            return new MyRepository().ListWithoutTennant(connection, request);
        }

        [HttpPost]
        public RetrieveResponse<MyRow> RetrieveWT(IDbConnection connection, EventRetrieveRequest request)
        {
            return new MyRepository().RetrieveWithoutTennant(connection, request);
        }

        [HttpGet]
        public ListResponse<MyRow> CountFutureEvents(IDbConnection connection)
        {
            return new MyRepository().CountFutureEvents(connection);
        }

        
    }
}
