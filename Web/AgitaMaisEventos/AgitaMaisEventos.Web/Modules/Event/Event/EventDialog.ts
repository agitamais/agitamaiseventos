﻿
namespace AgitaMaisEventos.Event {

    @Serenity.Decorators.registerClass()
    @Serenity.Decorators.panel()
    export class EventDialog extends Common.AgitaMaisEventosDialog<EventRow, any> {
        protected getFormKey() { return EventForm.formKey; }
        protected getIdProperty() { return EventRow.idProperty; }
        protected getLocalTextPrefix() { return EventRow.localTextPrefix; }
        protected getNameProperty() { return EventRow.nameProperty; }
        protected getService() { return EventService.baseUrl; }

        protected form = new EventForm(this.idPrefix);

    }
}