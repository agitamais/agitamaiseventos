﻿
namespace AgitaMaisEventos.Event.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("Event/Event"), Route("{action=index}")]
    [PageAuthorize(typeof(Entities.EventRow))]
    public class EventController : Controller
    {
        public ActionResult Index()
        {
            return View(MVC.Views.Event.Event_.EventIndex);
        }
    }
}