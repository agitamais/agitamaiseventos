﻿
namespace AgitaMaisEventos.Event {

    @Serenity.Decorators.registerClass()
    export class EventGrid extends Common.AgitaMaisEventosGrid<EventRow, any> {
        protected getColumnsKey() { return 'Event.Event'; }
        protected getDialogType() { return EventDialog; }
        protected getIdProperty() { return EventRow.idProperty; }
        protected getLocalTextPrefix() { return EventRow.localTextPrefix; }
        protected getService() { return EventService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }
        //protected onViewSubmit() {
        //    // only continue if base class returns true (didn't cancel request)
        //    if (!super.onViewSubmit()) {
        //        return false;
        //    }

        //    // view object is the data source for grid (SlickRemoteView)
        //    // this is an EntityGrid so its Params object is a ListRequest
        //    var request = this.view.params as Serenity.ListRequest;

        //    // list request has a Criteria parameter
        //    // we AND criteria here to existing one because 
        //    // otherwise we might clear filter set by 
        //    // an edit filter dialog if any.

        //    request.Criteria = Serenity.Criteria.and(request.Criteria,
        //        [['StartDate'], '>=', '2019-03-19'],
        //        [['StartDate'], '<=', '2019-03-23'],
        //        [['Price'], '>', 15]);

        //    request.Criteria = Serenity.Criteria.and(request.Criteria,
        //        [['CategoryPrimId'], 'in', [['pop_rock','sertanejo']] ],
        //        [['CategorySecId'], 'in', [['pop_rock', 'sertanejo']]]);

        //    // TypeScript doesn't support operator overloading
        //    // so we had to use array syntax above to build criteria.

        //    // Make sure you write
        //    // [['Field'], '>', 10] (which means field A is greater than 10)
        //    // not 
        //    // ['A', '>', 10] (which means string 'A' is greater than 10

        //    return true;
        //}

  
    }
}