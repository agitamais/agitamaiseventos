﻿
namespace AgitaMaisEventos.Event.Columns
{
    using Serenity.ComponentModel;
    using System;
    using System.ComponentModel;

    [ColumnsScript("Event.Event")]
    [BasedOnRow(typeof(Entities.EventRow), CheckNames = true)]
    public class EventColumns
    {
        public Int32 EventId { get; set; }
        public String ProducerName { get; set; }
        public String UserUsername { get; set; }
        [EditLink]
        public String Name { get; set; }
        public String Logo { get; set; }
        public String Banner { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public Decimal Price { get; set; }
        public String CategoryPrimDescription { get; set; }
        public String CategorySecDescription { get; set; }
        public Boolean IsPrivateEvent { get; set; }
        public String EventAddressName { get; set; }
    }
}