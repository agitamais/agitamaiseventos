﻿
namespace AgitaMaisEventos.Event.Repositories
{
    using AgitaMaisEventos.Modules.Event;
    using Serenity;
    using Serenity.Data;
    using Serenity.Services;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using MyRow = Entities.EventRow;
    using System.Linq;
    using AgitaMaisEventos.Modules.Administration;

    public class EventRepository
    {
        private static MyRow.RowFields fld { get { return MyRow.Fields; } }

        public SaveResponse Create(IUnitOfWork uow, SaveRequest<MyRow> request)
        {
            return new MySaveHandler().Process(uow, request, SaveRequestType.Create);
        }

        public SaveResponse Update(IUnitOfWork uow, SaveRequest<MyRow> request)
        {
            return new MySaveHandler().Process(uow, request, SaveRequestType.Update);
        }

        public DeleteResponse Delete(IUnitOfWork uow, DeleteRequest request)
        {
            return new MyDeleteHandler().Process(uow, request);
        }

        public RetrieveResponse<MyRow> Retrieve(IDbConnection connection, RetrieveRequest request)
        {
            return new MyRetrieveHandler().Process(connection, request);
        }

        public ListResponse<MyRow> List(IDbConnection connection, ListRequest request)
        {
            return new MyListHandler().Process(connection, request);
        }

        public ListResponse<MyRow> ListWithoutTennant(IDbConnection connection, EventListRequest request)
        {
            return new ListWithoutTennantHandler().Process(connection, request);
        }
        public RetrieveResponse<MyRow> RetrieveWithoutTennant(IDbConnection connection, EventRetrieveRequest request)
        {
            return new RetrieveWithoutTennantHandler().Process(connection, request);
        }
        public ListResponse<MyRow> CountFutureEvents(IDbConnection connection)
        {
            int total = connection.Count<MyRow>(
                  fld.IsActive == 1 &&
                  fld.IsPrivateEvent == 0 &&
                  fld.EndDate > DateTime.Now.Date
              );
            return new ListResponse<MyRow>()
            {
                TotalCount = total
            };
        }

        private class MySaveHandler : SaveRequestHandler<MyRow> { }
        private class MyDeleteHandler : DeleteRequestHandler<MyRow> { }
        private class MyRetrieveHandler : RetrieveRequestHandler<MyRow> { }
        private class MyListHandler : ListRequestHandler<MyRow> { }

        private class ListWithoutTennantHandler : ListRequestHandler<MyRow> {

            protected override void ApplyFilters(SqlQuery query)
            {
                base.ApplyFilters(query);
                query.Where(fld.IsActive == 1);
            }
            protected override IEnumerable<IListBehavior> GetBehaviors()
            {
                //return base.GetBehaviors();
                return base.GetBehaviors().Where(behav => !(behav is MultiUserBehavior) && !(behav is MultiProducerBehavior));
            }
        }

       

        private class RetrieveWithoutTennantHandler : RetrieveRequestHandler<MyRow> {

            protected override IEnumerable<IRetrieveBehavior> GetBehaviors()
            {
                //remove behaviours de tennant
                return base.GetBehaviors().Where(behav => !(behav is MultiUserBehavior) && !(behav is MultiProducerBehavior));
            }
        }


    }
}