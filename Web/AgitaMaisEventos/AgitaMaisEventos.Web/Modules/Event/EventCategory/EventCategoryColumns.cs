﻿
namespace AgitaMaisEventos.Event.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("Event.EventCategory")]
    [BasedOnRow(typeof(Entities.EventCategoryRow), CheckNames = true)]
    public class EventCategoryColumns
    {
        public String EventCategoryId { get; set; }
        public String Description { get; set; }
    }
}