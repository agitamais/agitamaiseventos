﻿
namespace AgitaMaisEventos.Event.Entities
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Default"), Module("Event"), TableName("[dbo].[EventCategory]")]
    [DisplayName("Event Category"), InstanceName("Event Category")]
    [ReadPermission(PermissionKeys.EventUser)]
    [ModifyPermission(Administration.PermissionKeys.SystemAdmin)]
    [LookupScript]
    public sealed class EventCategoryRow : Row, IIdRow, INameRow
    {
        [DisplayName("Event Category Id"), Size(15), PrimaryKey, QuickSearch]
        public String EventCategoryId
        {
            get { return Fields.EventCategoryId[this]; }
            set { Fields.EventCategoryId[this] = value; }
        }

        [DisplayName("Description"), Size(50), NotNull]
        public String Description
        {
            get { return Fields.Description[this]; }
            set { Fields.Description[this] = value; }
        }

        [DisplayName("Icon")]
        public String Icon
        {
            get { return Fields.Icon[this]; }
            set { Fields.Icon[this] = value; }
        }

        [DisplayName("IconFamily")]
        public String IconFamily
        {
            get { return Fields.IconFamily[this]; }
            set { Fields.IconFamily[this] = value; }
        }

        [ImageUploadEditor(FilenameFormat = "EventCategory/~", CopyToHistory = true)]
        [DisplayName("Image")]
        public String Image
        {
            get { return Fields.Image[this]; }
            set { Fields.Image[this] = value; }
        }

        IIdField IIdRow.IdField
        {
            get { return Fields.EventCategoryId; }
        }

        StringField INameRow.NameField
        {
            get { return Fields.EventCategoryId; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public EventCategoryRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public StringField EventCategoryId;
            public StringField Description;
            public StringField Image;
            public StringField IconFamily;
            public StringField Icon;
        }
    }
}
