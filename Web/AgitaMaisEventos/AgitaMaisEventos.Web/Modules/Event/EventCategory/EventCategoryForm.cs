﻿
namespace AgitaMaisEventos.Event.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("Event.EventCategory")]
    [BasedOnRow(typeof(Entities.EventCategoryRow), CheckNames = true)]
    public class EventCategoryForm
    {
        public String EventCategoryId { get; set; }
        public String Description { get; set; }
        public String Image { get; set; }
        public String Icon { get; set; }
        public String IconFamily { get; set; }

    }
}