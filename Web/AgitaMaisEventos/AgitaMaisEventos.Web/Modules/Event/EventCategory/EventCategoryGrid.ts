﻿
namespace AgitaMaisEventos.Event {

    @Serenity.Decorators.registerClass()
    export class EventCategoryGrid extends Common.AgitaMaisEventosGrid<EventCategoryRow, any> {
        protected getColumnsKey() { return 'Event.EventCategory'; }
        protected getDialogType() { return EventCategoryDialog; }
        protected getIdProperty() { return EventCategoryRow.idProperty; }
        protected getLocalTextPrefix() { return EventCategoryRow.localTextPrefix; }
        protected getService() { return EventCategoryService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}