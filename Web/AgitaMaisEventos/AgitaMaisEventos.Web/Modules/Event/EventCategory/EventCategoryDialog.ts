﻿
namespace AgitaMaisEventos.Event {

    @Serenity.Decorators.registerClass()
    export class EventCategoryDialog extends Common.AgitaMaisEventosDialog<EventCategoryRow, any> {
        protected getFormKey() { return EventCategoryForm.formKey; }
        protected getIdProperty() { return EventCategoryRow.idProperty; }
        protected getLocalTextPrefix() { return EventCategoryRow.localTextPrefix; }
        protected getNameProperty() { return EventCategoryRow.nameProperty; }
        protected getService() { return EventCategoryService.baseUrl; }

        protected form = new EventCategoryForm(this.idPrefix);

    }
}