﻿
namespace AgitaMaisEventos.Event.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("Event/EventCategory"), Route("{action=index}")]
    [PageAuthorize(typeof(Entities.EventCategoryRow))]
    public class EventCategoryController : Controller
    {
        public ActionResult Index()
        {
            return View("~/Modules/Event/EventCategory/EventCategoryIndex.cshtml");
        }
    }
}