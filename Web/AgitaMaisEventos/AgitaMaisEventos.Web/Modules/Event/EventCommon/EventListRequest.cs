﻿using Serenity.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AgitaMaisEventos.Modules.Event
{
    public class EventListRequest : ListRequest
    {
        public double UserLatitude { get; set; }

        public double UserLongitude { get; set; }
    }
}