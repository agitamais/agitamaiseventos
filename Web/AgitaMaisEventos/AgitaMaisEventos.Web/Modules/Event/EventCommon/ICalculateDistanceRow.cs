﻿using Serenity.Data;

namespace AgitaMaisEventos.Modules.Event
{
    public interface ICalculateDistanceRow
    {
        DecimalField DistanceKM { get; }
        DecimalField Latitude { get; }
        DecimalField Longitude { get; }
    }
}