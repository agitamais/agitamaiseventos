﻿using AgitaMaisEventos.Administration;
using Serenity;
using Serenity.Data;
using Serenity.Services;
using System.Linq;

namespace AgitaMaisEventos.Modules.Event
{
    public class CalculateDistanceBehavior : IImplicitBehavior,
        IListBehavior, IRetrieveBehavior
    {
        private DecimalField distanceFld;
        private DecimalField latitudeFld;
        private DecimalField longitudeFld;

        public bool ActivateFor(Row row)
        {
            var mt = row as ICalculateDistanceRow;
            if (mt == null)
                return false;

            distanceFld = mt.DistanceKM;
            latitudeFld = mt.Latitude;
            longitudeFld = mt.Longitude;

            return true;
        }

        public void OnPrepareQuery(IRetrieveRequestHandler handler,
            SqlQuery query)
        {
            var request = handler.Request as EventRetrieveRequest;
            if (request != null)
            {
                query.AddParam("UserLatitude", request.UserLatitude);
                query.AddParam("UserLongitude", request.UserLongitude);
                query.Select($"dbo.DistanceKM(@UserLatitude,@UserLongitude,{latitudeFld},{longitudeFld})", distanceFld.Name);
            }
        }

        public void OnPrepareQuery(IListRequestHandler handler,
            SqlQuery query)
        {
            var request = handler.Request as EventListRequest;
            if (request != null)
            {
                query.AddParam("UserLatitude", request.UserLatitude);
                query.AddParam("UserLongitude", request.UserLongitude);
                query.Select($"dbo.DistanceKM(@UserLatitude,@UserLongitude,{latitudeFld},{longitudeFld})", distanceFld.Name);
                query.OrderBy(distanceFld.Name);
            }
        }



        public void OnAfterDelete(IDeleteRequestHandler handler) { }
        public void OnAfterExecuteQuery(IRetrieveRequestHandler handler) { }
        public void OnAfterExecuteQuery(IListRequestHandler handler) { }
        public void OnAfterSave(ISaveRequestHandler handler) { }
        public void OnApplyFilters(IListRequestHandler handler, SqlQuery query) { }
        public void OnAudit(IDeleteRequestHandler handler) { }
        public void OnAudit(ISaveRequestHandler handler) { }
        public void OnBeforeDelete(IDeleteRequestHandler handler) { }
        public void OnBeforeExecuteQuery(IRetrieveRequestHandler handler) { }
        public void OnBeforeExecuteQuery(IListRequestHandler handler) { }
        public void OnBeforeSave(ISaveRequestHandler handler) { }
        public void OnPrepareQuery(IDeleteRequestHandler handler, SqlQuery query) { }
        public void OnPrepareQuery(ISaveRequestHandler handler, SqlQuery query) { }
        public void OnReturn(IDeleteRequestHandler handler) { }
        public void OnReturn(IRetrieveRequestHandler handler) { }
        public void OnReturn(IListRequestHandler handler) { }
        public void OnReturn(ISaveRequestHandler handler) { }
        public void OnValidateRequest(IRetrieveRequestHandler handler) { }
        public void OnValidateRequest(IListRequestHandler handler) { }
    }
}