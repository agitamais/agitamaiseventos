﻿
namespace AgitaMaisEventos.Event {

    declare var google: any
    @Serenity.Decorators.registerClass()
    @Serenity.Decorators.panel()   
    export class EventAddressDialog extends Common.AgitaMaisEventosDialog<EventAddressRow, any> {
        protected getFormKey() { return EventAddressForm.formKey; }
        protected getIdProperty() { return EventAddressRow.idProperty; }
        protected getLocalTextPrefix() { return EventAddressRow.localTextPrefix; }
        protected getNameProperty() { return EventAddressRow.nameProperty; }
        protected getService() { return EventAddressService.baseUrl; }

        protected form = new EventAddressForm(this.idPrefix);

        placeSearch: any
        autocomplete: any;
        isValidGoogleLocation: boolean;

        componentForm = {
            street_number: this.form.AddressNum,
            route: this.form.Address,
            sublocality_level_1: this.form.Neighborhood,
            administrative_area_level_2: this.form.City,
            administrative_area_level_1: this.form.State,
            country: this.form.Country,
            postal_code: this.form.ZipCode
        };


        protected updateInterface() {
            super.updateInterface();
            this.initAutocomplete();

            //var a = (<any>this.form.WhatsappNumber.element).intlTelInput({
            //    initialCountry: "auto",
            //    geoIpLookup: function (success, failure) {
            //        $.get("https://ipinfo.io", function () { }, "jsonp").always(function (resp) {
            //            var countryCode = (resp && resp.country) ? resp.country : "";
            //            success(countryCode);
            //        });
            //    },
            //});
            //console.log(a)
            this.form.Address.element.on('focus', () => {
                this.geolocate();
            })
            this.form.LocationSearch.element.attr('autocomplete','off')

            this.form.LocationSearch.change((e) => {
                this.isValidGoogleLocation = false;
            })
            this.form.LocationSearch.addValidationRule(this.uniqueName, (e) => {
                if (!this.isValidGoogleLocation)
                    return 'Localização inválida'
            })

            //for (var component in this.componentForm) {
            //    (<any>component).element.attr('disabled', 'disabled');
            //}

        }

        protected initAutocomplete() {            
            // Create the autocomplete object, restricting the search predictions to
            // geographical location types.
            this.autocomplete = new google.maps.places.Autocomplete(
                this.form.LocationSearch.element[0]);           
            // Avoid paying for data that you don't need by restricting the set of
            // place fields that are returned to just the address components.
            this.autocomplete.setFields(['address_components','geometry','name','type']);

           

            // When the user selects an address from the drop-down, populate the
            // address fields in the form.
            this.autocomplete.addListener('place_changed', () => this.fillInAddress());
        }

        protected fillInAddress() {
            
            // Get the place details from the autocomplete object.
            var place = this.autocomplete.getPlace();
            for (var component in this.componentForm) {
                (<any>component).value = '';
                //(<any>component).disabled = false;
            }
            this.isValidGoogleLocation = true;    

            this.form.Name.value = '';
            this.form.Latitude.value = place.geometry.location.lat()
            this.form.Longitude.value = place.geometry.location.lng()

            if (place.types.indexOf('establishment') > -1)
                this.form.Name.value = place.name || this.form.Name.value


          

            // Get each component of the address from the place details,
            // and then fill-in the corresponding field on the form.
            for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];
                if (this.componentForm[addressType]) {
                    var val = '';
                    if (addressType == 'administrative_area_level_1' ||
                        addressType == 'route')
                        val = place.address_components[i]['short_name'];
                    else {
                        val = place.address_components[i]['long_name'];
                    }
                    (<any>this.componentForm[addressType]).value = val;
                }
            }

            this.form.City.value = this.form.City.value || 'not especified'
            this.validateForm();
        }

        // Bias the autocomplete object to the user's geographical location,
        // as supplied by the browser's 'navigator.geolocation' object.
        protected geolocate() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition((position) => {
                    var geolocation = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    };
                    var circle = new google.maps.Circle(
                        { center: geolocation, radius: position.coords.accuracy });
                    this.autocomplete.setBounds(circle.getBounds());
                });
            }
        }

       
    }


}