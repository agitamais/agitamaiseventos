﻿
namespace AgitaMaisEventos.Event.Entities
{
    using AgitaMaisEventos.Administration.Entities;
    using AgitaMaisEventos.Common;
    using AgitaMaisEventos.Modules.Administration;
    using AgitaMaisEventos.Modules.Event;
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Default"), Module("Event"), TableName("[dbo].[EventAddress]")]
    [DisplayName("Event Address"), InstanceName("Event Address")]
    [ReadPermission(PermissionKeys.EventUser)]
    [ModifyPermission(PermissionKeys.EventAdmin)]
    [LookupScript]
    public sealed class EventAddressRow : Row, IIdRow, INameRow, IMultiProducerRow, IMultiUserRow, ICalculateDistanceRow
    {
        [DisplayName("Event Address Id"), Identity]
        public Int32? EventAddressId
        {
            get { return Fields.EventAddressId[this]; }
            set { Fields.EventAddressId[this] = value; }
        }
        [LookupEditor(typeof(ProducerRow))]
        [DisplayName("Producer"), NotNull, ForeignKey("[dbo].[Producer]", "ProducerId"), LeftJoin("jProducer"), TextualField("ProducerName"),LookupInclude]
        public Int32? ProducerId
        {
            get { return Fields.ProducerId[this]; }
            set { Fields.ProducerId[this] = value; }
        }

        [DisplayName("User Id"), NotNull]
        public Int32? UserId
        {
            get { return Fields.UserId[this]; }
            set { Fields.UserId[this] = value; }
        }

        [DisplayName("Name"), Size(100), NotNull, QuickSearch]
        public String Name
        {
            get { return Fields.Name[this]; }
            set { Fields.Name[this] = value; }
        }

        [DisplayName("Description"), TextAreaEditor]
        public String Description
        {
            get { return Fields.Description[this]; }
            set { Fields.Description[this] = value; }
        }
        [DisplayName("HtmlDescription"), HtmlContentEditor(Rows = 30)]
        public String HtmlDescription
        {
            get { return Fields.HtmlDescription[this]; }
            set { Fields.HtmlDescription[this] = value; }
        }

        [DisplayName("Is Active"), NotNull]
        public Boolean? IsActive
        {
            get { return Fields.IsActive[this]; }
            set { Fields.IsActive[this] = value; }
        }


        [DisplayName("Zip Code"), Size(8)]
        public String ZipCode
        {
            get { return Fields.ZipCode[this]; }
            set { Fields.ZipCode[this] = value; }
        }


        [DisplayName("Address"), Size(100)]
        public String Address
        {
            get { return Fields.Address[this]; }
            set { Fields.Address[this] = value; }
        }

        [DisplayName("Address Num"), Size(10)]
        public String AddressNum
        {
            get { return Fields.AddressNum[this]; }
            set { Fields.AddressNum[this] = value; }
        }

        [DisplayName("Address Alt"), Size(100)]
        public String AddressAlt
        {
            get { return Fields.AddressAlt[this]; }
            set { Fields.AddressAlt[this] = value; }
        }

        [DisplayName("Neighborhood"), Size(50)]
        public String Neighborhood
        {
            get { return Fields.Neighborhood[this]; }
            set { Fields.Neighborhood[this] = value; }
        }

        [DisplayName("City"), Size(50)]
        public String City
        {
            get { return Fields.City[this]; }
            set { Fields.City[this] = value; }
        }

        [DisplayName("State"), Size(2)]
        public String State
        {
            get { return Fields.State[this]; }
            set { Fields.State[this] = value; }
        }
        [PhoneEditor]
        [DisplayName("WhatsappNumber"), Size(15)]
        public String WhatsappNumber
        {
            get { return Fields.WhatsappNumber[this]; }
            set { Fields.WhatsappNumber[this] = value; }
        }
        [PhoneEditor]
        //[MaskedEditor(Mask = "(99) 9999999-9999")]
        [DisplayName("PhoneNumber"), Size(15)]
        public String PhoneNumber
        {
            get { return Fields.PhoneNumber[this]; }
            set { Fields.PhoneNumber[this] = value; }
        }



        [DisplayName("Location Search"), NotMapped]
        public String LocationSearch
        {
            get { return Fields.LocationSearch[this]; }
            set { Fields.LocationSearch[this] = value; }
        }


        [DisplayName("Country"), Size(20)]
        public String Country
        {
            get { return Fields.Country[this]; }
            set { Fields.Country[this] = value; }
        }

        [DisplayName("Latitude"), Size(8), Scale(6), NotNull]
        public Decimal? Latitude
        {
            get { return Fields.Latitude[this]; }
            set { Fields.Latitude[this] = value; }
        }

        [DisplayName("Longitude"), Size(9), Scale(6), NotNull]
        public Decimal? Longitude
        {
            get { return Fields.Longitude[this]; }
            set { Fields.Longitude[this] = value; }
        }
        [DisplayName("Distance"), NotMapped]
        public Decimal? DistanceKM
        {
            get { return Fields.DistanceKM[this]; }
            set { Fields.DistanceKM[this] = value; }
        }


        [LookupEditor(typeof(EventAddressCategoryRow), InplaceAdd = true, InplaceAddPermission = Administration.PermissionKeys.SystemAdmin)]
        [DisplayName("Event Address Category Prim"), Size(15), NotNull, ForeignKey("[dbo].[EventAddressCategory]", "EventAddressCategoryId"), LeftJoin("jEventAddressCategoryPrim"), TextualField("EventAddressCategoryPrimDescription")]
        public String EventAddressCategoryPrimId
        {
            get { return Fields.EventAddressCategoryPrimId[this]; }
            set { Fields.EventAddressCategoryPrimId[this] = value; }
        }

        [DisplayName("Event Address Category Prim Description"), Expression("jEventAddressCategoryPrim.[Description]")]
        public String EventAddressCategoryPrimDescription
        {
            get { return Fields.EventAddressCategoryPrimDescription[this]; }
            set { Fields.EventAddressCategoryPrimDescription[this] = value; }
        }

        [LookupEditor(typeof(EventAddressCategoryRow), InplaceAdd = true, InplaceAddPermission = Administration.PermissionKeys.SystemAdmin)]
        [DisplayName("Event Address Category Sec"), Size(15), ForeignKey("[dbo].[EventAddressCategory]", "EventAddressCategoryId"), LeftJoin("jEventAddressCategorySec"), TextualField("EventAddressCategorySecDescription")]
        public String EventAddressCategorySecId
        {
            get { return Fields.EventAddressCategorySecId[this]; }
            set { Fields.EventAddressCategorySecId[this] = value; }
        }

        [DisplayName("Event Address Category Sec Description"), Expression("jEventAddressCategorySec.[Description]")]
        public String EventAddressCategorySecDescription
        {
            get { return Fields.EventAddressCategorySecDescription[this]; }
            set { Fields.EventAddressCategorySecDescription[this] = value; }
        }


        [DisplayName("Logo"), Size(255)]
        [ImageUploadEditor(FilenameFormat = "EventAddressLogo/~", CopyToHistory = true)]
        public String Logo
        {
            get { return Fields.Logo[this]; }
            set { Fields.Logo[this] = value; }
        }
        [ImageUploadEditor(FilenameFormat = "EventAddressBanner/~", CopyToHistory = true)]
        [DisplayName("Banner"), Size(255)]
        public String Banner
        {
            get { return Fields.Banner[this]; }
            set { Fields.Banner[this] = value; }
        }

        [MultipleImageUploadEditor(FilenameFormat = "EventAddressGallery/~", CopyToHistory = true)]
        [DisplayName("Gallery")]
        public String Gallery
        {
            get { return Fields.Gallery[this]; }
            set { Fields.Gallery[this] = value; }
        }

        [DisplayName("Producer Name"), Expression("jProducer.[Name]")]
        public String ProducerName
        {
            get { return Fields.ProducerName[this]; }
            set { Fields.ProducerName[this] = value; }
        }

        [DisplayName("Producer Company Name"), Expression("jProducer.[CompanyName]")]
        public String ProducerCompanyName
        {
            get { return Fields.ProducerCompanyName[this]; }
            set { Fields.ProducerCompanyName[this] = value; }
        }

        [DisplayName("Producer Cpf Cnpj"), Expression("jProducer.[CpfCnpj]")]
        public String ProducerCpfCnpj
        {
            get { return Fields.ProducerCpfCnpj[this]; }
            set { Fields.ProducerCpfCnpj[this] = value; }
        }

        [DisplayName("Producer Zip Code"), Expression("jProducer.[ZipCode]")]
        public String ProducerZipCode
        {
            get { return Fields.ProducerZipCode[this]; }
            set { Fields.ProducerZipCode[this] = value; }
        }

        [DisplayName("Producer Address"), Expression("jProducer.[Address]")]
        public String ProducerAddress
        {
            get { return Fields.ProducerAddress[this]; }
            set { Fields.ProducerAddress[this] = value; }
        }

        [DisplayName("Producer Address Num"), Expression("jProducer.[AddressNum]")]
        public String ProducerAddressNum
        {
            get { return Fields.ProducerAddressNum[this]; }
            set { Fields.ProducerAddressNum[this] = value; }
        }

        [DisplayName("Producer Address Alt"), Expression("jProducer.[AddressAlt]")]
        public String ProducerAddressAlt
        {
            get { return Fields.ProducerAddressAlt[this]; }
            set { Fields.ProducerAddressAlt[this] = value; }
        }

        [DisplayName("Producer Neighborhood"), Expression("jProducer.[Neighborhood]")]
        public String ProducerNeighborhood
        {
            get { return Fields.ProducerNeighborhood[this]; }
            set { Fields.ProducerNeighborhood[this] = value; }
        }

        [DisplayName("Producer City"), Expression("jProducer.[City]")]
        public String ProducerCity
        {
            get { return Fields.ProducerCity[this]; }
            set { Fields.ProducerCity[this] = value; }
        }

        [DisplayName("Producer State"), Expression("jProducer.[State]")]
        public String ProducerState
        {
            get { return Fields.ProducerState[this]; }
            set { Fields.ProducerState[this] = value; }
        }


        IIdField IIdRow.IdField
        {
            get { return Fields.EventAddressId; }
        }

        StringField INameRow.NameField
        {
            get { return Fields.Name; }
        }

        Int32Field IMultiProducerRow.ProducerIdField => Fields.ProducerId;

        Int32Field IMultiUserRow.UserIdField => Fields.UserId;

        DecimalField ICalculateDistanceRow.DistanceKM => Fields.DistanceKM;

        DecimalField ICalculateDistanceRow.Latitude => Fields.Latitude;

        DecimalField ICalculateDistanceRow.Longitude => Fields.Longitude;

        public static readonly RowFields Fields = new RowFields().Init();

        public EventAddressRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field EventAddressId;
            public Int32Field ProducerId;
            public Int32Field UserId;
            public StringField Name;
            public StringField Description;
            public StringField HtmlDescription;
            public StringField ZipCode;
            public StringField Address;
            public StringField AddressNum;
            public StringField AddressAlt;
            public StringField Neighborhood;
            public StringField City;
            public StringField State;
            public StringField WhatsappNumber;
            public StringField PhoneNumber;
            public StringField Country;
            public StringField LocationSearch;
            public DecimalField Latitude;
            public DecimalField Longitude;
            public StringField Logo;
            public StringField Banner;
            public StringField Gallery;
            public StringField EventAddressCategoryPrimId;
            public StringField EventAddressCategorySecId;
            public BooleanField IsActive;
            public DecimalField DistanceKM;


            
            public StringField ProducerName;
            public StringField ProducerCompanyName;
            public StringField ProducerCpfCnpj;
            public StringField ProducerZipCode;
            public StringField ProducerAddress;
            public StringField ProducerAddressNum;
            public StringField ProducerAddressAlt;
            public StringField ProducerNeighborhood;
            public StringField ProducerCity;
            public StringField ProducerState;

            public StringField EventAddressCategoryPrimDescription;
            public StringField EventAddressCategorySecDescription;
        }
    }
}
