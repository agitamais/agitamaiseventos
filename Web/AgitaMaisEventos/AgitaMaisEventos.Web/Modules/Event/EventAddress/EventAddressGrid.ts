﻿
namespace AgitaMaisEventos.Event {

    @Serenity.Decorators.registerClass()
    export class EventAddressGrid extends Common.AgitaMaisEventosGrid<EventAddressRow, any> {
        protected getColumnsKey() { return 'Event.EventAddress'; }
        protected getDialogType() { return EventAddressDialog; }
        protected getIdProperty() { return EventAddressRow.idProperty; }
        protected getLocalTextPrefix() { return EventAddressRow.localTextPrefix; }
        protected getService() { return EventAddressService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}