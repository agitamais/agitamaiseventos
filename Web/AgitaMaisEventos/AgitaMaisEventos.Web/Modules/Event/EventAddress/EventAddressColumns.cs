﻿
namespace AgitaMaisEventos.Event.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("Event.EventAddress")]
    [BasedOnRow(typeof(Entities.EventAddressRow), CheckNames = true)]
    public class EventAddressColumns
    {
        [Hidden]
        public Int32 EventAddressId { get; set; }
        public String ProducerName { get; set; }
        public String Name { get; set; }
        public String ZipCode { get; set; }
        public String Address { get; set; }
        public String AddressNum { get; set; }
        public String AddressAlt { get; set; }
        public String Neighborhood { get; set; }
        public String City { get; set; }
        public String State { get; set; }
        public String EventAddressCategoryPrimDescription { get; set; }
        public String EventAddressCategorySecDescription { get; set; }
    }
}