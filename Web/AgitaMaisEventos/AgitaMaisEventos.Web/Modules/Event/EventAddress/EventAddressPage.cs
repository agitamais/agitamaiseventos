﻿
namespace AgitaMaisEventos.Event.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("Event/EventAddress"), Route("{action=index}")]
    [PageAuthorize(typeof(Entities.EventAddressRow))]
    public class EventAddressController : Controller
    {
        public ActionResult Index()
        {
            return View("~/Modules/Event/EventAddress/EventAddressIndex.cshtml");
        }
    }
}