﻿
namespace AgitaMaisEventos.Event.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;
    using AgitaMaisEventos.Common;

    [FormScript("Event.EventAddress")]
    [BasedOnRow(typeof(Entities.EventAddressRow), CheckNames = true)]
    public class EventAddressForm
    {
        public Int32 ProducerId { get; set; }
        public Int32 UserId { get; set; }
        public String LocationSearch { get; set; }
        public String Name { get; set; }
        public String PhoneNumber { get; set; }
        public String WhatsappNumber { get; set; }

        public String AddressNum { get; set; }
        public String AddressAlt { get; set; }

        public String EventAddressCategoryPrimId { get; set; }
        public String EventAddressCategorySecId { get; set; }

        public bool IsActive { get; set; }

     
        [Hidden]
        public String Country { get; set; }
        [Hidden]
        public Decimal Latitude { get; set; }
        [Hidden]
        public Decimal Longitude { get; set; }
        [Hidden]
        public String ZipCode { get; set; }
        [Hidden]
        public String Address { get; set; }
        [Hidden]
        public String Neighborhood { get; set; }
        [Hidden]
        public String City { get; set; }
        [Hidden]
        public String State { get; set; }

        public String Banner { get; set; }
        public String Logo { get; set; }
        public String Gallery { get; set; }
 
        public String Description { get; set; }
        public String HtmlDescription { get; set; }
    }
}