﻿
using Serenity.Extensibility;
using System.ComponentModel;

namespace AgitaMaisEventos.Event
{
    [NestedPermissionKeys]
    [DisplayName("Eventos")]
    public class PermissionKeys
    {
        public const string EventAdmin = "Event:Admin";
        public const string EventUser = "Event:User";

    }
}
