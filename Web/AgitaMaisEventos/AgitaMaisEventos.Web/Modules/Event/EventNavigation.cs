﻿using AgitaMaisEventos.Administration;
using Serenity.Navigation;
using MyPages = AgitaMaisEventos.Event.Pages;

[assembly: NavigationLink(int.MaxValue, "Event/Event", typeof(MyPages.EventController), icon: null)]
[assembly: NavigationLink(int.MaxValue, "Event/Event Address", typeof(MyPages.EventAddressController), icon: null)]
[assembly: NavigationLink(int.MaxValue, "Event/Event Category", typeof(MyPages.EventCategoryController), icon: null, Permission = PermissionKeys.SystemAdmin)]
[assembly: NavigationLink(int.MaxValue, "Event/Event Address Category", typeof(MyPages.EventAddressCategoryController), icon: null, Permission = PermissionKeys.SystemAdmin)]