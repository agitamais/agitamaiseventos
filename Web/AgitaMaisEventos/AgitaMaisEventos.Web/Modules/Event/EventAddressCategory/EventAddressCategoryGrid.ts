﻿
namespace AgitaMaisEventos.Event {

    @Serenity.Decorators.registerClass()
    export class EventAddressCategoryGrid extends Common.AgitaMaisEventosGrid<EventAddressCategoryRow, any> {
        protected getColumnsKey() { return 'Event.EventAddressCategory'; }
        protected getDialogType() { return EventAddressCategoryDialog; }
        protected getIdProperty() { return EventAddressCategoryRow.idProperty; }
        protected getLocalTextPrefix() { return EventAddressCategoryRow.localTextPrefix; }
        protected getService() { return EventAddressCategoryService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}