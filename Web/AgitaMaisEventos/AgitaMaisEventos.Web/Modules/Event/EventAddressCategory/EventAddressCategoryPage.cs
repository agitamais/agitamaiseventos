﻿
namespace AgitaMaisEventos.Event.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("Event/EventAddressCategory"), Route("{action=index}")]
    [PageAuthorize(typeof(Entities.EventAddressCategoryRow))]
    public class EventAddressCategoryController : Controller
    {
        public ActionResult Index()
        {
            return View("~/Modules/Event/EventAddressCategory/EventAddressCategoryIndex.cshtml");
        }
    }
}