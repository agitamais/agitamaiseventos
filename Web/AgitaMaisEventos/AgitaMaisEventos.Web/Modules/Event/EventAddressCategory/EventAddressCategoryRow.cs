﻿
namespace AgitaMaisEventos.Event.Entities
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Default"), Module("Event"), TableName("[dbo].[EventAddressCategory]")]
    [DisplayName("Event Address Category"), InstanceName("Event Address Category")]
    [ReadPermission(PermissionKeys.EventUser)]
    [ModifyPermission(Administration.PermissionKeys.SystemAdmin)]
    [LookupScript]
    public sealed class EventAddressCategoryRow : Row, IIdRow, INameRow
    {
        [DisplayName("Event Address Category Id"), Size(15), PrimaryKey, QuickSearch]
        public String EventAddressCategoryId
        {
            get { return Fields.EventAddressCategoryId[this]; }
            set { Fields.EventAddressCategoryId[this] = value; }
        }

        [DisplayName("Description"), Size(50), NotNull]
        public String Description
        {
            get { return Fields.Description[this]; }
            set { Fields.Description[this] = value; }
        }
        [ImageUploadEditor(FilenameFormat = "EventAddressCategory/~", CopyToHistory = true)]
        [DisplayName("Image"), Size(255)]
        public String Image
        {
            get { return Fields.Image[this]; }
            set { Fields.Image[this] = value; }
        }

        [DisplayName("Icon"), Size(20)]
        public String Icon
        {
            get { return Fields.Icon[this]; }
            set { Fields.Icon[this] = value; }
        }

        [DisplayName("Icon Family"), Size(20)]
        public String IconFamily
        {
            get { return Fields.IconFamily[this]; }
            set { Fields.IconFamily[this] = value; }
        }

        IIdField IIdRow.IdField
        {
            get { return Fields.EventAddressCategoryId; }
        }

        StringField INameRow.NameField
        {
            get { return Fields.EventAddressCategoryId; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public EventAddressCategoryRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public StringField EventAddressCategoryId;
            public StringField Description;
            public StringField Image;
            public StringField Icon;
            public StringField IconFamily;
        }
    }
}
