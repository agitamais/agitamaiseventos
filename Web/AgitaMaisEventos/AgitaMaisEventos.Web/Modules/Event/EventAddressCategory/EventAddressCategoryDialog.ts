﻿
namespace AgitaMaisEventos.Event {

    @Serenity.Decorators.registerClass()
    export class EventAddressCategoryDialog extends Common.AgitaMaisEventosDialog<EventAddressCategoryRow, any> {
        protected getFormKey() { return EventAddressCategoryForm.formKey; }
        protected getIdProperty() { return EventAddressCategoryRow.idProperty; }
        protected getLocalTextPrefix() { return EventAddressCategoryRow.localTextPrefix; }
        protected getNameProperty() { return EventAddressCategoryRow.nameProperty; }
        protected getService() { return EventAddressCategoryService.baseUrl; }

        protected form = new EventAddressCategoryForm(this.idPrefix);

       
    }
}