﻿
namespace AgitaMaisEventos.Event.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("Event.EventAddressCategory")]
    [BasedOnRow(typeof(Entities.EventAddressCategoryRow), CheckNames = true)]
    public class EventAddressCategoryColumns
    {        
        public String EventAddressCategoryId { get; set; }
        public String Description { get; set; }
        public String Image { get; set; }
        public String Icon { get; set; }
        public String IconFamily { get; set; }
    }
}