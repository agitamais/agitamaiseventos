﻿
namespace AgitaMaisEventos.SpecialEvent.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("SpecialEvent.MenuCompetitionItem")]
    [BasedOnRow(typeof(Entities.MenuCompetitionItemRow), CheckNames = true)]
    public class MenuCompetitionItemForm
    {
        public Int32 EventAddressId { get; set; }

        public Int32 MenuItemId { get; set; }
    }
}