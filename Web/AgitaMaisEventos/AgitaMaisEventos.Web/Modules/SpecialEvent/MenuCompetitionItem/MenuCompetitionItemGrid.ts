﻿
namespace AgitaMaisEventos.SpecialEvent {

    @Serenity.Decorators.registerClass()
    export class MenuCompetitionItemGrid extends Serenity.EntityGrid<MenuCompetitionItemRow, any> {
        protected getColumnsKey() { return 'SpecialEvent.MenuCompetitionItem'; }
        protected getDialogType() { return MenuCompetitionItemDialog; }
        protected getIdProperty() { return MenuCompetitionItemRow.idProperty; }
        protected getLocalTextPrefix() { return MenuCompetitionItemRow.localTextPrefix; }
        protected getService() { return MenuCompetitionItemService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}