﻿
namespace AgitaMaisEventos.SpecialEvent {

    @Serenity.Decorators.registerClass()
    export class MenuCompetitionItemDialog extends _Ext.EditorDialogBase<MenuCompetitionItemRow> {
        protected getFormKey() { return MenuCompetitionItemForm.formKey; }
        //protected getIdProperty() { return MenuCompetitionItemRow.idProperty; }
        protected getLocalTextPrefix() { return MenuCompetitionItemRow.localTextPrefix; }
        //protected getService() { return MenuCompetitionItemService.baseUrl; }

        protected form: MenuCompetitionItemForm;

        constructor() {
            super();

            this.form = new MenuCompetitionItemForm(this.idPrefix);
            this.filterMenuItem();
            this.form.EventAddressId.changeSelect2(e => {
                this.filterMenuItem()
            });
        }

        protected updateInterface() {
            super.updateInterface();
            this.filterMenuItem();
        }

        private filterMenuItem() {
            let itemId = Q.toId(this.form.EventAddressId.value);
            this.form.MenuItemId.filterField = 'ProducerId';
            if (itemId != null) {
                let item = AgitaMaisEventos.Event.EventAddressRow.getLookup().itemById[itemId];               
                this.form.MenuItemId.filterValue = item.ProducerId;
            }
            else {
                this.form.MenuItemId.filterValue = 0;
            }
        }

    }
}