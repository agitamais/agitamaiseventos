﻿
namespace AgitaMaisEventos.SpecialEvent.Entities
{
    using AgitaMaisEventos.Event.Entities;
    using AgitaMaisEventos.Menu.Entities;
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Default"), Module("SpecialEvent"), TableName("[ESP].[MenuCompetitionItem]")]
    [DisplayName("Menu Competition Item"), InstanceName("Menu Competition Item")]
    [ReadPermission(AgitaMaisEventos.Event.PermissionKeys.EventUser)]
    [ModifyPermission(AgitaMaisEventos.Event.PermissionKeys.EventAdmin)]
    [LookupScript]
    public sealed class MenuCompetitionItemRow : Row, IIdRow
    {
        [DisplayName("Menu Competition Item Id"), Identity]
        public Int32? MenuCompetitionItemId
        {
            get { return Fields.MenuCompetitionItemId[this]; }
            set { Fields.MenuCompetitionItemId[this] = value; }
        }

        [DisplayName("Menu Competition"), ForeignKey("[ESP].[MenuCompetition]", "MenuCompetitionId"), LeftJoin("jMenuCompetition"), MinSelectLevel(SelectLevel.Always)]
        public Int32? MenuCompetitionId
        {
            get { return Fields.MenuCompetitionId[this]; }
            set { Fields.MenuCompetitionId[this] = value; }
        }

      
        [LookupEditor(typeof(EventAddressRow), InplaceAdd = true)]
        [DisplayName("Event Address"), NotNull, ForeignKey("[dbo].[EventAddress]", "EventAddressId"), LeftJoin("jEventAddress"), TextualField("EventAddressName"), MinSelectLevel(SelectLevel.Always)]
        public Int32? EventAddressId
        {
            get { return Fields.EventAddressId[this]; }
            set { Fields.EventAddressId[this] = value; }
        }

     
        [LookupEditor(typeof(MenuItemRow), InplaceAdd = true)]
        [DisplayName("Menu Item"), NotNull, ForeignKey("[dbo].[MenuItem]", "MenuItemId"), LeftJoin("jMenuItem"), TextualField("MenuItemName"), MinSelectLevel(SelectLevel.Always)]
        public Int32? MenuItemId
        {
            get { return Fields.MenuItemId[this]; }
            set { Fields.MenuItemId[this] = value; }
        }

        [DisplayName("Menu Competition Event Id"), Expression("jMenuCompetition.[EventId]"), MinSelectLevel(SelectLevel.Always)]
        public Int32? MenuCompetitionEventId
        {
            get { return Fields.MenuCompetitionEventId[this]; }
            set { Fields.MenuCompetitionEventId[this] = value; }
        }

        [DisplayName("Menu Competition Qtd Menu Item Allowed Per Partner"), Expression("jMenuCompetition.[QtdMenuItemAllowedPerPartner]"), MinSelectLevel(SelectLevel.Always)]
        public Int32? MenuCompetitionQtdMenuItemAllowedPerPartner
        {
            get { return Fields.MenuCompetitionQtdMenuItemAllowedPerPartner[this]; }
            set { Fields.MenuCompetitionQtdMenuItemAllowedPerPartner[this] = value; }
        }

        [DisplayName("Event Address Producer Id"), Expression("jEventAddress.[ProducerId]")]
        public Int32? EventAddressProducerId
        {
            get { return Fields.EventAddressProducerId[this]; }
            set { Fields.EventAddressProducerId[this] = value; }
        }

        [DisplayName("Event Address User Id"), Expression("jEventAddress.[UserId]")]
        public Int32? EventAddressUserId
        {
            get { return Fields.EventAddressUserId[this]; }
            set { Fields.EventAddressUserId[this] = value; }
        }

        [DisplayName("Event Address Name"), Expression("jEventAddress.[Name]"),LookupInclude, MinSelectLevel(SelectLevel.Always)]
        public String EventAddressName
        {
            get { return Fields.EventAddressName[this]; }
            set { Fields.EventAddressName[this] = value; }
        }

        [DisplayName("Event Address Description"), Expression("jEventAddress.[Description]"), MinSelectLevel(SelectLevel.Always)]
        public String EventAddressDescription
        {
            get { return Fields.EventAddressDescription[this]; }
            set { Fields.EventAddressDescription[this] = value; }
        }

        [DisplayName("Event Address Html Description"), Expression("jEventAddress.[HtmlDescription]"), MinSelectLevel(SelectLevel.Always)]
        public String EventAddressHtmlDescription
        {
            get { return Fields.EventAddressHtmlDescription[this]; }
            set { Fields.EventAddressHtmlDescription[this] = value; }
        }

        [DisplayName("Event Address Zip Code"), Expression("jEventAddress.[ZipCode]"), MinSelectLevel(SelectLevel.Always)]
        public String EventAddressZipCode
        {
            get { return Fields.EventAddressZipCode[this]; }
            set { Fields.EventAddressZipCode[this] = value; }
        }

        [DisplayName("Event Address Address"), Expression("jEventAddress.[Address]"), MinSelectLevel(SelectLevel.Always)]
        public String EventAddressAddress
        {
            get { return Fields.EventAddressAddress[this]; }
            set { Fields.EventAddressAddress[this] = value; }
        }

        [DisplayName("Event Address Address Num"), Expression("jEventAddress.[AddressNum]"), MinSelectLevel(SelectLevel.Always)]
        public String EventAddressAddressNum
        {
            get { return Fields.EventAddressAddressNum[this]; }
            set { Fields.EventAddressAddressNum[this] = value; }
        }

        [DisplayName("Event Address Address Alt"), Expression("jEventAddress.[AddressAlt]"), MinSelectLevel(SelectLevel.Always)]
        public String EventAddressAddressAlt
        {
            get { return Fields.EventAddressAddressAlt[this]; }
            set { Fields.EventAddressAddressAlt[this] = value; }
        }

        [DisplayName("Event Address Neighborhood"), Expression("jEventAddress.[Neighborhood]"), MinSelectLevel(SelectLevel.Always)]
        public String EventAddressNeighborhood
        {
            get { return Fields.EventAddressNeighborhood[this]; }
            set { Fields.EventAddressNeighborhood[this] = value; }
        }

        [DisplayName("Event Address City"), Expression("jEventAddress.[City]"), MinSelectLevel(SelectLevel.Always)]
        public String EventAddressCity
        {
            get { return Fields.EventAddressCity[this]; }
            set { Fields.EventAddressCity[this] = value; }
        }

        [DisplayName("Event Address State"), Expression("jEventAddress.[State]"), MinSelectLevel(SelectLevel.Always)]
        public String EventAddressState
        {
            get { return Fields.EventAddressState[this]; }
            set { Fields.EventAddressState[this] = value; }
        }

        [DisplayName("Event Address Country"), Expression("jEventAddress.[Country]"), MinSelectLevel(SelectLevel.Always)]
        public String EventAddressCountry
        {
            get { return Fields.EventAddressCountry[this]; }
            set { Fields.EventAddressCountry[this] = value; }
        }

        [DisplayName("Event Address Latitude"), Expression("jEventAddress.[Latitude]"), MinSelectLevel(SelectLevel.Always)]
        public Decimal? EventAddressLatitude
        {
            get { return Fields.EventAddressLatitude[this]; }
            set { Fields.EventAddressLatitude[this] = value; }
        }

        [DisplayName("Event Address Longitude"), Expression("jEventAddress.[Longitude]"), MinSelectLevel(SelectLevel.Always)]
        public Decimal? EventAddressLongitude
        {
            get { return Fields.EventAddressLongitude[this]; }
            set { Fields.EventAddressLongitude[this] = value; }
        }

        [DisplayName("Event Address Gallery"), Expression("jEventAddress.[Gallery]"), MinSelectLevel(SelectLevel.Always)]
        public String EventAddressGallery
        {
            get { return Fields.EventAddressGallery[this]; }
            set { Fields.EventAddressGallery[this] = value; }
        }

        [DisplayName("Event Address Banner"), Expression("jEventAddress.[Banner]"), MinSelectLevel(SelectLevel.Always)]
        public String EventAddressBanner
        {
            get { return Fields.EventAddressBanner[this]; }
            set { Fields.EventAddressBanner[this] = value; }
        }

        [DisplayName("Event Address Logo"), Expression("jEventAddress.[Logo]"), MinSelectLevel(SelectLevel.Always)]
        public String EventAddressLogo
        {
            get { return Fields.EventAddressLogo[this]; }
            set { Fields.EventAddressLogo[this] = value; }
        }

        [DisplayName("Event Address Event Address Category Prim Id"), Expression("jEventAddress.[EventAddressCategoryPrimId]"), MinSelectLevel(SelectLevel.Always)]
        public String EventAddressEventAddressCategoryPrimId
        {
            get { return Fields.EventAddressEventAddressCategoryPrimId[this]; }
            set { Fields.EventAddressEventAddressCategoryPrimId[this] = value; }
        }

        [DisplayName("Event Address Event Address Category Sec Id"), Expression("jEventAddress.[EventAddressCategorySecId]"), MinSelectLevel(SelectLevel.Always)]
        public String EventAddressEventAddressCategorySecId
        {
            get { return Fields.EventAddressEventAddressCategorySecId[this]; }
            set { Fields.EventAddressEventAddressCategorySecId[this] = value; }
        }

        [DisplayName("Event Address Is Active"), Expression("jEventAddress.[IsActive]")]
        public Boolean? EventAddressIsActive
        {
            get { return Fields.EventAddressIsActive[this]; }
            set { Fields.EventAddressIsActive[this] = value; }
        }

        [DisplayName("Menu Item Event Address Id"), Expression("jMenuItem.[EventAddressId]")]
        public Int32? MenuItemEventAddressId
        {
            get { return Fields.MenuItemEventAddressId[this]; }
            set { Fields.MenuItemEventAddressId[this] = value; }
        }

        [DisplayName("Menu Item Name"), Expression("jMenuItem.[Name]"), LookupInclude, MinSelectLevel(SelectLevel.Always)]
        public String MenuItemName
        {
            get { return Fields.MenuItemName[this]; }
            set { Fields.MenuItemName[this] = value; }
        }

        [DisplayName("Menu Item Description"), Expression("jMenuItem.[Description]"), MinSelectLevel(SelectLevel.Always)]
        public String MenuItemDescription
        {
            get { return Fields.MenuItemDescription[this]; }
            set { Fields.MenuItemDescription[this] = value; }
        }

        [DisplayName("Menu Item Gallery"), Expression("jMenuItem.[Gallery]"), MinSelectLevel(SelectLevel.Always)]
        public String MenuItemGallery
        {
            get { return Fields.MenuItemGallery[this]; }
            set { Fields.MenuItemGallery[this] = value; }
        }

        IIdField IIdRow.IdField
        {
            get { return Fields.MenuCompetitionItemId; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public MenuCompetitionItemRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field MenuCompetitionItemId;
            public Int32Field MenuCompetitionId;
            public Int32Field EventAddressId;
            public Int32Field MenuItemId;

            public Int32Field MenuCompetitionEventId;
            public Int32Field MenuCompetitionQtdMenuItemAllowedPerPartner;

            public Int32Field EventAddressProducerId;
            public Int32Field EventAddressUserId;
            public StringField EventAddressName;
            public StringField EventAddressDescription;
            public StringField EventAddressHtmlDescription;
            public StringField EventAddressZipCode;
            public StringField EventAddressAddress;
            public StringField EventAddressAddressNum;
            public StringField EventAddressAddressAlt;
            public StringField EventAddressNeighborhood;
            public StringField EventAddressCity;
            public StringField EventAddressState;
            public StringField EventAddressCountry;
            public DecimalField EventAddressLatitude;
            public DecimalField EventAddressLongitude;
            public StringField EventAddressGallery;
            public StringField EventAddressBanner;
            public StringField EventAddressLogo;
            public StringField EventAddressEventAddressCategoryPrimId;
            public StringField EventAddressEventAddressCategorySecId;
            public BooleanField EventAddressIsActive;

            public Int32Field MenuItemEventAddressId;
            public StringField MenuItemName;
            public StringField MenuItemDescription;
            public StringField MenuItemGallery;
        }
    }
}
