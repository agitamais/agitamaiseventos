﻿
namespace AgitaMaisEventos.SpecialEvent.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("SpecialEvent.MenuCompetitionItem")]
    [BasedOnRow(typeof(Entities.MenuCompetitionItemRow), CheckNames = true)]
    public class MenuCompetitionItemColumns
    {
        [EditLink, DisplayName("Db.Shared.RecordId"), AlignRight]
        public Int32 MenuCompetitionItemId { get; set; }
        public Int32 MenuCompetitionId { get; set; }
        public String EventAddressName { get; set; }
        public String MenuItemName { get; set; }
    }
}