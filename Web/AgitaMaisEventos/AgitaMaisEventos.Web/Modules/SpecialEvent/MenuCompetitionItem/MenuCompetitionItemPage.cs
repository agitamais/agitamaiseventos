﻿
namespace AgitaMaisEventos.SpecialEvent.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("SpecialEvent/MenuCompetitionItem"), Route("{action=index}")]
    [PageAuthorize(typeof(Entities.MenuCompetitionItemRow))]
    public class MenuCompetitionItemController : Controller
    {
        public ActionResult Index()
        {
            return View("~/Modules/SpecialEvent/MenuCompetitionItem/MenuCompetitionItemIndex.cshtml");
        }
    }
}