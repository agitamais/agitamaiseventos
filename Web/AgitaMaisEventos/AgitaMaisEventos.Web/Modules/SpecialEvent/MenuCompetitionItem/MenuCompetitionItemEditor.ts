﻿
namespace AgitaMaisEventos.SpecialEvent {

    @Serenity.Decorators.registerClass()
    export class MenuCompetitionItemEditor extends _Ext.GridEditorBase<MenuCompetitionItemRow> {
        protected getColumnsKey() { return 'SpecialEvent.MenuCompetitionItem'; }
        protected getDialogType() { return MenuCompetitionItemDialog; }
        protected getLocalTextPrefix() { return MenuCompetitionItemRow.localTextPrefix; }

        constructor(container: JQuery) {
            super(container);

        }

        protected populateWhenVisible() {
            return true;
        }

        

        validateEntity(row, id) {
            row.MenuCompetitionId = Q.toId(row.MenuCompetitionId);

            //var sameProduct = Q.tryFirst(this.view.getItems(), x => x.MenuItemId === row.MenuItemId && x.MenuItemEventAddressId === row.EventAddressId);
            //if (sameProduct && this.id(sameProduct) !== id) {
            //    Q.alert('This product is already in order details!');
            //    return false;
            //}

            row.MenuItemName = Menu.MenuItemRow.getLookup().itemById[row.MenuItemId].Name;
            row.EventAddressName = Event.EventAddressRow.getLookup().itemById[row.EventAddressId].Name;
            //row.LineTotal = (row.Quantity || 0) * (row.UnitPrice || 0) - (row.Discount || 0);
            return true;
        }
    }
}