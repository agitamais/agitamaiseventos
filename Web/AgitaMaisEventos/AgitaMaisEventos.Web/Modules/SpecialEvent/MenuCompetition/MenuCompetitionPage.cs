﻿
namespace AgitaMaisEventos.SpecialEvent.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("SpecialEvent/MenuCompetition"), Route("{action=index}")]
    [PageAuthorize(typeof(Entities.MenuCompetitionRow))]
    public class MenuCompetitionController : Controller
    {
        public ActionResult Index()
        {
            return View("~/Modules/SpecialEvent/MenuCompetition/MenuCompetitionIndex.cshtml");
        }
    }
}