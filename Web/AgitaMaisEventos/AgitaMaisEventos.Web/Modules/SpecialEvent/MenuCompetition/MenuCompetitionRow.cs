﻿
namespace AgitaMaisEventos.SpecialEvent.Entities
{
    using AgitaMaisEventos.Event.Entities;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;

    [ConnectionKey("Default"), Module("SpecialEvent"), TableName("[ESP].[MenuCompetition]")]
    [DisplayName("Menu Competition"), InstanceName("Menu Competition")]
    [ReadPermission(AgitaMaisEventos.Event.PermissionKeys.EventUser)]
    [ModifyPermission(AgitaMaisEventos.Event.PermissionKeys.EventAdmin)]
    public sealed class MenuCompetitionRow : Row, IIdRow
    {
        [DisplayName("Menu Competition"), Identity]
        public Int32? MenuCompetitionId
        {
            get { return Fields.MenuCompetitionId[this]; }
            set { Fields.MenuCompetitionId[this] = value; }
        }
        [LookupEditor(typeof(EventRow), InplaceAdd = true, InplaceAddPermission = Administration.PermissionKeys.SystemAdmin)]
        [DisplayName("Event Id"), NotNull]
        [ForeignKey("[dbo].[Event]", "EventId"), LeftJoin("jMenuCompetition"), TextualField("MenuCompetitionName")]
        public Int32? EventId
        {
            get { return Fields.EventId[this]; }
            set { Fields.EventId[this] = value; }
        }

        [DisplayName("Qtd Menu Item Allowed Per Partner"), Hidden, DefaultValue(1), NotNull]
        public Int32? QtdMenuItemAllowedPerPartner
        {
            get { return Fields.QtdMenuItemAllowedPerPartner[this]; }
            set { Fields.QtdMenuItemAllowedPerPartner[this] = value; }
        }

        [DisplayName("Menu Competition Producer Id"), Expression("jMenuCompetition.[ProducerId]")]
        public Int32? MenuCompetitionProducerId
        {
            get { return Fields.MenuCompetitionProducerId[this]; }
            set { Fields.MenuCompetitionProducerId[this] = value; }
        }

        [DisplayName("Menu Competition User Id"), Expression("jMenuCompetition.[UserId]")]
        public Int32? MenuCompetitionUserId
        {
            get { return Fields.MenuCompetitionUserId[this]; }
            set { Fields.MenuCompetitionUserId[this] = value; }
        }

        [DisplayName("Menu Competition Name"), Expression("jMenuCompetition.[Name]")]
        public String MenuCompetitionName
        {
            get { return Fields.MenuCompetitionName[this]; }
            set { Fields.MenuCompetitionName[this] = value; }
        }

        [DisplayName("Menu Competition Logo"), Expression("jMenuCompetition.[Logo]")]
        public String MenuCompetitionLogo
        {
            get { return Fields.MenuCompetitionLogo[this]; }
            set { Fields.MenuCompetitionLogo[this] = value; }
        }

        [DisplayName("Menu Competition Banner"), Expression("jMenuCompetition.[Banner]")]
        public String MenuCompetitionBanner
        {
            get { return Fields.MenuCompetitionBanner[this]; }
            set { Fields.MenuCompetitionBanner[this] = value; }
        }

        [DisplayName("Menu Competition Start Date"), Expression("jMenuCompetition.[StartDate]")]
        public DateTime? MenuCompetitionStartDate
        {
            get { return Fields.MenuCompetitionStartDate[this]; }
            set { Fields.MenuCompetitionStartDate[this] = value; }
        }

        [DisplayName("Menu Competition End Date"), Expression("jMenuCompetition.[EndDate]")]
        public DateTime? MenuCompetitionEndDate
        {
            get { return Fields.MenuCompetitionEndDate[this]; }
            set { Fields.MenuCompetitionEndDate[this] = value; }
        }

        [DisplayName("Menu Competition Price"), Expression("jMenuCompetition.[Price]")]
        public Decimal? MenuCompetitionPrice
        {
            get { return Fields.MenuCompetitionPrice[this]; }
            set { Fields.MenuCompetitionPrice[this] = value; }
        }

        [DisplayName("Menu Competition Category Prim Id"), Expression("jMenuCompetition.[CategoryPrimId]")]
        public String MenuCompetitionCategoryPrimId
        {
            get { return Fields.MenuCompetitionCategoryPrimId[this]; }
            set { Fields.MenuCompetitionCategoryPrimId[this] = value; }
        }

        [DisplayName("Menu Competition Category Sec Id"), Expression("jMenuCompetition.[CategorySecId]")]
        public String MenuCompetitionCategorySecId
        {
            get { return Fields.MenuCompetitionCategorySecId[this]; }
            set { Fields.MenuCompetitionCategorySecId[this] = value; }
        }

        [DisplayName("Menu Competition Is Private Event"), Expression("jMenuCompetition.[IsPrivateEvent]")]
        public Boolean? MenuCompetitionIsPrivateEvent
        {
            get { return Fields.MenuCompetitionIsPrivateEvent[this]; }
            set { Fields.MenuCompetitionIsPrivateEvent[this] = value; }
        }

        [DisplayName("Menu Competition Event Address Id"), Expression("jMenuCompetition.[EventAddressId]")]
        public Int32? MenuCompetitionEventAddressId
        {
            get { return Fields.MenuCompetitionEventAddressId[this]; }
            set { Fields.MenuCompetitionEventAddressId[this] = value; }
        }

        [DisplayName("Menu Competition Galery"), Expression("jMenuCompetition.[Galery]")]
        public String MenuCompetitionGalery
        {
            get { return Fields.MenuCompetitionGalery[this]; }
            set { Fields.MenuCompetitionGalery[this] = value; }
        }

        [DisplayName("Menu Competition Description"), Expression("jMenuCompetition.[Description]")]
        public String MenuCompetitionDescription
        {
            get { return Fields.MenuCompetitionDescription[this]; }
            set { Fields.MenuCompetitionDescription[this] = value; }
        }

        [DisplayName("Menu Competition Html Description"), Expression("jMenuCompetition.[HtmlDescription]")]
        public String MenuCompetitionHtmlDescription
        {
            get { return Fields.MenuCompetitionHtmlDescription[this]; }
            set { Fields.MenuCompetitionHtmlDescription[this] = value; }
        }

        [DisplayName("Menu Competition Is Active"), Expression("jMenuCompetition.[IsActive]")]
        public Boolean? MenuCompetitionIsActive
        {
            get { return Fields.MenuCompetitionIsActive[this]; }
            set { Fields.MenuCompetitionIsActive[this] = value; }
        }

        [DisplayName("MenuCompetitionItem"), MasterDetailRelation(foreignKey: "MenuCompetitionId"), NotMapped, MinSelectLevel(SelectLevel.Always)]
        public List<MenuCompetitionItemRow> MenuCompetitionItemList
        {
            get { return Fields.MenuCompetitionItemList[this]; }
            set { Fields.MenuCompetitionItemList[this] = value; }
        }
        
        

        IIdField IIdRow.IdField
        {
            get { return Fields.MenuCompetitionId; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public MenuCompetitionRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field MenuCompetitionId;
            public Int32Field EventId;
            public Int32Field QtdMenuItemAllowedPerPartner;

            public Int32Field MenuCompetitionProducerId;
            public Int32Field MenuCompetitionUserId;
            public StringField MenuCompetitionName;
            public StringField MenuCompetitionLogo;
            public StringField MenuCompetitionBanner;
            public DateTimeField MenuCompetitionStartDate;
            public DateTimeField MenuCompetitionEndDate;
            public DecimalField MenuCompetitionPrice;
            public StringField MenuCompetitionCategoryPrimId;
            public StringField MenuCompetitionCategorySecId;
            public BooleanField MenuCompetitionIsPrivateEvent;
            public Int32Field MenuCompetitionEventAddressId;
            public StringField MenuCompetitionGalery;
            public StringField MenuCompetitionDescription;
            public StringField MenuCompetitionHtmlDescription;
            public BooleanField MenuCompetitionIsActive;

            public RowListField<MenuCompetitionItemRow> MenuCompetitionItemList;

        }
    }
}
