﻿
namespace AgitaMaisEventos.SpecialEvent.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("SpecialEvent.MenuCompetition")]
    [BasedOnRow(typeof(Entities.MenuCompetitionRow), CheckNames = true)]
    public class MenuCompetitionForm
    {
        public Int32 EventId { get; set; }
        public Int32 QtdMenuItemAllowedPerPartner { get; set; }

        [MenuCompetitionItemEditor]
        public List<Entities.MenuCompetitionItemRow> MenuCompetitionItemList { get; set; }
    }
}