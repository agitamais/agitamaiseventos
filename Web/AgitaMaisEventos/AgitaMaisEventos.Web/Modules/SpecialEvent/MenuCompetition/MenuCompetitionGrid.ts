﻿
namespace AgitaMaisEventos.SpecialEvent {

    @Serenity.Decorators.registerClass()
    export class MenuCompetitionGrid extends Serenity.EntityGrid<MenuCompetitionRow, any> {
        protected getColumnsKey() { return 'SpecialEvent.MenuCompetition'; }
        protected getDialogType() { return MenuCompetitionDialog; }
        protected getIdProperty() { return MenuCompetitionRow.idProperty; }
        protected getLocalTextPrefix() { return MenuCompetitionRow.localTextPrefix; }
        protected getService() { return MenuCompetitionService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}