﻿
namespace AgitaMaisEventos.SpecialEvent {

    @Serenity.Decorators.panel()
    @Serenity.Decorators.registerClass()
    export class MenuCompetitionDialog extends Common.AgitaMaisEventosDialog<MenuCompetitionRow, any> {
        protected getFormKey() { return MenuCompetitionForm.formKey; }
        protected getIdProperty() { return MenuCompetitionRow.idProperty; }
        protected getLocalTextPrefix() { return MenuCompetitionRow.localTextPrefix; }
        protected getService() { return MenuCompetitionService.baseUrl; }

        protected form = new MenuCompetitionForm(this.idPrefix);

        constructor() {
            super();
        }

    }
}