﻿
namespace AgitaMaisEventos.SpecialEvent.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("SpecialEvent.MenuCompetition")]
    [BasedOnRow(typeof(Entities.MenuCompetitionRow), CheckNames = true)]
    public class MenuCompetitionColumns
    {
        [EditLink, DisplayName("Db.Shared.RecordId"), AlignRight]
        public String MenuCompetitionName { get; set; }
        public Int32 EventId { get; set; }
        public Int32 QtdMenuItemAllowedPerPartner { get; set; }
    }
}