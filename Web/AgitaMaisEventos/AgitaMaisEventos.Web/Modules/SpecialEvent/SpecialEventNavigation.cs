﻿using Serenity.Navigation;
using MyPages = AgitaMaisEventos.SpecialEvent.Pages;

[assembly: NavigationLink(int.MaxValue, "SpecialEvent/Menu Competition", typeof(MyPages.MenuCompetitionController), icon: null)]
[assembly: NavigationLink(int.MaxValue, "SpecialEvent/Menu Competition Item", typeof(MyPages.MenuCompetitionItemController), icon: null)]