﻿namespace AgitaMaisEventos.Common {

    @Serenity.Decorators.registerEditor()
    export class PhoneEditor extends Serenity.StringEditor {

        @Serenity.Decorators.option()
        public isCellPhoneNumber: boolean;

        telInput: any;

        constructor(input: JQuery) {
            super(input);
            let inputFld = <any>input;

            //let itil = intlTelInput(input[0], {
            //    initialCountry: 'br'
            //});
            this.telInput = inputFld.intlTelInput({
                initialCountry: "br"
            });

            setTimeout(x => {
                this.formatNumber();
            }, 1000);

            //this.formatNumber();            
            this.element.keyup(x => this.formatNumber());



            //this.addValidationRule(this.uniqueName, e => {
            //    var value = Q.trimToNull(this.get_value());
            //    if (value == null) {
            //        return null;
            //    }
            //    return PhoneEditor.validate(value, this.multiple);
            //});

            //input.bind('change', e => {
            //    if (!Serenity.WX.hasOriginalEvent(e)) {
            //        return;
            //    }
            //    this.formatValue();
            //});

            //input.bind('blur', e => {
            //    if (this.element.hasClass('valid')) {
            //        this.formatValue();
            //    }
            //});
        }

        //protected formatValue(): void {
        //    super.
        //}

        protected formatNumber() {
            var input = <any>this.element;
            var v = (this.getFormattedValue());

            v = v.replace(/\D/g, ""); //Remove tudo o que não é dígito

            //coloca () em volta dos dois primeiros digitos
            v = v.replace(/(\d{0})(\d)/, "$1($2");
            v = v.replace(/(\d{2})(\d)/, "$1) $2");

            //se for celular, coloca - apos o 5 digito
            if (v.length >= 14) {
                v = v.replace(/(\d{5})(\d)/, "$1-$2"); //Coloca um ponto entre o terceiro e o quarto dígitos
            }
            //se não coloca hifem apos o 4 digito
            else {
                v = v.replace(/(\d{4})(\d)/, "$1-$2"); //Coloca um ponto entre o terceiro e o quarto dígitos
            }
            this.value = v;
        }

        protected getFormattedValue(): string {

            var value = this.element.val() || "";
            value = value.replace(/\(/g, '');
            value = value.replace(/\)/g, '');
            value = value.replace(/-/g, '');
            value = value.replace(/ /g, '');
            value = value.replace(/_/g, '');

            return value;
        }



        get_value() {
            return this.telInput.intlTelInput('getSelectedCountryData').dialCode + this.getFormattedValue();
        }

        set_value(value: string) {
            if (value) {
                if (value.length > 10) {
                    value = value.substring(2, value.length)
                }
                if (value.length > 11) {
                    value = value.substring(3, value.length)
                }
            }
            this.value = value;
        }




    }
}