﻿///<reference path="../../_Ext/Bases/GridBase.ts"/>
namespace AgitaMaisEventos.Common {

    @Serenity.Decorators.registerClass()
    export class AgitaMaisEventosGrid<TItem, TOptions> extends _Ext.GridBase<TItem, TOptions> {

        protected getColumnsKey() { return null; }
        protected getDialogType() { return null; }
        protected getIdProperty() { return null; }
        protected getLocalTextPrefix() { return null; }
        protected getService() { return null; }

        allowEdit = true;

        constructor(container: JQuery, options?: TOptions) {
            super(container, options);
            //this.element.hide();         
        }

        protected markupReady() {
            super.markupReady();            
        }
        protected getViewOptions() {
            let opt = super.getViewOptions();
            //Define o valor padrão selecionado no dropdown de resultados           
            return opt;
        }

        protected getPagerOptions() {
            let opt = super.getPagerOptions();
            //Define os valores que aparecerão no dropdown de resultados
            //opt.rowsPerPageOptions = [10, 25, 50, 100];
            return opt;
        }

        protected getSlickOptions() {
            let opt = super.getSlickOptions();
            opt.enableTextSelectionOnCells = true;
            opt.selectedCellCssClass = "slick-row-selected";
            opt.enableCellNavigation = true;
            opt.showFooterRow = false;
            opt.rowHeight = 40;
            
            return opt;
        }

        protected get_ExtGridOptions() {
            let opt = super.get_ExtGridOptions();
            opt.AutoColumnSize = true;
            return opt;
        }

        public getTitle() {
            return "";
        }
        
        protected getButtons(): Serenity.ToolButton[] {
            var button = super.getButtons().filter(x => x.cssClass != "refresh-button" &&
                x.cssClass != "column-picker-button" &&
                x.cssClass != "export-pdf-button"
            );


            return button;
        }
        
        protected getColumns() {
            var columns = super.getColumns();

            //columns.forEach((col, index) => {
            //    //if (col.sourceItem && col.sourceItem.title && col.sourceItem.title.toUpperCase().indexOf("CPF") > -1) {
            //    //    col.format = (ctx) => {
            //    //        if (ctx.value) {
            //    //            return Utils.formatCpf(ctx.value as string)
            //    //        }
                       
            //    //    }
            //    //}
            //    //else if (col.sourceItem && col.sourceItem.title && col.sourceItem.title.toUpperCase().indexOf("CNPJ") > -1) {
            //    //    col.format = (ctx) => {
            //    //        if (ctx.value) {
            //    //            return Utils.formatCnpj(ctx.value as string)
            //    //        }

            //    //    }
            //    //}
            //})

            ////Remove o botão de editar e o contador de linhas do slickgrid
            columns.splice(0, 2)

            columns.push({
                field: 'Detalhes',
                name: '',
                format: ctx => '<a class="center inline-action view-details" title="Detalhes">' +
                    '<i class="fa fa-pencil text-blue "></i></a>',
                width: 24,
                minWidth: 24,
                maxWidth: 24
            });
            columns.push({
                field: 'Excluir',
                name: '',
                format: ctx => '<a class="center inline-action delete-row" title="Excluir" >' +
                    '<i class="fa fa-trash-o text-secondary"></i></a>',
                width: 24,
                minWidth: 24,
                maxWidth: 24
            });
            return columns;
        }

        protected onClick(e: JQueryEventObject, row: number, cell: number) {

            if (e.isDefaultPrevented())
                return;

            var item = this.itemAt(row);
            let recordId = item[this.getIdProperty()]
            var target = $(e.target);

            if (target.parent().hasClass('inline-action'))
                target = target.parent();


            if (target.hasClass("slick-cell")) {
                e.preventDefault();
                //Cria o diálogo de edição de dialogs que implementem a classe ImpostoRendaDialog
                this.createEntityDialog(this.getItemType(), dlg => {
                    let dialog = (ss as any).safeCast(dlg, AgitaMaisEventosDialog);
                    if (dialog == null)
                        throw new (ss as any).InvalidOperationException(
                            Q.format("{0} Não implementa a classe AgitaMaisEventosDialog",
                                (ss as any).getTypeFullName((ss as any).getInstanceType(dlg))));
                    //Define o item que foi selecionado e abre o dialog como panel
                    dialog.load(recordId, () => {
                        dialog.dialogOpen(this.openDialogsAsPanel);
                        dialog.setAllowEdit(this.allowEdit);
                    });
                    return;

                });
            }

            //Exclui o registro
            if (target.hasClass('inline-action')) {
                if (target.hasClass('delete-row')) {
                    Q.confirm('Este item será excluído. Deseja continuar?', () => {

                        var self = this;
                        var request: Serenity.DeleteRequest = {
                            EntityId: recordId
                        };

                        var baseOptions: Q.ServiceOptions<Serenity.DeleteResponse> = {
                            service: this.getService() + '/Delete',
                            request: request,
                            onSuccess: response => {
                                self.element.triggerHandler('ondatachange', [{
                                    entityId: request.EntityId,
                                    entity: item,
                                    type: 'delete'
                                }]);
                                self.refresh();
                            }
                        };
                        Q.serviceCall(baseOptions);

                    });
                }
            }
        }


    }
}