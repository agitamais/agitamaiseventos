﻿///<reference path="../Helpers/GridEditorBase.ts"/>
namespace ImpostoRenda.Common.Base {

    @Serenity.Decorators.registerClass()
    export class AgitaMaisEventosEditor<TEntity> extends AgitaMaisEventos.Common.GridEditorBase<TEntity> {

        constructor(container: JQuery) {
            super(container);
        }    
    }
}