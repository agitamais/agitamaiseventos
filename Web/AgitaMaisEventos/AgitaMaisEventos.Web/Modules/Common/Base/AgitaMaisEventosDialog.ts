﻿///<reference path="../../_Ext/Bases/DialogBase.ts"/>
namespace AgitaMaisEventos.Common{

    @Serenity.Decorators.registerClass()
    export class AgitaMaisEventosDialog<TItem, TOptions> extends _Ext.DialogBase<TItem, TOptions> {

        public allowEdit: boolean;
        public hideSave = true;

        constructor(allowEdit: boolean = true) {
            super();
            this.setAllowEdit(allowEdit);
           
        }


        protected getPropertyItems() {
            var items = super.getPropertyItems();
            items = items.filter(item => item.title.indexOf('.UserId') == -1);
            if (!Q.Authorization.hasPermission("Administration:SystemAdmin"))
                items = items.filter(item => {
                   
                    if (item.editorParams && item.editorParams.lookupKey) {
                        if (item.editorParams.lookupKey == "Administration.Producer") {
                            return false;
                        }
                    }
                    return item;
                });
            return items;
        }


        protected updateInterface() {
            super.updateInterface();
            this.applyChangesButton.hide();
        }

        protected setAllowEdit(allowEdit: boolean) {
            if (!allowEdit) {
                this.disableInputs();
            }
            else {
                this.enableInputs();
            }
            this.allowEdit = allowEdit;
        }

        protected getAllowEdit() {
            return this.allowEdit;
        }

        protected get_ExtDialogOptions() {
            let opt = super.get_ExtDialogOptions();
            opt.AutoFitContentArea = false;
            opt.ShowCloseButtonInToolbar = true;
            return opt;
        }

        /**
         * Modifica as opções do Validator (Verificador de campos do dialog) 
         * */
        protected getValidatorOptions() {
            var opts = super.getValidatorOptions();
            //Limpa as mensagens de erro ao dar foco no campo com erro
            opts.onkeyup = false;
            opts.focusCleanup = true;
            //Ignora a função de que é executada ao tentar salvar campos com problema. (Remove a mensagem e erro e o popover)
            opts.invalidHandler = function () { };
            return opts;
        }

        protected disableInputs() {
            Serenity.EditorUtils.setReadonly(this.element.find('.editor'), true);
            // remove required asterisk (*)
            this.element.find('sup').hide();
            this.deleteButton.hide();
            //Não desabilita o salvar, para o caso de duas dialogs na mesma, uma readonly e a outra não (Exemplo: Gestão de Usuários)
            if (this.hideSave)
                this.saveAndCloseButton.hide();
        }

        protected enableInputs() {
            Serenity.EditorUtils.setReadonly(this.element.find('.editor'), false)
            this.element.find('sup').show();
            if (!this.isNew())
                this.deleteButton.show();
            this.saveAndCloseButton.show();
        }
    }
}