﻿
namespace AgitaMaisEventos.Menu.Entities
{
    using AgitaMaisEventos.Administration.Entities;
    using AgitaMaisEventos.Event.Entities;
    using AgitaMaisEventos.Modules.Administration;
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Default"), Module("Menu"), TableName("[dbo].[MenuItem]")]
    [DisplayName("Menu Item"), InstanceName("Menu Item")]
    [ReadPermission("Administration:General")]
    [ModifyPermission("Administration:General")]
    [LookupScript]
    public sealed class MenuItemRow : Row, IIdRow, INameRow, IMultiProducerRow
    {
        [DisplayName("Menu Item Id"), Identity]
        public Int32? MenuItemId
        {
            get { return Fields.MenuItemId[this]; }
            set { Fields.MenuItemId[this] = value; }
        }
        //[LookupEditor(typeof(EventAddressRow), InplaceAdd = true)]
        //[DisplayName("Event Address Id"), NotNull]
        //public Int32? EventAddressId
        //{
        //    get { return Fields.EventAddressId[this]; }
        //    set { Fields.EventAddressId[this] = value; }
        //}
        [LookupEditor(typeof(ProducerRow))]
        [DisplayName("Producer"), NotNull, ForeignKey("[dbo].[Producer]", "ProducerId"), LeftJoin("jProducer"), TextualField("ProducerName"), LookupInclude]
        public Int32? ProducerId
        {
            get { return Fields.ProducerId[this]; }
            set { Fields.ProducerId[this] = value; }
        }

        [DisplayName("Name"), Size(50), NotNull, QuickSearch]
        public String Name
        {
            get { return Fields.Name[this]; }
            set { Fields.Name[this] = value; }
        }

        [DisplayName("Description"), Size(200)]
        public String Description
        {
            get { return Fields.Description[this]; }
            set { Fields.Description[this] = value; }
        }

        [DisplayName("Gallery"), ImageUploadEditor(FilenameFormat = "MenuItemGallery/~", CopyToHistory = true)]
        public String Gallery
        {
            get { return Fields.Gallery[this]; }
            set { Fields.Gallery[this] = value; }
        }


        IIdField IIdRow.IdField
        {
            get { return Fields.MenuItemId; }
        }

        StringField INameRow.NameField
        {
            get { return Fields.Name; }
        }

        Int32Field IMultiProducerRow.ProducerIdField => Fields.ProducerId;

        public static readonly RowFields Fields = new RowFields().Init();

        public MenuItemRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field MenuItemId;
            //public Int32Field EventAddressId;
            public StringField Name;
            public StringField Description;
            public StringField Gallery;
            public Int32Field ProducerId;

            //public StringField ProducerName;
            //public StringField ProducerCompanyName;
            //public StringField ProducerCpfCnpj;
            //public StringField ProducerZipCode;
            //public StringField ProducerAddress;
            //public StringField ProducerAddressNum;
            //public StringField ProducerAddressAlt;
            //public StringField ProducerNeighborhood;
            //public StringField ProducerCity;
            //public StringField ProducerState;
        }
    }
}
