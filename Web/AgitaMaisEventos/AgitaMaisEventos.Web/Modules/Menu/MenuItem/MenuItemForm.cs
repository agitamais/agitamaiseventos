﻿
namespace AgitaMaisEventos.Menu.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("Menu.MenuItem")]
    [BasedOnRow(typeof(Entities.MenuItemRow), CheckNames = true)]
    public class MenuItemForm
    {
        public Int32 ProducerId { get; set; }
        public String Name { get; set; }
        public String Description { get; set; }
        public String Gallery { get; set; }
    }
}