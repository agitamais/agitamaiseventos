﻿
namespace AgitaMaisEventos.Menu.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("Menu.MenuItem")]
    [BasedOnRow(typeof(Entities.MenuItemRow), CheckNames = true)]
    public class MenuItemColumns
    {
        [EditLink, DisplayName("Db.Shared.RecordId"), AlignRight]
        public Int32 MenuItemId { get; set; }
        [EditLink]
        public String Name { get; set; }
        public String Description { get; set; }
        public String Gallery { get; set; }
    }
}