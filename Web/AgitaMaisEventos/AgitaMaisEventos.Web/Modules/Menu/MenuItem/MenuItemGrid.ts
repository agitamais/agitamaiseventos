﻿
namespace AgitaMaisEventos.Menu {

    @Serenity.Decorators.registerClass()
    export class MenuItemGrid extends Serenity.EntityGrid<MenuItemRow, any> {
        protected getColumnsKey() { return 'Menu.MenuItem'; }
        protected getDialogType() { return MenuItemDialog; }
        protected getIdProperty() { return MenuItemRow.idProperty; }
        protected getLocalTextPrefix() { return MenuItemRow.localTextPrefix; }
        protected getService() { return MenuItemService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}