﻿
namespace AgitaMaisEventos.Menu {

    @Serenity.Decorators.registerClass()
    export class MenuItemDialog extends Serenity.EntityDialog<MenuItemRow, any> {
        protected getFormKey() { return MenuItemForm.formKey; }
        protected getIdProperty() { return MenuItemRow.idProperty; }
        protected getLocalTextPrefix() { return MenuItemRow.localTextPrefix; }
        protected getNameProperty() { return MenuItemRow.nameProperty; }
        protected getService() { return MenuItemService.baseUrl; }

        protected form = new MenuItemForm(this.idPrefix);

    }
}