﻿
namespace AgitaMaisEventos.Menu.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("Menu/MenuItem"), Route("{action=index}")]
    [PageAuthorize(typeof(Entities.MenuItemRow))]
    public class MenuItemController : Controller
    {
        public ActionResult Index()
        {
            return View("~/Modules/Menu/MenuItem/MenuItemIndex.cshtml");
        }
    }
}