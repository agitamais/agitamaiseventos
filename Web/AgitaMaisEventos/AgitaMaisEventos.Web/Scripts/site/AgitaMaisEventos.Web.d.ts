﻿/// <reference path="../../Modules/_Ext/_q/_q.d.ts" />
/// <reference types="jquery" />
/// <reference types="jqueryui" />
/// <reference types="jquery.validation" />
declare namespace AgitaMaisEventos.Administration {
}
declare namespace AgitaMaisEventos.Administration {
    interface LanguageForm {
        LanguageId: Serenity.StringEditor;
        LanguageName: Serenity.StringEditor;
    }
    class LanguageForm extends Serenity.PrefixedContext {
        static formKey: string;
        private static init;
        constructor(prefix: string);
    }
}
declare namespace AgitaMaisEventos.Administration {
    interface LanguageRow {
        Id?: number;
        LanguageId?: string;
        LanguageName?: string;
    }
    namespace LanguageRow {
        const idProperty = "Id";
        const nameProperty = "LanguageName";
        const localTextPrefix = "Administration.Language";
        const lookupKey = "Administration.Language";
        function getLookup(): Q.Lookup<LanguageRow>;
        const enum Fields {
            Id = "Id",
            LanguageId = "LanguageId",
            LanguageName = "LanguageName"
        }
    }
}
declare namespace AgitaMaisEventos.Administration {
    namespace LanguageService {
        const baseUrl = "Administration/Language";
        function Create(request: Serenity.SaveRequest<LanguageRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Update(request: Serenity.SaveRequest<LanguageRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Delete(request: Serenity.DeleteRequest, onSuccess?: (response: Serenity.DeleteResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Retrieve(request: Serenity.RetrieveRequest, onSuccess?: (response: Serenity.RetrieveResponse<LanguageRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function List(request: Serenity.ListRequest, onSuccess?: (response: Serenity.ListResponse<LanguageRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        const enum Methods {
            Create = "Administration/Language/Create",
            Update = "Administration/Language/Update",
            Delete = "Administration/Language/Delete",
            Retrieve = "Administration/Language/Retrieve",
            List = "Administration/Language/List"
        }
    }
}
declare namespace AgitaMaisEventos.Administration {
}
declare namespace AgitaMaisEventos.Administration {
    interface ProducerForm {
        Name: Serenity.StringEditor;
        CompanyName: Serenity.StringEditor;
        CpfCnpj: Serenity.StringEditor;
        ZipCode: Serenity.StringEditor;
        Address: Serenity.StringEditor;
        AddressNum: Serenity.StringEditor;
        AddressAlt: Serenity.StringEditor;
        Neighborhood: Serenity.StringEditor;
        City: Serenity.StringEditor;
        State: Serenity.StringEditor;
    }
    class ProducerForm extends Serenity.PrefixedContext {
        static formKey: string;
        private static init;
        constructor(prefix: string);
    }
}
declare namespace AgitaMaisEventos.Administration {
    interface ProducerRow {
        ProducerId?: number;
        Name?: string;
        CompanyName?: string;
        CpfCnpj?: string;
        ZipCode?: string;
        Address?: string;
        AddressNum?: string;
        AddressAlt?: string;
        Neighborhood?: string;
        City?: string;
        State?: string;
    }
    namespace ProducerRow {
        const idProperty = "ProducerId";
        const nameProperty = "Name";
        const localTextPrefix = "Administration.Producer";
        const lookupKey = "Administration.Producer";
        function getLookup(): Q.Lookup<ProducerRow>;
        const enum Fields {
            ProducerId = "ProducerId",
            Name = "Name",
            CompanyName = "CompanyName",
            CpfCnpj = "CpfCnpj",
            ZipCode = "ZipCode",
            Address = "Address",
            AddressNum = "AddressNum",
            AddressAlt = "AddressAlt",
            Neighborhood = "Neighborhood",
            City = "City",
            State = "State"
        }
    }
}
declare namespace AgitaMaisEventos.Administration {
    namespace ProducerService {
        const baseUrl = "Administration/Producer";
        function Create(request: Serenity.SaveRequest<ProducerRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Update(request: Serenity.SaveRequest<ProducerRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Delete(request: Serenity.DeleteRequest, onSuccess?: (response: Serenity.DeleteResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Retrieve(request: Serenity.RetrieveRequest, onSuccess?: (response: Serenity.RetrieveResponse<ProducerRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function List(request: Serenity.ListRequest, onSuccess?: (response: Serenity.ListResponse<ProducerRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        const enum Methods {
            Create = "Administration/Producer/Create",
            Update = "Administration/Producer/Update",
            Delete = "Administration/Producer/Delete",
            Retrieve = "Administration/Producer/Retrieve",
            List = "Administration/Producer/List"
        }
    }
}
declare namespace AgitaMaisEventos.Administration {
}
declare namespace AgitaMaisEventos.Administration {
    interface RoleForm {
        RoleName: Serenity.StringEditor;
    }
    class RoleForm extends Serenity.PrefixedContext {
        static formKey: string;
        private static init;
        constructor(prefix: string);
    }
}
declare namespace AgitaMaisEventos.Administration {
    interface RolePermissionListRequest extends Serenity.ServiceRequest {
        RoleID?: number;
        Module?: string;
        Submodule?: string;
    }
}
declare namespace AgitaMaisEventos.Administration {
    interface RolePermissionListResponse extends Serenity.ListResponse<string> {
    }
}
declare namespace AgitaMaisEventos.Administration {
    interface RolePermissionRow {
        RolePermissionId?: number;
        RoleId?: number;
        PermissionKey?: string;
        RoleRoleName?: string;
    }
    namespace RolePermissionRow {
        const idProperty = "RolePermissionId";
        const nameProperty = "PermissionKey";
        const localTextPrefix = "Administration.RolePermission";
        const enum Fields {
            RolePermissionId = "RolePermissionId",
            RoleId = "RoleId",
            PermissionKey = "PermissionKey",
            RoleRoleName = "RoleRoleName"
        }
    }
}
declare namespace AgitaMaisEventos.Administration {
    namespace RolePermissionService {
        const baseUrl = "Administration/RolePermission";
        function Update(request: RolePermissionUpdateRequest, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function List(request: RolePermissionListRequest, onSuccess?: (response: RolePermissionListResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        const enum Methods {
            Update = "Administration/RolePermission/Update",
            List = "Administration/RolePermission/List"
        }
    }
}
declare namespace AgitaMaisEventos.Administration {
    interface RolePermissionUpdateRequest extends Serenity.ServiceRequest {
        RoleID?: number;
        Module?: string;
        Submodule?: string;
        Permissions?: string[];
    }
}
declare namespace AgitaMaisEventos.Administration {
    interface RoleRow {
        RoleId?: number;
        RoleName?: string;
    }
    namespace RoleRow {
        const idProperty = "RoleId";
        const nameProperty = "RoleName";
        const localTextPrefix = "Administration.Role";
        const lookupKey = "Administration.Role";
        function getLookup(): Q.Lookup<RoleRow>;
        const enum Fields {
            RoleId = "RoleId",
            RoleName = "RoleName"
        }
    }
}
declare namespace AgitaMaisEventos.Administration {
    namespace RoleService {
        const baseUrl = "Administration/Role";
        function Create(request: Serenity.SaveRequest<RoleRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Update(request: Serenity.SaveRequest<RoleRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Delete(request: Serenity.DeleteRequest, onSuccess?: (response: Serenity.DeleteResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Retrieve(request: Serenity.RetrieveRequest, onSuccess?: (response: Serenity.RetrieveResponse<RoleRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function List(request: Serenity.ListRequest, onSuccess?: (response: Serenity.ListResponse<RoleRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        const enum Methods {
            Create = "Administration/Role/Create",
            Update = "Administration/Role/Update",
            Delete = "Administration/Role/Delete",
            Retrieve = "Administration/Role/Retrieve",
            List = "Administration/Role/List"
        }
    }
}
declare namespace AgitaMaisEventos.Administration {
    interface TranslationItem {
        Key?: string;
        SourceText?: string;
        TargetText?: string;
        CustomText?: string;
    }
}
declare namespace AgitaMaisEventos.Administration {
    interface TranslationListRequest extends Serenity.ListRequest {
        SourceLanguageID?: string;
        TargetLanguageID?: string;
    }
}
declare namespace AgitaMaisEventos.Administration {
    namespace TranslationService {
        const baseUrl = "Administration/Translation";
        function List(request: TranslationListRequest, onSuccess?: (response: Serenity.ListResponse<TranslationItem>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Update(request: TranslationUpdateRequest, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        const enum Methods {
            List = "Administration/Translation/List",
            Update = "Administration/Translation/Update"
        }
    }
}
declare namespace AgitaMaisEventos.Administration {
    interface TranslationUpdateRequest extends Serenity.ServiceRequest {
        TargetLanguageID?: string;
        Translations?: {
            [key: string]: string;
        };
    }
}
declare namespace AgitaMaisEventos.Administration {
}
declare namespace AgitaMaisEventos.Administration {
    interface UserForm {
        Username: Serenity.StringEditor;
        DisplayName: Serenity.StringEditor;
        Email: Serenity.EmailEditor;
        UserImage: Serenity.ImageUploadEditor;
        Password: Serenity.PasswordEditor;
        PasswordConfirm: Serenity.PasswordEditor;
        Source: Serenity.StringEditor;
        ProducerId: Serenity.LookupEditor;
    }
    class UserForm extends Serenity.PrefixedContext {
        static formKey: string;
        private static init;
        constructor(prefix: string);
    }
}
declare namespace AgitaMaisEventos.Administration {
    interface UserPermissionListRequest extends Serenity.ServiceRequest {
        UserID?: number;
        Module?: string;
        Submodule?: string;
    }
}
declare namespace AgitaMaisEventos.Administration {
    interface UserPermissionRow {
        UserPermissionId?: number;
        UserId?: number;
        PermissionKey?: string;
        Granted?: boolean;
        Username?: string;
        User?: string;
    }
    namespace UserPermissionRow {
        const idProperty = "UserPermissionId";
        const nameProperty = "PermissionKey";
        const localTextPrefix = "Administration.UserPermission";
        const enum Fields {
            UserPermissionId = "UserPermissionId",
            UserId = "UserId",
            PermissionKey = "PermissionKey",
            Granted = "Granted",
            Username = "Username",
            User = "User"
        }
    }
}
declare namespace AgitaMaisEventos.Administration {
    namespace UserPermissionService {
        const baseUrl = "Administration/UserPermission";
        function Update(request: UserPermissionUpdateRequest, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function List(request: UserPermissionListRequest, onSuccess?: (response: Serenity.ListResponse<UserPermissionRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function ListRolePermissions(request: UserPermissionListRequest, onSuccess?: (response: Serenity.ListResponse<string>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function ListPermissionKeys(request: Serenity.ServiceRequest, onSuccess?: (response: Serenity.ListResponse<string>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        const enum Methods {
            Update = "Administration/UserPermission/Update",
            List = "Administration/UserPermission/List",
            ListRolePermissions = "Administration/UserPermission/ListRolePermissions",
            ListPermissionKeys = "Administration/UserPermission/ListPermissionKeys"
        }
    }
}
declare namespace AgitaMaisEventos.Administration {
    interface UserPermissionUpdateRequest extends Serenity.ServiceRequest {
        UserID?: number;
        Module?: string;
        Submodule?: string;
        Permissions?: UserPermissionRow[];
    }
}
declare namespace AgitaMaisEventos.Administration {
    interface UserRoleListRequest extends Serenity.ServiceRequest {
        UserID?: number;
    }
}
declare namespace AgitaMaisEventos.Administration {
    interface UserRoleListResponse extends Serenity.ListResponse<number> {
    }
}
declare namespace AgitaMaisEventos.Administration {
    interface UserRoleRow {
        UserRoleId?: number;
        UserId?: number;
        RoleId?: number;
        Username?: string;
        User?: string;
    }
    namespace UserRoleRow {
        const idProperty = "UserRoleId";
        const localTextPrefix = "Administration.UserRole";
        const enum Fields {
            UserRoleId = "UserRoleId",
            UserId = "UserId",
            RoleId = "RoleId",
            Username = "Username",
            User = "User"
        }
    }
}
declare namespace AgitaMaisEventos.Administration {
    namespace UserRoleService {
        const baseUrl = "Administration/UserRole";
        function Update(request: UserRoleUpdateRequest, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function List(request: UserRoleListRequest, onSuccess?: (response: UserRoleListResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        const enum Methods {
            Update = "Administration/UserRole/Update",
            List = "Administration/UserRole/List"
        }
    }
}
declare namespace AgitaMaisEventos.Administration {
    interface UserRoleUpdateRequest extends Serenity.ServiceRequest {
        UserID?: number;
        Roles?: number[];
    }
}
declare namespace AgitaMaisEventos.Administration {
    interface UserRow {
        UserId?: number;
        Username?: string;
        Source?: string;
        PasswordHash?: string;
        PasswordSalt?: string;
        DisplayName?: string;
        Email?: string;
        UserImage?: string;
        LastDirectoryUpdate?: string;
        IsActive?: number;
        Password?: string;
        PasswordConfirm?: string;
        IsProducer?: boolean;
        ProducerId?: number;
        ProducerName?: string;
        ProducerCompanyName?: string;
        ProducerCpfCnpj?: string;
        ProducerZipCode?: string;
        ProducerAddress?: string;
        ProducerAddressNum?: string;
        ProducerAddressAlt?: string;
        ProducerNeighborhood?: string;
        ProducerCity?: string;
        ProducerState?: string;
        InsertUserId?: number;
        InsertDate?: string;
        UpdateUserId?: number;
        UpdateDate?: string;
    }
    namespace UserRow {
        const idProperty = "UserId";
        const isActiveProperty = "IsActive";
        const nameProperty = "Username";
        const localTextPrefix = "Administration.User";
        const lookupKey = "Administration.User";
        function getLookup(): Q.Lookup<UserRow>;
        const enum Fields {
            UserId = "UserId",
            Username = "Username",
            Source = "Source",
            PasswordHash = "PasswordHash",
            PasswordSalt = "PasswordSalt",
            DisplayName = "DisplayName",
            Email = "Email",
            UserImage = "UserImage",
            LastDirectoryUpdate = "LastDirectoryUpdate",
            IsActive = "IsActive",
            Password = "Password",
            PasswordConfirm = "PasswordConfirm",
            IsProducer = "IsProducer",
            ProducerId = "ProducerId",
            ProducerName = "ProducerName",
            ProducerCompanyName = "ProducerCompanyName",
            ProducerCpfCnpj = "ProducerCpfCnpj",
            ProducerZipCode = "ProducerZipCode",
            ProducerAddress = "ProducerAddress",
            ProducerAddressNum = "ProducerAddressNum",
            ProducerAddressAlt = "ProducerAddressAlt",
            ProducerNeighborhood = "ProducerNeighborhood",
            ProducerCity = "ProducerCity",
            ProducerState = "ProducerState",
            InsertUserId = "InsertUserId",
            InsertDate = "InsertDate",
            UpdateUserId = "UpdateUserId",
            UpdateDate = "UpdateDate"
        }
    }
}
declare namespace AgitaMaisEventos.Administration {
    namespace UserService {
        const baseUrl = "Administration/User";
        function Create(request: Serenity.SaveRequest<UserRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Update(request: Serenity.SaveRequest<UserRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Delete(request: Serenity.DeleteRequest, onSuccess?: (response: Serenity.DeleteResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Undelete(request: Serenity.UndeleteRequest, onSuccess?: (response: Serenity.UndeleteResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Retrieve(request: Serenity.RetrieveRequest, onSuccess?: (response: Serenity.RetrieveResponse<UserRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function List(request: Serenity.ListRequest, onSuccess?: (response: Serenity.ListResponse<UserRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        const enum Methods {
            Create = "Administration/User/Create",
            Update = "Administration/User/Update",
            Delete = "Administration/User/Delete",
            Undelete = "Administration/User/Undelete",
            Retrieve = "Administration/User/Retrieve",
            List = "Administration/User/List"
        }
    }
}
declare namespace AgitaMaisEventos.Common {
    interface UserPreferenceRetrieveRequest extends Serenity.ServiceRequest {
        PreferenceType?: string;
        Name?: string;
    }
}
declare namespace AgitaMaisEventos.Common {
    interface UserPreferenceRetrieveResponse extends Serenity.ServiceResponse {
        Value?: string;
    }
}
declare namespace AgitaMaisEventos.Common {
    interface UserPreferenceRow {
        UserPreferenceId?: number;
        UserId?: number;
        PreferenceType?: string;
        Name?: string;
        Value?: string;
    }
    namespace UserPreferenceRow {
        const idProperty = "UserPreferenceId";
        const nameProperty = "Name";
        const localTextPrefix = "Common.UserPreference";
        const enum Fields {
            UserPreferenceId = "UserPreferenceId",
            UserId = "UserId",
            PreferenceType = "PreferenceType",
            Name = "Name",
            Value = "Value"
        }
    }
}
declare namespace AgitaMaisEventos.Common {
    namespace UserPreferenceService {
        const baseUrl = "Common/UserPreference";
        function Update(request: UserPreferenceUpdateRequest, onSuccess?: (response: Serenity.ServiceResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Retrieve(request: UserPreferenceRetrieveRequest, onSuccess?: (response: UserPreferenceRetrieveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        const enum Methods {
            Update = "Common/UserPreference/Update",
            Retrieve = "Common/UserPreference/Retrieve"
        }
    }
}
declare namespace AgitaMaisEventos.Common {
    interface UserPreferenceUpdateRequest extends Serenity.ServiceRequest {
        PreferenceType?: string;
        Name?: string;
        Value?: string;
    }
}
declare namespace AgitaMaisEventos.Event {
}
declare namespace AgitaMaisEventos.Event {
    interface EventAddressCategoryForm {
        EventAddressCategoryId: Serenity.StringEditor;
        Description: Serenity.StringEditor;
        Image: Serenity.ImageUploadEditor;
        Icon: Serenity.StringEditor;
        IconFamily: Serenity.StringEditor;
    }
    class EventAddressCategoryForm extends Serenity.PrefixedContext {
        static formKey: string;
        private static init;
        constructor(prefix: string);
    }
}
declare namespace AgitaMaisEventos.Event {
    interface EventAddressCategoryRow {
        EventAddressCategoryId?: string;
        Description?: string;
        Image?: string;
        Icon?: string;
        IconFamily?: string;
    }
    namespace EventAddressCategoryRow {
        const idProperty = "EventAddressCategoryId";
        const nameProperty = "EventAddressCategoryId";
        const localTextPrefix = "Event.EventAddressCategory";
        const lookupKey = "Event.EventAddressCategory";
        function getLookup(): Q.Lookup<EventAddressCategoryRow>;
        const enum Fields {
            EventAddressCategoryId = "EventAddressCategoryId",
            Description = "Description",
            Image = "Image",
            Icon = "Icon",
            IconFamily = "IconFamily"
        }
    }
}
declare namespace AgitaMaisEventos.Event {
    namespace EventAddressCategoryService {
        const baseUrl = "Event/EventAddressCategory";
        function Create(request: Serenity.SaveRequest<EventAddressCategoryRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Update(request: Serenity.SaveRequest<EventAddressCategoryRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Delete(request: Serenity.DeleteRequest, onSuccess?: (response: Serenity.DeleteResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Retrieve(request: Serenity.RetrieveRequest, onSuccess?: (response: Serenity.RetrieveResponse<EventAddressCategoryRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function List(request: Serenity.ListRequest, onSuccess?: (response: Serenity.ListResponse<EventAddressCategoryRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        const enum Methods {
            Create = "Event/EventAddressCategory/Create",
            Update = "Event/EventAddressCategory/Update",
            Delete = "Event/EventAddressCategory/Delete",
            Retrieve = "Event/EventAddressCategory/Retrieve",
            List = "Event/EventAddressCategory/List"
        }
    }
}
declare namespace AgitaMaisEventos.Event {
}
declare namespace AgitaMaisEventos.Event {
    interface EventAddressForm {
        ProducerId: Serenity.LookupEditor;
        UserId: Serenity.IntegerEditor;
        LocationSearch: Serenity.StringEditor;
        Name: Serenity.StringEditor;
        PhoneNumber: Common.PhoneEditor;
        WhatsappNumber: Common.PhoneEditor;
        AddressNum: Serenity.StringEditor;
        AddressAlt: Serenity.StringEditor;
        EventAddressCategoryPrimId: Serenity.LookupEditor;
        EventAddressCategorySecId: Serenity.LookupEditor;
        IsActive: Serenity.BooleanEditor;
        Country: Serenity.StringEditor;
        Latitude: Serenity.DecimalEditor;
        Longitude: Serenity.DecimalEditor;
        ZipCode: Serenity.StringEditor;
        Address: Serenity.StringEditor;
        Neighborhood: Serenity.StringEditor;
        City: Serenity.StringEditor;
        State: Serenity.StringEditor;
        Banner: Serenity.ImageUploadEditor;
        Logo: Serenity.ImageUploadEditor;
        Gallery: Serenity.MultipleImageUploadEditor;
        Description: Serenity.TextAreaEditor;
        HtmlDescription: Serenity.HtmlContentEditor;
    }
    class EventAddressForm extends Serenity.PrefixedContext {
        static formKey: string;
        private static init;
        constructor(prefix: string);
    }
}
declare namespace AgitaMaisEventos.Event {
    interface EventAddressRow {
        EventAddressId?: number;
        ProducerId?: number;
        UserId?: number;
        Name?: string;
        Description?: string;
        HtmlDescription?: string;
        ZipCode?: string;
        Address?: string;
        AddressNum?: string;
        AddressAlt?: string;
        Neighborhood?: string;
        City?: string;
        State?: string;
        WhatsappNumber?: string;
        PhoneNumber?: string;
        Country?: string;
        LocationSearch?: string;
        Latitude?: number;
        Longitude?: number;
        Logo?: string;
        Banner?: string;
        Gallery?: string;
        EventAddressCategoryPrimId?: string;
        EventAddressCategorySecId?: string;
        IsActive?: boolean;
        DistanceKM?: number;
        ProducerName?: string;
        ProducerCompanyName?: string;
        ProducerCpfCnpj?: string;
        ProducerZipCode?: string;
        ProducerAddress?: string;
        ProducerAddressNum?: string;
        ProducerAddressAlt?: string;
        ProducerNeighborhood?: string;
        ProducerCity?: string;
        ProducerState?: string;
        EventAddressCategoryPrimDescription?: string;
        EventAddressCategorySecDescription?: string;
    }
    namespace EventAddressRow {
        const idProperty = "EventAddressId";
        const nameProperty = "Name";
        const localTextPrefix = "Event.EventAddress";
        const lookupKey = "Event.EventAddress";
        function getLookup(): Q.Lookup<EventAddressRow>;
        const enum Fields {
            EventAddressId = "EventAddressId",
            ProducerId = "ProducerId",
            UserId = "UserId",
            Name = "Name",
            Description = "Description",
            HtmlDescription = "HtmlDescription",
            ZipCode = "ZipCode",
            Address = "Address",
            AddressNum = "AddressNum",
            AddressAlt = "AddressAlt",
            Neighborhood = "Neighborhood",
            City = "City",
            State = "State",
            WhatsappNumber = "WhatsappNumber",
            PhoneNumber = "PhoneNumber",
            Country = "Country",
            LocationSearch = "LocationSearch",
            Latitude = "Latitude",
            Longitude = "Longitude",
            Logo = "Logo",
            Banner = "Banner",
            Gallery = "Gallery",
            EventAddressCategoryPrimId = "EventAddressCategoryPrimId",
            EventAddressCategorySecId = "EventAddressCategorySecId",
            IsActive = "IsActive",
            DistanceKM = "DistanceKM",
            ProducerName = "ProducerName",
            ProducerCompanyName = "ProducerCompanyName",
            ProducerCpfCnpj = "ProducerCpfCnpj",
            ProducerZipCode = "ProducerZipCode",
            ProducerAddress = "ProducerAddress",
            ProducerAddressNum = "ProducerAddressNum",
            ProducerAddressAlt = "ProducerAddressAlt",
            ProducerNeighborhood = "ProducerNeighborhood",
            ProducerCity = "ProducerCity",
            ProducerState = "ProducerState",
            EventAddressCategoryPrimDescription = "EventAddressCategoryPrimDescription",
            EventAddressCategorySecDescription = "EventAddressCategorySecDescription"
        }
    }
}
declare namespace AgitaMaisEventos.Event {
    namespace EventAddressService {
        const baseUrl = "Event/EventAddress";
        function Create(request: Serenity.SaveRequest<EventAddressRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Update(request: Serenity.SaveRequest<EventAddressRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Delete(request: Serenity.DeleteRequest, onSuccess?: (response: Serenity.DeleteResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Retrieve(request: Serenity.RetrieveRequest, onSuccess?: (response: Serenity.RetrieveResponse<EventAddressRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function List(request: Serenity.ListRequest, onSuccess?: (response: Serenity.ListResponse<EventAddressRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function ListWT(request: Modules.Event.EventListRequest, onSuccess?: (response: Serenity.ListResponse<EventAddressRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function RetrieveWT(request: Modules.Event.EventRetrieveRequest, onSuccess?: (response: Serenity.RetrieveResponse<EventAddressRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function CountAllEventAddress(request: Serenity.ServiceRequest, onSuccess?: (response: Serenity.ListResponse<EventAddressRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        const enum Methods {
            Create = "Event/EventAddress/Create",
            Update = "Event/EventAddress/Update",
            Delete = "Event/EventAddress/Delete",
            Retrieve = "Event/EventAddress/Retrieve",
            List = "Event/EventAddress/List",
            ListWT = "Event/EventAddress/ListWT",
            RetrieveWT = "Event/EventAddress/RetrieveWT",
            CountAllEventAddress = "Event/EventAddress/CountAllEventAddress"
        }
    }
}
declare namespace AgitaMaisEventos.Event {
}
declare namespace AgitaMaisEventos.Event {
    interface EventCategoryForm {
        EventCategoryId: Serenity.StringEditor;
        Description: Serenity.StringEditor;
        Image: Serenity.ImageUploadEditor;
        Icon: Serenity.StringEditor;
        IconFamily: Serenity.StringEditor;
    }
    class EventCategoryForm extends Serenity.PrefixedContext {
        static formKey: string;
        private static init;
        constructor(prefix: string);
    }
}
declare namespace AgitaMaisEventos.Event {
    interface EventCategoryRow {
        EventCategoryId?: string;
        Description?: string;
        Image?: string;
        IconFamily?: string;
        Icon?: string;
    }
    namespace EventCategoryRow {
        const idProperty = "EventCategoryId";
        const nameProperty = "EventCategoryId";
        const localTextPrefix = "Event.EventCategory";
        const lookupKey = "Event.EventCategory";
        function getLookup(): Q.Lookup<EventCategoryRow>;
        const enum Fields {
            EventCategoryId = "EventCategoryId",
            Description = "Description",
            Image = "Image",
            IconFamily = "IconFamily",
            Icon = "Icon"
        }
    }
}
declare namespace AgitaMaisEventos.Event {
    namespace EventCategoryService {
        const baseUrl = "Event/EventCategory";
        function Create(request: Serenity.SaveRequest<EventCategoryRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Update(request: Serenity.SaveRequest<EventCategoryRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Delete(request: Serenity.DeleteRequest, onSuccess?: (response: Serenity.DeleteResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Retrieve(request: Serenity.RetrieveRequest, onSuccess?: (response: Serenity.RetrieveResponse<EventCategoryRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function List(request: Serenity.ListRequest, onSuccess?: (response: Serenity.ListResponse<EventCategoryRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        const enum Methods {
            Create = "Event/EventCategory/Create",
            Update = "Event/EventCategory/Update",
            Delete = "Event/EventCategory/Delete",
            Retrieve = "Event/EventCategory/Retrieve",
            List = "Event/EventCategory/List"
        }
    }
}
declare namespace AgitaMaisEventos.Event {
}
declare namespace AgitaMaisEventos.Event {
    interface EventForm {
        ProducerId: Serenity.LookupEditor;
        Name: Serenity.StringEditor;
        Logo: Serenity.ImageUploadEditor;
        Banner: Serenity.ImageUploadEditor;
        StartDate: Serenity.DateTimeEditor;
        EndDate: Serenity.DateTimeEditor;
        Price: Serenity.DecimalEditor;
        CategoryPrimId: Serenity.LookupEditor;
        CategorySecId: Serenity.LookupEditor;
        IsActive: Serenity.BooleanEditor;
        IsPrivateEvent: Serenity.BooleanEditor;
        EventAddressId: Serenity.LookupEditor;
        Description: Serenity.TextAreaEditor;
        HtmlDescription: Serenity.HtmlContentEditor;
    }
    class EventForm extends Serenity.PrefixedContext {
        static formKey: string;
        private static init;
        constructor(prefix: string);
    }
}
declare namespace AgitaMaisEventos.Event {
    interface EventRow {
        EventId?: number;
        ProducerId?: number;
        UserId?: number;
        Name?: string;
        Logo?: string;
        Banner?: string;
        StartDate?: string;
        EndDate?: string;
        Price?: number;
        CategoryPrimId?: string;
        CategorySecId?: string;
        IsPrivateEvent?: boolean;
        IsActive?: boolean;
        EventAddressId?: number;
        Description?: string;
        HtmlDescription?: string;
        SpecialEventTypeId?: string;
        SpecialEventTypeDescription?: string;
        ProducerName?: string;
        ProducerCompanyName?: string;
        ProducerCpfCnpj?: string;
        ProducerZipCode?: string;
        ProducerAddress?: string;
        ProducerAddressNum?: string;
        ProducerAddressAlt?: string;
        ProducerNeighborhood?: string;
        ProducerCity?: string;
        ProducerState?: string;
        UserUsername?: string;
        UserDisplayName?: string;
        UserEmail?: string;
        UserSource?: string;
        UserIsActive?: number;
        UserIsProducer?: boolean;
        UserProducerId?: number;
        CategoryPrimDescription?: string;
        CategorySecDescription?: string;
        EventAddressProducerId?: number;
        EventAddressUserId?: number;
        EventAddressName?: string;
        EventAddressZipCode?: string;
        EventAddressAddress?: string;
        EventAddressAddressNum?: string;
        EventAddressAddressAlt?: string;
        EventAddressNeighborhood?: string;
        EventAddressCity?: string;
        EventAddressState?: string;
        EventAddressDistanceKM?: number;
        EventAddressLongitude?: number;
        EventAddressLatitude?: number;
        EventAddressCountry?: string;
        EventAddressPhoneNumber?: string;
        EventAddressWhatsappNumber?: string;
    }
    namespace EventRow {
        const idProperty = "EventId";
        const nameProperty = "Name";
        const localTextPrefix = "Event.Event";
        const lookupKey = "Event.Event";
        function getLookup(): Q.Lookup<EventRow>;
        const enum Fields {
            EventId = "EventId",
            ProducerId = "ProducerId",
            UserId = "UserId",
            Name = "Name",
            Logo = "Logo",
            Banner = "Banner",
            StartDate = "StartDate",
            EndDate = "EndDate",
            Price = "Price",
            CategoryPrimId = "CategoryPrimId",
            CategorySecId = "CategorySecId",
            IsPrivateEvent = "IsPrivateEvent",
            IsActive = "IsActive",
            EventAddressId = "EventAddressId",
            Description = "Description",
            HtmlDescription = "HtmlDescription",
            SpecialEventTypeId = "SpecialEventTypeId",
            SpecialEventTypeDescription = "SpecialEventTypeDescription",
            ProducerName = "ProducerName",
            ProducerCompanyName = "ProducerCompanyName",
            ProducerCpfCnpj = "ProducerCpfCnpj",
            ProducerZipCode = "ProducerZipCode",
            ProducerAddress = "ProducerAddress",
            ProducerAddressNum = "ProducerAddressNum",
            ProducerAddressAlt = "ProducerAddressAlt",
            ProducerNeighborhood = "ProducerNeighborhood",
            ProducerCity = "ProducerCity",
            ProducerState = "ProducerState",
            UserUsername = "UserUsername",
            UserDisplayName = "UserDisplayName",
            UserEmail = "UserEmail",
            UserSource = "UserSource",
            UserIsActive = "UserIsActive",
            UserIsProducer = "UserIsProducer",
            UserProducerId = "UserProducerId",
            CategoryPrimDescription = "CategoryPrimDescription",
            CategorySecDescription = "CategorySecDescription",
            EventAddressProducerId = "EventAddressProducerId",
            EventAddressUserId = "EventAddressUserId",
            EventAddressName = "EventAddressName",
            EventAddressZipCode = "EventAddressZipCode",
            EventAddressAddress = "EventAddressAddress",
            EventAddressAddressNum = "EventAddressAddressNum",
            EventAddressAddressAlt = "EventAddressAddressAlt",
            EventAddressNeighborhood = "EventAddressNeighborhood",
            EventAddressCity = "EventAddressCity",
            EventAddressState = "EventAddressState",
            EventAddressDistanceKM = "EventAddressDistanceKM",
            EventAddressLongitude = "EventAddressLongitude",
            EventAddressLatitude = "EventAddressLatitude",
            EventAddressCountry = "EventAddressCountry",
            EventAddressPhoneNumber = "EventAddressPhoneNumber",
            EventAddressWhatsappNumber = "EventAddressWhatsappNumber"
        }
    }
}
declare namespace AgitaMaisEventos.Event {
    namespace EventService {
        const baseUrl = "Event/Event";
        function Create(request: Serenity.SaveRequest<EventRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Update(request: Serenity.SaveRequest<EventRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Delete(request: Serenity.DeleteRequest, onSuccess?: (response: Serenity.DeleteResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Retrieve(request: Serenity.RetrieveRequest, onSuccess?: (response: Serenity.RetrieveResponse<EventRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function List(request: Serenity.ListRequest, onSuccess?: (response: Serenity.ListResponse<EventRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function ListWT(request: Modules.Event.EventListRequest, onSuccess?: (response: Serenity.ListResponse<EventRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function RetrieveWT(request: Modules.Event.EventRetrieveRequest, onSuccess?: (response: Serenity.RetrieveResponse<EventRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function CountFutureEvents(request: Serenity.ServiceRequest, onSuccess?: (response: Serenity.ListResponse<EventRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        const enum Methods {
            Create = "Event/Event/Create",
            Update = "Event/Event/Update",
            Delete = "Event/Event/Delete",
            Retrieve = "Event/Event/Retrieve",
            List = "Event/Event/List",
            ListWT = "Event/Event/ListWT",
            RetrieveWT = "Event/Event/RetrieveWT",
            CountFutureEvents = "Event/Event/CountFutureEvents"
        }
    }
}
declare namespace AgitaMaisEventos {
    interface ExcelImportRequest extends Serenity.ServiceRequest {
        FileName?: string;
    }
}
declare namespace AgitaMaisEventos {
    interface ExcelImportResponse extends Serenity.ServiceResponse {
        Inserted?: number;
        Updated?: number;
        ErrorList?: string[];
    }
}
declare namespace AgitaMaisEventos {
    interface GetNextNumberRequest extends Serenity.ServiceRequest {
        Prefix?: string;
        Length?: number;
    }
}
declare namespace AgitaMaisEventos {
    interface GetNextNumberResponse extends Serenity.ServiceResponse {
        Number?: number;
        Serial?: string;
    }
}
declare namespace AgitaMaisEventos.Membership {
    interface ChangePasswordForm {
        OldPassword: Serenity.PasswordEditor;
        NewPassword: Serenity.PasswordEditor;
        ConfirmPassword: Serenity.PasswordEditor;
    }
    class ChangePasswordForm extends Serenity.PrefixedContext {
        static formKey: string;
        private static init;
        constructor(prefix: string);
    }
}
declare namespace AgitaMaisEventos.Membership {
    interface ChangePasswordRequest extends Serenity.ServiceRequest {
        OldPassword?: string;
        NewPassword?: string;
        ConfirmPassword?: string;
    }
}
declare namespace AgitaMaisEventos.Membership {
    interface ForgotPasswordForm {
        Email: Serenity.EmailEditor;
    }
    class ForgotPasswordForm extends Serenity.PrefixedContext {
        static formKey: string;
        private static init;
        constructor(prefix: string);
    }
}
declare namespace AgitaMaisEventos.Membership {
    interface ForgotPasswordRequest extends Serenity.ServiceRequest {
        Email?: string;
    }
}
declare namespace AgitaMaisEventos.Membership {
    interface LoginForm {
        Username: Serenity.StringEditor;
        Password: Serenity.PasswordEditor;
        KeepLoggedIn: Serenity.BooleanEditor;
    }
    class LoginForm extends Serenity.PrefixedContext {
        static formKey: string;
        private static init;
        constructor(prefix: string);
    }
}
declare namespace AgitaMaisEventos.Membership {
    interface LoginRequest extends Serenity.ServiceRequest {
        Username?: string;
        Password?: string;
        KeepLoggedIn?: boolean;
    }
}
declare namespace AgitaMaisEventos.Membership {
    interface ResetPasswordForm {
        NewPassword: Serenity.PasswordEditor;
        ConfirmPassword: Serenity.PasswordEditor;
    }
    class ResetPasswordForm extends Serenity.PrefixedContext {
        static formKey: string;
        private static init;
        constructor(prefix: string);
    }
}
declare namespace AgitaMaisEventos.Membership {
    interface ResetPasswordRequest extends Serenity.ServiceRequest {
        Token?: string;
        NewPassword?: string;
        ConfirmPassword?: string;
    }
}
declare namespace AgitaMaisEventos.Membership {
    interface SignUpForm {
        DisplayName: Serenity.StringEditor;
        Email: Serenity.EmailEditor;
        ConfirmEmail: Serenity.EmailEditor;
        Password: Serenity.PasswordEditor;
        ConfirmPassword: Serenity.PasswordEditor;
    }
    class SignUpForm extends Serenity.PrefixedContext {
        static formKey: string;
        private static init;
        constructor(prefix: string);
    }
}
declare namespace AgitaMaisEventos.Membership {
    interface SignUpRequest extends Serenity.ServiceRequest {
        DisplayName?: string;
        Email?: string;
        Password?: string;
    }
}
declare namespace AgitaMaisEventos.Menu {
}
declare namespace AgitaMaisEventos.Menu {
    interface MenuItemForm {
        ProducerId: Serenity.LookupEditor;
        Name: Serenity.StringEditor;
        Description: Serenity.StringEditor;
        Gallery: Serenity.ImageUploadEditor;
    }
    class MenuItemForm extends Serenity.PrefixedContext {
        static formKey: string;
        private static init;
        constructor(prefix: string);
    }
}
declare namespace AgitaMaisEventos.Menu {
    interface MenuItemRow {
        MenuItemId?: number;
        Name?: string;
        Description?: string;
        Gallery?: string;
        ProducerId?: number;
    }
    namespace MenuItemRow {
        const idProperty = "MenuItemId";
        const nameProperty = "Name";
        const localTextPrefix = "Menu.MenuItem";
        const lookupKey = "Menu.MenuItem";
        function getLookup(): Q.Lookup<MenuItemRow>;
        const enum Fields {
            MenuItemId = "MenuItemId",
            Name = "Name",
            Description = "Description",
            Gallery = "Gallery",
            ProducerId = "ProducerId"
        }
    }
}
declare namespace AgitaMaisEventos.Menu {
    namespace MenuItemService {
        const baseUrl = "Menu/MenuItem";
        function Create(request: Serenity.SaveRequest<MenuItemRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Update(request: Serenity.SaveRequest<MenuItemRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Delete(request: Serenity.DeleteRequest, onSuccess?: (response: Serenity.DeleteResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Retrieve(request: Serenity.RetrieveRequest, onSuccess?: (response: Serenity.RetrieveResponse<MenuItemRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function List(request: Serenity.ListRequest, onSuccess?: (response: Serenity.ListResponse<MenuItemRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        const enum Methods {
            Create = "Menu/MenuItem/Create",
            Update = "Menu/MenuItem/Update",
            Delete = "Menu/MenuItem/Delete",
            Retrieve = "Menu/MenuItem/Retrieve",
            List = "Menu/MenuItem/List"
        }
    }
}
declare namespace AgitaMaisEventos.Modules.Event {
    interface EventListRequest extends Serenity.ListRequest {
        UserLatitude?: number;
        UserLongitude?: number;
    }
}
declare namespace AgitaMaisEventos.Modules.Event {
    interface EventRetrieveRequest extends Serenity.RetrieveRequest {
        UserLatitude?: number;
        UserLongitude?: number;
    }
}
declare namespace AgitaMaisEventos {
    interface ScriptUserDefinition {
        Username?: string;
        DisplayName?: string;
        IsAdmin?: boolean;
        Permissions?: {
            [key: string]: boolean;
        };
    }
}
declare namespace AgitaMaisEventos.SpecialEvent {
}
declare namespace AgitaMaisEventos.SpecialEvent {
    interface MenuCompetitionForm {
        EventId: Serenity.LookupEditor;
        QtdMenuItemAllowedPerPartner: Serenity.IntegerEditor;
        MenuCompetitionItemList: MenuCompetitionItemEditor;
    }
    class MenuCompetitionForm extends Serenity.PrefixedContext {
        static formKey: string;
        private static init;
        constructor(prefix: string);
    }
}
declare namespace AgitaMaisEventos.SpecialEvent {
}
declare namespace AgitaMaisEventos.SpecialEvent {
    interface MenuCompetitionItemForm {
        EventAddressId: Serenity.LookupEditor;
        MenuItemId: Serenity.LookupEditor;
    }
    class MenuCompetitionItemForm extends Serenity.PrefixedContext {
        static formKey: string;
        private static init;
        constructor(prefix: string);
    }
}
declare namespace AgitaMaisEventos.SpecialEvent {
    interface MenuCompetitionItemRow {
        MenuCompetitionItemId?: number;
        MenuCompetitionId?: number;
        EventAddressId?: number;
        MenuItemId?: number;
        MenuCompetitionEventId?: number;
        MenuCompetitionQtdMenuItemAllowedPerPartner?: number;
        EventAddressProducerId?: number;
        EventAddressUserId?: number;
        EventAddressName?: string;
        EventAddressDescription?: string;
        EventAddressHtmlDescription?: string;
        EventAddressZipCode?: string;
        EventAddressAddress?: string;
        EventAddressAddressNum?: string;
        EventAddressAddressAlt?: string;
        EventAddressNeighborhood?: string;
        EventAddressCity?: string;
        EventAddressState?: string;
        EventAddressCountry?: string;
        EventAddressLatitude?: number;
        EventAddressLongitude?: number;
        EventAddressGallery?: string;
        EventAddressBanner?: string;
        EventAddressLogo?: string;
        EventAddressEventAddressCategoryPrimId?: string;
        EventAddressEventAddressCategorySecId?: string;
        EventAddressIsActive?: boolean;
        MenuItemEventAddressId?: number;
        MenuItemName?: string;
        MenuItemDescription?: string;
        MenuItemGallery?: string;
    }
    namespace MenuCompetitionItemRow {
        const idProperty = "MenuCompetitionItemId";
        const localTextPrefix = "SpecialEvent.MenuCompetitionItem";
        const lookupKey = "SpecialEvent.MenuCompetitionItem";
        function getLookup(): Q.Lookup<MenuCompetitionItemRow>;
        const enum Fields {
            MenuCompetitionItemId = "MenuCompetitionItemId",
            MenuCompetitionId = "MenuCompetitionId",
            EventAddressId = "EventAddressId",
            MenuItemId = "MenuItemId",
            MenuCompetitionEventId = "MenuCompetitionEventId",
            MenuCompetitionQtdMenuItemAllowedPerPartner = "MenuCompetitionQtdMenuItemAllowedPerPartner",
            EventAddressProducerId = "EventAddressProducerId",
            EventAddressUserId = "EventAddressUserId",
            EventAddressName = "EventAddressName",
            EventAddressDescription = "EventAddressDescription",
            EventAddressHtmlDescription = "EventAddressHtmlDescription",
            EventAddressZipCode = "EventAddressZipCode",
            EventAddressAddress = "EventAddressAddress",
            EventAddressAddressNum = "EventAddressAddressNum",
            EventAddressAddressAlt = "EventAddressAddressAlt",
            EventAddressNeighborhood = "EventAddressNeighborhood",
            EventAddressCity = "EventAddressCity",
            EventAddressState = "EventAddressState",
            EventAddressCountry = "EventAddressCountry",
            EventAddressLatitude = "EventAddressLatitude",
            EventAddressLongitude = "EventAddressLongitude",
            EventAddressGallery = "EventAddressGallery",
            EventAddressBanner = "EventAddressBanner",
            EventAddressLogo = "EventAddressLogo",
            EventAddressEventAddressCategoryPrimId = "EventAddressEventAddressCategoryPrimId",
            EventAddressEventAddressCategorySecId = "EventAddressEventAddressCategorySecId",
            EventAddressIsActive = "EventAddressIsActive",
            MenuItemEventAddressId = "MenuItemEventAddressId",
            MenuItemName = "MenuItemName",
            MenuItemDescription = "MenuItemDescription",
            MenuItemGallery = "MenuItemGallery"
        }
    }
}
declare namespace AgitaMaisEventos.SpecialEvent {
    namespace MenuCompetitionItemService {
        const baseUrl = "SpecialEvent/MenuCompetitionItem";
        function Create(request: Serenity.SaveRequest<MenuCompetitionItemRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Update(request: Serenity.SaveRequest<MenuCompetitionItemRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Delete(request: Serenity.DeleteRequest, onSuccess?: (response: Serenity.DeleteResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Retrieve(request: Serenity.RetrieveRequest, onSuccess?: (response: Serenity.RetrieveResponse<MenuCompetitionItemRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function List(request: Serenity.ListRequest, onSuccess?: (response: Serenity.ListResponse<MenuCompetitionItemRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        const enum Methods {
            Create = "SpecialEvent/MenuCompetitionItem/Create",
            Update = "SpecialEvent/MenuCompetitionItem/Update",
            Delete = "SpecialEvent/MenuCompetitionItem/Delete",
            Retrieve = "SpecialEvent/MenuCompetitionItem/Retrieve",
            List = "SpecialEvent/MenuCompetitionItem/List"
        }
    }
}
declare namespace AgitaMaisEventos.SpecialEvent {
    interface MenuCompetitionRow {
        MenuCompetitionId?: number;
        EventId?: number;
        QtdMenuItemAllowedPerPartner?: number;
        MenuCompetitionProducerId?: number;
        MenuCompetitionUserId?: number;
        MenuCompetitionName?: string;
        MenuCompetitionLogo?: string;
        MenuCompetitionBanner?: string;
        MenuCompetitionStartDate?: string;
        MenuCompetitionEndDate?: string;
        MenuCompetitionPrice?: number;
        MenuCompetitionCategoryPrimId?: string;
        MenuCompetitionCategorySecId?: string;
        MenuCompetitionIsPrivateEvent?: boolean;
        MenuCompetitionEventAddressId?: number;
        MenuCompetitionGalery?: string;
        MenuCompetitionDescription?: string;
        MenuCompetitionHtmlDescription?: string;
        MenuCompetitionIsActive?: boolean;
        MenuCompetitionItemList?: MenuCompetitionItemRow[];
    }
    namespace MenuCompetitionRow {
        const idProperty = "MenuCompetitionId";
        const localTextPrefix = "SpecialEvent.MenuCompetition";
        const enum Fields {
            MenuCompetitionId = "MenuCompetitionId",
            EventId = "EventId",
            QtdMenuItemAllowedPerPartner = "QtdMenuItemAllowedPerPartner",
            MenuCompetitionProducerId = "MenuCompetitionProducerId",
            MenuCompetitionUserId = "MenuCompetitionUserId",
            MenuCompetitionName = "MenuCompetitionName",
            MenuCompetitionLogo = "MenuCompetitionLogo",
            MenuCompetitionBanner = "MenuCompetitionBanner",
            MenuCompetitionStartDate = "MenuCompetitionStartDate",
            MenuCompetitionEndDate = "MenuCompetitionEndDate",
            MenuCompetitionPrice = "MenuCompetitionPrice",
            MenuCompetitionCategoryPrimId = "MenuCompetitionCategoryPrimId",
            MenuCompetitionCategorySecId = "MenuCompetitionCategorySecId",
            MenuCompetitionIsPrivateEvent = "MenuCompetitionIsPrivateEvent",
            MenuCompetitionEventAddressId = "MenuCompetitionEventAddressId",
            MenuCompetitionGalery = "MenuCompetitionGalery",
            MenuCompetitionDescription = "MenuCompetitionDescription",
            MenuCompetitionHtmlDescription = "MenuCompetitionHtmlDescription",
            MenuCompetitionIsActive = "MenuCompetitionIsActive",
            MenuCompetitionItemList = "MenuCompetitionItemList"
        }
    }
}
declare namespace AgitaMaisEventos.SpecialEvent {
    namespace MenuCompetitionService {
        const baseUrl = "SpecialEvent/MenuCompetition";
        function Create(request: Serenity.SaveRequest<MenuCompetitionRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Update(request: Serenity.SaveRequest<MenuCompetitionRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Delete(request: Serenity.DeleteRequest, onSuccess?: (response: Serenity.DeleteResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Retrieve(request: Serenity.RetrieveRequest, onSuccess?: (response: Serenity.RetrieveResponse<MenuCompetitionRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function List(request: Serenity.ListRequest, onSuccess?: (response: Serenity.ListResponse<MenuCompetitionRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        const enum Methods {
            Create = "SpecialEvent/MenuCompetition/Create",
            Update = "SpecialEvent/MenuCompetition/Update",
            Delete = "SpecialEvent/MenuCompetition/Delete",
            Retrieve = "SpecialEvent/MenuCompetition/Retrieve",
            List = "SpecialEvent/MenuCompetition/List"
        }
    }
}
declare namespace AgitaMaisEventos.Texts {
}
declare namespace _Ext {
    enum AuditActionType {
        Insert = 1,
        Update = 2,
        Delete = 3
    }
}
declare namespace _Ext {
}
declare namespace _Ext {
    interface AuditLogForm {
        EntityTableName: Serenity.StringEditor;
        VersionNo: Serenity.IntegerEditor;
        UserId: Serenity.LookupEditor;
        ActionType: Serenity.EnumEditor;
        ActionDate: Serenity.DateTimeEditor;
        EntityId: Serenity.IntegerEditor;
        OldEntity: Serenity.StringEditor;
        NewEntity: Serenity.StringEditor;
        Differences: StaticTextBlock;
        IpAddress: Serenity.StringEditor;
        SessionId: Serenity.StringEditor;
    }
    class AuditLogForm extends Serenity.PrefixedContext {
        static formKey: string;
        private static init;
        constructor(prefix: string);
    }
}
declare namespace _Ext {
    interface AuditLogRow {
        Id?: number;
        VersionNo?: number;
        UserId?: number;
        ActionType?: AuditActionType;
        ActionDate?: string;
        EntityTableName?: string;
        EntityId?: number;
        OldEntity?: string;
        NewEntity?: string;
        IpAddress?: string;
        SessionId?: string;
    }
    namespace AuditLogRow {
        const idProperty = "Id";
        const nameProperty = "EntityTableName";
        const localTextPrefix = "_Ext.AuditLog";
        const enum Fields {
            Id = "Id",
            VersionNo = "VersionNo",
            UserId = "UserId",
            ActionType = "ActionType",
            ActionDate = "ActionDate",
            EntityTableName = "EntityTableName",
            EntityId = "EntityId",
            OldEntity = "OldEntity",
            NewEntity = "NewEntity",
            IpAddress = "IpAddress",
            SessionId = "SessionId"
        }
    }
}
declare namespace _Ext {
    namespace AuditLogService {
        const baseUrl = "_Ext/AuditLog";
        function Create(request: Serenity.SaveRequest<AuditLogRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Update(request: Serenity.SaveRequest<AuditLogRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Delete(request: Serenity.DeleteRequest, onSuccess?: (response: Serenity.DeleteResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Retrieve(request: Serenity.RetrieveRequest, onSuccess?: (response: Serenity.RetrieveResponse<AuditLogRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function List(request: Serenity.ListRequest, onSuccess?: (response: Serenity.ListResponse<AuditLogRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        const enum Methods {
            Create = "_Ext/AuditLog/Create",
            Update = "_Ext/AuditLog/Update",
            Delete = "_Ext/AuditLog/Delete",
            Retrieve = "_Ext/AuditLog/Retrieve",
            List = "_Ext/AuditLog/List"
        }
    }
}
declare namespace _Ext {
    interface AuditLogViewerRequest extends Serenity.ServiceRequest {
        FormKey?: string;
        EntityId?: number;
    }
}
declare namespace _Ext {
    interface AuditLogViewerResponse extends Serenity.ServiceResponse {
        EntityVersions?: AuditLogRow[];
    }
}
declare namespace _Ext {
    namespace AuditLogViewerService {
        const baseUrl = "AuditLogViewer";
        function List(request: AuditLogViewerRequest, onSuccess?: (response: AuditLogViewerResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        const enum Methods {
            List = "AuditLogViewer/List"
        }
    }
}
declare namespace _Ext.DevTools {
    interface SergenConnection {
        Key?: string;
    }
}
declare namespace _Ext.DevTools {
    interface SergenGenerateOptions {
        Row?: boolean;
        Service?: boolean;
        UI?: boolean;
    }
}
declare namespace _Ext.DevTools {
    interface SergenGenerateRequest extends Serenity.ServiceRequest {
        ConnectionKey?: string;
        Table?: SergenTable;
        GenerateOptions?: SergenGenerateOptions;
    }
}
declare namespace _Ext.DevTools {
    interface SergenListTablesRequest extends Serenity.ServiceRequest {
        ConnectionKey?: string;
    }
}
declare namespace _Ext.DevTools {
    namespace SergenService {
        const baseUrl = "DevTools/Sergen";
        function ListConnections(request: Serenity.ServiceRequest, onSuccess?: (response: Serenity.ListResponse<SergenConnection>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function ListTables(request: SergenListTablesRequest, onSuccess?: (response: Serenity.ListResponse<SergenTable>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        function Generate(request: SergenGenerateRequest, onSuccess?: (response: Serenity.ServiceResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        const enum Methods {
            ListConnections = "DevTools/Sergen/ListConnections",
            ListTables = "DevTools/Sergen/ListTables",
            Generate = "DevTools/Sergen/Generate"
        }
    }
}
declare namespace _Ext.DevTools {
    interface SergenTable {
        Tablename?: string;
        Identifier?: string;
        Module?: string;
        PermissionKey?: string;
    }
}
declare namespace _Ext {
    interface EntityReportRequest extends Serenity.RetrieveRequest {
        ReportKey?: string;
        ReportServiceMethodName?: string;
        ReportDesignPath?: string;
    }
}
declare namespace _Ext {
    interface ListReportRequest extends Serenity.ListRequest {
        ReportKey?: string;
        ReportServiceMethodName?: string;
        ListExcelServiceMethodName?: string;
        ReportDesignPath?: string;
        EqualityFilterWithTextValue?: {
            [key: string]: string;
        };
    }
}
declare namespace _Ext {
    enum Months {
        January = 0,
        February = 1,
        March = 2,
        April = 3,
        May = 4,
        June = 5,
        July = 6,
        August = 7,
        September = 8,
        October = 9,
        November = 10,
        December = 11
    }
}
declare namespace _Ext {
    interface ReplaceRowForm {
        DeletedEntityName: Serenity.StringEditor;
        ReplaceWithEntityId: EmptyLookupEditor;
    }
    class ReplaceRowForm extends Serenity.PrefixedContext {
        static formKey: string;
        private static init;
        constructor(prefix: string);
    }
}
declare namespace _Ext {
    interface ReplaceRowRequest extends Serenity.ServiceRequest {
        FormKey?: string;
        IdProperty?: string;
        NameProperty?: string;
        EntityTypeTitle?: string;
        DeletedEntityId?: number;
        DeletedEntityName?: string;
        ReplaceWithEntityId?: number;
    }
}
declare namespace _Ext {
    interface ReplaceRowResponse extends Serenity.ServiceResponse {
    }
}
declare namespace _Ext {
    namespace ReplaceRowService {
        const baseUrl = "ReplaceRow";
        function Replace(request: ReplaceRowRequest, onSuccess?: (response: ReplaceRowResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        const enum Methods {
            Replace = "ReplaceRow/Replace"
        }
    }
}
declare namespace _Ext.Reports {
    interface CommonReportRequest extends Serenity.ListRequest {
        ColumnKey?: string;
        ReportKey?: string;
        ReportServiceMethodName?: string;
        ListExcelServiceMethodName?: string;
        ReportDesignPath?: string;
        EqualityFilterWithTextValue?: {
            [key: string]: string;
        };
    }
}
declare namespace _Ext.Reports {
    namespace CommonReportService {
        const baseUrl = "Reports/CommonReport";
        const enum Methods {
        }
    }
}
declare namespace _Ext {
    enum TimeUoM {
        Hour = 1,
        Day = 2,
        Week = 3,
        Month = 4,
        CalenderMonth = 5,
        Year = 6
    }
}
declare namespace AgitaMaisEventos.LanguageList {
    function getValue(): string[][];
}
declare namespace AgitaMaisEventos.ScriptInitialization {
}
declare namespace AgitaMaisEventos.Administration {
    class LanguageDialog extends Serenity.EntityDialog<LanguageRow, any> {
        protected getFormKey(): string;
        protected getIdProperty(): string;
        protected getLocalTextPrefix(): string;
        protected getNameProperty(): string;
        protected getService(): string;
        protected form: LanguageForm;
    }
}
declare namespace AgitaMaisEventos.Administration {
    class LanguageGrid extends Serenity.EntityGrid<LanguageRow, any> {
        protected getColumnsKey(): string;
        protected getDialogType(): typeof LanguageDialog;
        protected getIdProperty(): string;
        protected getLocalTextPrefix(): string;
        protected getService(): string;
        constructor(container: JQuery);
        protected getDefaultSortBy(): LanguageRow.Fields[];
    }
}
declare namespace AgitaMaisEventos.Administration {
    class ProducerDialog extends Serenity.EntityDialog<ProducerRow, any> {
        protected getFormKey(): string;
        protected getIdProperty(): string;
        protected getLocalTextPrefix(): string;
        protected getNameProperty(): string;
        protected getService(): string;
        protected form: ProducerForm;
    }
}
declare namespace AgitaMaisEventos.Administration {
    class ProducerGrid extends Serenity.EntityGrid<ProducerRow, any> {
        protected getColumnsKey(): string;
        protected getDialogType(): typeof ProducerDialog;
        protected getIdProperty(): string;
        protected getLocalTextPrefix(): string;
        protected getService(): string;
        constructor(container: JQuery);
    }
}
declare namespace AgitaMaisEventos.Administration {
    class RoleDialog extends Serenity.EntityDialog<RoleRow, any> {
        protected getFormKey(): string;
        protected getIdProperty(): string;
        protected getLocalTextPrefix(): string;
        protected getNameProperty(): string;
        protected getService(): string;
        protected form: RoleForm;
        protected getToolbarButtons(): Serenity.ToolButton[];
        protected updateInterface(): void;
    }
}
declare namespace AgitaMaisEventos.Administration {
    class RoleGrid extends Serenity.EntityGrid<RoleRow, any> {
        protected getColumnsKey(): string;
        protected getDialogType(): typeof RoleDialog;
        protected getIdProperty(): string;
        protected getLocalTextPrefix(): string;
        protected getService(): string;
        constructor(container: JQuery);
        protected getDefaultSortBy(): RoleRow.Fields[];
    }
}
declare namespace AgitaMaisEventos.Administration {
    class RolePermissionDialog extends Serenity.TemplatedDialog<RolePermissionDialogOptions> {
        private permissions;
        constructor(opt: RolePermissionDialogOptions);
        protected getDialogOptions(): JQueryUI.DialogOptions;
        protected getTemplate(): string;
    }
    interface RolePermissionDialogOptions {
        roleID?: number;
        title?: string;
    }
}
declare namespace AgitaMaisEventos.Administration {
    class TranslationGrid extends Serenity.EntityGrid<TranslationItem, any> {
        protected getIdProperty(): string;
        protected getLocalTextPrefix(): string;
        protected getService(): string;
        private hasChanges;
        private searchText;
        private sourceLanguage;
        private targetLanguage;
        private targetLanguageKey;
        constructor(container: JQuery);
        protected onClick(e: JQueryEventObject, row: number, cell: number): any;
        protected getColumns(): Slick.Column[];
        protected createToolbarExtensions(): void;
        protected saveChanges(language: string): PromiseLike<any>;
        protected onViewSubmit(): boolean;
        protected getButtons(): Serenity.ToolButton[];
        protected createQuickSearchInput(): void;
        protected onViewFilter(item: TranslationItem): boolean;
        protected usePager(): boolean;
    }
}
declare namespace AgitaMaisEventos.Administration {
    class UserDialog extends Serenity.EntityDialog<UserRow, any> {
        protected getFormKey(): string;
        protected getIdProperty(): string;
        protected getIsActiveProperty(): string;
        protected getLocalTextPrefix(): string;
        protected getNameProperty(): string;
        protected getService(): string;
        protected form: UserForm;
        constructor();
        protected getToolbarButtons(): Serenity.ToolButton[];
        protected updateInterface(): void;
        protected afterLoadEntity(): void;
        protected getPropertyItems(): Serenity.PropertyItem[];
    }
}
declare namespace AgitaMaisEventos.Administration {
    class UserGrid extends Serenity.EntityGrid<UserRow, any> {
        protected getColumnsKey(): string;
        protected getDialogType(): typeof UserDialog;
        protected getIdProperty(): string;
        protected getIsActiveProperty(): string;
        protected getLocalTextPrefix(): string;
        protected getService(): string;
        constructor(container: JQuery);
        protected getDefaultSortBy(): UserRow.Fields[];
    }
}
declare namespace AgitaMaisEventos.Administration {
    class PermissionCheckEditor extends Serenity.DataGrid<PermissionCheckItem, PermissionCheckEditorOptions> {
        protected getIdProperty(): string;
        private searchText;
        private byParentKey;
        constructor(container: JQuery, opt: PermissionCheckEditorOptions);
        private getItemGrantRevokeClass;
        private roleOrImplicit;
        private getItemEffectiveClass;
        protected getColumns(): Slick.Column[];
        setItems(items: PermissionCheckItem[]): void;
        protected onViewSubmit(): boolean;
        protected onViewFilter(item: PermissionCheckItem): boolean;
        private matchContains;
        private getDescendants;
        protected onClick(e: any, row: any, cell: any): void;
        private getParentKey;
        protected getButtons(): Serenity.ToolButton[];
        protected createToolbarExtensions(): void;
        private getSortedGroupAndPermissionKeys;
        value: UserPermissionRow[];
        private _rolePermissions;
        rolePermissions: string[];
        private _implicitPermissions;
        implicitPermissions: Q.Dictionary<string[]>;
    }
    interface PermissionCheckEditorOptions {
        showRevoke?: boolean;
    }
    interface PermissionCheckItem {
        ParentKey?: string;
        Key?: string;
        Title?: string;
        IsGroup?: boolean;
        GrantRevoke?: boolean;
    }
}
declare namespace AgitaMaisEventos.Administration {
    class UserPermissionDialog extends Serenity.TemplatedDialog<UserPermissionDialogOptions> {
        private permissions;
        constructor(opt: UserPermissionDialogOptions);
        protected getDialogOptions(): JQueryUI.DialogOptions;
        protected getTemplate(): string;
    }
    interface UserPermissionDialogOptions {
        userID?: number;
        username?: string;
    }
}
declare namespace AgitaMaisEventos.Administration {
    class RoleCheckEditor extends Serenity.CheckTreeEditor<Serenity.CheckTreeItem<any>, any> {
        private searchText;
        constructor(div: JQuery);
        protected createToolbarExtensions(): void;
        protected getButtons(): any[];
        protected getTreeItems(): Serenity.CheckTreeItem<any>[];
        protected onViewFilter(item: any): boolean;
    }
}
declare namespace AgitaMaisEventos.Administration {
    class UserRoleDialog extends Serenity.TemplatedDialog<UserRoleDialogOptions> {
        private permissions;
        constructor(opt: UserRoleDialogOptions);
        protected getDialogOptions(): JQueryUI.DialogOptions;
        protected getTemplate(): string;
    }
    interface UserRoleDialogOptions {
        userID: number;
        username: string;
    }
}
declare namespace _Ext {
    class DialogBase<TEntity, TOptions> extends Serenity.EntityDialog<TEntity, TOptions> {
        protected get_ExtDialogOptions(): ExtDialogOptions;
        private loadedState;
        isReadOnly: boolean;
        protected form: any;
        constructor(opt?: any);
        protected updateInterface(): void;
        protected onDialogOpen(): void;
        protected onDialogClose(): void;
        protected setReadOnly(value: boolean): void;
        protected getToolbarButtons(): Serenity.ToolButton[];
        onRefreshClick(): void;
        protected getSaveState(): string;
        protected onSaveSuccess(response: any): void;
        loadResponse(data: any): void;
        maximize(): void;
        fullContentArea(): void;
        setDialogSize(width?: any, height?: any, top?: any, left?: any, $content?: any): void;
        onAfterSetDialogSize(): void;
        onAfterDialogClose(entity: TEntity): void;
        parentGrid: GridBase<TEntity, any>;
    }
}
declare namespace AgitaMaisEventos.Common {
    class AgitaMaisEventosDialog<TItem, TOptions> extends _Ext.DialogBase<TItem, TOptions> {
        allowEdit: boolean;
        hideSave: boolean;
        constructor(allowEdit?: boolean);
        protected getPropertyItems(): Serenity.PropertyItem[];
        protected updateInterface(): void;
        protected setAllowEdit(allowEdit: boolean): void;
        protected getAllowEdit(): boolean;
        protected get_ExtDialogOptions(): ExtDialogOptions;
        /**
         * Modifica as opções do Validator (Verificador de campos do dialog)
         * */
        protected getValidatorOptions(): JQueryValidation.ValidationOptions;
        protected disableInputs(): void;
        protected enableInputs(): void;
    }
}
declare namespace _Ext {
    class EditorDialogBase<TEntity> extends DialogBase<TEntity, any> {
        protected get_ExtDialogOptions(): ExtDialogOptions;
        protected getIdProperty(): string;
        onSave: (options: Serenity.ServiceOptions<Serenity.SaveResponse>, callback: (response: Serenity.SaveResponse) => void) => void;
        onDelete: (options: Serenity.ServiceOptions<Serenity.DeleteResponse>, callback: (response: Serenity.DeleteResponse) => void) => void;
        destroy(): void;
        protected updateInterface(): void;
        protected saveHandler(options: Serenity.ServiceOptions<Serenity.SaveResponse>, callback: (response: Serenity.SaveResponse) => void): void;
        protected deleteHandler(options: Serenity.ServiceOptions<Serenity.DeleteResponse>, callback: (response: Serenity.DeleteResponse) => void): void;
        parentEditor: GridEditorBase<TEntity>;
    }
}
declare namespace AgitaMaisEventos.Common {
    class GridEditorDialog<TEntity> extends _Ext.EditorDialogBase<TEntity> {
        protected getIdProperty(): string;
        onSave: (options: Serenity.ServiceOptions<Serenity.SaveResponse>, callback: (response: Serenity.SaveResponse) => void) => void;
        onDelete: (options: Serenity.ServiceOptions<Serenity.DeleteResponse>, callback: (response: Serenity.DeleteResponse) => void) => void;
        destroy(): void;
        protected updateInterface(): void;
        protected saveHandler(options: Serenity.ServiceOptions<Serenity.SaveResponse>, callback: (response: Serenity.SaveResponse) => void): void;
        protected deleteHandler(options: Serenity.ServiceOptions<Serenity.DeleteResponse>, callback: (response: Serenity.DeleteResponse) => void): void;
    }
}
declare namespace AgitaMaisEventos.Common {
    class AgitaMaisEventosEditorDialog<TEntity> extends GridEditorDialog<TEntity> {
    }
}
declare namespace _Ext {
    class GridBase<TItem, TOptions> extends Serenity.EntityGrid<TItem, TOptions> {
        protected get_ExtGridOptions(): ExtGridOptions;
        isReadOnly: boolean;
        isRequired: boolean;
        isAutosized: boolean;
        isChildGrid: boolean;
        nextRowNumber: number;
        autoColumnSizePlugin: any;
        rowSelection: Serenity.GridRowSelectionMixin;
        pickerDialog: GridItemPickerDialog;
        constructor(container: JQuery, options?: TOptions);
        protected markupReady(): void;
        protected getButtons(): Serenity.ToolButton[];
        protected getReportRequest(): any;
        protected getColumns(): Slick.Column[];
        protected createSlickGrid(): Slick.Grid;
        resetColumns(columns: Slick.Column[]): void;
        resizeAllCulumn(): void;
        protected getSlickOptions(): Slick.GridOptions;
        protected getViewOptions(): Slick.RemoteViewOptions;
        protected onClick(e: JQueryEventObject, row: number, cell: number): void;
        protected onInlineActionClick(target: JQuery, recordId: any, item: TItem): void;
        protected resetRowNumber(): void;
        protected setGrouping(groupInfo: Slick.GroupInfo<TItem>[]): void;
        protected onViewProcessData(response: Serenity.ListResponse<TItem>): Serenity.ListResponse<TItem>;
        initDialog(dialog: DialogBase<TItem, any>): void;
        readonly selectedItems: TItem[];
        selectedKeys: any[];
    }
}
declare namespace AgitaMaisEventos.Common {
    class AgitaMaisEventosGrid<TItem, TOptions> extends _Ext.GridBase<TItem, TOptions> {
        protected getColumnsKey(): any;
        protected getDialogType(): any;
        protected getIdProperty(): any;
        protected getLocalTextPrefix(): any;
        protected getService(): any;
        allowEdit: boolean;
        constructor(container: JQuery, options?: TOptions);
        protected markupReady(): void;
        protected getViewOptions(): Slick.RemoteViewOptions;
        protected getPagerOptions(): Slick.PagerOptions;
        protected getSlickOptions(): Slick.GridOptions;
        protected get_ExtGridOptions(): ExtGridOptions;
        getTitle(): string;
        protected getButtons(): Serenity.ToolButton[];
        protected getColumns(): Slick.Column[];
        protected onClick(e: JQueryEventObject, row: number, cell: number): void;
    }
}
declare namespace _Ext {
    class GridEditorBase<TEntity> extends _Ext.GridBase<TEntity, any> implements Serenity.IGetEditValue, Serenity.ISetEditValue, Serenity.IReadOnly {
        protected get_ExtGridOptions(): ExtGridOptions;
        protected getIdProperty(): string;
        protected nextId: number;
        constructor(container: JQuery);
        private sortGridFunction;
        protected getQuickFilters(): any[];
        protected id(entity: TEntity): any;
        protected save(opt: Serenity.ServiceOptions<any>, callback: (r: Serenity.ServiceResponse) => void): void;
        protected deleteEntity(id: number): boolean;
        protected validateEntity(row: TEntity, id: number): boolean;
        protected getNewEntity(): TEntity;
        protected getButtons(): Serenity.ToolButton[];
        protected addButtonClick(): void;
        protected editItem(entityOrId: any): void;
        getEditValue(property: any, target: any): void;
        setEditValue(source: any, property: any): void;
        value: TEntity[];
        protected getGridCanLoad(): boolean;
        protected usePager(): boolean;
        protected getInitialTitle(): any;
        private searchText;
        protected createToolbarExtensions(): void;
        protected onViewFilter(row: any): boolean;
        private matchContains;
        protected enableFiltering(): boolean;
        protected onViewSubmit(): boolean;
        get_readOnly(): boolean;
        set_readOnly(value: boolean): void;
        protected getSlickOptions(): Slick.GridOptions;
        parentDialog: DialogBase<any, any>;
        onItemsChanged(): void;
        onBeforeGetValue(items: TEntity[]): void;
    }
}
declare namespace AgitaMaisEventos.Common {
    class GridEditorBase<TEntity> extends _Ext.GridEditorBase<TEntity> implements Serenity.IGetEditValue, Serenity.ISetEditValue {
        protected getIdProperty(): string;
        protected nextId: number;
        constructor(container: JQuery);
        protected id(entity: TEntity): any;
        protected getNextId(): string;
        protected setNewId(entity: TEntity): void;
        protected save(opt: Serenity.ServiceOptions<any>, callback: (r: Serenity.ServiceResponse) => void): void;
        protected deleteEntity(id: number): boolean;
        protected validateEntity(row: TEntity, id: number): boolean;
        protected setEntities(items: TEntity[]): void;
        protected getNewEntity(): TEntity;
        protected getButtons(): Serenity.ToolButton[];
        protected editItem(entityOrId: any): void;
        getEditValue(property: any, target: any): void;
        setEditValue(source: any, property: any): void;
        value: TEntity[];
        protected getGridCanLoad(): boolean;
        protected usePager(): boolean;
        protected getInitialTitle(): any;
        protected createQuickSearchInput(): void;
    }
}
declare namespace ImpostoRenda.Common.Base {
    class AgitaMaisEventosEditor<TEntity> extends AgitaMaisEventos.Common.GridEditorBase<TEntity> {
        constructor(container: JQuery);
    }
}
declare namespace AgitaMaisEventos.Common {
    class AgitaMaisEventosGridEditorDialog<TEntity> extends _Ext.GridEditorBase<TEntity> {
    }
}
declare namespace AgitaMaisEventos {
    class BasicProgressDialog extends Serenity.TemplatedDialog<any> {
        constructor();
        cancelled: boolean;
        max: number;
        value: number;
        title: string;
        cancelTitle: string;
        getDialogOptions(): JQueryUI.DialogOptions;
        initDialog(): void;
        getTemplate(): string;
    }
}
declare namespace AgitaMaisEventos.Common {
    class BulkServiceAction {
        protected keys: string[];
        protected queue: string[];
        protected queueIndex: number;
        protected progressDialog: BasicProgressDialog;
        protected pendingRequests: number;
        protected completedRequests: number;
        protected errorByKey: Q.Dictionary<Serenity.ServiceError>;
        private successCount;
        private errorCount;
        done: () => void;
        protected createProgressDialog(): void;
        protected getConfirmationFormat(): string;
        protected getConfirmationMessage(targetCount: any): string;
        protected confirm(targetCount: any, action: any): void;
        protected getNothingToProcessMessage(): string;
        protected nothingToProcess(): void;
        protected getParallelRequests(): number;
        protected getBatchSize(): number;
        protected startParallelExecution(): void;
        protected serviceCallCleanup(): void;
        protected executeForBatch(batch: string[]): void;
        protected executeNextBatch(): void;
        protected getAllHadErrorsFormat(): string;
        protected showAllHadErrors(): void;
        protected getSomeHadErrorsFormat(): string;
        protected showSomeHadErrors(): void;
        protected getAllSuccessFormat(): string;
        protected showAllSuccess(): void;
        protected showResults(): void;
        execute(keys: string[]): void;
        get_successCount(): any;
        set_successCount(value: number): void;
        get_errorCount(): any;
        set_errorCount(value: number): void;
    }
}
declare namespace AgitaMaisEventos.DialogUtils {
    function pendingChangesConfirmation(element: JQuery, hasPendingChanges: () => boolean): void;
}
declare namespace AgitaMaisEventos.Common {
    class EnumSelectFormatter implements Slick.Formatter {
        constructor();
        format(ctx: Slick.FormatterContext): string;
        enumKey: string;
        allowClear: boolean;
        emptyItemText: string;
    }
}
declare namespace AgitaMaisEventos.Common {
    interface ExcelExportOptions {
        grid: Serenity.DataGrid<any, any>;
        service: string;
        onViewSubmit: () => boolean;
        title?: string;
        hint?: string;
        separator?: boolean;
    }
    namespace ExcelExportHelper {
        function createToolButton(options: ExcelExportOptions): Serenity.ToolButton;
    }
}
declare namespace AgitaMaisEventos {
    /**
     * This is an editor widget but it only displays a text, not edits it.
     *
     */
    class StaticTextBlock extends Serenity.Widget<StaticTextBlockOptions> implements Serenity.ISetEditValue {
        private value;
        constructor(container: JQuery, options: StaticTextBlockOptions);
        private updateElementContent;
        /**
         * By implementing ISetEditValue interface, we allow this editor to display its field value.
         * But only do this when our text content is not explicitly set in options
         */
        setEditValue(source: any, property: Serenity.PropertyItem): void;
    }
    interface StaticTextBlockOptions {
        text: string;
        isHtml: boolean;
        isLocalText: boolean;
        hideLabel: boolean;
    }
}
declare namespace AgitaMaisEventos.Common {
    class LanguageSelection extends Serenity.Widget<any> {
        constructor(select: JQuery, currentLanguage: string);
    }
}
declare namespace AgitaMaisEventos.Common {
    class SidebarSearch extends Serenity.Widget<any> {
        private menuUL;
        constructor(input: JQuery, menuUL: JQuery);
        protected updateMatchFlags(text: string): void;
    }
}
declare namespace AgitaMaisEventos.Common {
    class ThemeSelection extends Serenity.Widget<any> {
        constructor(select: JQuery);
        protected getCurrentTheme(): string;
    }
}
declare var jsPDF: any;
declare namespace AgitaMaisEventos.Common {
    interface PdfExportOptions {
        grid: Serenity.DataGrid<any, any>;
        onViewSubmit: () => boolean;
        title?: string;
        hint?: string;
        separator?: boolean;
        reportTitle?: string;
        titleTop?: number;
        titleFontSize?: number;
        fileName?: string;
        pageNumbers?: boolean;
        columnTitles?: {
            [key: string]: string;
        };
        tableOptions?: jsPDF.AutoTableOptions;
        output?: string;
        autoPrint?: boolean;
        printDateTimeHeader?: boolean;
    }
    namespace PdfExportHelper {
        function exportToPdf(options: PdfExportOptions): void;
        function createToolButton(options: PdfExportOptions): Serenity.ToolButton;
    }
}
declare var jsPDF: any;
declare namespace AgitaMaisEventos.Common {
    class ReportDialog extends Serenity.TemplatedDialog<ReportDialogOptions> {
        private report;
        private propertyGrid;
        constructor(options: ReportDialogOptions);
        protected getDialogButtons(): any;
        protected createPropertyGrid(): void;
        protected loadReport(reportKey: string): void;
        protected updateInterface(): void;
        executeReport(target: string, ext: string, download: boolean): void;
        getToolbarButtons(): {
            title: string;
            cssClass: string;
            onClick: () => void;
        }[];
    }
    interface ReportDialogOptions {
        reportKey: string;
    }
}
declare namespace AgitaMaisEventos.Common {
    interface ReportExecuteOptions {
        reportKey: string;
        download?: boolean;
        extension?: 'pdf' | 'htm' | 'html' | 'xlsx' | 'docx';
        getParams?: () => any;
        params?: {
            [key: string]: any;
        };
        target?: string;
    }
    interface ReportButtonOptions extends ReportExecuteOptions {
        title?: string;
        cssClass?: string;
        icon?: string;
    }
    namespace ReportHelper {
        function createToolButton(options: ReportButtonOptions): Serenity.ToolButton;
        function execute(options: ReportExecuteOptions): void;
    }
}
declare var jsPDF: any;
declare namespace AgitaMaisEventos.Common {
    class ReportPage extends Serenity.Widget<any> {
        private reportKey;
        private propertyItems;
        private propertyGrid;
        constructor(element: JQuery);
        protected updateMatchFlags(text: string): void;
        protected categoryClick(e: any): void;
        protected reportLinkClick(e: any): void;
    }
}
declare namespace AgitaMaisEventos.Common {
    class PhoneEditor extends Serenity.StringEditor {
        isCellPhoneNumber: boolean;
        telInput: any;
        constructor(input: JQuery);
        protected formatNumber(): void;
        protected getFormattedValue(): string;
        get_value(): string;
        set_value(value: string): void;
    }
}
declare namespace AgitaMaisEventos.Common {
    class UserPreferenceStorage implements Serenity.SettingStorage {
        getItem(key: string): string;
        setItem(key: string, data: string): void;
    }
}
declare namespace AgitaMaisEventos.Event {
    class EventDialog extends Common.AgitaMaisEventosDialog<EventRow, any> {
        protected getFormKey(): string;
        protected getIdProperty(): string;
        protected getLocalTextPrefix(): string;
        protected getNameProperty(): string;
        protected getService(): string;
        protected form: EventForm;
    }
}
declare namespace AgitaMaisEventos.Event {
    class EventGrid extends Common.AgitaMaisEventosGrid<EventRow, any> {
        protected getColumnsKey(): string;
        protected getDialogType(): typeof EventDialog;
        protected getIdProperty(): string;
        protected getLocalTextPrefix(): string;
        protected getService(): string;
        constructor(container: JQuery);
    }
}
declare namespace AgitaMaisEventos.Event {
    class EventAddressDialog extends Common.AgitaMaisEventosDialog<EventAddressRow, any> {
        protected getFormKey(): string;
        protected getIdProperty(): string;
        protected getLocalTextPrefix(): string;
        protected getNameProperty(): string;
        protected getService(): string;
        protected form: EventAddressForm;
        placeSearch: any;
        autocomplete: any;
        isValidGoogleLocation: boolean;
        componentForm: {
            street_number: Serenity.StringEditor;
            route: Serenity.StringEditor;
            sublocality_level_1: Serenity.StringEditor;
            administrative_area_level_2: Serenity.StringEditor;
            administrative_area_level_1: Serenity.StringEditor;
            country: Serenity.StringEditor;
            postal_code: Serenity.StringEditor;
        };
        protected updateInterface(): void;
        protected initAutocomplete(): void;
        protected fillInAddress(): void;
        protected geolocate(): void;
    }
}
declare namespace AgitaMaisEventos.Event {
    class EventAddressGrid extends Common.AgitaMaisEventosGrid<EventAddressRow, any> {
        protected getColumnsKey(): string;
        protected getDialogType(): typeof EventAddressDialog;
        protected getIdProperty(): string;
        protected getLocalTextPrefix(): string;
        protected getService(): string;
        constructor(container: JQuery);
    }
}
declare namespace AgitaMaisEventos.Event {
    class EventAddressCategoryDialog extends Common.AgitaMaisEventosDialog<EventAddressCategoryRow, any> {
        protected getFormKey(): string;
        protected getIdProperty(): string;
        protected getLocalTextPrefix(): string;
        protected getNameProperty(): string;
        protected getService(): string;
        protected form: EventAddressCategoryForm;
    }
}
declare namespace AgitaMaisEventos.Event {
    class EventAddressCategoryGrid extends Common.AgitaMaisEventosGrid<EventAddressCategoryRow, any> {
        protected getColumnsKey(): string;
        protected getDialogType(): typeof EventAddressCategoryDialog;
        protected getIdProperty(): string;
        protected getLocalTextPrefix(): string;
        protected getService(): string;
        constructor(container: JQuery);
    }
}
declare namespace AgitaMaisEventos.Event {
    class EventCategoryDialog extends Common.AgitaMaisEventosDialog<EventCategoryRow, any> {
        protected getFormKey(): string;
        protected getIdProperty(): string;
        protected getLocalTextPrefix(): string;
        protected getNameProperty(): string;
        protected getService(): string;
        protected form: EventCategoryForm;
    }
}
declare namespace AgitaMaisEventos.Event {
    class EventCategoryGrid extends Common.AgitaMaisEventosGrid<EventCategoryRow, any> {
        protected getColumnsKey(): string;
        protected getDialogType(): typeof EventCategoryDialog;
        protected getIdProperty(): string;
        protected getLocalTextPrefix(): string;
        protected getService(): string;
        constructor(container: JQuery);
    }
}
declare namespace AgitaMaisEventos.Menu {
    class MenuItemDialog extends Serenity.EntityDialog<MenuItemRow, any> {
        protected getFormKey(): string;
        protected getIdProperty(): string;
        protected getLocalTextPrefix(): string;
        protected getNameProperty(): string;
        protected getService(): string;
        protected form: MenuItemForm;
    }
}
declare namespace AgitaMaisEventos.Menu {
    class MenuItemGrid extends Serenity.EntityGrid<MenuItemRow, any> {
        protected getColumnsKey(): string;
        protected getDialogType(): typeof MenuItemDialog;
        protected getIdProperty(): string;
        protected getLocalTextPrefix(): string;
        protected getService(): string;
        constructor(container: JQuery);
    }
}
declare namespace AgitaMaisEventos.SpecialEvent {
    class MenuCompetitionDialog extends Common.AgitaMaisEventosDialog<MenuCompetitionRow, any> {
        protected getFormKey(): string;
        protected getIdProperty(): string;
        protected getLocalTextPrefix(): string;
        protected getService(): string;
        protected form: MenuCompetitionForm;
        constructor();
    }
}
declare namespace AgitaMaisEventos.SpecialEvent {
    class MenuCompetitionGrid extends Serenity.EntityGrid<MenuCompetitionRow, any> {
        protected getColumnsKey(): string;
        protected getDialogType(): typeof MenuCompetitionDialog;
        protected getIdProperty(): string;
        protected getLocalTextPrefix(): string;
        protected getService(): string;
        constructor(container: JQuery);
    }
}
declare namespace AgitaMaisEventos.SpecialEvent {
    class MenuCompetitionItemDialog extends _Ext.EditorDialogBase<MenuCompetitionItemRow> {
        protected getFormKey(): string;
        protected getLocalTextPrefix(): string;
        protected form: MenuCompetitionItemForm;
        constructor();
        protected updateInterface(): void;
        private filterMenuItem;
    }
}
declare namespace AgitaMaisEventos.SpecialEvent {
    class MenuCompetitionItemEditor extends _Ext.GridEditorBase<MenuCompetitionItemRow> {
        protected getColumnsKey(): string;
        protected getDialogType(): typeof MenuCompetitionItemDialog;
        protected getLocalTextPrefix(): string;
        constructor(container: JQuery);
        protected populateWhenVisible(): boolean;
        validateEntity(row: any, id: any): boolean;
    }
}
declare namespace AgitaMaisEventos.SpecialEvent {
    class MenuCompetitionItemGrid extends Serenity.EntityGrid<MenuCompetitionItemRow, any> {
        protected getColumnsKey(): string;
        protected getDialogType(): typeof MenuCompetitionItemDialog;
        protected getIdProperty(): string;
        protected getLocalTextPrefix(): string;
        protected getService(): string;
        constructor(container: JQuery);
    }
}
declare namespace _Ext {
    class AuditLogActionTypeFormatter implements Slick.Formatter {
        static format(ctx: Slick.FormatterContext): string;
        format(ctx: Slick.FormatterContext): string;
    }
}
declare namespace _Ext {
    class AuditLogDialog extends DialogBase<AuditLogRow, any> {
        protected getFormKey(): string;
        protected getIdProperty(): string;
        protected getLocalTextPrefix(): string;
        protected getNameProperty(): string;
        protected getService(): string;
        protected form: AuditLogForm;
        protected afterLoadEntity(): void;
    }
}
declare namespace _Ext {
    class AuditLogGrid extends GridBase<AuditLogRow, any> {
        protected getColumnsKey(): string;
        protected getDialogType(): typeof AuditLogDialog;
        protected getIdProperty(): string;
        protected getLocalTextPrefix(): string;
        protected getService(): string;
        constructor(container: JQuery);
        protected getButtons(): Serenity.ToolButton[];
    }
}
declare var jsondiffpatch: any;
declare namespace _Ext {
    class AuditLogViewer {
        el: string;
        data: {
            entityVersions: any[];
        };
        entity: any;
        entityId: any;
        constructor(el: string, entityVersions: AuditLogRow[]);
        mounted: () => void;
        computed: {
            test: () => string;
        };
        filters: {
            filterByYardId: () => any[];
        };
        methods: {
            showDiff: (versionInfo: AuditLogRow) => void;
            getDiff: (versionInfo: AuditLogRow) => any;
        };
        destroyed(): void;
    }
}
declare namespace _Ext {
    class AuditLogViewerDialog extends Serenity.TemplatedDialog<any> {
        request: AuditLogViewerRequest;
        constructor(request: AuditLogViewerRequest);
        protected getTemplateName(): string;
    }
}
declare namespace _Ext {
    class ReportGridBase<TItem, TOptions> extends _Ext.GridBase<TItem, TOptions> {
        protected getButtons(): Serenity.ToolButton[];
        protected getColumns(): Slick.Column[];
    }
}
declare namespace _Ext {
    class ReportPanelBase<TRequest> extends Serenity.PropertyPanel<TRequest, any> {
        protected getTemplateName(): string;
        protected getReportTitle(): string;
        protected getReportKey(): string;
        protected getReportRequest(): TRequest;
        constructor(container: JQuery);
    }
}
declare namespace _Ext {
    class AutoCompleteEditor extends Serenity.StringEditor {
        constructor(input: JQuery, options: AutoCompleteOptions);
        protected bindAutoComplete(input: any): void;
    }
    interface AutoCompleteOptions {
        lookupKey: string;
        sourceArray: string[];
        sourceCSV: string;
        minSearchLength: number;
    }
}
declare namespace _Ext {
    class ColorEditor extends Serenity.TemplatedWidget<any> implements Serenity.IGetEditValue, Serenity.ISetEditValue {
        protected getTemplate(): string;
        constructor(container: JQuery);
        getEditValue(property: any, target: any): void;
        setEditValue(source: any, property: any): void;
    }
}
declare namespace _Ext {
    class DateTimePickerEditor extends Serenity.Widget<any> implements Serenity.IGetEditValue, Serenity.ISetEditValue, Serenity.IReadOnly {
        getEditValue(property: any, target: any): void;
        setEditValue(source: any, property: any): void;
        constructor(container: JQuery);
        value: string;
        valueAsDate: Date;
        get_readOnly(): boolean;
        set_readOnly(value: boolean): void;
        set_minDate(date: Date): void;
        set_maxDate(date: Date): void;
        set_minDateTime(date: Date): void;
        set_maxDateTime(date: Date): void;
    }
}
declare namespace _Ext {
    class EmptyLookupEditor extends Serenity.Select2Editor<any, any> {
        setSelect2Items(items: Serenity.Select2Item[]): void;
        setLookupItems(lookup: Q.Lookup<any>): void;
    }
}
declare namespace _Ext {
    class HardCodedLookupEditor extends Serenity.Select2Editor<any, any> {
        constructor(container: JQuery, options: HardCodedLookupEditorOptions);
        protected getSelect2Options(): Select2Options;
    }
    interface HardCodedLookupEditorOptions {
        sourceArray: string[];
        sourceCSV: string;
        allowOtherValue: boolean;
    }
}
declare namespace _Ext {
    class HtmlTemplateEditor extends Serenity.HtmlContentEditor {
        constructor(textArea: JQuery, opt?: HtmlTemplateEditorOptions);
        protected getConfig(): Serenity.CKEditorConfig;
    }
    interface HtmlTemplateEditorOptions extends Serenity.HtmlContentEditorOptions {
        cols?: any;
        rows?: any;
        placeholders?: any;
    }
}
declare namespace _Ext {
    class StaticTextBlock extends Serenity.Widget<StaticTextBlockOptions> implements Serenity.ISetEditValue {
        private _value;
        constructor(container: JQuery, options: StaticTextBlockOptions);
        private updateElementContent;
        /**
         * By implementing ISetEditValue interface, we allow this editor to display its field value.
         * But only do this when our text content is not explicitly set in options
         */
        setEditValue(source: any, property: Serenity.PropertyItem): void;
        value: string;
    }
    interface StaticTextBlockOptions {
        text: string;
        isHtml: boolean;
        isLocalText: boolean;
        hideLabel: boolean;
    }
}
declare namespace _Ext {
    class CardViewMixin<TItem> {
        private options;
        private dataGrid;
        private getId;
        private vm;
        private cardContainer;
        viewType: ('list' | 'card');
        constructor(options: CardViewMixinOptions<TItem>);
        switchView(viewType: ('grid' | 'card')): void;
        updateCardItems(): void;
        resizeCardView(): void;
    }
    interface CardViewMixinOptions<TItem> {
        grid: Serenity.DataGrid<TItem, any>;
        containerTemplate?: string;
        itemTemplate?: string;
        methods?: any;
        itemCssClass?: string;
        defaultViewType?: ('list' | 'card');
        itemsCssStyle?: string;
        itemCssStyle?: string;
    }
}
declare namespace _Ext {
    /**
     * A mixin that can be applied to a DataGrid for excel style filtering functionality
     */
    class HeaderFiltersMixin<TItem> {
        private options;
        private dataGrid;
        constructor(options: HeaderFiltersMixinOptions<TItem>);
    }
    interface HeaderFiltersMixinOptions<TItem> {
        grid: Serenity.DataGrid<TItem, any>;
    }
}
declare namespace _Ext {
    /**
     * A mixin that can be applied to a DataGrid for tree functionality
     */
    class TreeGridMixin<TItem> {
        private options;
        private dataGrid;
        private getId;
        constructor(options: TreeGridMixinOptions<TItem>);
        /**
         * Expands / collapses all rows in a grid automatically
         */
        toggleAll(): void;
        expandAll(): void;
        collapsedAll(): void;
        /**
         * Reorders a set of items so that parents comes before their children.
         * This method is required for proper tree ordering, as it is not so easy to perform with SQL.
         * @param items list of items to be ordered
         * @param getId a delegate to get ID of a record (must return same ID with grid identity field)
         * @param getParentId a delegate to get parent ID of a record
         */
        static applyTreeOrdering<TItem>(items: TItem[], getId: (item: TItem) => any, getParentId: (item: TItem) => any): TItem[];
        static filterById<TItem>(item: TItem, view: Slick.RemoteView<TItem>, idProperty: any, getParentId: (x: TItem) => any): boolean;
        static treeToggle<TItem>(getView: () => Slick.RemoteView<TItem>, getId: (x: TItem) => any, formatter: Slick.Format): Slick.Format;
        static toggleClick<TItem>(e: JQueryEventObject, row: number, cell: number, view: Slick.RemoteView<TItem>, getId: (x: TItem) => any): void;
    }
    interface TreeGridMixinOptions<TItem> {
        grid: Serenity.DataGrid<TItem, any>;
        idField?: string;
        getParentId: (item: TItem) => any;
        toggleField: string;
        initialCollapse?: () => boolean;
    }
}
declare namespace _Ext {
    interface ExcelExportOptions {
        grid: Serenity.DataGrid<any, any>;
        service: string;
        onViewSubmit: () => boolean;
        title?: string;
        hint?: string;
        separator?: boolean;
    }
    namespace ExcelExportHelper {
        function createToolButton(options: ExcelExportOptions): Serenity.ToolButton;
    }
}
declare var jsPDF: any;
declare namespace _Ext {
    interface PdfExportOptions {
        grid: Serenity.DataGrid<any, any>;
        onViewSubmit: () => boolean;
        title?: string;
        hint?: string;
        separator?: boolean;
        reportTitle?: string;
        titleTop?: number;
        titleFontSize?: number;
        fileName?: string;
        pageNumbers?: boolean;
        columnTitles?: {
            [key: string]: string;
        };
        tableOptions?: jsPDF.AutoTableOptions;
        output?: string;
        autoPrint?: boolean;
    }
    namespace PdfExportHelper {
        function exportToPdf(options: PdfExportOptions): void;
        function createToolButton(options: PdfExportOptions): Serenity.ToolButton;
    }
}
declare namespace Slick {
    interface RemoteView<TEntity> {
        getGroups(): Slick.Group<TEntity>[];
        getGrouping(): Slick.GroupInfo<TEntity>[];
    }
}
declare namespace _Ext {
    interface ReportExecuteOptions {
        reportKey: string;
        download?: boolean;
        extension?: 'pdf' | 'htm' | 'html' | 'xlsx' | 'docx';
        getParams?: () => any;
        params?: {
            [key: string]: any;
        };
        target?: string;
    }
    interface ReportButtonOptions extends ReportExecuteOptions {
        title?: string;
        cssClass?: string;
        icon?: string;
    }
    namespace ReportHelper {
        function createToolButton(options: ReportButtonOptions): Serenity.ToolButton;
        function execute(options: ReportExecuteOptions): void;
    }
}
declare namespace _Ext.DialogUtils {
    function pendingChangesConfirmation(element: JQuery, hasPendingChanges: () => boolean): void;
}
declare function loadScriptAsync(url: any, callback: any): void;
declare function loadScript(url: any): void;
declare function usingVuejs(): void;
declare function includeBootstrapColorPickerCss(): void;
declare function usingBootstrapColorPicker(): void;
declare function includeJqueryUITimepickerAddonCss(): void;
declare function usingJqueryUITimepickerAddon(): void;
declare function usingBabylonjs(): void;
declare function usingChartjs(): void;
declare function includeCustomMarkerCss(): void;
declare function usingCustomMarker(): void;
declare function includeGoogleMap(callback?: Function, callbackFullName?: string): void;
declare function includeMarkerWithLabel(): void;
declare function includeVisCss(): void;
declare function usingVisjs(): void;
declare function usingJsonDiffPatch(): void;
declare function usingSlickGridEditors(): void;
declare function usingSlickAutoColumnSize(): void;
declare function usingSlickHeaderFilters(): void;
declare namespace q {
    function groupBy(xs: any[], key: any): any;
    function sortBy<T>(xs: T[], key: any): T[];
    function sortByDesc<T>(xs: T[], key: any): T[];
}
declare namespace q {
    function nextTick(date: any): Date;
    function addMinutes(date: Date, minutes: number): Date;
    function addHours(date: Date, hours: number): Date;
    function getHours(fromDate: Date, toDate: Date): number;
    function getDays24HourPulse(fromDate: Date, toDate: Date): number;
    function getDays(pFromDate: Date, pToDate: Date): number;
    function getMonths(fromDate: Date, toDate: Date): number;
    function getCalenderMonths(fromDate: Date, toDate: Date): number;
    function getCalenderMonthsCeil(fromDate: Date, toDate: Date): number;
    function addDays(date: Date, days: number): Date;
    function addMonths(date: Date, months: number): Date;
    function addYear(date: Date, years: number): Date;
    function getPeriods(fromDate: Date, toDate: Date, periodUnit: _Ext.TimeUoM): number;
    function addPeriod(date: Date, period: number, periodUnit: _Ext.TimeUoM): Date;
    function formatISODate(date: Date): string;
    function bindDateTimeEditorChange(editor: any, handler: any): void;
    function initDateRangeEditor(fromDateEditor: Serenity.DateEditor, toDateEditor: Serenity.DateEditor, onChangeHandler?: (e: JQueryEventObject) => void): void;
    function initDateTimeRangeEditor(fromDateTimeEditor: _Ext.DateTimePickerEditor, toDateTimeEditor: _Ext.DateTimePickerEditor, onChangeHandler?: (e: JQueryEventObject) => void): void;
    function formatDate(d: Date | string, format?: string): string;
}
declare namespace q {
    function initDetailEditor(dialog: _Ext.DialogBase<any, any>, editor: _Ext.GridEditorBase<any>, options?: ExtGridEditorOptions): void;
    function setGridEditorHeight(editor: JQuery, heightInPx: number): void;
    function addNotificationIcon(editor: Serenity.StringEditor, isSuccess: boolean): void;
    function setEditorLabel(editor: Serenity.Widget<any>, value: string): void;
    function hideEditorLabel(editor: Serenity.Widget<any>): void;
    function setEditorCategoryLabel(editor: Serenity.Widget<any>, value: string): void;
    function hideEditorCategory(editor: Serenity.Widget<any>, value?: boolean): void;
    function hideCategories(containerElement: JQuery, value?: boolean): void;
    function hideFields(containerElement: JQuery, value?: boolean): void;
    function hideFieldsAndCategories(containerElement: JQuery, value?: boolean): void;
    function hideField(editor: Serenity.Widget<any>, value?: boolean): void;
    function showField(editor: Serenity.Widget<any>, value?: boolean): void;
    function showFieldAndCategory(editor: Serenity.Widget<any>, value?: boolean): void;
    function hideEditorTab(editor: Serenity.Widget<any>, value?: boolean): void;
    function readOnlyEditorTab(editor: Serenity.Widget<any>, value?: boolean): void;
    function readOnlyEditorCategory(editor: Serenity.Widget<any>, value?: boolean): void;
    function readonlyEditorCategory($editor: JQuery, value?: boolean): void;
    function readOnlyEditor(editor: Serenity.Widget<any>, value?: boolean): void;
    function readonlyEditor($editor: JQuery, value?: boolean): void;
    function moveEditorFromTab(editor: Serenity.Widget<any>, toElement: JQuery, isPrepend?: boolean): void;
    function moveEditorCategoryFromTab(editor: Serenity.Widget<any>, toElement: JQuery, isPrepend?: boolean): void;
    function selectEditorTab(editor: Serenity.Widget<any>): void;
    function getSelectedRow<TRow>(e: JQueryEventObject): TRow;
}
declare namespace q {
    function getEnumText(enumKey: any, value: any): string;
    function getEnumValues(enumType: any): string[];
    function getEnumKeys(enumType: any): string[];
}
declare namespace q {
    function isCosmicThemeApplied(): boolean;
    function getSelectedLanguage(): string;
    function formatDecimal(value: any): string;
    function formatInt(value: any): string;
    function ToNumber(value: any): any;
    function ToBool(value: any): boolean;
    function getRandomColor(hexLetters: any): string;
}
declare var isPageRefreshRequired: boolean;
declare namespace q {
    var queryString: {};
    var jsPDFHeaderImageData: string;
    var jsPDFHeaderTitle: string;
    var useSerenityInlineEditors: boolean;
    var DefaultMainGridOptions: ExtGridOptions;
    var DefaultEditorGridOptions: ExtGridOptions;
    var DefaultEntityDialogOptions: ExtDialogOptions;
    var DefaultEditorDialogOptions: ExtDialogOptions;
    var fiscalYearMonths: number[];
}
declare namespace AgitaMaisEventos.Authorization {
    let userDefinition: ScriptUserDefinition;
    function hasPermission(permissionKey: string): boolean;
}
declare namespace AgitaMaisEventos.Membership {
    class ChangePasswordPanel extends Serenity.PropertyPanel<ChangePasswordRequest, any> {
        protected getFormKey(): string;
        private form;
        constructor(container: JQuery);
    }
}
declare namespace AgitaMaisEventos.Membership {
    class ForgotPasswordPanel extends Serenity.PropertyPanel<ForgotPasswordRequest, any> {
        protected getFormKey(): string;
        private form;
        constructor(container: JQuery);
    }
}
declare namespace AgitaMaisEventos.Membership {
    class ResetPasswordPanel extends Serenity.PropertyPanel<ResetPasswordRequest, any> {
        protected getFormKey(): string;
        private form;
        constructor(container: JQuery);
    }
}
declare namespace AgitaMaisEventos.Membership {
    class SignUpPanel extends Serenity.PropertyPanel<SignUpRequest, any> {
        protected getFormKey(): string;
        private form;
        constructor(container: JQuery);
    }
}
declare namespace _Ext {
    class ReplaceRowDialog extends _Ext.DialogBase<any, any> {
        request: ReplaceRowRequest;
        entityList: Array<any>;
        protected getFormKey(): string;
        protected form: ReplaceRowForm;
        constructor(request: ReplaceRowRequest, entityList: Array<any>);
        protected getToolbarButtons(): Serenity.ToolButton[];
    }
}
declare var Vue: any;
declare namespace _Ext.DevTools {
    class SergenPanel extends Serenity.Widget<any> {
        constructor(container: JQuery);
    }
}
declare namespace _Ext {
    class GridItemPickerDialog extends Serenity.TemplatedDialog<GridItemPickerEditorOptions> {
        getTemplate(): string;
        checkGrid: GridBase<any, GridItemPickerEditorOptions>;
        readonly selectedItems: any[];
        constructor(options: GridItemPickerEditorOptions);
        onSuccess: (selectedItems: any) => void;
        getDialogOptions(): JQueryUI.DialogOptions;
    }
}
declare namespace _Ext {
    class GridItemPickerEditor extends Serenity.TemplatedWidget<GridItemPickerEditorOptions> implements Serenity.IGetEditValue, Serenity.ISetEditValue {
        protected getTemplate(): string;
        getEditValue(property: any, target: any): void;
        setEditValue(source: any, property: any): void;
        constructor(container: JQuery, options: GridItemPickerEditorOptions);
        protected: any;
        value: string;
        text: string;
    }
    interface GridItemPickerEditorOptions {
        gridType: any;
        nameFieldInThisRow?: string;
        rowType?: string;
        nameFieldInGridRow?: string;
        multiple: boolean;
        preSelectedKeys?: any[];
    }
}
