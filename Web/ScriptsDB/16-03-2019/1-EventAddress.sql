/*
   sábado, 16 de março de 201915:55:29
   User: Rafael
   Server: DESKTOP-P4PSS5S
   Database: AgitaMaisEventosDB
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.EventAddressCategory SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.EventAddressCategory', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.EventAddressCategory', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.EventAddressCategory', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.Producer SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Producer', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Producer', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Producer', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_EventAddress
	(
	EventAddressId int NOT NULL IDENTITY (1, 1),
	ProducerId int NOT NULL,
	UserId int NOT NULL,
	Name nvarchar(100) NOT NULL,
	Description nvarchar(255) NULL,
	HtmlDescription nvarchar(MAX) NULL,
	ZipCode nvarchar(8) NOT NULL,
	Address nvarchar(100) NOT NULL,
	AddressNum nvarchar(10) NOT NULL,
	AddressAlt nvarchar(100) NULL,
	Neighborhood nvarchar(50) NOT NULL,
	City nvarchar(50) NOT NULL,
	State nvarchar(2) NOT NULL,
	Gallery nvarchar(MAX) NULL,
	Banner nvarchar(255) NULL,
	Logo nvarchar(255) NULL,
	EventAddressCategoryPrimId nvarchar(15) NOT NULL,
	EventAddressCategorySecId nvarchar(15) NULL,
	IsActive bit NOT NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_EventAddress SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Tmp_EventAddress ADD CONSTRAINT
	DF_EventAddress_IsActive DEFAULT 1 FOR IsActive
GO
SET IDENTITY_INSERT dbo.Tmp_EventAddress ON
GO
IF EXISTS(SELECT * FROM dbo.EventAddress)
	 EXEC('INSERT INTO dbo.Tmp_EventAddress (EventAddressId, ProducerId, UserId, Name, ZipCode, Address, AddressNum, AddressAlt, Neighborhood, City, State, Gallery, Banner, Logo, EventAddressCategoryPrimId, EventAddressCategorySecId)
		SELECT EventAddressId, ProducerId, UserId, Name, ZipCode, Address, AddressNum, AddressAlt, Neighborhood, City, State, Gallery, Banner, Logo, EventAddressCategoryPrimId, EventAddressCategorySecId FROM dbo.EventAddress WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_EventAddress OFF
GO
ALTER TABLE dbo.Event
	DROP CONSTRAINT FK_Event_EventAddress
GO
DROP TABLE dbo.EventAddress
GO
EXECUTE sp_rename N'dbo.Tmp_EventAddress', N'EventAddress', 'OBJECT' 
GO
ALTER TABLE dbo.EventAddress ADD CONSTRAINT
	PK_EventAddress PRIMARY KEY CLUSTERED 
	(
	EventAddressId
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.EventAddress ADD CONSTRAINT
	FK_EventAddress_Producer FOREIGN KEY
	(
	ProducerId
	) REFERENCES dbo.Producer
	(
	ProducerId
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.EventAddress ADD CONSTRAINT
	FK_EventAddress_EventAddressCategory FOREIGN KEY
	(
	EventAddressCategoryPrimId
	) REFERENCES dbo.EventAddressCategory
	(
	EventAddressCategoryId
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.EventAddress ADD CONSTRAINT
	FK_EventAddress_EventAddressCategory1 FOREIGN KEY
	(
	EventAddressCategorySecId
	) REFERENCES dbo.EventAddressCategory
	(
	EventAddressCategoryId
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
select Has_Perms_By_Name(N'dbo.EventAddress', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.EventAddress', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.EventAddress', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.Event ADD CONSTRAINT
	FK_Event_EventAddress FOREIGN KEY
	(
	EventAddressId
	) REFERENCES dbo.EventAddress
	(
	EventAddressId
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Event SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Event', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Event', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Event', 'Object', 'CONTROL') as Contr_Per 