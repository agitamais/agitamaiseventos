/*
   quarta-feira, 5 de junho de 201923:53:14
   User: Rafael
   Server: DESKTOP-P4PSS5S\MSSQLSERVER2017
   Database: AgitaMaisEventosDB
   Application: 
*/


Use AgitaMaisEventosDB
go

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.EventAddress ADD
	WhatsappNumber nvarchar(13) NULL,
	PhoneNumber nvarchar(13) NULL
GO
ALTER TABLE dbo.EventAddress SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.EventAddress', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.EventAddress', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.EventAddress', 'Object', 'CONTROL') as Contr_Per 