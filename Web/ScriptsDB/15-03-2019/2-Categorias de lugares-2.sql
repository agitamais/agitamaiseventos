/*
   sexta-feira, 15 de março de 201910:02:48
   User: Rafael
   Server: DESKTOP-P4PSS5S
   Database: AgitaMaisEventosDB
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.EventAddressCategory SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.EventAddressCategory', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.EventAddressCategory', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.EventAddressCategory', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.Users SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Users', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Users', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Users', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.Producer SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Producer', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Producer', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Producer', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.Event
	DROP CONSTRAINT FK_Event_EventAddress
GO
ALTER TABLE dbo.EventAddress ADD
	EventAddressCategoryPrimId nvarchar(15) NULL,
	EventAddressCategorySecId nvarchar(15) NULL
GO
ALTER TABLE dbo.EventAddress ADD CONSTRAINT
	FK_EventAddress_EventAddressCategory FOREIGN KEY
	(
	EventAddressCategoryPrimId
	) REFERENCES dbo.EventAddressCategory
	(
	EventAddressCategoryId
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.EventAddress ADD CONSTRAINT
	FK_EventAddress_EventAddressCategory1 FOREIGN KEY
	(
	EventAddressCategorySecId
	) REFERENCES dbo.EventAddressCategory
	(
	EventAddressCategoryId
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.EventAddress SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.EventAddress', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.EventAddress', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.EventAddress', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_EventCategory
	(
	EventCategoryId nvarchar(15) NOT NULL,
	Description nvarchar(50) NOT NULL,
	Image nvarchar(255) NULL,
	Icon nvarchar(20) NULL,
	IconFamily nvarchar(50) NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_EventCategory SET (LOCK_ESCALATION = TABLE)
GO
IF EXISTS(SELECT * FROM dbo.EventCategory)
	 EXEC('INSERT INTO dbo.Tmp_EventCategory (EventCategoryId, Description, Image, Icon, IconFamily)
		SELECT EventCategoryId, Description, Image, CONVERT(nvarchar(20), Icon), CONVERT(nvarchar(50), IconFamily) FROM dbo.EventCategory WITH (HOLDLOCK TABLOCKX)')
GO
ALTER TABLE dbo.Event
	DROP CONSTRAINT FK_Event_EventCategory
GO
ALTER TABLE dbo.Event
	DROP CONSTRAINT FK_Event_EventCategory1
GO
DROP TABLE dbo.EventCategory
GO
EXECUTE sp_rename N'dbo.Tmp_EventCategory', N'EventCategory', 'OBJECT' 
GO
ALTER TABLE dbo.EventCategory ADD CONSTRAINT
	PK_EventCategory PRIMARY KEY CLUSTERED 
	(
	EventCategoryId
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT
select Has_Perms_By_Name(N'dbo.EventCategory', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.EventCategory', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.EventCategory', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_Event
	(
	EventId int NOT NULL IDENTITY (1, 1),
	ProducerId int NOT NULL,
	UserId int NOT NULL,
	Name nvarchar(50) NOT NULL,
	Logo nvarchar(255) NULL,
	Banner nvarchar(255) NULL,
	StartDate datetime2(7) NOT NULL,
	EndDate datetime2(7) NOT NULL,
	Price decimal(18, 2) NOT NULL,
	CategoryPrimId nvarchar(15) NOT NULL,
	CategorySecId nvarchar(15) NULL,
	IsPrivateEvent bit NOT NULL,
	EventAddressId int NOT NULL,
	Galery nvarchar(MAX) NULL,
	Description nvarchar(255) NOT NULL,
	HtmlDescription nvarchar(MAX) NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_Event SET (LOCK_ESCALATION = TABLE)
GO
SET IDENTITY_INSERT dbo.Tmp_Event ON
GO
IF EXISTS(SELECT * FROM dbo.Event)
	 EXEC('INSERT INTO dbo.Tmp_Event (EventId, ProducerId, UserId, Name, Logo, Banner, StartDate, EndDate, Price, CategoryPrimId, CategorySecId, IsPrivateEvent, EventAddressId, Galery, Description, HtmlDescription)
		SELECT EventId, ProducerId, UserId, Name, Logo, Banner, StartDate, EndDate, Price, CategoryPrimId, CategorySecId, IsPrivateEvent, EventAddressId, Galery, Description, HtmlDescription FROM dbo.Event WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_Event OFF
GO
DROP TABLE dbo.Event
GO
EXECUTE sp_rename N'dbo.Tmp_Event', N'Event', 'OBJECT' 
GO
ALTER TABLE dbo.Event ADD CONSTRAINT
	PK_Event PRIMARY KEY CLUSTERED 
	(
	EventId
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.Event ADD CONSTRAINT
	FK_Event_Producer FOREIGN KEY
	(
	ProducerId
	) REFERENCES dbo.Producer
	(
	ProducerId
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Event ADD CONSTRAINT
	FK_Event_Users FOREIGN KEY
	(
	UserId
	) REFERENCES dbo.Users
	(
	UserId
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Event ADD CONSTRAINT
	FK_Event_EventAddress FOREIGN KEY
	(
	EventAddressId
	) REFERENCES dbo.EventAddress
	(
	EventAddressId
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Event ADD CONSTRAINT
	FK_Event_EventCategory FOREIGN KEY
	(
	CategoryPrimId
	) REFERENCES dbo.EventCategory
	(
	EventCategoryId
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Event ADD CONSTRAINT
	FK_Event_EventCategory1 FOREIGN KEY
	(
	CategorySecId
	) REFERENCES dbo.EventCategory
	(
	EventCategoryId
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Event', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Event', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Event', 'Object', 'CONTROL') as Contr_Per 