/*
   sexta-feira, 15 de março de 201909:49:30
   User: Rafael
   Server: DESKTOP-P4PSS5S
   Database: AgitaMaisEventosDB
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Event
	DROP CONSTRAINT FK_Event_EventCategory
GO
ALTER TABLE dbo.Event
	DROP CONSTRAINT FK_Event_EventCategory1
GO
ALTER TABLE dbo.EventCategory SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.EventCategory', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.EventCategory', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.EventCategory', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.Event
	DROP CONSTRAINT FK_Event_Users
GO
ALTER TABLE dbo.Users SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Users', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Users', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Users', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.Event
	DROP CONSTRAINT FK_Event_Producer
GO
ALTER TABLE dbo.Producer SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Producer', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Producer', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Producer', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.Event
	DROP CONSTRAINT FK_Event_EventAddress
GO
ALTER TABLE dbo.EventAddress ADD
	Banner nvarchar(255) NULL,
	Logo nvarchar(255) NULL
GO
ALTER TABLE dbo.EventAddress SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.EventAddress', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.EventAddress', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.EventAddress', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_Event
	(
	EventId int NOT NULL IDENTITY (1, 1),
	ProducerId int NOT NULL,
	UserId int NOT NULL,
	Name nvarchar(50) NOT NULL,
	Logo nvarchar(255) NULL,
	Banner nvarchar(255) NULL,
	StartDate datetime2(7) NOT NULL,
	EndDate datetime2(7) NOT NULL,
	Price decimal(18, 2) NOT NULL,
	CategoryPrimId nvarchar(6) NOT NULL,
	CategorySecId nvarchar(6) NULL,
	IsPrivateEvent bit NOT NULL,
	EventAddressId int NOT NULL,
	Galery nvarchar(MAX) NULL,
	Description nvarchar(255) NOT NULL,
	HtmlDescription nvarchar(MAX) NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_Event SET (LOCK_ESCALATION = TABLE)
GO
SET IDENTITY_INSERT dbo.Tmp_Event ON
GO
IF EXISTS(SELECT * FROM dbo.Event)
	 EXEC('INSERT INTO dbo.Tmp_Event (EventId, ProducerId, UserId, Name, Logo, Banner, StartDate, EndDate, Price, CategoryPrimId, CategorySecId, IsPrivateEvent, EventAddressId, Galery, Description)
		SELECT EventId, ProducerId, UserId, Name, Logo, Banner, StartDate, EndDate, Price, CategoryPrimId, CategorySecId, IsPrivateEvent, EventAddressId, Galery, CONVERT(nvarchar(255), Description) FROM dbo.Event WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_Event OFF
GO
DROP TABLE dbo.Event
GO
EXECUTE sp_rename N'dbo.Tmp_Event', N'Event', 'OBJECT' 
GO
ALTER TABLE dbo.Event ADD CONSTRAINT
	PK_Event PRIMARY KEY CLUSTERED 
	(
	EventId
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.Event ADD CONSTRAINT
	FK_Event_Producer FOREIGN KEY
	(
	ProducerId
	) REFERENCES dbo.Producer
	(
	ProducerId
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Event ADD CONSTRAINT
	FK_Event_Users FOREIGN KEY
	(
	UserId
	) REFERENCES dbo.Users
	(
	UserId
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Event ADD CONSTRAINT
	FK_Event_EventAddress FOREIGN KEY
	(
	EventAddressId
	) REFERENCES dbo.EventAddress
	(
	EventAddressId
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Event ADD CONSTRAINT
	FK_Event_EventCategory FOREIGN KEY
	(
	CategoryPrimId
	) REFERENCES dbo.EventCategory
	(
	EventCategoryId
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Event ADD CONSTRAINT
	FK_Event_EventCategory1 FOREIGN KEY
	(
	CategorySecId
	) REFERENCES dbo.EventCategory
	(
	EventCategoryId
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Event', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Event', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Event', 'Object', 'CONTROL') as Contr_Per 