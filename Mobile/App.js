import React from 'react'
import { Dimensions } from 'react-native'
import { Root } from "native-base";
import { createDrawerNavigator, createStackNavigator, createAppContainer } from "react-navigation"
//import Loader from 'react-native-mask-loader';
import SplashScreen from 'react-native-splash-screen'
import DrawerContent from '@Component/Menu/Left'

import PublicIntro from '@Screen/Public/Intro'
import PublicHome from '@Screen/Public/Home'
// import PublicProperties from '@Screen/Public/Properties'
// import PublicPropertySearch from '@Screen/Public/PropertySearch'
// import PublicPropertyDetail from '@Screen/Public/PropertyDetail'
// import PublicAgents from '@Screen/Public/Agents'
// import PublicAgentDetail from '@Screen/Public/AgentDetail'
// import PublicAboutUs from '@Screen/Public/AboutUs'
// import PublicContact from '@Screen/Public/Contact'
import TabsPublic from '@Screen/Public/router';
// import PublicFavorites from '@Screen/Public/Favorites'
import PublicEventDetail from '@Screen/Public/EventDetail';
import PublicEventAddressDetail from '@Screen/Public/EventAddressDetail';

import PublicEventList from '@Screen/Public/EventList';
import PublicEventAddressList from '@Screen/Public/EventAddressList';

import PublicExploreFilter from '@Screen/Public/ExploreFilter';
import PublicSpecialEventMenuCompetition from '@Screen/Public/SpecialEvents/MenuCompetition';
//import PublicProfile from '@Screen/Public/Profile';

// import MemberSignUp from '@Screen/Member/SignUp'
// import MemberSignIn from '@Screen/Member/SignIn'
// import MemberHome from '@Screen/Member/Home'
// import MemberProperties from '@Screen/Member/Properties'
// import MemberPropertyAdd from '@Screen/Member/PropertyAdd'
// import MemberPropertyAddLocation from '@Screen/Member/PropertyAdd/Location'
// import MemberPropertyAddAmenities from '@Screen/Member/PropertyAdd/Amenities'
// import MemberPropertyAddPhotos from '@Screen/Member/PropertyAdd/Photos'
// import MemberPropertyAddPublished from '@Screen/Member/PropertyAdd/Published'
// import MemberProfile from '@Screen/Member/Profile'
// import MemberSettings from '@Screen/Member/Settings'
// import MemberFavorites from '@Screen/Member/Favorites'
// import MemberMessages from '@Screen/Member/Messages'
import InitialScreen from '@Screen/InitialScreen';
import PublicLogin from '@Screen/Public/Login';
import PublicCreateProfile from '@Screen/Public/Profile/CreateProfile';
import PublicEditProfile from '@Screen/Public/Profile/EditProfile';
import PublicPasswordEdit from '@Screen/Public/PasswordEdit';
import NavigationService from '@Service/Navigation'

const deviceWidth = Dimensions.get("window").width;

const Drawer = createDrawerNavigator(
  {
    PublicHome: {
      screen: PublicHome,
    },
    // MemberHome: {
    //   screen: MemberHome,
    // },
    TabsPublic: {
      screen: TabsPublic
    },
    // PublicFavorites: {
    //   screen: PublicFavorites
    // }
  },
  {
    contentComponent: DrawerContent,
    contentOptions: {
      activeTintColor: "#e91e63"
    },
    headerMode: "none",
    initialRouteName: "PublicHome",
    drawerWidth: deviceWidth - 50
  }
)

const AppNav = createStackNavigator(
  {
    InitialScreen:{
      screen: InitialScreen
    },
    PublicIntro: {
      screen: PublicIntro
    },
    // PublicProperties: {
    //   screen: PublicProperties
    // },
    // PublicPropertySearch: {
    //   screen: PublicPropertySearch
    // },
    // PublicPropertyDetail: {
    //   screen: PublicPropertyDetail
    // },
    // PublicAgents: {
    //   screen: PublicAgents
    // },
    // PublicAgentDetail: {
    //   screen: PublicAgentDetail
    // },
    // PublicAboutUs: {
    //   screen: PublicAboutUs
    // },
    // PublicContact: {
    //   screen: PublicContact
    // },
    PublicEventDetail: {
      screen: PublicEventDetail
    },
    PublicEventAddressDetail:{
      screen: PublicEventAddressDetail
    },
    PublicEventList:{
      screen:PublicEventList
    },
    PublicEventAddressList:{
      screen:PublicEventAddressList
    },
    PublicExploreFilter:{
      screen:PublicExploreFilter
    },
    PublicSpecialEventMenuCompetition:{
      screen:PublicSpecialEventMenuCompetition
    },
    PublicLogin:{
      screen:PublicLogin
    },
    PublicCreateProfile:{
      screen:PublicCreateProfile
    },
    PublicEditProfile:{
      screen:PublicEditProfile
    },
    PublicPasswordEdit:{
      screen:PublicPasswordEdit
    },
    //
    // MemberSignUp: {
    //   screen: MemberSignUp
    // },
    // MemberSignIn: {
    //   screen: MemberSignIn
    // },
    // MemberMessages: {
    //   screen: MemberMessages
    // },
    // MemberProperties: {
    //   screen: MemberProperties
    // },
    // MemberPropertyAdd: {
    //   screen: MemberPropertyAdd
    // },
    // MemberPropertyAddLocation: {
    //   screen: MemberPropertyAddLocation
    // },
    // MemberPropertyAddAmenities: {
    //   screen: MemberPropertyAddAmenities
    // },
    // MemberPropertyAddPhotos: {
    //   screen: MemberPropertyAddPhotos
    // },
    // MemberPropertyAddPublished: {
    //   screen: MemberPropertyAddPublished
    // },
    // MemberProfile: {
    //   screen: MemberProfile
    // },
    // MemberSettings: {
    //   screen: MemberSettings
    // },
    // MemberFavorites: {
    //   screen: MemberFavorites
    // },
    Drawer: {
      screen: Drawer
    }
  },
  {
    headerMode: "none",
    initialRouteName: 'InitialScreen'
    //"PublicIntro"
  }
)
console.disableYellowBox = true;

const AppContainer = createAppContainer(AppNav);

export default class App extends React.Component {
  state = {
    appReady: false,
    rootKey: Math.random(),
  };

  constructor() {
    super();

  }

  componentDidMount() {
    SplashScreen.hide();
    this.resetAnimation();
  }

  resetAnimation() {
    this.setState({
      appReady: false,
      rootKey: Math.random()
    });

    setTimeout(() => {
      this.setState({
        appReady: true,
      });
    }, 2000);
  }
  render() {
    return (
      <Root>
        {/*<Loader
          isLoaded={this.state.appReady}
          imageSource={require('./assets/images/music_6x.png')}
          backgroundStyle={{ backgroundColor: "#DF3A62" }}
        >
          <AppContainer
            ref={navigatorRef => {
              NavigationService.setTopLevelNavigator(navigatorRef);
            }}
          />
        </Loader>
        */}
        <AppContainer
          ref={navigatorRef => {
            NavigationService.setTopLevelNavigator(navigatorRef);
          }}
        />
      </Root>
    );
  }
}