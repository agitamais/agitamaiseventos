import React from 'react';
import ProgressiveLoadImage from '@Component/ProgressiveLoadImage';
import Useful from '@Service/Useful'


class ServerImageProgressiveLoader extends React.Component {


  render() {

    let {
      thumbnailSource,
      source,
      style,
      styleContainer,
      ...props
    } = this.props;


    let imgUrl = Useful.getImageUrl(source);

    if(source)
      thumbnailSource = Useful.getThumbUrl(source);
    

    return (
      <ProgressiveLoadImage
         thumbnailSource={{ uri: (thumbnailSource) }}
         source={{ uri: (imgUrl) }} 
       style={[style]}
       styleContainer={[styleContainer]}/>               
      
    );
  }
}

export default ServerImageProgressiveLoader;