import React from 'react';
import { Icon, Text, View} from 'native-base'
import {TouchableOpacity, FlatList} from 'react-native'
import ServerImageProgressiveLoader from '@Component/ServerImageProgressiveLoader';
import Useful from '@Service/Useful';
import Styles from './Style'
import EventService from '@Service/EventService';


let EventServiceInstance = new EventService();

class SimilarEvents extends React.Component {

  constructor(props){
    super(props)
    this.state = {
      similarEvents : []
    }

    let event = this.props.event;
   
    EventServiceInstance.listRelatedEvents(event)
        .then((data) => {
            this.setState({
                similarEvents: data.Entities
            })
    })
  }
  render() {

    return (
      <View style={Styles.section}>
                    <View style={Styles.headerBg}>

                    {this.state.similarEvents.length ? <Text style={Styles.sHeader}>{'Eventos similares'.toUpperCase()}</Text> : null}
                   
                    </View>
                    <FlatList
                        data={this.state.similarEvents}
                        horizontal
                        showsHorizontalScrollIndicator={false}
                        renderItem={({ item }) => (
                            <TouchableOpacity style={Styles.item} underlayColor='transparent' onPress={() => EventServiceInstance.openEventById(item.EventId)}>
                                <View>
                                    <View>
                                        <ServerImageProgressiveLoader source={item.Banner} style={Styles.itemImg}/>  
                                        <View style={Styles.itemNoCrv}></View>
                                        <Icon name="heart" type="MaterialCommunityIcons" style={[Styles.itemFavorite, { display: 'none' }]} />
                                    </View>
                                    <Text style={Styles.itemPriceSm}>{Useful.formatDateAsString(item.StartDate)}</Text>
                                    <Text style={Styles.itemLocation}>{item.Name}</Text>

                                </View>
                            </TouchableOpacity>
                        )}
                    />
                </View>             
      
    );
  }
}

export default SimilarEvents;