import {
  BaseService
} from "./BaseService";

export  default class EventCategoryService extends BaseService {  

  api_url = this.servicesUrl + "Event/EventCategory/";

}