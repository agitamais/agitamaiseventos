import Axios from "axios";
import Config from "@App/Config";

export class BaseService {  
    baseRequest = {
        method: "", //POST, GET, PUT ETC
        headers: {
            "accept": "application/json, text/javascript, */*; q=0.01",
            "content-type": "application/json",
        },
        url: "",
        data: {}
    };
    static axioInstance;    
    //baseUrl = "http://192.168.15.20:62818/";
    baseUrl = Config.BASE_URL;
    servicesUrl = this.baseUrl + "Services/"

    constructor(){
        if(!this.axionsInstance)
            this.axioInstance = Axios.create();
    }

    async list(listRequest) {
        return await this.post(this.api_url + 'List/', listRequest)
    };
    async retrieve(id) {
        return await this.post(this.api_url + 'Retrieve/',{EntityId: id} )
    };
    
    /**
    * ShortHand for post requests
    * @param {string} url 
    * @param {Object} data post data
    * @param {Object} axiosConfig Optional - custom object to axios request configuration
    * @returns {Promise<Response>} promise with json data
    */
    async post(url, data, axiosConfig){
        return this.doRequest("POST",url,data, axiosConfig);
    }

    /**
    * ShortHand for GET requests
    * @param {string} url 
    * @returns {Promise<Response>} promise with json data
    */
    async get(url) {
        return await this.doRequest("GET", url, null)
    }
    async doRequest(method, url, data, axiosConfig) {
        var request = this.baseRequest;
        request.method = method || request.method;
        request.data = data;
        request.url = url;
        return this.axioInstance.request(request).then((response) => response.data);
    }
       

 };