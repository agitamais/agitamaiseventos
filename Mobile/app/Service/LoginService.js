import {
  BaseService
} from "./BaseService";

export default class LoginService extends BaseService {  
  
  async publicLogin() {
    return await this.login({
      Username: "publicLogin",
      Password: "pb_7855@13"
    })
  }
  
  async login(loginRequest) {
    
    var loginResponse = {
      isAuthenticated : false,
      message : "",
    }   
    return await this.post(this.baseUrl + 'Account/Login/', loginRequest)
      .then((response) => {
        loginResponse.isAuthenticated = true;
      })
      .catch((error) => {
        loginResponse.isAuthenticated = false;
        loginResponse.message = error.message;
      })
      .finally((response) => {
        return loginResponse;
      })
  };
}
