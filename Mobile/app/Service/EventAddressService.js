import {
  BaseService
} from "./BaseService";
import {AsyncStorage} from "react-native";
import Serenity from '@Service/Serenity'
import moment from 'moment'
import NavigationService from '@Service/Navigation'

export  default class EventAddressService extends BaseService {  
  
  api_url = this.servicesUrl + "Event/EventAddress/"
   

  static callback = () => {};

  static setCallbackAfterInsertRecentlySeen(fn){
      if(fn)
        callback = fn;
  }
  static execCallbackAfterInsertRecentlySeen(){
        callback();
  }

  async list(listRequest) {
    return await this.post(this.api_url + 'ListWT/', listRequest)
  };
  async retrieve(id) {
    return await this.post(this.api_url + 'RetrieveWT/', {EntityId: id})
  };
  
  async countAll() {
    return await this.get(this.api_url + 'CountAllEventAddress/')
                 .then((data) => data.TotalCount)
    
  };

  openEventAddressById(eventAddressId,params){
    return this.retrieve(eventAddressId)
    .then((data) => {
        let eventAddress = data.Entity;
        this.openEventAddress(eventAddress,params)
    })
  }
  openEventAddress(eventAddress,params){
      var parameters = params || {};
      parameters.detail = eventAddress
      NavigationService.navigate('PublicEventAddressDetail', parameters, null, 'event'+ eventAddress.EventAddressId);
  }

  async setRecentlySeenEventAddress(eventAddress){
    
    return  await AsyncStorage.getItem('RECENTLY_SEEN_EVENTS_ADDRESS')
     .then(eventListStr => {
      let eventAddressList = [];
      if (eventListStr){
          let eventMaxLength = 15
          eventAddressList = JSON.parse(eventListStr)
          if(eventAddressList.length > eventMaxLength){
            eventAddressList.splice(eventMaxLength,eventAddressList.length - eventMaxLength)
          }
          eventAddressList = eventAddressList.filter( 
                                x => x.EventAddressId != eventAddress.EventAddressId)
          eventAddressList.unshift(eventAddress);
      }                        
      else{
        eventAddressList = [eventAddress]      
      }
        let obj = JSON.stringify(eventAddressList);
        AsyncStorage.setItem('RECENTLY_SEEN_EVENTS_ADDRESS', obj);
        return eventAddress;
      });

  }

  async getRecentlySeenEventAddresses(){
    return await AsyncStorage.getItem('RECENTLY_SEEN_EVENTS_ADDRESS')
    .then(eventListStr => {
      let eventAddressList = [];
      if (eventListStr) {
        eventAddressList = JSON.parse(eventListStr);
      }
      return eventAddressList;
    })  
  }

  
}