import {
  BaseService
} from "./BaseService";

export  default class EventAddressCategoryService extends BaseService {  
  
  api_url = this.servicesUrl + "Event/EventAddressCategory/";
  
}