import React from 'react'
import { StatusBar, TouchableOpacity, Image, ImageBackground, FlatList, Linking, Platform } from 'react-native'
import { Container, Header, Content, Button, Icon, Text, Left, Right, Body, View } from 'native-base'
import Style from '@Theme/Style'
import Styles from '@Screen/Public/EventDetail/Style'
import EventService from '@Service/EventService';
import Useful from '@Service/Useful';
import SimilarEvents from '@Component/SimilarEvents'
import MapView, { Marker/*, PROVIDER_GOOGLE*/ } from 'react-native-maps';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';
import call from 'react-native-phone-call'

let EventServiceInstance = new EventService();
let _event = {};
let _specialEventsRoutes = [
    {
        specialEventTypeId: 'menu_competition',
        route: 'PublicSpecialEventMenuCompetition',
    }
]

export default class extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            similarEvents: [],
            isSpecialEvent: false
        }
        _event = this.props.navigation.getParam('detail', {});

        EventServiceInstance.setRecentlySeenEvent(_event)
            .then(x => {
                EventService.execCallbackAfterInsertRecentlySeen();
            })

        if (_event.SpecialEventTypeId) {
            let specialEventRoute = _specialEventsRoutes.find(x => x.specialEventTypeId === _event.SpecialEventTypeId);
            if (specialEventRoute) {
                this.setState({ isSpecialEvent: true });
                this.props.navigation.replace(specialEventRoute.route, { 'event': _event });
                return false;
            }

        }




        // let request = {
        //     Take: 5,
        //     Criteria: {}
        // }
        // // request.Criteria = Serenity.Criteria.and(
        // //     [['EventId'], '!=', _event.EventId],
        // //     [['StartDate'], '>=', moment().format('YYYY-MM-DD')]);

        //  request.Criteria = Serenity.Criteria.and(
        //      [['EventId'], '!=', _event.EventId],
        //      [['EndDate'], '>', moment().format('YYYY-MM-DD')]);

        // var categoryCriteria = Serenity.Criteria.or(
        //     [['CategoryPrimId'], 'in', [[_event.CategoryPrimId, _event.CategorySecId]]],
        //     [['CategorySecId'], 'in', [[_event.CategoryPrimId, _event.CategorySecId]]]
        // );

        // request.Criteria = Serenity.Criteria.and(request.Criteria, categoryCriteria)
        // EventServiceInstance.list(request)
        //     .then((data) => {
        //         this.setState({
        //             similarEvents: data.Entities
        //         })

        //     })
    }
    // onLearnMoreEvent(item) {
    //     EventServiceInstance.retrieve(item.EventId)
    //         .then((data) => {
    //             let itemSelected = data.Entity;
    //             this.props.navigation.push('PublicEventDetail', { 'detail': itemSelected });
    //         })
    // }

    onOpenWhatsApp(number) {

        if (!number) {
            alert('O whatsApp ainda não foi informado');
            return;
        }

        let link = 'https://api.whatsapp.com/send?1=pt_BR&phone=' + number;

        if (link) {
            Linking.canOpenURL(link)
                .then(supported => {
                    if (!supported) {
                        Alert.alert(
                            'Por favor, instale o whatsApp caso queira enviar mensagens'
                        );
                    } else {
                        return Linking.openURL(link);
                    }
                })
                .catch(err => console.error('An error occurred', err));
        } else {
            console.log('sendWhatsAppMessage -----> ', 'message link is undefined');
        }
    }

    onOpenPhoneCall = (number) => {

        if (!number) {
            alert('O número de telefone ainda não foi informado');
            return;
        }

        if (number && number.substring(0, 2))
            number = number.substring(2, number.length);
            
        const args = {
            number: number, // String value with the number to call
            prompt: true // Optional boolean property. Determines if the user should be prompt prior to the call 
        }
        call(args).catch(console.error)
    }

    /*onOpenPhoneCall(phone) {
        let phoneNumber = phone;
        if (Platform.OS !== 'android') {
            phoneNumber = `telprompt:${phone}`;
        }
        else {
            phoneNumber = `tel:${phone}`;
        }

        Linking.canOpenURL(phoneNumber)
            .then(supported => {
        alert(supported);

                if (!supported) {
                    Alert.alert('Phone number is not available');
                } else {
                    return Linking.openURL(phoneNumber);
                }
            })
            .catch(err => console.log(err));
    }*/

    render() {

        return this.state.isSpecialEvent === true ? <View></View> :
            <Container style={Style.bgMain}>
                <Header style={[Style.navigation]} hasTabs={this.props.hasTabs}>
                    <StatusBar backgroundColor="#DF3A62" animated barStyle="light-content" />
                    <Left style={{ flex: 1 }}>
                        <Button transparent onPress={() => this.props.navigation.goBack()}>
                            <Icon active name='arrow-left' style={Style.textWhite} type="MaterialCommunityIcons" />
                        </Button>
                    </Left>
                    <Body style={{ flex: 3 }}>
                        <Text style={[Style.actionBarText]}>{_event.Name.toUpperCase()}</Text>
                    </Body>
                    <Right style={{ flex: 1 }}>
                    </Right>
                </Header>
                <Content style={Style.layoutInner} contentContainerStyle={Style.layoutContent}>



                    <ImageBackground source={{ uri: (Useful.getImageUrl(_event.Banner)) }} imageStyle={'cover'} style={Styles.coverImg}>

                    </ImageBackground>
                    <View style={Styles.section}>
                        <Text style={Styles.price}>{_event.EventAddressName}</Text>
                        <View style={Styles.locationTop}>
                            <Icon active name='map-marker-radius' style={Styles.locationTopIcon} type="MaterialCommunityIcons" />

                            <Text style={Styles.locationTopInfo}>{_event.EventAddressCity} - {_event.EventAddressState}</Text>
                        </View>
                        <View style={Styles.locationTop}>
                            <Icon active name='currency-usd' style={Styles.locationTopIcon} type="MaterialCommunityIcons" />
                            <Text style={Styles.locationTopInfo}>{_event.Price}</Text>
                        </View>
                        <View style={Styles.locationTop}>
                            <Icon active name='calendar-clock' style={Styles.locationTopIcon} type="MaterialCommunityIcons" />
                            <Text style={Styles.locationTopInfo}>{Useful.formatDateAsString(_event.StartDate)} - {Useful.formatDateAsString(_event.EndDate)}</Text>
                        </View>

                    </View>

                    <ImageBackground source={require('@Asset/images/shadow.png')} imageStyle={'cover'} style={Styles.shadow} />

                    <View style={Styles.overview}>
                        <Text style={Styles.overviewTitle}>Descrição do Evento</Text>
                        <Text style={Styles.overviewDesc}>
                            {_event.Description}
                        </Text>
                    </View>

                    <View style={Styles.overview}>
                        <Text style={Styles.overviewTitle}>Local</Text>
                        <Icon active name='map-marker-radius' style={Styles.locationTopIcon} type="MaterialCommunityIcons" />
                        <Text style={Styles.overviewDesc}>
                            {_event.EventAddressAddress}, {_event.EventAddressAddressNum}, {_event.EventAddressNeighborhood}
                        </Text>
                        <Text style={Styles.overviewDesc}>
                            {_event.EventAddressCity}, {_event.EventAddressState}
                        </Text>

                    </View>

                    <View style={Styles.location}>
                        <MapView
                            initialRegion={{
                                latitude: _event.EventAddressLatitude,
                                longitude: _event.EventAddressLongitude,
                                latitudeDelta: 0.0922,
                                longitudeDelta: 0.0421
                            }}
                            minZoomLevel={2}
                            //mapType={"hybrid"}
                            /*provider={PROVIDER_GOOGLE}*/
                            style={[{ position: "absolute", top: 0, left: 0, bottom: 0, right: 0 }]}>
                            <Marker
                                coordinate={{
                                    latitude: _event.EventAddressLatitude,
                                    longitude: _event.EventAddressLongitude
                                }}
                                description={_event.EventAddressAddress + ", " + _event.EventAddressAddressNum + ", " + _event.EventAddressNeighborhood + ", " + _event.EventAddressCity + " - " + _event.EventAddressState}
                                title={_event.ProducerName}
                                image={require('@Asset/images/icon-map.png')}
                            />
                        </MapView>

                    </View>

                    <View style={Styles.owner}>
                        <Text style={Styles.ownerTitle}>Sobre o produtor</Text>
                        <View style={Styles.ownerAvatar}>
                            <Image source={{ uri: (Useful.getImageUrl(_event.Logo)) }} style={Styles.ownerAvatarImg} />
                        </View>
                        <View style={Styles.ownerInfo}>
                            <View>
                                <Text style={Styles.ownerName}>{_event.ProducerName}</Text>
                                <Text style={Styles.ownerLocation}>{_event.ProducerCity} - {_event.ProducerState}</Text>
                            </View>
                        </View>
                    </View>

                    <View style={Styles.containerContact}>
                        <View style={{ flex: 1 }}>
                            <Text style={Styles.containerContactTitle}>{'contato'.toUpperCase()}</Text>
                        </View>

                        <View style={{ flex: 1, flexDirection: 'row' }}>
                            <View style={{ paddingTop: 20 }}>
                                <TouchableOpacity onPress={() => this.onOpenWhatsApp(_event.EventAddressWhatsappNumber)} style={[Styles.buttonContact, { backgroundColor: "#25D366" }]}>
                                    <IconFontAwesome name={"whatsapp"} style={Styles.buttonContactIcon}>
                                    </IconFontAwesome>
                                </TouchableOpacity>
                            </View>
                            <View style={{ paddingTop: 20, paddingLeft: 20 }}>
                                <TouchableOpacity onPress={() => this.onOpenPhoneCall(_event.EventAddressPhoneNumber)} style={[Styles.buttonContact, { backgroundColor: "#39b6ec" }]}>
                                    <IconFontAwesome name={"phone"} style={Styles.buttonContactIcon}>
                                    </IconFontAwesome>
                                </TouchableOpacity>
                            </View>
                        </View>

                    </View>

                    <SimilarEvents event={_event} />
                    {/* <Similar/> */}
                    {/* <View style={Styles.section}>
                    <View style={Styles.headerBg}>

                        <Text style={Styles.sHeader}>{'Eventos similares'.toUpperCase()}</Text>

                    </View>
                    <FlatList
                        data={this.state.similarEvents}
                        horizontal
                        showsHorizontalScrollIndicator={false}
                        style={Styles.flatList}
                        renderItem={({ item }) => (
                            <TouchableOpacity style={Styles.item} underlayColor='transparent' onPress={() => this.onLearnMoreEvent(item)}>
                                <View>
                                    <View>
                                        <Image source={{ uri: Config.IMAGE_BASE_URL + item.Banner }} style={[Styles.itemImg]} />
                                        <View style={Styles.itemNoCrv}></View>
                                        <Icon name="heart" type="MaterialCommunityIcons" style={[Styles.itemFavorite, { display: 'none' }]} />
                                    </View>
                                    <Text style={Styles.itemPriceSm}>{moment(item.StartDate).format('DD/MM')}-{moment(item.StartDate).format('hh')}h{moment(item.StartDate).format('mm')}min</Text>
                                    <Text style={Styles.itemLocation}>{item.Name}</Text>

                                </View>
                            </TouchableOpacity>
                        )}
                    />
                </View> */}
                </Content>
            </Container>

    }
}