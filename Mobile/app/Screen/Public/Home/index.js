import React from 'react'
import { StatusBar, TouchableOpacity, TextInput, Image, ImageBackground, FlatList } from 'react-native'
import { Container, Content, Button, Text, Right, View } from 'native-base'
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/FontAwesome';

import NavigationService from '@Service/Navigation'

import Style from '@Theme/Style'
import Styles from '@Screen/Public/Home/Style'
import EventService from '@Service/EventService';
import EventCategoryService from '@Service/EventCategoryService';
import EventAddressService from '@Service/EventAddressService';
import EventAddressCategoryService from '@Service/EventAddressCategoryService';
import UserPreferencesService from '@Service/UserPreferencesService';
import Config from '../../../Config';
import Serenity from '@Service/Serenity';
import moment from 'moment';
import Useful from '@Service/Useful';
import ServerImageProgressiveLoader from '@Component/ServerImageProgressiveLoader';
import CustomIcon from '@Component/CustomIcon'

export const btnType = [{
    label: 'BUY',
    value: 'btn_buy'
}, {
    label: 'RENT',
    value: 'btn_rent'
}, {
    label: 'PROJECTS',
    value: 'btn_project'
}];

let EventServiceInstance = new EventService();

let EventCategoryServiceInstance = new EventCategoryService();

let EventAddressServiceInstance = new EventAddressService();

let EventAddressCategoryServiceInstance = new EventAddressCategoryService();

export default class extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isSearchByEvent: true,
            isModalVisible: false,
            searchText: '',
            events: [],
            eventCount: 0,
            eventAddressCount: 0
        }
        var listEventRequest = {
            Take: 5,
            Criteria: Serenity.Criteria.or([['EndDate'], '>', moment().format('YYYY-MM-DD')], [['SpecialEventTypeId'], '!=', 'NULL']),
            Sort: ["SpecialEventTypeId DESC"]
        }

        EventServiceInstance.list(listEventRequest)
            .then((data) => {
                this.setState({
                    events: data.Entities
                })
            })
        EventCategoryServiceInstance.list()
            .then((data) => {
                this.setState({
                    categories: data.Entities
                })
            })

        EventServiceInstance.countFutureEvents()
            .then((count) => {
                this.setState({
                    eventCount: count
                })

            })

        EventAddressServiceInstance.list({
            Take: 5
        })
            .then((data) => {
                this.setState({
                    eventsAddress: data.Entities
                })
            })
        EventAddressCategoryServiceInstance.list()
            .then((data) => {
                this.setState({
                    eventsAddressCategories: data.Entities
                })
            })
        EventAddressServiceInstance.countAll()
            .then((count) => {
                this.setState({
                    eventAddressCount: count
                })
            })

        this.loadRecentlySeenEvents();
        this.loadRecentlySeenEventAddresses();
        EventService.setCallbackAfterInsertRecentlySeen(x => this.loadRecentlySeenEvents())
        EventAddressService.setCallbackAfterInsertRecentlySeen(x => this.loadRecentlySeenEventAddresses())

    }
    componentDidMount() {
        this.setState({
            isSearchByEvent: UserPreferencesService.get().searchByEvent
        })

        UserPreferencesService.onChange(userPreferences => {
            this.setState({
                isSearchByEvent: userPreferences.searchByEvent
            })
        })
    }

    loadRecentlySeenEvents() {
        EventServiceInstance.getRecentlySeenEvents()
            .then(events => {
                this.setState({
                    recentlySeenEvents: events
                })
            })
    }

    loadRecentlySeenEventAddresses() {
        EventAddressServiceInstance.getRecentlySeenEventAddresses()
            .then(adresses => {
                this.setState({
                    recentlySeenEventAddress: adresses
                })
            })
    }

    onLearnMoreEvent = (item) => {
        EventServiceInstance.openEventById(item.EventId)
    };
    onLearnMoreEventAddress = (item) => {
        EventAddressServiceInstance.retrieve(item.EventAddressId)
            .then((data) => {
                let itemSelected = data.Entity
                this.props.navigation.navigate('PublicEventAddressDetail', { 'detail': itemSelected })
            })
    };

    onChangeTypeSearch(type) {
        this.setState({
            isSearchByEvent: type
        });

        UserPreferencesService.set({
            searchByEvent: type
        })
    }
    //typeFilterValue
    seeAllEvents(searchTxt) {

        var listRequest = {
            Criteria: []
        };
        if (searchTxt) {
            listRequest.ContainsText = searchTxt;
        }

        // listRequest.Criteria = Serenity.Criteria.and(listRequest.Criteria,
        //  [['StartDate'], '>=', moment().format('YYYY-MM-DD')]);
        listRequest.Criteria = Serenity.Criteria.and(listRequest.Criteria,
            [['EndDate'], '>', moment().format('YYYY-MM-DD')]);
        EventServiceInstance.list(listRequest)
            .then((data) => {
               NavigationService.navigate('PublicEventList', { events: data.Entities });
            })
            
            
    }


    seeAllEventsAddress(searchTxt) {
        var listRequest = {
        };
        if (searchTxt) {
            listRequest.ContainsText = searchTxt;
        };
        EventAddressServiceInstance.list(listRequest)
            .then((data) => {
                NavigationService.navigate('PublicEventAddressList', { eventAddresses: data.Entities });
            })
    }

    onSelectEventCategory(selectedCategory) {

        var request = {};
        //    request.Criteria = Serenity.Criteria.and(request.Criteria,
        //     [['StartDate'], '>=', moment().format('YYYY-MM-DD')]);
        request.Criteria = Serenity.Criteria.and(request.Criteria,
            [['EndDate'], '>', moment().format('YYYY-MM-DD')]);

        var categoryCriteria = Serenity.Criteria.or(
            [['CategoryPrimId'], '=', selectedCategory.EventCategoryId],
            [['CategorySecId'], '=', selectedCategory.EventCategoryId]
        )
        request.Criteria = Serenity.Criteria.and(request.Criteria, categoryCriteria)

        EventServiceInstance.list(request)
            .then((data) => {
                NavigationService.navigate('PublicEventList', { events: data.Entities });
            })
    }

    onSelectEventAddressCategory(selectedCategory) {

        var request = {};
        request.Criteria = Serenity.Criteria.or(
            [['EventAddressCategoryPrimId'], '=', selectedCategory.EventAddressCategoryId],
            [['EventAddressCategorySecId'], '=', selectedCategory.EventAddressCategoryId]
        )

        EventAddressServiceInstance.list(request)
            .then((data) => {
                NavigationService.navigate('PublicEventAddressList', { eventAddresses: data.Entities });
            })
    }

    _toggleModal = () =>
        this.setState({ isModalVisible: !this.state.isModalVisible });

    render() {
        return <Container style={Style.bgMain}>
            <StatusBar barStyle="light-content" />
            {
                this.state.isSearchByEvent ?

                    //Busca por eventos
                    <Content style={[Style.layoutInner]} contentContainerStyle={[Style.layoutContent/*, Platform.OS === 'ios' ? { marginTop: 40 } : undefined*/]}>

                        {/*<TouchableOpacity onPress={this._toggleModal}>
                            <Text>Show Modal</Text>
                            </TouchableOpacity>
                            <Modal isVisible={this.state.isModalVisible}>
                            <View style={{ flex: 1 }}>
                                <Text>Hello!</Text>
                                <TouchableOpacity onPress={this._toggleModal}>
                                <Text>Hide me!</Text>
                                </TouchableOpacity>
                            </View>
                                </Modal>*/}

                        <ImageBackground source={{ uri: 'https://backgroundcheckall.com/wp-content/uploads/2017/12/balada-background-10.jpg' }} style={Styles.backgroundImageHeader}>
                            <View style={Styles.viewContainerImageHeader}>
                                <View style={[Styles.search]} >
                                    <TextInput placeholder={'ex.: Carnaval'} style={Styles.textInput} onChangeText={(searchText) => this.setState({ searchText })} />
                                    <Button transparent style={Styles.searchBtn} onPress={() => {
                                        this.seeAllEvents(this.state.searchText)

                                    }}>
                                        <Icon active name='search' type="FontAwesome" style={Styles.searchBtnIcon} />
                                    </Button>
                                </View>
                                <Text style={[Styles.textHeader, { marginBottom: 5 }]}>{'Divirta-se!'.toUpperCase()}</Text>
                                <Text style={[Styles.textHeader, { marginTop: 0 }]}>Se preferir, clique no botão abaixo e busque por locais</Text>
                                <View style={Styles.containerButtonChangeSearch}>
                                    <Button rounded style={[Style.BtnAction, Style.bgOrange, Styles.buttonChangeSearch]} onPress={() => this.onChangeTypeSearch(false)}>
                                        <Text uppercase={true}>Quero buscar meus locais</Text>
                                    </Button>
                                </View>
                            </View>
                        </ImageBackground>

                        <View style={Styles.sectionGrey}>

                            <View style={Styles.headerBg}>
                                <Text style={Styles.sHeader}>{this.state.eventCount + ' EVENTOS'}</Text>
                                <Right>
                                    <Button small rounded style={Styles.sBtn} onPress={() => { this.seeAllEvents() }}>
                                        <Text style={Styles.sLink} >Ver todos</Text>
                                    </Button>
                                    {/*<Button small rounded style={Styles.sBtn} onPress={() => { this.abrirSpecialEvent() }}>
                                        <Text style={Styles.sLink} >especial</Text>
                                    </Button>*/}
                                </Right>
                            </View>

                            <FlatList
                                data={this.state.categories}
                                style={Styles.agents}
                                numColumns={4}
                                renderItem={({ item }) => (
                                    <TouchableOpacity style={Styles.itemAgent} underlayColor='transparent' onPress={() => this.onSelectEventCategory(item)}>
                                        <View style={Styles.category}>
                                            {/*<ServerImageProgressiveLoader source={item.Image} style={Styles.itemAgentImg}/> */}
                                            <LinearGradient colors={['#CD347E', '#ED3D49']} style={Styles.containerCategoryIcon}>
                                                <CustomIcon name={item.Icon} size={30} color={"#fff"} style={[Styles.itemAgentImg]} />
                                            </LinearGradient>
                                            <Text style={Styles.containerCategoryDescription}>{item.Description}</Text>
                                        </View>
                                    </TouchableOpacity>
                                )}
                            />
                        </View>


                        <View style={Styles.sectionGrey}>
                            <View style={Styles.headerBg}>
                                <Text style={Styles.sHeader}>{'Destaques'.toUpperCase()}</Text>
                            </View>
                            <FlatList
                                data={this.state.events}
                                horizontal
                                showsHorizontalScrollIndicator={false}
                                style={Styles.flatList}
                                renderItem={({ item }) => (
                                    <TouchableOpacity style={Styles.itemBig} underlayColor='transparent' onPress={() => this.onLearnMoreEvent(item)}>

                                        <View>
                                            <ImageBackground source={{ uri: Useful.getImageUrl(item.Banner) }} style={Styles.itemImgBig}>
                                                <View style={{ marginRight: 3, display: 'none' }}>
                                                    <View style={Styles.eventImageOptions}>
                                                        <Icon name="heart" type="MaterialCommunityIcons" style={Styles.itemFavorite} size={20} />
                                                    </View>
                                                    <View style={Styles.eventImageOptions}>
                                                        <Icon name="share-alt" type="MaterialCommunityIcons" style={Styles.itemShare} size={20} />
                                                    </View>
                                                </View>
                                            </ImageBackground>
                                        </View>

                                    </TouchableOpacity>
                                )}
                            />
                        </View>

                        <View style={Styles.section}>
                            <View style={Styles.headerBg}>

                                <Text style={Styles.sHeader}>{'Vistos recentemente'.toUpperCase()}</Text>

                            </View>
                            <FlatList
                                data={this.state.recentlySeenEvents}
                                horizontal
                                showsHorizontalScrollIndicator={false}
                                style={Styles.flatList}
                                renderItem={({ item }) => (
                                    <TouchableOpacity style={Styles.item} underlayColor='transparent' onPress={() => this.onLearnMoreEvent(item)}>
                                        <View>
                                            <View>
                                                <ServerImageProgressiveLoader source={item.Banner} style={Styles.itemImg} />
                                                <View style={Styles.itemNoCrv}></View>
                                                <Icon name="heart" type="MaterialCommunityIcons" style={[Styles.itemFavorite, { display: 'none' }]} />
                                            </View>
                                            <Text style={Styles.itemPriceSm}>{Useful.formatDateAsString(item.StartDate)}</Text>
                                            <Text style={Styles.itemLocation}>{item.Name}</Text>

                                        </View>
                                    </TouchableOpacity>
                                )}
                            />
                        </View>
                    </Content>

                    :

                    //Busca por locais
                    <Content style={[Style.layoutInner]} contentContainerStyle={[Style.layoutContent/*, Platform.OS === 'ios' ? { marginTop: 40 } : undefined*/]}>
                        <ImageBackground source={{ uri: 'https://cdn.pixabay.com/photo/2016/03/09/09/42/buildings-1245953_640.jpg' }} style={Styles.backgroundImageHeader}>
                            <View style={Styles.viewContainerImageHeader}>
                                <View style={Styles.search} >
                                    <TextInput placeholder={'ex.: Restaurante trem bão'} style={Styles.textInput} onChangeText={(searchText) => this.setState({ searchText })} />
                                    <Button transparent style={Styles.searchBtn} onPress={() => {
                                        this.seeAllEventsAddress(this.state.searchText)
                                    }}>
                                        <Icon active name='search' type="FontAwesome" style={Styles.searchBtnIcon} />
                                    </Button>
                                </View>
                                <Text style={[Styles.textHeader, { marginBottom: 5 }]}>{'Divirta-se!'.toUpperCase()}</Text>
                                <Text style={[Styles.textHeader, { marginTop: 0 }]}>Se preferir, clique no botão abaixo e busque por eventos</Text>
                                <View style={Styles.containerButtonChangeSearch}>
                                    <Button rounded style={[Style.BtnAction, Styles.buttonChangeSearch, { backgroundColor: '#191970' }]} onPress={() => this.onChangeTypeSearch(true)}>
                                        <Text uppercase={true}>Quero buscar meus eventos</Text>
                                    </Button>
                                </View>
                            </View>
                        </ImageBackground>

                        <View style={Styles.sectionGrey}>

                            <View style={Styles.headerBg}>
                                <Text style={Styles.sHeader}>{this.state.eventAddressCount + ' LOCAIS'}</Text>
                                <Right>
                                    <Button small rounded style={Styles.sBtn} onPress={() => { this.seeAllEventsAddress() }}>
                                        <Text style={Styles.sLink} >Ver todos</Text>
                                    </Button>
                                </Right>
                            </View>

                            <FlatList
                                data={this.state.eventsAddressCategories}
                                style={Styles.agents}
                                numColumns={4}
                                renderItem={({ item }) => (
                                    <TouchableOpacity style={[Styles.itemAgent, {alignItems: 'flex-start'}]} underlayColor='transparent' onPress={() => { this.onSelectEventAddressCategory(item) }}>
                                        <View style={Styles.category}>
                                            {/*<ServerImageProgressiveLoader source={item.Image} style={Styles.itemAgentImg} />*/}
                                            <LinearGradient colors={['#CD347E', '#ED3D49']} style={Styles.containerCategoryIcon}>
                                                <CustomIcon name={item.Icon} size={30} color={"#fff"} style={[Styles.itemAgentImg]} />
                                            </LinearGradient>
                                            <Text style={Styles.containerCategoryDescription}>{item.Description}</Text>
                                        </View>
                                    </TouchableOpacity>
                                )}
                            />
                        </View>


                        <View style={Styles.sectionGrey}>
                            <View style={Styles.headerBg}>
                                <Text style={Styles.sHeader}>{'Destaques'.toUpperCase()}</Text>
                            </View>
                            <FlatList
                                data={this.state.eventsAddress}
                                horizontal
                                showsHorizontalScrollIndicator={false}
                                style={Styles.flatList}
                                renderItem={({ item }) => (
                                    <TouchableOpacity style={Styles.itemBig} underlayColor='transparent' onPress={() => this.onLearnMoreEventAddress(item)}>

                                        <View>
                                            <ImageBackground source={{ uri: Useful.getImageUrl(item.Banner) }} style={Styles.itemImgBig}>
                                                <View style={{ marginRight: 3, display: 'none' }}>
                                                    <View style={Styles.eventImageOptions}>
                                                        <Icon name="heart" type="MaterialCommunityIcons" style={Styles.itemFavorite} size={20} />
                                                    </View>
                                                    <View style={Styles.eventImageOptions}>
                                                        <Icon name="share-alt" type="MaterialCommunityIcons" style={Styles.itemShare} size={20} />
                                                    </View>
                                                </View>
                                            </ImageBackground>
                                        </View>

                                    </TouchableOpacity>
                                )}
                            />
                        </View>

                        <View style={Styles.section}>
                            <View style={Styles.headerBg}>

                                <Text style={Styles.sHeader}>{'Vistos recentemente'.toUpperCase()}</Text>

                            </View>
                            <FlatList
                                data={this.state.recentlySeenEventAddress}
                                horizontal
                                showsHorizontalScrollIndicator={false}
                                style={Styles.flatList}
                                renderItem={({ item }) => (
                                    <TouchableOpacity style={Styles.item} underlayColor='transparent' onPress={() => this.onLearnMoreEventAddress(item)}>
                                        <View>
                                            <View>
                                                <ServerImageProgressiveLoader source={item.Banner} style={Styles.itemImg} />
                                                <View style={Styles.itemNoCrv}></View>
                                                <Icon name="heart" type="MaterialCommunityIcons" style={[Styles.itemFavorite, { display: 'none' }]} />
                                            </View>
                                            <Text style={Styles.itemLocation}>{item.Name}</Text>
                                            <Text style={Styles.itemPriceSm}>{item.City} - {item.State}</Text>


                                        </View>
                                    </TouchableOpacity>
                                )}
                            />
                        </View>
                    </Content>
            }

        </Container>
    }
}