import React from 'react'
import { StatusBar, ImageBackground} from 'react-native'
import { Container, Header, Content, Button, Icon, Text, Left, Right, Body, View,Tab,Tabs,List,ListItem,Accordion } from 'native-base'
// import IconFontAwesome from 'react-native-vector-icons/FontAwesome';
import MapView, { Marker/*, PROVIDER_GOOGLE*/ } from 'react-native-maps';
//import ProgressiveLoadImage from '@Component/ProgressiveLoadImage';
import ServerImageProgressiveLoader from '@Component/ServerImageProgressiveLoader';


import Style from '@Theme/Style'
import Styles from '@Screen/Public/SpecialEvents/MenuCompetition/Style'
import Useful from '@Service/Useful';


import EventService from '@Service/EventService';
import EventAddressService from '@Service/EventAddressService';
import MenuCompetitionService from '@Service/MenuCompetitionService';

//import Config from '../../../../Config';


let EventServiceInstance = new EventService();
let EventAddressServiceInstance = new EventAddressService();
let MenuCompetitionInstance = new MenuCompetitionService();

let _event = {};

export default class extends React.Component {

    constructor(props) {
        super(props);

        _event = this.props.navigation.getParam('event', {});  
        this.state = {
            markers : []
        };
        
        MenuCompetitionInstance.retrieveByEventId(_event.EventId)
        .then(menuCompetition => {    
            let list =  menuCompetition.MenuCompetitionItemList;
            this.setState({
                markers: list         
            });  
        });

        this.renderAccordionHeader = this.renderAccordionHeader.bind(this)
        this.renderAccordionContent = this.renderAccordionContent.bind(this)


    }
    renderAccordionHeader(item, expanded) {

         let imgUrl = item.EventAddressLogo;

        return (
            <View style={Styles.accordionTab}>
                    <View style={Styles.ownerAvatarEvent}>   
                     <ServerImageProgressiveLoader source={imgUrl} style={Styles.ownerAvatarEventImg}  styleContainer={Styles.ownerAvatarEventImg}/>  
                    </View>          
                   <View style={{
                        flex: 1,
                        flexDirection: 'column',
                        justifyContent: 'center',
                        alignItems: 'stretch',
                        paddingHorizontal:20
                    }}>
                        <Text style={Styles.accordionTabText}>{item.EventAddressName}</Text>
                        <Text style={Styles.accordionTabTextType}>{item.EventAddressEventAddressCategoryPrimId}</Text>
                        
                    </View>   
            </View>
        );
    }
    renderAccordionContent(item) {
       
        return <View style={Styles.accordionContent} >
        <Text style={Styles.accordionTabTextprato} >
            {item.MenuItemName}    
        </Text>
       
            <ServerImageProgressiveLoader source={item.MenuItemGallery} style={Styles.ownerItemImg} styleContainer={Styles.ownerItemImg}/> 
      
           <Text style={Styles.accordionTabTextDesc}>
                {item.MenuItemDescription}     
           </Text>
           
             <Button small rounded style={[Styles.sBtn,Style.bgOrange]} onPress={() => EventAddressServiceInstance.openEventAddressById(item.EventAddressId)} >
                <Text>Ver local</Text>
            </Button>
        </View>
    }
    render() {


        return <Container style={Style.bgMain}>
            <Header style={[Style.navigation]} hasTabs={this.props.hasTabs}>
                <StatusBar backgroundColor="#DF3A62" animated barStyle="light-content" />
                <Left style={{ flex: 1 }}>
                    <Button transparent onPress={() => this.props.navigation.goBack()}>
                        <Icon active name='arrow-left' style={Style.textWhite} type="MaterialCommunityIcons" />
                    </Button>
                </Left>
                <Body style={{ flex: 1 }}>
                    <Text style={[Style.actionBarText]}>{_event.Name}</Text>
                </Body>
                <Right style={{ flex: 1 }}>
                  
                   
                </Right>
            </Header>
            <Content style={Styles.layoutInner} contentContainerStyle={Styles.layoutContent}>
            <Tabs tabBarUnderlineStyle={Styles.tabBorder}>
                    <Tab tabStyle={Styles.tabGrey} textStyle={Styles.tabTextActive} activeTabStyle={Styles.tabGrey} activeTextStyle={Styles.tabTextActive} heading="Detalhes">
                        <Content style={Styles.sectionTab}>
                       
                         
                          <ImageBackground source={{ uri:  Useful.getImageUrl(_event.Banner)}} imageStyle={'cover'} style={Styles.coverImg}>
                          </ImageBackground>
                        
                        <View style={Styles.section}>
                            
                            <View style={Styles.locationTop}>
                                <Icon active name='calendar-clock' style={Styles.locationTopIcon} type="MaterialCommunityIcons" />
                                <Text style={Styles.locationTopInfo}>
                                {Useful.formatDateAsString(_event.StartDate) + " "}                                
                                até 
                                {" " + Useful.formatDateAsString(_event.EndDate)}
                               </Text>
                            </View>

                        </View>
                        <ImageBackground source={require('@Asset/images/shadow.png')} imageStyle={'cover'} style={Styles.shadow} />
                        <View style={Styles.overview}>
                            <Text style={Styles.overviewTitle}>Descrição do Evento</Text>
                            <Text style={Styles.overviewDesc}>
                                {_event.Description}
                            </Text>
                        </View>
                        <View style={Styles.overview}>
                            <Text style={Styles.overviewTitle}>Locais Participantes</Text>
                        </View>
                    <View style={Styles.location}>
                    <MapView
                        style={[{  position: "absolute", top: 0, left: 0, bottom: 0, right: 0}]}
                        minZoomLevel={2}
                        zoomEnabled={true}
                        initialRegion={{
                            latitude: _event.EventAddressLatitude,
                            longitude: _event.EventAddressLongitude,
                            latitudeDelta: 0.0922,
                            longitudeDelta: 0.0421
                        }}
                        //annotations={markers}
                    >  
                           {this.state.markers.map(event => (
                            <MapView.Marker 
                            coordinate={{
                                    latitude: event.EventAddressLatitude,
                                    longitude: event.EventAddressLongitude
                                }
                            }
                            description={EventServiceInstance.getFullAddress(event)}
                            title={event.EventAddressName}
                            image={require('@Asset/images/icon-map.png')}
                            />
                        ))}
                    </MapView>
                    </View>
               

                <View style={Styles.owner}>
                    <Text style={Styles.ownerTitle}>Sobre o produtor</Text>
                    <View style={Styles.ownerAvatar}>
                         <ServerImageProgressiveLoader source={ _event.Logo} style={Styles.ownerAvatarImg}/>  
                    </View>
                    <View style={Styles.ownerInfo}>
                        <View>
                            <Text style={Styles.ownerName}>{_event.ProducerName}</Text>
                            <Text style={Styles.ownerLocation}>{_event.ProducerCity} - {_event.ProducerState}</Text>
                        </View>
                    </View>
                </View>

               
                        </Content>
                    </Tab>
                    <Tab tabStyle={Styles.tabGrey} textStyle={Styles.tabTextActive} activeTabStyle={Styles.tabGrey} activeTextStyle={Styles.tabTextActive} heading="Parceiros do evento">
                            <View style={Styles.section}>
                            

                                <Text style={Styles.sHeader}>{'Estabelecimentos'.toUpperCase()}</Text>

                            
                            <View>
                                <Accordion
                                style={Styles.accordion}
                                dataArray={this.state.markers}
                                expanded={-1}
                                renderHeader={this.renderAccordionHeader}
                                renderContent={this.renderAccordionContent}
                                    />
                            </View>   
                            </View>       
                    </Tab>
                </Tabs>
              
            </Content>
        </Container>
    }
}
