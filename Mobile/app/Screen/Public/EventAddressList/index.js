import React from 'react'
import { StatusBar, TouchableOpacity, Image, Dimensions, FlatList } from 'react-native'
import { Container, Header, Content, Button, Icon, Text, Left, Right, Body, View } from 'native-base'

import NavigationService from '@Service/Navigation'

import Style from '@Theme/Style'
import Styles from '@Screen/Public/EventAddressList/Style'
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';
import Config from '../../../Config';
import Useful from '@Service/Useful';

import EventAddressService from '@Service/EventAddressService';


let EventAddressServiceInstance = new EventAddressService();

class EventAddressList extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            refreshList: false,
            eventAddresses: this.props.navigation.state.params.eventAddresses
        };

    }

    onLearnMore = (item) => {
        EventAddressServiceInstance.retrieve(item.EventAddressId)
            .then((response) => {
                this.props.navigation.navigate('PublicEventAddressDetail', { 'detail': response.Entity });
            })
    }

    render() {

        return <Container style={Style.bgMain} backgroundColor="#fff">
            <Content style={Style.layoutInner} contentContainerStyle={Style.layoutContent}>
                <Header style={[Style.navigation]} hasTabs={this.props.hasTabs}>
                    <StatusBar backgroundColor="#DF3A62" animated barStyle="light-content" />
                    <Left style={{ flex: 1 }}>
                        <Button transparent onPress={() => this.props.navigation.goBack()}>
                            <Icon active name='arrow-left' style={Style.textWhite} type="MaterialCommunityIcons" />
                        </Button>
                    </Left>
                    <Body style={{ flex: 1 }}>
                        <Text style={[Style.actionBarText, { alignSelf: 'center' }]}>{'Locais'.toUpperCase()}</Text>
                    </Body>
                    <Right style={{ flex: 1 }}>
                        <Button transparent onPress={() => NavigationService.navigate('PublicExploreFilter')}>
                            <IconFontAwesome name="sliders" style={Style.textWhite} size={23}></IconFontAwesome>
                        </Button>
                        {/*<Button transparent onPress={() => this.setState({ isModalFilterVisible: true })}>
              <IconFontAwesome name={"filter"} color={"#fff"} size={25} />
            </Button>*/}
                    </Right>
                </Header>
                <View style={Styles.section}>
                    <FlatList
                        data={this.state.eventAddresses}
                        horizontal={false}
                        numColumns={2}
                        showsHorizontalScrollIndicator={false}
                        style={Styles.flatList}
                        renderItem={({ item }) => (
                            <TouchableOpacity style={Styles.item} underlayColor='transparent' onPress={() => this.onLearnMore(item)}>
                                <View>
                                    <View>
                                        
                                         <Image source={{ uri:  Useful.getImageUrl(item.Banner)}} style={Styles.itemImg} />
                                        
                                        <View style={Styles.itemNoCrv}></View>
                                        {/* <Icon name="heart" type="MaterialCommunityIcons" style={Styles.itemFavorite} />*/}
                                    </View>

                                    <View>
                                        <Text style={Styles.itemDate}>{item.Name}</Text>
                                        <Text style={Styles.itemPriceSm}>{item.City} - {item.State}</Text>
                                        <Text style={Styles.itemLocation}>{item.Address},{item.AddressNum} - {item.Neighborhood}</Text>
                                    </View>
                            </View>
                            </TouchableOpacity>
                        )}
                    />
                </View>
            </Content>
        </Container>

    }

}

export default EventAddressList;

