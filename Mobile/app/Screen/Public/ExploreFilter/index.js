import React from 'react'
import { StatusBar, TouchableOpacity, Image, FlatList } from 'react-native'
import { Container, Header, Content, Button, Icon, Text, Left, Right, Body, View } from 'native-base'
import RadioGroup from 'react-native-custom-radio-group'
import moment from 'moment'
import NavigationService from '@Service/Navigation'

import Style from '@Theme/Style'
import Styles from '@Screen/Public/ExploreFilter/Style'
import EventCategoryService from '@Service/EventCategoryService'
import EventService from '@Service/EventService'
import EventAddressService from '@Service/EventAddressService';
import EventAddressCategoryService from '@Service/EventAddressCategoryService';
import UserPreferencesService from '@Service/UserPreferencesService'
import Config from '../../../Config'
import Serenity from '@Service/Serenity'

export const eventLocation = [{
    label: 'Escolher uma cidade',
    value: 'type_choose'
}, {
    label: 'Minha localização',
    value: 'type_location'
}, {
    label: 'Todas as cidades',
    value: 'type_allcity'
}];

export const typeFilter = [
    {
        label: 'Evento',
        value: true
    }, {
        label: 'Local',
        value: false
    }];


export const eventPrice = [
    {
        label: 'Todos',
        value: 'type_all'
    }, {
        label: 'Grátis',
        value: 'type_free',
        condition: '=',
        conditionPrice: 0
    }, {
        label: 'Pagos',
        value: 'type_others',
        condition: '>',
        conditionPrice: 0
    }];

export const eventDate = [{
    label: 'Todas',
    value: 'date_all',
    min_date: moment("1997-01-30"),
    max_date: moment("9999-01-30")
}, {
    label: 'Hoje',
    value: 'date_today',
    min_date: moment(),
    max_date: moment()
}, {
    label: 'Amanhã',
    value: 'date_tomorrow',
    min_date: moment().add(1, 'day'),
    max_date: moment().add(1, 'day')

}
    , {
    label: 'Esta Semana',
    value: 'date_week',
    min_date: moment().startOf('week'),
    max_date: moment().endOf('week')
}
    , {
    label: 'Este fim de semana',
    value: 'date_weekend',
    min_date: moment().weekday(5),
    max_date: moment().weekday(7)
}
    , {
    label: 'Próxima semana',
    value: 'date_next_week',
    min_date: moment().add(1, 'week').startOf('week'),
    max_date: moment().add(1, 'week').endOf('week')
}
    , {
    label: 'Este mês',
    value: 'date_month',
    min_date: moment().startOf('month'),
    max_date: moment().endOf('month')
}

];


let EventCategoryServiceInstance = new EventCategoryService();
let EventServiceInstance = new EventService()
let EventAddressServiceInstance = new EventAddressService();
let EventAddressCategoryServiceInstance = new EventAddressCategoryService();

class ExploreFilter extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isSearchByEvent: true,
            isModalVisible: false,
            refreshList: false,
            refreshListLocal: false,
            events: [],
            eventDateValue: '',
            eventPriceValue: '',
            categories: []
        }
        this._selectCategory = this._selectCategory.bind(this);
        this._selectCategoryAddress = this._selectCategoryAddress.bind(this);
        this.searchLocal = this.searchLocal.bind(this);
    }
    componentDidMount(){

     
        // this._sub = this.props.navigation.addListener('didFocus', () => {

        // })
        this.setState({
            isSearchByEvent : UserPreferencesService.get().searchByEvent
        })

       EventCategoryServiceInstance.list()
       .then((data)=>{
           this.setState({
             categories : data.Entities
           })
       })

       EventAddressCategoryServiceInstance.list()
           .then((data) => {
               this.setState({
                   eventsAddressCategories: data.Entities
               })
           })
    }
    

    onChangeSearchType(type){
        this.setState(
            { isSearchByEvent : type }
        )
        UserPreferencesService.set({
            searchByEvent : type
        })
    }

    search() {
        if(!this.state.isSearchByEvent){
            this.searchLocal();
            return;
        }
        let dateValue = ((this.state.eventDateValue) || "date_all")
        let selectedDate = eventDate.find(x => x.value == dateValue);
        //verifica se data selecionada é antes de hoje, se sim seta a data mínima pra hoje
        selectedDate.min_date = selectedDate.min_date.isBefore(moment(), 'day') ? moment().startOf('day') : selectedDate.min_date
        let dateRange = {
            min_date: selectedDate.min_date.format('YYYY-MM-DD'),
            max_date: selectedDate.max_date.format('YYYY-MM-DD')
        }

        var request = {};

        if(dateValue != 'date_all'){
            request.Criteria = Serenity.Criteria.and(request.Criteria,
                [['StartDate'], '<=', moment().format('YYYY-MM-DD')],
                [['EndDate'], '>=', dateRange.min_date]);
        }
        else{
            var dateCriteria = Serenity.Criteria.and(
                    [['StartDate'], '<=', dateRange.max_date],
                    [['EndDate'], '>=', dateRange.min_date]);
            request.Criteria = Serenity.Criteria.and(request.Criteria, dateCriteria)
        }
         


        let selectedCat = this.state.categories.filter(x => { return x.selected == true });
        if (selectedCat.length) {
            let allSelectedCategories = [];
            selectedCat.forEach(category => {
                allSelectedCategories.push(category.EventCategoryId);
            });
            var categoryCriteria = Serenity.Criteria.or(
                [['CategoryPrimId'], 'in', [allSelectedCategories]],
                [['CategorySecId'], 'in', [allSelectedCategories]]
            )
            request.Criteria = Serenity.Criteria.and(request.Criteria, categoryCriteria)
        }

        if (this.state.eventPriceValue && this.state.eventPriceValue != "type_all") {

            let selectedPrice = eventPrice.find(x => x.value == this.state.eventPriceValue)
            if (selectedPrice) {
                request.Criteria = Serenity.Criteria.and(request.Criteria,
                    [['Price'], selectedPrice.condition, selectedPrice.conditionPrice]);
            }
        }

        EventServiceInstance.list(request)
            .then((data) => {
                //NavigationService.navigate('PublicEventList');
                NavigationService.navigate('PublicEventList', { events: data.Entities });
            })
    }

    
    

    searchLocal() {
        var request = {};
        let selectedCat = this.state.eventsAddressCategories.filter(x => { return x.selected == true });
        if (selectedCat.length) {
            let allSelectedCategories = [];
            selectedCat.forEach(category => {
                allSelectedCategories.push(category.EventAddressCategoryId);
            });
            var categoryCriteria = Serenity.Criteria.or(
                [['EventAddressCategoryPrimId'], 'in', [allSelectedCategories]],
                [['EventAddressCategorySecId'], 'in', [allSelectedCategories]]
            )
            request.Criteria = Serenity.Criteria.and(request.Criteria, categoryCriteria)
        }

        EventAddressServiceInstance.list(request)
            .then((data) => {
                NavigationService.navigate('PublicEventAddressList', { eventAddresses: data.Entities });
            })
    }

    _selectCategory(selectedCategory) {
        selectedCategory.selected = (!selectedCategory.selected);

        this.setState(
            {
                refreshList: !this.state.refreshList
            }
        )

    }
    _selectCategoryAddress(selectedCategory) {

        selectedCategory.selected = (!selectedCategory.selected);

        this.setState(
            {
                refreshListLocal: !this.state.refreshListLocal
            }
        )

    }

    render() {

        // this.setState({
        //     isSearchByEvent :true,
        // })


        return <Container style={Style.bgMain}>

            <Header style={[Style.navigation]} hasTabs={this.props.hasTabs}>
                <StatusBar backgroundColor="#DF3A62" animated barStyle="light-content" />
                <Left style={{ flex: 1 }}>
                    <Button transparent onPress={() => this.props.navigation.goBack()}>
                        <Icon active name='arrow-left' style={Style.textWhite} type="MaterialCommunityIcons" />
                    </Button>
                </Left>
                <Body style={{ flex: 1 }}>
                    <Text style={[Style.actionBarText, { alignSelf: 'center' }]}>{'Filtro'.toUpperCase()}</Text>
                </Body>
                <Right style={{ flex: 1 }}>
                </Right>
            </Header>




            <Content style={Style.layoutInner} contentContainerStyle={Style.layoutContent}>

                <View style={Styles.section}>

                    
                            <View style={Styles.type}>
                            <Text style={Styles.ContainerTitle}>Procurar por</Text>
                            <RadioGroup
                                containerStyle={Styles.typeBg}
                                onChange={(isSearchByEvent) => this.onChangeSearchType(isSearchByEvent)}
                                buttonContainerStyle={Styles.typeBtn}
                                initialValue={true}
                                buttonTextStyle={Styles.typeBtnText}
                                buttonContainerActiveStyle={Styles.typeBtnActive}
                                buttonContainerInactiveStyle={Styles.typeBtnInactive}
                                buttonTextActiveStyle={Styles.typeActiveText}
                                buttonTextInactiveStyle={Styles.typeInactiveText}
                                radioGroupList={typeFilter}
                            />
                        </View>
                    
                        

                    {
                        this.state.isSearchByEvent ?

                            <Content>

                                <View style={Styles.type}>
                                    <Text style={Styles.ContainerTitle}>Quando?</Text>
                                    <RadioGroup
                                        containerStyle={Styles.typeBg}
                                        onChange={(eventDateValue) => this.setState({ eventDateValue })}
                                        initialValue={'date_select'}
                                        buttonContainerStyle={Styles.typeBtn}
                                        buttonTextStyle={Styles.typeBtnText}
                                        buttonContainerActiveStyle={Styles.typeBtnActive}
                                        buttonContainerInactiveStyle={Styles.typeBtnInactive}
                                        buttonTextActiveStyle={Styles.typeActiveText}
                                        buttonTextInactiveStyle={Styles.typeInactiveText}
                                        radioGroupList={eventDate}
                                    />
                                </View>
                                <View style={Styles.type}>
                                    <Text style={Styles.ContainerTitle}>O que?</Text>
                                    <FlatList
                                        extraData={this.state.refreshList}
                                        keyExtractor={(item, index) => item.EventCategoryId + index}
                                        data={this.state.categories}
                                        style={[Styles.categories]}
                                        contentContainerStyle={Styles.categoriesContainer}
                                        numColumns={4}
                                        renderItem={({ item }) => (
                                            <View style={Styles.category} >
                                                <TouchableOpacity underlayColor='transparent' onPress={() => this._selectCategory(item)}>
                                                    <Image source={{ uri:  Config.IMAGE_BASE_URL + item.Image }} style={[Styles.itemCategoryIcon, item.selected ? Styles.itemCategorySelected : {}]} />

                                                </TouchableOpacity>
                                                <Text style={Styles.itemCategoryName}>{item.Description} </Text>
                                            </View>


                                        )}
                                    />
                                </View>
                                <View style={Styles.type}>
                                    <Text style={Styles.ContainerTitle}>Valor</Text>
                                    <RadioGroup
                                        onChange={(eventPriceValue) => this.setState({ eventPriceValue })}
                                        containerStyle={Styles.typeBg}
                                        initialValue={'date_select'}
                                        buttonContainerStyle={Styles.typeBtn}
                                        buttonTextStyle={Styles.typeBtnText}
                                        buttonContainerActiveStyle={Styles.typeBtnActive}
                                        buttonContainerInactiveStyle={Styles.typeBtnInactive}
                                        buttonTextActiveStyle={Styles.typeActiveText}
                                        buttonTextInactiveStyle={Styles.typeInactiveText}
                                        radioGroupList={eventPrice}
                                    />
                                </View>
                            </Content>
                            :
                            <Content>
                                <View style={Styles.type}>
                                    <Text style={Styles.ContainerTitle}>O que?</Text>
                                    <FlatList
                                        extraData={this.state.refreshListLocal}
                                        keyExtractor={(item, index) => item.EventAddressCategoryId + index}
                                        data={this.state.eventsAddressCategories}
                                        style={[Styles.categories]}
                                        contentContainerStyle={Styles.categoriesContainer}
                                        numColumns={4}
                                        renderItem={({ item }) => (
                                            <View style={Styles.category} >
                                                <TouchableOpacity underlayColor='transparent' onPress={() => this._selectCategoryAddress(item)}>
                                                    <Image source={{ uri: Config.IMAGE_BASE_URL + item.Image }} style={[Styles.itemCategoryIcon, item.selected ? Styles.itemCategorySelected : {}]} />

                                                </TouchableOpacity>
                                                <Text style={Styles.itemCategoryName}>{item.Description} </Text>
                                            </View>


                                        )}
                                    />
                                </View>
                            </Content>
                    }
                    <View style={Styles.type}>
                        <TouchableOpacity onPress={()=> this.search()}>
                            <Text style={[Styles.typeBtn, Styles.typeBtnActive, Style.typeActiveText]}>
                                Filtrar
                            </Text>
                        </TouchableOpacity>

                    </View>
                </View>
            </Content>

        </Container>
    }

}

export default ExploreFilter;

