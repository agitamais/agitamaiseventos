const React = require("react-native");
const { Platform } = React;
import { blue } from "ansi-colors";

export default {
  layoutContent: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  section: {
    flex: 1,
    paddingVertical: 30,
  },

  categories: {

  },
  /* -- Agents -- */
  categoriesContainer: {
    //  paddingVertical: 10,   
    // justifyContent: 'center',
    // flex: 1,
    // justifyContent: 'center',
  },
  category: {
    flexBasis: 0,
    alignItems: "center",

    flexGrow: 1,
    margin: 4,


  },
  itemCategory: {

    // alignItems:'center',
    // justifyContent:'center',
    // width:80,
    // height:80,
    // borderRadius:100,
  },
  itemCategoryIcon: {
    borderRadius: Platform.OS === "ios" ? 26 : 50,
    borderWidth: 1,
    borderColor: "transparent",
    opacity: 0.5,
    padding: 25,
    marginBottom: 10
  },
  itemCategoryName: {
    color: '#333',
    fontSize: 11,
    fontFamily: 'Montserrat-SemiBold',
    textAlign: 'center',

  },
  itemCategorySelected: {
    backgroundColor: '#DF3A62',
    color: '#fff',
    opacity: 1,
    borderColor: "transparent",
  },
  type: {
    marginBottom: 20,
    paddingHorizontal: 15,
  },
  location: {
    marginBottom: 20,
    paddingHorizontal: 20,
  },
  bed: {
    marginBottom: 20,
    paddingHorizontal: 20,
  },
  price: {
    marginBottom: 20,
    paddingHorizontal: 15,
  },
  btnBg: {
    marginBottom: 20,
    paddingHorizontal: 20,
  },


  propertyType: {
    marginBottom: 30,
    justifyContent: 'flex-start',
    alignItems: 'flex-start'
  },
  eventFilter: {
    marginBottom: 30,
    justifyContent: 'flex-start',
    alignItems: 'flex-start'
  },

  label: {
    fontFamily: 'Montserrat-Regular',
    fontSize: 12,
    color: '#333',
    marginBottom: 10,
  },
  labelPrice: {
    fontFamily: 'Montserrat-Regular',
    fontSize: 12,
    color: '#333',
    marginBottom: 10,
    paddingHorizontal: 5,
  },
  textInput: {
    fontFamily: 'Montserrat-Regular',
    fontSize: 12,
    color: '#333',
    backgroundColor: '#f0f0f0',
    borderRadius: 5,
    paddingHorizontal: 20,
    paddingVertical: 15,
  },
  btn: {
    width: '100%',
    borderRadius: 5,
    backgroundColor: '#FCC300',
    paddingVertical: 20,
    paddingHorizontal: 20,
  },
  btnText: {
    fontFamily: 'Montserrat-SemiBold',
    color: '#333',
    fontSize: 12,
    alignSelf: 'center',
  },
  btnIcon: {
    fontFamily: 'Montserrat-SemiBold',
    color: '#333',
    fontSize: 16,
    alignSelf: 'center',
  },
  radio: {
    flexDirection: 'column',
    marginLeft: 0,
    justifyContent: 'center',
    fontFamily: 'Montserrat-Regular',
    color: '#666',
    backgroundColor: '#f0f0f0',
    borderRadius: 5,
    marginRight: 10,
  },
  radioList: {
    flexDirection: 'row',
    flex: 1,
  },
  radioText: {
    fontFamily: 'Montserrat-Regular',
    fontSize: 12,
    textAlign: 'center',
  },

  col: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  add: {
    flex: 1,
    flexDirection: 'row',
  },
  addText: {
    fontFamily: 'Montserrat-SemiBold',
    color: '#333',
    paddingVertical: 10,
    paddingHorizontal: 20,
    fontSize: 12,
  },
  addBtn: {
    color: '#333',
    backgroundColor: '#f0f0f0',
    borderRadius: 5,
  },
  addIcon: {
    color: '#666',
    fontSize: 12,
  },
  itemAgent: {
    flexGrow: 1,
    flexBasis: 0,
    width: 72,
    marginLeft: 5,
    marginRight: 5,
    marginBottom: 15
  },
  typeBg: {
    flexDirection: 'row',
    flexWrap: 'wrap'
  },
  btnFiltrar: {
    flex: 1,
    justifyContent: 'center',
    alignSelf: 'center', 
    marginBottom: 0
  },
  typeBtn: {
    flexGrow: 2,
    borderRadius: 25,
    borderWidth: 0,
    width: 'auto',
    marginHorizontal: 5,
    marginBottom: 5,
    paddingHorizontal: 10
  },
  typeBtnText: {
    fontFamily: 'Montserrat-Regular',
    color: '#999',
    fontSize: 12,

  },
  ContainerTitle: {
    padding: 5
  },
  typeBtnActive: {
    textAlign: 'center',
    color: '#FFF',
    backgroundColor: '#DF3A62',
    paddingVertical: 12,
  },
  typeBtnInactive: {
    backgroundColor: '#f0f0f0',
    paddingVertical: 12,
  },
  typeActiveText: {
    color: '#FFF',
    textAlign: 'center',
    fontFamily: 'Montserrat-SemiBold',
  },
  typeInactiveText: {
    color: '#999',
    fontFamily: 'Montserrat-Regular',
  },

  priceDropdown: {
    flex: 1,
  },
  priceSelect: {
    fontFamily: 'Montserrat-Regular',
    fontSize: 12,
  },
  pricePicker: {
    flex: 1,
    backgroundColor: '#f0f0f0',
    paddingLeft: 10,
    fontFamily: 'Montserrat-Regular',
    borderRadius: 5,
    marginHorizontal: 5,
  },
  button: {
    backgroundColor: '#00aeef',
    borderColor: 'red',
    color: '#000',
    borderWidth: 5,
    borderRadius: 15
  }

}