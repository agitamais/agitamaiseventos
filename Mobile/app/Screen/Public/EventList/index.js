import React from 'react'
import { StatusBar, TouchableOpacity, Image, FlatList } from 'react-native'
import { Container, Header, Content, Button, Icon, Text, Left, Right, Body, View } from 'native-base'
import NavigationService from '@Service/Navigation'
import ServerImageProgressiveLoader from '@Component/ServerImageProgressiveLoader'
import Style from '@Theme/Style'
import Styles from '@Screen/Public/EventList/Style'
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';
import EventService from '@Service/EventService';
import Useful from '@Service/Useful'


let EventServiceInstance = new EventService();
let _listRequestCriteria = null;
let _listResponse = {};

class EventList extends React.Component {

    constructor(props) {
        super(props);
        this.state = {         
            events: this.props.navigation.state.params.events
        }
        _listRequestCriteria = this.props.navigation.state.params.criteria;
        _listResponse = this.props.navigation.state.params.listResponse;
    }
    render() {

        return <Container style={Style.bgMain} backgroundColor="#fff">
            <Content style={Style.layoutInner} contentContainerStyle={Style.layoutContent}>
                <Header style={[Style.navigation]} hasTabs={this.props.hasTabs}>
                    <StatusBar backgroundColor="#DF3A62" animated barStyle="light-content" />
                    <Left style={{ flex: 1 }}>
                        <Button transparent onPress={() => this.props.navigation.goBack()}>
                            <Icon active name='arrow-left' style={Style.textWhite} type="MaterialCommunityIcons" />
                        </Button>
                    </Left>
                    <Body style={{ flex: 1 }}>
                        <Text style={[Style.actionBarText, { alignSelf: 'center' }]}>{'Eventos'.toUpperCase()}</Text>
                    </Body>
                    <Right style={{ flex: 1 }}>
                        <Button transparent onPress={() => NavigationService.navigate('PublicExploreFilter')}>
                            <IconFontAwesome name="sliders" style={Style.textWhite} size={23}></IconFontAwesome>
                        </Button>
                        {/*<Button transparent onPress={() => this.setState({ isModalFilterVisible: true })}>
              <IconFontAwesome name={"filter"} color={"#fff"} size={25} />
            </Button>*/}
                    </Right>
                </Header>
                <View style={Styles.section}>
                    <FlatList
                        data={this.state.events}
                        horizontal={false}
                        numColumns={2}
                        showsHorizontalScrollIndicator={false}
                        style={Styles.flatList}
                        renderItem={({ item }) => (
                            <TouchableOpacity style={Styles.item} underlayColor='transparent' onPress={() => EventServiceInstance.openEventById(item.EventId) }>
                                <View>
                                    <View>
                                        <ServerImageProgressiveLoader source={item.Banner} style={Styles.itemImg}/>                       
                                        <View style={Styles.itemNoCrv}></View>
                                        {/* <Icon name="heart" type="MaterialCommunityIcons" style={Styles.itemFavorite} />*/}
                                    </View>

                                    <View style={paddingVertical=5}>
                                        <Text style={Styles.itemDate}> {Useful.formatDateAsString(item.StartDate)} </Text>
                                        <Text style={Styles.itemPriceSm}>{item.Name}</Text>
                                       {/* <Text style={Styles.itemLocation}>{item}</Text>*/}
                                    </View>

                                </View>
                            </TouchableOpacity>
                        )}
                    />
                </View>
            </Content>
        </Container>

    }

}

export default EventList;

