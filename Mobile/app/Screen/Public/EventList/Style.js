const React = require("react-native");
const { Platform } = React;
export default {
    layoutContent: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },

    navigation:{
        shadowOpacity: 0,
        elevation: 0,
        shadowOffset: {
            height: 0,
        },
        shadowRadius: 0,
        backgroundColor: 'transparent',
        width: '100%',
        borderBottomWidth: 0,
        backgroundColor:'#fff',
        color:'#32404d'
    },
    actionBarText: {
        color: '#333333',
        fontFamily: "Montserrat-Regular",
        fontSize: 14,
        textAlign: 'center',
    },
/* -- Search -- */
search: {
    flex: 1,
    paddingHorizontal: 20,
    paddingTop: 5,
    paddingBottom: 20,
    backgroundColor: '#fff',
    flexDirection: 'row',
    borderRadius: 4,
    borderWidth: 0.5,
    borderColor: '#d6d7da',
},
textInput: {
    flex: 8,
    paddingHorizontal: 20,
    backgroundColor: '#FFF',
    fontFamily: 'Montserrat-Regular',
    fontSize: 12,
    borderColor:'#ccc',
    borderTopLeftRadius: 5,
    borderBottomLeftRadius: 5,
    height: 50,
},
searchBtn: {
    flex: 2,
    backgroundColor: '#FFF',
    fontFamily: 'Montserrat-Regular',
    fontSize: 12,
    borderTopRightRadius: 5,
    borderBottomRightRadius: 5,
    borderTopLeftRadius: 0,
    borderBottomLeftRadius: 0,
    justifyContent: 'center',
    height: 50,
},
searchBtnIcon: {
    color: '#999',
    fontSize: 18,
},
    /* -- Slider -- */
    slider: {
        flex: 1,
        paddingBottom: 10,
    },

    /* -- Featured -- */
    section: {
        flex: 1,
        paddingTop: 15,
        paddingBottom: 30,
    },
    sectionGrey: {
        flex: 1,
        paddingVertical: 30,
        backgroundColor: '#fff',
    },
    flatList: {
        paddingLeft: 10,
    },
    headerBg: {
        flexDirection: 'row',
        marginBottom: 15,
        paddingHorizontal: 20,
    },
    headerIcon: {
        fontSize: 24,
        color: '#333',
    },
    sHeader: {
        color: '#333',
        marginLeft: 3,
        fontSize: 14,
        fontFamily: 'Montserrat-SemiBold',
        marginTop: 5,
    },
    sLink: {
        color: '#666',
        fontSize: 10,
        fontFamily: 'Montserrat',
    },
    itemList: {
        flexDirection: 'row',
        width: '100%',
        marginBottom: 10,
    },
    itemBig: {
        width: 300,
        backgroundColor: '#FFF',
        borderRadius: 5,
        elevation: 10,
        shadowOffset: {
            width: 15,
            height: 15
        },
        shadowColor: '#999',
        shadowOpacity: 0.1,
        shadowRadius: 0,
        margin: 10,
        marginBottom: 20,
    },
    itemImgBig: {
        marginBottom: 0,
        width: '100%',
        height: 180,
        borderRadius:5,
        ...Platform.select({
            ios: {
                borderRadius: 5,
            },
        }),
    },
    itemNoCrv: {
        ...Platform.select({
            ios: {
                width: '100%',
                height: 5,
                backgroundColor: '#FFF',
                bottom: 10,
                position: 'absolute',
            },
        }),
    },
    itemBg: {
        ...Platform.select({
            ios: {
                
            },
        }),
    },

    item: {
        width: '47%',
        marginBottom: 20,
        marginLeft: 5,
        marginRight: 5,
        backgroundColor: '#FFF',
        borderRadius: 5,
        elevation: 5,
        shadowOffset: {
            width: 6,
            height: 6
        },
        shadowColor: "grey",
        shadowOpacity: 0.1,
        shadowRadius: 5,
    },
    itemImg: {
        marginBottom: 10,
        width: '100%',
        height: 100,
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5,
        ...Platform.select({
            ios: {
                borderRadius: 5,
            },
        }),
    },
    itemMes:{
        color:'#df0a04',
        padding:10,
        paddingLeft:20,
        paddingBottom:0,
        fontSize:13
    },
    itemDia:{
        color:'#797979',
        padding:10,
        paddingLeft:20,
        paddingTop:0,
        fontSize:20
    },
    itemFavorite: {
        position: 'absolute',
        alignSelf: 'flex-end',
        color: '#ff7400',
        marginTop: 140,
        paddingRight: 12,
    },
    itemShare: {
        position: 'absolute',
        alignSelf: 'flex-end',
        color: '#ff7400',
        marginTop: 120,
        paddingRight: 50
    },
    itemPrice: {
        color: '#333',
        fontSize: 16,
        fontFamily: 'Montserrat-SemiBold',
        paddingHorizontal: 10,
    },
    itemDate:{
        color: '#999',
        fontSize: 11,
        fontFamily: 'Montserrat-SemiBold',
        paddingHorizontal: 10,  
    },
    itemPriceSm: {
        color: '#333',
        fontSize: 11,
        fontFamily: 'Montserrat-SemiBold',
        paddingHorizontal: 10,
        paddingBottom:5
    },
    itemLocation: {
        color: '#999',
        fontSize: 11,
        fontFamily: 'Montserrat-Regular',
        marginBottom: 10,
        paddingHorizontal: 10,
    },
    crv: {
        borderRadius: 8,
    },
    itemRow: {
        flexDirection: 'row',
        paddingHorizontal: 20,
        paddingBottom: 15,
    },
    itemOverview: {
        flexGrow: 1,
        flexDirection: 'row',
    },
    itemIcon: {
        color: '#999',
        marginRight: 5,
        fontSize: 24,
    },
    itemNo: {
        color: '#333',
        marginRight: 5,
        fontFamily: 'Montserrat-SemiBold',
        marginTop: 5,
        fontSize: 14,
    },

    /* -- Top Cities -- */
    city: {
        flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
        paddingHorizontal: 20,
        justifyContent: 'space-between'
    },
    btnCity: {
        width: '48%',
        height: 100,
        marginBottom: 10,
    },
    btnCityImg: {
        flex: 1,
        borderRadius: 5,
    },
    btnCityLocation: {
        flex: 1,
        position: 'absolute',
        width: '100%',
        height: 100,
        justifyContent: 'center',
        alignItems: 'center',
    },
    btnCityText: {
        color: '#FFF',
        fontFamily: 'Montserrat-SemiBold',
        fontSize: 12,
    },


    flatCity: {
        paddingLeft: 20,
    },
    itemCity: {
        width: 150,
        marginLeft: 5,
        marginRight: 5,
    },
    itemCityCount: {
        position: 'absolute',
        justifyContent: 'center',
        alignItems: 'center',
        width: 150,
        height: 64,
    },
    itemCityLocation: {
        color: '#FFF',
        fontFamily: 'Montserrat-SemiBold',
        fontSize: 13,
    },
    itemCityImg: {
        marginBottom: 10,
        width: 150,
        height: 64,
        borderRadius: 5,
        textAlign: 'center'
    },

    /* -- Agents -- */
    agents: {
        paddingHorizontal: 15,
        
    },
    itemAgent: {
        flexGrow: 1,
        flexBasis: 0,
        width: 72,
        marginLeft: 5,
        marginRight: 5,
    },
    itemAgentImg: {
        marginBottom: 10,
        width: 72,
        height: 72,
        borderRadius: 35,
        textAlign: 'center'
    },
    itemAgentName: {
        color: '#333',
        fontSize: 11,
        fontFamily: 'Montserrat-SemiBold',
        textAlign: 'center',
    },


    typeBg: {
        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#FFF',
        width: '70%',
    },
    typeBtn: {
        flex: 1,
        borderWidth: 0,
        borderRadius: 0,
    },
    typeBtnText: {
        fontFamily: 'Montserrat-Regular',
        color: '#999',
        fontSize: 12,
    },

    typeBtnActive: {
        backgroundColor: '#FFF',
        paddingVertical: 8,
        paddingHorizontal: 5,
    },
    typeBtnInactive: {
        backgroundColor: 'transparent',
        paddingVertical: 8,
        paddingHorizontal: 5,
    },
    typeActiveText: {
        color: '#333',
        fontFamily: 'Montserrat-SemiBold',
        fontSize: 10,
    },
    typeInactiveText: {
        color: '#FFF',
        fontFamily: 'Montserrat-Regular',
        fontSize: 10,
    },

}