import React from 'react'
import { StatusBar, TouchableOpacity, Image, ImageBackground, FlatList,Linking } from 'react-native'
import { Container, Header, Content, Button, Icon, Text, Left, Right, Body, View } from 'native-base'
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';
import MapView, { Marker/*, PROVIDER_GOOGLE*/ } from 'react-native-maps';
import call from 'react-native-phone-call'

import Style from '@Theme/Style'
import Styles from '@Screen/Public/EventAddressDetail/Style'
import Config from '../../../Config';
import EventAddressService from '@Service/EventAddressService';
import Serenity from '@Service/Serenity';
import Useful from '@Service/Useful';

let EventAddressServiceInstance = new EventAddressService();
let _eventAddress = {};
export default class extends React.Component {


    constructor(props) {
        super(props);
        _eventAddress = this.props.navigation.getParam('detail', {});

        EventAddressServiceInstance.setRecentlySeenEventAddress(_eventAddress)
            .then(x => {
                EventAddressService.execCallbackAfterInsertRecentlySeen();
            })

        this.state = {
            similarEventAddresses: []
        }

        let request = {
            Take: 5,
            Criteria: {}
        }



        request.Criteria = Serenity.Criteria.and(
            [['EventAddressId'], '!=', _eventAddress.EventAddressId]);
        var categoryCriteria = Serenity.Criteria.or(
            [['EventAddressCategoryPrimId'], 'in', [[_eventAddress.EventAddressCategoryPrimId, _eventAddress.EventAddressCategorySecId]]],
            [['EventAddressCategorySecId'], 'in', [[_eventAddress.EventAddressCategoryPrimId, _eventAddress.EventAddressCategorySecId]]]
        );

        request.Criteria = Serenity.Criteria.and(request.Criteria, categoryCriteria)
        EventAddressServiceInstance.list(request)
            .then((data) => {
                this.setState({
                    similiarEventAddress: data.Entities
                })
            })
    }
    onLearnMoreEventAddress(item) {
        EventAddressServiceInstance.retrieve(item.EventAddressId)
            .then((data) => {
                let itemSelected = data.Entity;
                this.props.navigation.push('PublicEventAddressDetail', { 'detail': itemSelected });
            })
    }

    onOpenWhatsApp(number) {

        if (!number) {
            alert('O whatsApp ainda não foi informado');
            return;
        }

        let link = 'https://api.whatsapp.com/send?1=pt_BR&phone=' + number;

        if (link) {
            Linking.canOpenURL(link)
                .then(supported => {
                    if (!supported) {
                        Alert.alert(
                            'Por favor, instale o whatsApp caso queira enviar mensagens'
                        );
                    } else {
                        return Linking.openURL(link);
                    }
                })
                .catch(err => console.error('An error occurred', err));
        } else {
            console.log('sendWhatsAppMessage -----> ', 'message link is undefined');
        }
    }

    onOpenPhoneCall = (number) => {

        if (!number) {
            alert('O número de telefone ainda não foi informado');
            return;
        }

        if (number && number.substring(0, 2))
            number = number.substring(2, number.length);

        const args = {
            number: number, // String value with the number to call
            prompt: true // Optional boolean property. Determines if the user should be prompt prior to the call 
        }
        call(args).catch(console.error)
    }

    render() {


        return <Container style={Style.bgMain}>
            <Header style={[Style.navigation]} hasTabs={this.props.hasTabs}>
                <StatusBar backgroundColor="#DF3A62" animated barStyle="light-content" />
                <Left style={{ flex: 1 }}>
                    <Button transparent onPress={() => this.props.navigation.goBack()}>
                        <Icon active name='arrow-left' style={Style.textWhite} type="MaterialCommunityIcons" />
                    </Button>
                </Left>
                <Body style={{ flex: 3 }}>
                    <Text style={[Style.actionBarText]}>{_eventAddress.ProducerCompanyName.toUpperCase()}</Text>
                </Body>
                <Right style={{ flex: 1 }}>
                    {/*<Button transparent onPress={() => NavigationService.navigate('PublicExploreFilter')}>
                <IconFontAwesome name="sliders" style={Style.textWhite} size={23}></IconFontAwesome>
            </Button>*/}
                    {/*<Button transparent onPress={() => this.setState({ isModalFilterVisible: true })}>
              <IconFontAwesome name={"filter"} color={"#fff"} size={25} />
            </Button>*/}
                </Right>
            </Header>
            <Content style={Style.layoutInner} contentContainerStyle={Style.layoutContent}>
                <ImageBackground source={{ uri: (Useful.getImageUrl(_eventAddress.Banner)) }} imageStyle={'cover'} style={Styles.coverImg}></ImageBackground>
                <View style={Styles.section}>
                    <Text style={Styles.price}>{_eventAddress.ProducerCompanyName}</Text>
                    <View style={Styles.locationTop}>
                        <Icon active name='map-marker-radius' style={Styles.locationTopIcon} type="MaterialCommunityIcons" />
                        <Text style={Styles.locationTopInfo}>{_eventAddress.ProducerCity} - {_eventAddress.ProducerState}</Text>
                    </View>
                    {/* <View style={Styles.locationTop}>
                        <Icon active name='currency-usd' style={Styles.locationTopIcon} type="MaterialCommunityIcons" />
                        <Text style={Styles.locationTopInfo}>{_eventAddress.Price}</Text>
                    </View>
                    <View style={Styles.locationTop}>
                        <Icon active name='calendar-clock' style={Styles.locationTopIcon} type="MaterialCommunityIcons" />
                        <Text style={Styles.locationTopInfo}>{ new Date(_eventAddress.StartDate).getDay()  }/{new Date(_eventAddress.StartDate).getMonth()} - {new Date(_eventAddress.StartDate).getHours()}h{new Date(_eventAddress.StartDate).getMinutes()}min</Text>
        </View>*/}

                </View>

                <ImageBackground source={require('@Asset/images/shadow.png')} imageStyle={'cover'} style={Styles.shadow} />

                <View style={Styles.overview}>
                    <Text style={Styles.overviewTitle}>Descrição</Text>
                    <Text style={Styles.overviewDesc}>
                        {_eventAddress.Description}
                    </Text>
                </View>



                {/*}  <View style={Styles.amenities}>
                    <Text style={Styles.amenityTitle}>Amenities</Text>
                    <View>

                        <FlatList
                            data={AMENITIES}
                            horizontal
                            renderItem={({ item, separators }) => (
                                <View style={Styles.amenity}>
                                    <Image source={item.icon} style={Styles.amenityIcon} />
                                    <Text style={Styles.amenityItem}>{item.amenity}</Text>
                                </View>
                            )}
                        />

                    </View>
                            </View>*/}

                <View style={Styles.location}>
                    <MapView
                        initialRegion={{
                            latitude: _eventAddress.Latitude,
                            longitude: _eventAddress.Longitude,
                            latitudeDelta: 0.0922,
                            longitudeDelta: 0.0421
                        }}
                        minZoomLevel={2}
                        //mapType={"hybrid"}
                        /*provider={PROVIDER_GOOGLE}*/
                        style={[{ position: "absolute", top: 0, left: 0, bottom: 0, right: 0 }]}>
                        <Marker
                            coordinate={{
                                latitude: _eventAddress.Latitude,
                                longitude: _eventAddress.Longitude,
                            }}
                            description={_eventAddress.ProducerAddress + ", " + _eventAddress.ProducerAddressNum + ", " + _eventAddress.ProducerNeighborhood + ", " + _eventAddress.ProducerCity + " - " + _eventAddress.ProducerState}
                            title={_eventAddress.ProducerCompanyName}
                            image={require('@Asset/images/icon-map.png')}
                        />
                    </MapView>
                </View>


                {/*  <Tabs tabBarUnderlineStyle={Styles.tabBorder}>
                    <Tab tabStyle={Styles.tabGrey} textStyle={Styles.tabTextActive} activeTabStyle={Styles.tabGrey} activeTextStyle={Styles.tabTextActive} heading="Informations">
                        <List style={Styles.infoTab}>
                            <ListItem style={Styles.infoItem}>
                                <Icon name="map-marker-radius" type="MaterialCommunityIcons" style={Styles.infoIcon} />
                                <View>
                                    <Text style={Styles.infoHeader}>{'Address'.toUpperCase()}</Text>
                                    <Text style={Styles.infoDesc}>3-277-10, Susan Apartment, {"\n"}Liverpool, United Kingdoom</Text>
                                </View>
                            </ListItem>
                            <ListItem style={Styles.infoItem}>
                                <Icon name="phone" type="FontAwesome" style={Styles.infoIcon} />
                                <View>
                                    <Text style={Styles.infoHeader}>{'Phone'.toUpperCase()}</Text>
                                    <Text style={Styles.infoDesc}>+01 1234567982 / +01 9874658231</Text>
                                </View>
                            </ListItem>
                            <ListItem style={Styles.infoItem}>
                                <Icon name="mail" type="Entypo" style={Styles.infoIcon} />
                                <View>
                                    <Text style={Styles.infoHeader}>{'Email'.toUpperCase()}</Text>
                                    <Text style={Styles.infoDesc}>info@myyaowrealtor.com</Text>
                                </View>
                            </ListItem>
                            <ListItem style={[Styles.infoItem, Styles.infoItemLast]}>
                                <Icon name="web" type="MaterialCommunityIcons" style={Styles.infoIcon} />
                                <View>
                                    <Text style={Styles.infoHeader}>{'Website'.toUpperCase()}</Text>
                                    <Text style={Styles.infoDesc}>www.myyaowrealtor.com</Text>
                                </View>
                            </ListItem>
                        </List>
                    </Tab>
                    <Tab tabStyle={Styles.tabGrey} textStyle={Styles.tabText} activeTabStyle={Styles.tabGrey} activeTextStyle={Styles.tabText} heading="Enquire Now">
                       <View style={Styles.formBg}>
                            <View style={Styles.col}>
                                <TextInput style={Styles.textInputHalf} placeholder={'First Name'} />
                                <TextInput style={Styles.textInputHalf} placeholder={'Last Name'} />
                            </View>
                            <TextInput style={Styles.textInput} placeholder={'Your Email Address'} />
                            <TextInput style={Styles.textInput} placeholder={'Your Mobile No.'} />
                            <TextInput style={Styles.textInputMulti} multiline={true} numberOfLines={8} placeholder={'Your Message'} />
                            <Button style={Styles.btn} onPress={() => {
                                NavigationService.navigate('MemberLogin')
                            }}>
                                <Text style={Styles.formBtnText}>{'Send'.toUpperCase()}</Text>
                                <Icon active name='envelope' type="FontAwesome" style={Styles.formBtnIcon} />
                            </Button>
                        </View>
                    </Tab>
                </Tabs>
*/}
                <View style={Styles.containerContact}>
                    <View style={{ flex: 1 }}>
                        <Text style={Styles.containerContactTitle}>{'contato'.toUpperCase()}</Text>
                    </View>

                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <View style={{ paddingTop: 20 }}>
                            <TouchableOpacity onPress={() => this.onOpenWhatsApp(_eventAddress.WhatsappNumber)} style={[Styles.buttonContact, { backgroundColor: "#25D366" }]}>
                                <IconFontAwesome name={"whatsapp"} style={Styles.buttonContactIcon}>
                                </IconFontAwesome>
                            </TouchableOpacity>
                        </View>
                        <View style={{ paddingTop: 20, paddingLeft: 20 }}>
                            <TouchableOpacity onPress={() => this.onOpenPhoneCall(_eventAddress.PhoneNumber)} style={[Styles.buttonContact, { backgroundColor: "#39b6ec" }]}>
                                <IconFontAwesome name={"phone"} style={Styles.buttonContactIcon}>
                                </IconFontAwesome>
                            </TouchableOpacity>
                        </View>
                    </View>

                </View>

                <View style={Styles.containerSimilarEventAddress}>
                    <View style={Styles.headerBg}>

                        <Text style={Styles.sHeader}>{'Locais similares'.toUpperCase()}</Text>

                    </View>
                    <FlatList
                        data={this.state.similiarEventAddress}
                        horizontal
                        showsHorizontalScrollIndicator={false}
                        style={Styles.flatList}
                        renderItem={({ item }) => (
                            <TouchableOpacity style={Styles.owner} underlayColor='transparent' onPress={() => this.onLearnMoreEventAddress(item)}>
                                <View>
                                    <View style={Styles.ownerAvatar}>
                                        <Image source={{ uri: (Useful.getImageUrl(_eventAddress.Logo)) }} style={Styles.ownerAvatarImg} />
                                    </View>
                                    <View style={Styles.ownerInfo}>
                                        <View>
                                            <Text style={Styles.ownerName}>{item.Name}</Text>
                                            <Text style={Styles.ownerLocation}>{item.City} - {item.State}</Text>
                                        </View>
                                    </View>
                                </View>
                            </TouchableOpacity>
                        )}
                    />
                </View>



            </Content>
        </Container>
    }
}