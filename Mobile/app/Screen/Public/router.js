import React from 'react';
import { createBottomTabNavigator, createMaterialTopTabNavigator, createAppContainer } from 'react-navigation';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';
import { TouchableOpacity } from 'react-native';
import { View } from 'native-base';

import Home from "./Home";
import Search from "./ExploreFilter";
import Profile from "./Profile";
// import User from "../Member/Profile";
// import Heart from "./Home";
// import Bell from "./Home";
// var param = '';



const TabsPublic = createMaterialTopTabNavigator({
    Home: {
        screen: Home,
        navigationOptions: {
            tabBarIcon: ({ tintColor }) => <IconFontAwesome name="home" color={tintColor} size={23}></IconFontAwesome>
        },
    },
    Search: {
        screen: Search,
        navigationOptions: {            
            tabBarIcon: ({ tintColor }) => <IconFontAwesome name="sliders" color={tintColor} size={23}></IconFontAwesome>
        }
    },
    Profile:{
        screen:Profile,
        navigationOptions:{
            tabBarIcon:({tintColor})=> <IconFontAwesome name="user" color={tintColor} size={23}></IconFontAwesome>
        }
    }
    /*User: {
        screen: User,
        navigationOptions: {
            tabBarIcon: ({ tintColor }) => <IconFontAwesome name="user" color={tintColor} size={23}></IconFontAwesome>
        },
    },
    Heart: {
        screen: Heart,
        navigationOptions: {
            tabBarIcon: ({ tintColor }) => <IconFontAwesome name="heart" color={tintColor} size={23}></IconFontAwesome>
        },
    },
    Bell: {
        screen: Bell,
        navigationOptions: {
            tabBarIcon: ({ tintColor }) => <IconFontAwesome name="bell" color={tintColor} size={23}></IconFontAwesome>
        },
    },*/
}, {
        defaultNavigationOptions: ({ navigation }) => ({
            tabBarIcon: ({ focused, horizontal, tintColor }) => {
                const { routeName } = navigation.state;
                let IconComponent = Ionicons;
                let iconName;
                if (routeName === 'Home') {
                    iconName = `ios-information-circle${focused ? '' : '-outline'}`;
                    // Sometimes we want to add badges to some icons. 
                    // You can check the implementation below.
                    IconComponent = HomeIconWithBadge;
                } else if (routeName === 'Settings') {
                    iconName = `ios-options${focused ? '' : '-outline'}`;
                }

                // You can return any component that you like here!
                return
                (<IconComponent name={iconName} size={25} color={tintColor} />);
            },
        }),
        tabBarOptions: {
            activeTintColor: '#DF3A62',
            inactiveTintColor: '#999',
            style: {
                backgroundColor: 'rgba(255,255,255, 0.7)',
            },
            showLabel: false,
            tabBarSelectedItemStyle: {
                borderBottomWidth: 2,
                borderBottomColor: '#ee3a37',
            },
        },
        initialRouteName: "Home",
        animationEnabled: true,
        swipeEnabled: true,
        tabBarPosition: 'bottom',
        tabBarComponent: (props) => {
            const {
                navigation: { state: { index, routes } },
                style,
                activeTintColor,
                inactiveTintColor,
                renderIcon,
                jumpTo
            } = props;
            return (
                <View style={{
                    flexDirection: 'row',
                    height: 50,
                    width: '100%',
                    shadowOpacity: 0.75,
                    shadowRadius: 2,
                    shadowColor: '#bebebe',
                    shadowOffset: { height: 0, width: 0 },
                    ...style
                }}>
                    {
                        routes.map((route, idx) => (
                            <TouchableOpacity
                                key={route.key}
                                style={[{
                                    flex: 1,
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                    backgroundColor: "#fff",
                                    color: "#000",
                                }, index === idx ? { borderTopColor: "#DF3A62", borderTopWidth: 4 } : { borderTopWidth: 0 }]}

                                onPress={() => jumpTo(route.key)}>
                                {renderIcon({
                                    route,
                                    focused: index === idx,
                                    tintColor: index === idx ? activeTintColor : inactiveTintColor
                                })}
                            </TouchableOpacity>

                        ))
                    }
                </View>
            );
        },
    });
export default createAppContainer(TabsPublic);