import React from 'react'
import { StatusBar, TouchableOpacity, TextInput, Image, ImageBackground, FlatList } from 'react-native'
import { Container, Header,Left, Content, Button, Text, Right, View,Body } from 'native-base'

import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/FontAwesome';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';
import NavigationService from '@Service/Navigation'
import ServerImageProgressiveLoader from '@Component/ServerImageProgressiveLoader';
import Style from '@Theme/Style'
import Styles from '@Screen/Public/Home/Style'
import UserPreferencesService from '@Service/UserPreferencesService';
import Config from '../../../Config';
import Serenity from '@Service/Serenity';
import moment from 'moment';
import Useful from '@Service/Useful';
import CustomIcon from '@Component/CustomIcon'



export default class extends React.Component{

    constructor(props) {
        super(props);
        this.state = {
            logged: true
        }

        
   }


    render() {
        return <Container style={Style.bgMain}>
                    <Content style={[Style.layoutInner]} contentContainerStyle={[Style.layoutContent/*, Platform.OS === 'ios' ? { marginTop: 40 } : undefined*/]}>
                    <Header style={[Style.navigation]} hasTabs={this.props.hasTabs}>
                        <StatusBar backgroundColor="#DF3A62" animated barStyle="light-content" />
                        <Left style={{ flex: 1 }}>
                            <Button transparent onPress={() => this.props.navigation.goBack()}>
                                <Icon active name='arrow-left' style={Style.textWhite} type="MaterialCommunityIcons" />
                            </Button>
                           
                        </Left>
                        <Body style={{ flex: 5 }}> 
                            <Text style={[Style.actionBarText]}>{'Perfil'.toUpperCase()}</Text>
                        </Body>
                        
                    </Header>
                     {
                        this.state.logged == false ?

                       <View style={Styles.section}>
                            <View style={[Styles.headerBg, {paddingBottom:10,  borderBottomColor:'#f2f2f2',borderBottomWidth:2}]}>
                                <Text style={Styles.sHeader}>{'Perfil'.toUpperCase()}</Text>
                            </View>
                            <View style={[{marginHorizontal:20}]}>
                                <Text>Para usufruir de todas as facilidades do agita, você deve realizar o seu cadastro ou acessar sua conta.</Text>
                            </View>

                            <View style={Styles.sectionGrey}>
                                <View style={[{flex: 2,
                                            flexDirection: 'row',
                                            justifyContent: 'center'}]}>
                                    <View style={{width:'50%'}}>
                                        <Button   block
                                            onPress={() => this.props.navigation.navigate('PublicLogin')} 
                                            style={[Style.bgBlueLight,Style.roundSmall,{alignSelf:"center"}]}>
                                            <Text style={[Style.textWhite,Style.textButton,Style.textButton]} >ENTRAR</Text>
                                        </Button>
                                    </View>
                                    <View style={{width:'50%'}}>
                                        <Button   block 
                                            onPress={() => this.props.navigation.navigate('PublicCreateProfile')}  
                                            color={"#fff"}
                                            style={[Style.bgPinkLight,Style.roundSmall,  {alignSelf:"center",textAlign:"center",width:'80%'}]}>
                                            <Text style={[Style.textWhite,Style.textButton]} >CADASTRAR</Text>
                                        </Button>
                                    </View>
                                </View> 
                            </View>
                            <View >
                                <Text style={[{textAlign:"center"}]}>ou</Text>
                            </View>
                            <View style={Styles.sectionGrey}>
                                <View style={[{flex: 1,
                                            flexDirection: 'column',
                                            justifyContent: 'center'}]}>
                                    <View style={{width:'90%',alignSelf:"center",marginVertical:5}}>
                                        <Button   block
                                            onPress={() => this.onPressed()} 
                                            style={[Style.bgFacebook,Style.roundSmall,{alignSelf:"center",textAlign:"center",width:'100%'}]}>
                                                 <Icon
                                                  name="facebook-f"
                                                  size={15}
                                                  color="white"
                                                />
                                            <Text style={[Style.textWhite,Style.textButton]} >Acessar com Facebook</Text>
                                        </Button>
                                    </View>
                                    <View style={{width:'90%',alignSelf:"center",marginVertical:5}}>
                                        <Button   block 
                                            onPress={() => this.onPressed()}  
                                            color={"#fff"}
                                            style={[Style.bgGoogle,Style.roundSmall,  {alignSelf:"center",textAlign:"center",width:'100%'}]}>
                                                <Icon
                                                  name="google"
                                                  size={15}
                                                  color="white"
                                                />
                                            <Text style={[Style.textWhite,Style.textButton]} >Acessar com Google</Text>
                                        </Button>
                                    </View>
                                </View> 
                            </View>
                        </View>
                     :
                     <View >
                           <View style={[Style.bgOrange,Style.row,{height:115} ]}>
                           
                               <View style={[Style.col1,{ paddingHorizontal:30}]}>  
                                    <Image source={{ 
                                        uri: 'https://data.whicdn.com/images/299667332/large.jpg',
                                        cache: 'only-if-cached'
                                        }} style={[{ width: 80, 
                                            height: 80,
                                            borderRadius:80}]} />
                                </View> 
                                        
                                <View style={[Style.col4,{paddingVertical:20}]}>
                                    <Text style={Styles.textWhite}>debora@gmail.com</Text>
                                    <Text style={[Styles.profileName,{color:'#fff',fontSize: 20}]}>Debora dos Santos</Text>
                                </View>   
                            
                           
                         </View>
                       
                        <FlatList
                            data={[
                                {key: 'Editar perfil',ico:'user',page:'PublicEditProfile'},
                                {key: 'Alterar senha',ico:'lock',page:'PublicPasswordEdit'},
                                {key: 'Sair',ico:'sign-out',page:'PublicHome'},
                                
                            ]}
                            
                            renderItem={({item}) =><TouchableOpacity onPress={() => this.props.navigation.navigate(`${item.page}`)}
                                                                style={[Style.row1,{height:50, width:'100%',paddingHorizontal:0,
                                                                borderBottomColor:'#f2f2f2',borderBottomWidth:1,
                                                                paddingVertical:15}]}>
                                                        <IconFontAwesome name={item.ico} style={[Style.col1,Style.textGreyLight,{textAlign:"center"}]} size={19}></IconFontAwesome>
                                                         <Text style={[Style.col4,{textAlign:"left"} ]}>{item.key}</Text>
                                                    </TouchableOpacity>}
                            />
                      
                            
                        </View>
                     }
                    </Content>
            

        </Container>
    }
}