import React from 'react'
import { StatusBar, TouchableOpacity, TextInput, Image, ImageBackground, FlatList } from 'react-native'
import { Container, Header,Left, Content, Button, Text, Right, View,Body } from 'native-base'
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/FontAwesome';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';
import NavigationService from '@Service/Navigation'
import Style from '@Theme/Style'
import Styles from '@Screen/Public/Home/Style'
import UserPreferencesService from '@Service/UserPreferencesService';
import Config from '../../../../Config';
import Serenity from '@Service/Serenity';
import moment from 'moment';
import Useful from '@Service/Useful';
import CustomIcon from '@Component/CustomIcon'



export default class extends React.Component{

    constructor(props) {
        super(props);
  
   }

   onPressed = ()=>{

   } 
   
    _toggleModal = () =>
        this.setState({ isModalVisible: !this.state.isModalVisible });

    render() {
        return <Container style={Style.bgMain}>
                    <Content style={[Style.layoutInner]} contentContainerStyle={[Style.layoutContent/*, Platform.OS === 'ios' ? { marginTop: 40 } : undefined*/]}>
                    <Header style={[Style.navigation]} hasTabs={this.props.hasTabs}>
                        <StatusBar backgroundColor="#DF3A62" animated barStyle="light-content" />
                        <Left style={{ flex: 1 }}>
                            <Button transparent onPress={() => this.props.navigation.goBack()}>
                                <Icon active name='arrow-left' style={Style.textWhite} type="MaterialCommunityIcons" />
                            </Button>
                           
                        </Left>
                        <Body style={{ flex: 5 }}> 
                            <Text style={[Style.actionBarText]}>{'Editar perfil'.toUpperCase()}</Text>
                        </Body>
                        
                    </Header>
          
                        <View style={Styles.section}>
                      
                            <View style={[Styles.sectionGrey,{paddingHorizontal:25}]}>
                                <TextInput style={[Style.textInput,Style.input]} placeholder={'Nome'} />
                                <TextInput style={[Style.textInput,Style.input]} placeholder={'Sobrenome'} />
                                <Button block style={[Styles.btn,Style.roundSmall, Style.bgOrange,{marginVertical:20,width:'100%'}]} onPress={() => {
                                    NavigationService.navigate('PublicHome')
                                }}>
                                    <Text >{'Salvar'.toUpperCase()}</Text>
                                
                                </Button> 
                            </View>
                           
                        </View>
                    </Content>
            

        </Container>
    }
}