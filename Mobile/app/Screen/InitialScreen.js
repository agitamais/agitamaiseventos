import React from 'react'
import {
    AsyncStorage
} from "react-native";
import LoginService from "../Service/LoginService";

export default class InitialScreen extends React.Component {

    componentDidMount() {

        try {
            new LoginService().publicLogin()
                .then((response) => {
                    AsyncStorage.getItem('INITIAL_CONFIG', (err, result) => {
                        if (result){
                            this.props.navigation.navigate('Home');
                        }                        
                        else{
                            let initialConfig = {
                                isAppInstaled: true
                            };
                            let obj = JSON.stringify(initialConfig);
                            AsyncStorage.setItem('INITIAL_CONFIG', obj);
                            this.props.navigation.navigate('PublicIntro');
                            }
                        });
                })
        } catch (error) {
            console.log(error)
            //this.props.navigation.navigate('Home');
        }
    }
    render() {
        return null;
    }
}